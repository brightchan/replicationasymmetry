The bash scripts can be found in the _bash_ directory. The matlab scripts are in the _root_ directory (and some functions are in the _code_ directory).

**Preparation of replication domains based on replication timing**

* all_mfReplication.sh 
	* This script takes the following files as input:
		* Files with mutations
		* Regions annotated with left/right replication direction (term "replication territories" was adopted from Haradhvala et al. 2016 for these regions)
		* Blacklisted regions of genome to be removed from the analysis (such as regions with low mappability, genes, etc.)
	* Then territories files are prepared, background trinucleotide frequencies are computed, and mutations in the territories are computed
	* In between steps TERRITORIES PREPARATIONS and MUTATIONS COMPUTATION, matlab script_replPrepareTerritories.m was used to compute matlab tables.
	* The following files are called from this script: run_mfSequenceInBins.sh, run_mfReplicationMatlab.sh (matlab script fcn_replOneCT_step1.m, fcn_replOneCT_step2matrix.m), printFile.sh.
	* The results are saved to directories like replication/Haradhvala_territories/replPrepareTables1119_70114.
* script_step1_prepareCancerTissues.m
	* This script takes files like save/Haradhvala_territories/replPrepareTables1119_70114/structSamples_CANCER_TYPE.mat as input (computed in the previous part and copied to the right directory).
	* It prepares .mat files for the signature decomposition and saves them to save/Haradhvala_territories/dataForSignatureDecomposition/.
* all_msDetectSignatures_distributed.sh
	* This script computes the extraction of strand specific mutational signatures (in leading and lagging strands).
	* It takes files like blood_lymphoid_274_replication_asymmetry_genomes_LeadLagg_separately.mat as input (computed in the previous part).
	* The following files are called from this script: run_msDetectSignatures_distributed.sh (matlab script decipherMutationalProcesses_distributed.m, which calls matlab functions from Alexandrov et al. 2013)
	* The resulting files are copied (such as output/res_blood_lymphoid_274_2k_strandSpec_100runs.reconstructionError.txt and output/res_blood_lymphoid_274_2k_strandSpec_100runs.stability.txt --> data/Haradhvala_territories/evaluationOfDecomposedSignatures/, or output/res_blood_lymphoid_274_2k_strandSpec_100runs.mat --> save/Haradhvala_territories/decomposedSignaturesSmaller/res_blood_lymphoid_274_2k_strandSpec_100runs.mat)

**Preparation of replication domains based on SNS-seq**

* all_snsMap.sh
	* This script maps ORI measurements, calls peaks, computes overlaps between replicates, and generates resulting territories.
	* The following files are called from this script: run_snsMap.sh, run_snsCallPeaks.sh, bedGraphToBigWig (from UCSC), run_snsSummitsOverlap.sh, and run_snsGenerateTerritories.sh.
* all_mfReplication.sh (same as for Haradhvala_territories)
* script_step1_prepareCancerTissues.m (same as for Haradhvala_territories)
	
**Computation of strand asymmetry in mutational signatures**

* script_step2_evaluateDecomposedSignatures.m and script_step3_plotExposureResults.m
	* This script computes all the rest and plots all the figures.
		

---
## License

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

It is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the software.  If not, see <http://www.gnu.org/licenses/>.
