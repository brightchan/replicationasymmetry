%% script_step1_prepareCancerTissues.m
clear; addpath(genpath('code/'));
savePathDiary = 'diaries/'; createDir(savePathDiary); scriptName = 'script_step1_prepareCancerTissues'; compName = 'HOME';
diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
%% 
savePathBasic = 'save/'; 
dirDataCT = 'save/Haradhvala_territories/replPrepareTables1119_1125/'; nameDataCT = 'replPrepareTables1119_1125'; territoriesSource = 'Haradhvala_territories';
% dirDataCT = 'save/Besnard_territories/replPrepareTables1129/'; nameDataCT = 'replPrepareTables1129'; territoriesSource = 'Besnard_territories';
tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource, '.mat'];  %'tableTerritoriesHaradhvala'; % created in script_replPrepareTerritories (originally script_60726_replPrepareTables)
alwaysCompute = true;
replData = loadReplicationData(alwaysCompute, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, territoriesSource);
saveReplicationTissues(replData, territoriesSource); % for decomposition of signatures
%%
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
diary off;
if (sendingEmails)
    sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
end