A) First, left-/right-direction of replication annotations from Haradhvala et al. 2016
	1. all_mfReplication.sh 
		- This script takes files as input:
			- Files with mutations
			- Regions annotated with left/right replication direction (term "replication territories" was adopted from Haradhvala et al. 2016 for these regions)
			- Blacklisted regions of genome to be removed from the analysis
		- Then territories files are prepared, background trinucleotide frequencies are computed, and mutations in the territories are computed
		- In between steps TERRITORIES PREPARATIONS and MUTATIONS COMPUTATION, matlab script_replPrepareTerritories.m was used to compute matlab tables.
		- The following files are called from this script: run_mfSequenceInBins.sh, run_mfReplicationMatlab.sh (matlab script fcn_replOneCT_step1.m, fcn_replOneCT_step2matrix.m), printFile.sh.
		- The results are saved to directories like replication/Haradhvala_territories/replPrepareTables1119_70114.
	2. script_step1_prepareCancerTissues.m
		- This script takes files like save/Haradhvala_territories/replPrepareTables1119_70114/structSamples_CANCER_TYPE.mat as input (computed in the previous part and copied to the right directory).
		- It prepares .mat files for the signature decomposition and saves them to save/Haradhvala_territories/dataForSignatureDecomposition/.
	3. all_msDetectSignatures_distributed.sh
		- This script computes the extraction of strand specific mutational signatures (in leading and lagging strands).
		- It takes files like blood_lymphoid_274_replication_asymmetry_genomes_LeadLagg_separately.mat as input (computed in the previous part).
		- The following files are called from this script: run_msDetectSignatures_distributed.sh (matlab script decipherMutationalProcesses_distributed.m, which calls matlab functions from Alexandrov et al. 2013)
		- The resulting files like output/res_blood_lymphoid_274_2k_strandSpec_100runs.reconstructionError.txt and output/res_blood_lymphoid_274_2k_strandSpec_100runs.stability.txt --> data/Haradhvala_territories/evaluationOfDecomposedSignatures/
		- The resulting files like output/res_blood_lymphoid_274_2k_strandSpec_100runs.mat --> save/Haradhvala_territories/decomposedSignaturesSmaller/res_blood_lymphoid_274_2k_strandSpec_100runs.mat

B) Second, left-/right-direction of replication annotations based on ORI measurements from Besnard et al. 2012
	1. all_snsMap.sh
		- This script maps ORI measurements, calls peaks, computes overlaps between replicates, and generates resulting territories.
		- The following files are called from this script: run_snsMap.sh, run_snsCallPeaks.sh, bedGraphToBigWig (from UCSC), run_snsSummitsOverlap.sh, and run_snsGenerateTerritories.sh.
	2. all_mfReplication.sh (same as for Haradhvala_territories)
	3. script_step1_prepareCancerTissues.m (same as for Haradhvala_territories)
	
C) Third, everything is used in the final matlab part:
	1. script_step2_evaluateDecomposedSignatures.m and script_step3_plotExposureResults.m
		- This script computes all the rest and plots all the figures.
		
		
		
		save/Haradhvala_territories/evaluationOfDecomposedSignatures_Haradhvala_territories.mat (20 KB)
		save/Haradhvala_territories/decomposedSignatures_Haradhvala_territorieswithout_N1_N2.mat (351 KB)
		save/Besnard_territories/signatureExposuresComplex_cell_replPrepareTables1216_Besnard1k_territorieswithout_N1_N2.mat (270 MB)
		save/Haradhvala_territories/replData_replPrepareTables1119_70114_Haradhvala_territorieswithout_N1_N2.mat (151 MB)
		save/dataForVolcanoPlot_Haradhvala_territories.mat (16 KB)
		