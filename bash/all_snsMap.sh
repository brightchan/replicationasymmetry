#!/bin/bash

################################################
# SGE RUN ALL SUBMISSION SCRIPTs all_snsMap.sh #
################################################

module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
module load apps/samtools/1.0
module load apps/bedtools/2.20.1
module load apps/cutadapt/1.5
module load apps/R/3.1.1
module load apps/oraclejdk/8.0.20/bin
module load apps/bowtie/1.1.0/gcc-4.4.7
module load apps/fastqc/0.11.2/noarch
module load apps/macs/2.1.0/gcc-4.4.7+numpy-1.8.0


OUTPUT_FILE="jobs_all_snsMap_"`date '+%Y_%m_%d-%H_%M'`".txt"

echo "Running run_snsMap.sh for the following files:" >> $OUTPUT_FILE

function setGenome {
	if [ "$GENOME_TYPE" == "mouse" ]; then
		GENOME_REFERENCE_PATH="../genomes/mm10/mm10_bowtie2" # TODO PREPARE
		GENOME_SIZES_UCSC="../genomes/mm10/mm10.chrom.sizes"
	elif [ "$GENOME_TYPE" == "human" ]; then
		GENOME_REFERENCE_PATH="../hg19/hg19_full_bowtie2"
		GENOME_SIZES_UCSC="../hg19/hg19.chrom.sizes"
	else
		echo "Problem, GENOME_TYPE ($GENOME_TYPE) not recognised."
		exit 1
	fi
}

function call_run_snsMap {
	setGenome
	echo "qsub run_snsMap.sh  $DIRECTORY $SAMPLE_NAME  $PAIRED $GENOME_REFERENCE_PATH $DIRECTIONAL $GENOME_SIZES_UCSC"  >> $OUTPUT_FILE
	qsub run_snsMap.sh  $DIRECTORY $SAMPLE_NAME  $PAIRED $GENOME_REFERENCE_PATH $DIRECTIONAL $GENOME_SIZES_UCSC  >> $OUTPUT_FILE
}

function call_run_snsCallPeaks {
	setGenome
	echo "qsub run_snsCallPeaks.sh  $DIRECTORY $SAMPLE_NAME  $PAIRED $GENOME_REFERENCE_PATH $DIRECTIONAL $GENOME_SIZES_UCSC $CONTROL_SAMPLE_MAPPED_FILE $INPUT_BAMFILE"  >> $OUTPUT_FILE
	qsub run_snsCallPeaks.sh  $DIRECTORY $SAMPLE_NAME  $PAIRED $GENOME_REFERENCE_PATH $DIRECTIONAL $GENOME_SIZES_UCSC $CONTROL_SAMPLE_MAPPED_FILE $INPUT_BAMFILE >> $OUTPUT_FILE
}

#### Step 1: mapping the Besnard et al. 2012 measurements ####
# DIRECTORY="Besnard"; PAIRED=false; DIRECTIONAL=false; GENOME_TYPE="human"
# for SAMPLE_NAME_SUFFIX in "HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"; do 
	# SAMPLE_NAME="Besnard_"$SAMPLE_NAME_SUFFIX
	# call_run_snsMap
# done

#### Step 2: mapping the control Foulk et al. 2015 measurements (from non-replicating cells) ####
# DIRECTORY="Foulk"; PAIRED=false; DIRECTIONAL=false; GENOME_TYPE="human"
# for SAMPLE_NAME_SUFFIX in "LexoG0"; do 
	# SAMPLE_NAME="Foulk_"$SAMPLE_NAME_SUFFIX
	# call_run_snsMap
# done

#### Step 3: calling peaks of Besnard et al. 2012 measurements  using Foulk et al. 2015 measurements as a control ####
# CONTROL_SAMPLE_MAPPED_FILE="Foulk/output03mapped/Foulk_LexoG0_mapped.bam"
# DIRECTORY="Besnard"; PAIRED=false; DIRECTIONAL=false; GENOME_TYPE="human"
# for SAMPLE_NAME_SUFFIX in "HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"; do 
	# SAMPLE_NAME="Besnard_"$SAMPLE_NAME_SUFFIX
	# INPUT_BAMFILE="${DIRECTORY}/output03mapped/${SAMPLE_NAME}_mapped.bam"
	# call_run_snsCallPeaks
# done


cat $OUTPUT_FILE

cat $OUTPUT_FILE | mail -s "Running all_snsMap.sh... output ${OUTPUT_FILE} file" -a $OUTPUT_FILE marketa.tomkova@ndm.ox.ac.uk
cat run_snsMap.sh | mail -s "Running all_snsMap.sh... run_snsMap.sh script" -a run_snsMap.sh marketa.tomkova@ndm.ox.ac.uk
cat all_snsMap.sh | mail -s "Running all_snsMap.sh... all_snsMap.sh script" -a all_snsMap.sh marketa.tomkova@ndm.ox.ac.uk


