#!/bin/bash -l

#####################################################
# SGE SUBMISSION SCRIPT run_mfSequenceInBins.sh     #
#####################################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job? 
#$ -N mfSequenceInBins

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m as

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=24:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=10G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp 1

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
# module load apps/bismark/0.12.5/noarch
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch


###############################
# DESCRIPTION
###############################

# Takes a bedfile and computes for each line number of occurences of SEQUENCE (can be base, or e.g., CG) on the given strand.

# qsub run_mfSequenceInBins.sh tmpOut.bed AA 1 tmpOut_AAnew.csv ../hg19/hg19_full.fa 0 ../hg19/genomeSize_hg19.txt x
# qsub run_mfSequenceInBins.sh tmpOut.bed AA 1 tmpOut_AAnew_plus.csv ../hg19/hg19_full.fa 0 ../hg19/genomeSize_hg19.txt +
# qsub run_mfSequenceInBins.sh tmpOut.bed AA 1 tmpOut_AAnew_minus.csv ../hg19/hg19_full.fa 0 ../hg19/genomeSize_hg19.txt -

# GENOME_REFERENCE_PATH="../hg19/hg19_full.fa"
# GENOME_REFERENCE_PATH="../genomes/mm10/genome.fa"

# CHROM_SIZE_FILE="../hg19/genomeSize_hg19.txt"
# CHROM_SIZE_FILE="../genomes/mm10/genomeSize_mm10.txt"

###############################
# APPLICATION LAUNCH COMMANDS
###############################

INPUT_BED_FILE=$1
SEQUENCE=$2
SEQUENCE_INDEX_OF_MUTATED_BASE=$3
OUTPUT_FILE=$4
GENOME_REFERENCE_PATH=$5
NBINS_ON_SIDES=$6
CHROM_SIZE_FILE=$7
STRAND=$8
STRAND_TEXT=$9

if [ -z "$INPUT_BED_FILE" ] || [ -z "$SEQUENCE" ] || [ -z "$SEQUENCE_INDEX_OF_MUTATED_BASE" ] || [ -z "$OUTPUT_FILE" ] || [ -z "$GENOME_REFERENCE_PATH" ] || [ -z "$NBINS_ON_SIDES" ] || [ -z "$CHROM_SIZE_FILE" ] || [ -z "$STRAND" ] || [ -z "$STRAND_TEXT" ]; then
	echo "Not enough parameters." 1>&2
	exit 1
fi

echo "Script run_mfSequenceInBins.sh $INPUT_BED_FILE $SEQUENCE $SEQUENCE_INDEX_OF_MUTATED_BASE $OUTPUT_FILE $GENOME_REFERENCE_PATH $NBINS_ON_SIDES $CHROM_SIZE_FILE $STRAND $STRAND_TEXT..."


if [ $SEQUENCE_INDEX_OF_MUTATED_BASE -lt 0 ] || [ $SEQUENCE_INDEX_OF_MUTATED_BASE -gt ${#SEQUENCE} ]; then
	echo "Index of the mutated base must be within [0, ${#SEQUENCE}], but is ${SEQUENCE_INDEX_OF_MUTATED_BASE}." 1>&2
	exit 1
fi

sh printFile.sh $INPUT_BED_FILE


FILE_RESULT_FASTAFILE=$INPUT_BED_FILE"."$SEQUENCE"."$STRAND_TEXT".fa"
FILE_PADDED_BED=$INPUT_BED_FILE"."$SEQUENCE"."$STRAND_TEXT".padded.bed"
FILE_WINDOWSIZE_OUTPUT=$OUTPUT_FILE".windowSize.csv"
CHROM_SIZE_FILE_TABS=$INPUT_BED_FILE"."$SEQUENCE"."$STRAND_TEXT".chromSizes.txt"
cat $CHROM_SIZE_FILE | tr " " "\t" > $CHROM_SIZE_FILE_TABS

echo "First, we need to generate a file with a padding on the edges (-(${varSEQUENCE_INDEX_OF_MUTATED_BASE}-1, +length(${SEQUENCE})-${varSEQUENCE_INDEX_OF_MUTATED_BASE}; if it's in the chromosome, for + strand (-is the opposite))."
awk -v varCHROM_SIZE_FILE=$CHROM_SIZE_FILE_TABS -v varSEQUENCE="${SEQUENCE}" -v varSEQUENCE_INDEX_OF_MUTATED_BASE="${SEQUENCE_INDEX_OF_MUTATED_BASE}" -v varSTRAND="${STRAND}" 'BEGIN{
	if (varSTRAND == "+" ) {
		paddingLeft = varSEQUENCE_INDEX_OF_MUTATED_BASE-1
		paddingRight = length(varSEQUENCE)-varSEQUENCE_INDEX_OF_MUTATED_BASE
	} else {
		paddingLeft = length(varSEQUENCE)-varSEQUENCE_INDEX_OF_MUTATED_BASE
		paddingRight = varSEQUENCE_INDEX_OF_MUTATED_BASE-1
	}
}{ 				#BEGIN{FS = "\t"; OFS = "\t"}
	if (FILENAME ~ varCHROM_SIZE_FILE) {
		chrSizes[$1] = $2
	} else {
		$2 = $2 - paddingLeft;
		$3 = $3 + paddingRight;
		$2 = ($2 > 0 ? $2 : 0)								# max($2, 0)
		$3 = ($3 < chrSizes[$1] ? $3 : chrSizes[$1])		# min($3, chrSizes[$1])
		printf "%s\t%d\t%d\tname\t0\t%s\n", $1, $2, $3, varSTRAND
	}
}' $CHROM_SIZE_FILE_TABS $INPUT_BED_FILE > $FILE_PADDED_BED
sh printFile.sh $FILE_PADDED_BED

echo "Now, we will print size of each window in $INPUT_BED_FILE and save it into $FILE_WINDOWSIZE_BED..."
awk '{printf "%d\n", $3-$2 }' $INPUT_BED_FILE > $FILE_WINDOWSIZE_OUTPUT
sh printFile.sh $FILE_WINDOWSIZE_OUTPUT

# echo "Now, we will getfasta for the padded windows..."
# FILE_NEW_BED=$FILE_PADDED_BED".addedStrand.bed"	# We assume that current columns are chr, startRegion, endRegion --> and we will name, value, strand at the end (name and value are dummy)
# awk -v varStrand=$STRAND '{printf "%s\t%d\t%d\t%s\n", $1, $2, $3, varStrand }' $FILE_PADDED_BED > $FILE_NEW_BED #	sed "s/$/\tx\t0\t${STRAND}/" 
# sh printFile.sh $FILE_NEW_BED
echo "Running bedtools getfasta -fi $GENOME_REFERENCE_PATH -bed $FILE_PADDED_BED -fo $FILE_RESULT_FASTAFILE -tab -s..."
bedtools getfasta -fi $GENOME_REFERENCE_PATH -bed $FILE_PADDED_BED -fo $FILE_RESULT_FASTAFILE -tab -s
rm $FILE_PADDED_BED
rm $CHROM_SIZE_FILE_TABS

# echo "Running bedtools getfasta -fi $GENOME_REFERENCE_PATH -bed $FILE_PADDED_BED -fo $FILE_RESULT_FASTAFILE -tab ..."
# bedtools getfasta -fi $GENOME_REFERENCE_PATH -bed $FILE_PADDED_BED -fo $FILE_RESULT_FASTAFILE -tab

# sh printFile.sh $FILE_RESULT_FASTAFILE

echo "And finally, we will count number of ${SEQUENCE} in each window on the given strand."
cut -f 2 $FILE_RESULT_FASTAFILE | awk -v varSequence=$SEQUENCE 'BEGIN{IGNORECASE = 1;}{
		regionFasta = $1; sequenceLength = length(varSequence); nOccurences = 0;
		for (iPosition = 1; iPosition <= length(regionFasta); iPosition++) {
			if (substr(regionFasta, iPosition, sequenceLength) == varSequence) {
				nOccurences++
			}
		}
		print nOccurences
	}' | awk -v varNBINS_ON_SIDES=$NBINS_ON_SIDES 'BEGIN{iLine=0;}{
	iLine=iLine+1; printf "%s", $1; if (iLine == 2*varNBINS_ON_SIDES+1) {printf "\n"; iLine=0;} else {printf ",";}}' > $OUTPUT_FILE
sh printFile.sh $OUTPUT_FILE

rm $FILE_RESULT_FASTAFILE

echo "Script run_mfSequenceInBins.sh FINISHED..."
