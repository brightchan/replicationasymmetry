#!/bin/bash

#######################################################
# SGE SUBMISSION SCRIPT run_snsGenerateTerritories.sh #
#######################################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job?
#
#$ -N snsGenerateTerritories

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m eas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=10504:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=16G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows. 
#$ -pe smp 1

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch
# module load apps/macs/2.1.0/gcc-4.4.7+numpy-1.8.0


###############################
# APPLICATION LAUNCH COMMANDS
###############################

echo "Script run_snsGenerateTerritories.sh ..."
ORIGINAL_TERRITORIES_BED="replication/per_base_territories_20kb_line_numbers.bed.txt"

function printRegions {	#$1, e.g., $OUTPUT_BED_REGIONS
	sh printFile.sh $1
	awk 'BEGIN{sumRegions=0}{sumRegions+=($3-$2)}END{printf "Total regions covered by bedfile FILENAME: %d\n", sumRegions}' $1
}

function selectTrackFromOKseq { # $INPUT_BEDGRAPH_ORI, $TRACK_NAME --> $2
	OUTPUT_FILE_LOCAL=$1
	echo "RUNNING awk -v varTrackName=$TRACK_NAME '{...}' $INPUT_BEDGRAPH_ORI | sort -k1,1 -k2,2n > $OUTPUT_FILE_LOCAL ..."
	awk -v varTrackName=$TRACK_NAME 'BEGIN{isCorrectTrack=0;}{
		if ($0 ~ "track type") { # the line contains "track type"
			if ($0 ~ varTrackName) {
				isCorrectTrack=1;
			} else {
				isCorrectTrack=0;
			}
		} else {
			if (isCorrectTrack==1) {
				chromosome=$1; posStart1=$2; posEnd1=$3; value=$4; pos0=posStart1-1; pos1=posEnd1;
				if (chromosome ~ /^chr[0-9]+$/) { # autosomes only
					printf "%s\t%d\t%d\t%f\n", chromosome, pos0, pos1, value
				}
			}
		}
	}' $INPUT_BEDGRAPH_ORI | sort -k1,1 -k2,2n > $OUTPUT_FILE_LOCAL
	printRegions $OUTPUT_FILE_LOCAL
}


function generateRegionsFromOKseq {	# $INPUT_FILE_LOCAL=$1 --> $OUTPUT_BED_REGIONS	
	INPUT_FILE_LOCAL=$1
	printRegions $INPUT_FILE_LOCAL
	awk 'BEGIN{tID=1}{
		chromosomeC=$1; pos0C=$2; pos1C=$3; valueC=$4;		# Crick
		chromosomeW=$5; pos0W=$6; pos1W=$7; valueW=$8;		# Watson
		chromosomeR=$9; pos0R=$10; pos1R=$11; valueRFD=$12; 	# RFD = (C–W)/(C+W)
		if (chromosomeC == chromosomeW && chromosomeC == chromosomeR && pos0C == pos0W && pos0C == pos0R && pos1C == pos1W && pos1C == pos1R) {
			#printf "valueC=%d\tvalueW=%d\tvalueRFD=%d\n", valueC, valueW, valueRFD
			if ((valueC > 0 || valueW > 0) && valueRFD < 0) {tIsLeft=1; tIsRight=0;}
			else if ((valueC > 0 || valueW > 0) && valueRFD > 0) {tIsLeft=0; tIsRight=1;}
			else {tIsLeft=0; tIsRight=0;}
			if (tIsLeft==1 || tIsRight==1) {
				printf "%s\t%d\t%d\t%d\t%d\t%d\n", chromosomeC, pos0C, pos1C, tID, tIsLeft, tIsRight
				tID++
			}
		} else {
			print "ERROR: chromosomes or positions in the three files do not match." > "/dev/stderr"
			exit 1
		}
	}' $INPUT_FILE_LOCAL > $OUTPUT_BED_REGIONS
	
	# awk 'BEGIN{tID=1}{
		# chromosome=$1; posStart1=$2; posEnd1=$3; RFD=$4; pos0=posStart1-1; pos1=posEnd1;
		# if (chromosome ~ /^chr[0-9]+$/) { # autosomes only
			# if (RFD < 0) {tIsLeft=1; tIsRight=0;}
			# else if (RFD > 0) {tIsLeft=0; tIsRight=1;}
			# else {tIsLeft=0; tIsRight=0;}
			# printf "%s\t%d\t%d\t%d\t%d\t%d\n", chromosome, pos0, pos1, tID, tIsLeft, tIsRight
			# tID++
		# }
	# }' $INPUT_BEDGRAPH_ORI | sort -k1,1 -k2,2n > $OUTPUT_BED_REGIONS
	# track type=bedGraph name="Hela_rep1 (RFD)" description="Generated by GBrowse from http://157.136.54.88/cgi-bin/gbrowse/gbrowse/okazaki_ref?l=Hela_rep1"
	# chr1    1       1000    -2
	# chr1    1001    2000    -2
	# chr1    2001    3000    -2
	printRegions $OUTPUT_BED_REGIONS
}

function generateRegionsAroundORI {	# $INPUT_BED_ORI --> $OUTPUT_BED_REGIONS
	echo "Running awk -v varBIN_SIZE=$BIN_SIZE -v varMAX_BINS=$MAX_BINS '{...}' $INPUT_BED_ORI > $OUTPUT_BED_REGIONS"
	awk -v varBIN_SIZE=$BIN_SIZE -v varMAX_BINS=$MAX_BINS '
	function myMin(value1, value2) { return(value1 < value2 ? value1 : value2) } # printf "myMin(%d, %d)=%d\n", value1, value2, value1 < value2 ? value1 : value2; 
	function myMax(value1, value2) { return(value1 > value2 ? value1 : value2) } 
	function printBinsRightReplicating(chr, ORI_pos1, middle_pos1_local) { 	# printf "printBinsRightReplicating(%s, %d, %d, %d)\n", chr, ORI_pos1, middle_pos1_local, tID
		middle_pos1_local = myMin(middle_pos1_local, ORI_pos1+(varMAX_BINS*varBIN_SIZE));	tIsLeft=0; tIsRight=1; 	# printf "for (bin_pos0=%d; bin_pos0+%d<=%d; bin_pos0+=%d)\n", ORI_pos1, varBIN_SIZE, middle_pos1_local, varBIN_SIZE 
		for (bin_pos0=ORI_pos1; bin_pos0+varBIN_SIZE<=middle_pos1_local; bin_pos0+=varBIN_SIZE) {
			printf "%s\t%d\t%d\t%d\t%d\t%d\n", chr, bin_pos0, bin_pos0+varBIN_SIZE, tID, tIsLeft, tIsRight
			tID++
		}
	}
	function printBinsLeftReplicating(chr, ORI_pos0, middle_pos1_local) {
		distanceFromORI = (ORI_pos0 - middle_pos1_local)
		nBinsFromORI = (distanceFromORI - distanceFromORI%varBIN_SIZE)/varBIN_SIZE # floor(nBinsFromORI/varBIN_SIZE) 	# printf "distanceFromORI=%d, nBinsFromORI=%d\n", distanceFromORI, nBinsFromORI
		nBinsFromORI = myMin(nBinsFromORI, varMAX_BINS)
		middle_pos1_local = myMax(middle_pos1_local, ORI_pos0-varMAX_BINS*varBIN_SIZE);	tIsLeft=1; tIsRight=0; 	# printf "for (bin_pos0=%d-%d; bin_pos0+%d<=%d; bin_pos0+=%d)\n", ORI_pos0, nBinsFromORI*varBIN_SIZE, varBIN_SIZE, ORI_pos0, varBIN_SIZE 
		for (bin_pos0=ORI_pos0-nBinsFromORI*varBIN_SIZE; bin_pos0+varBIN_SIZE<=ORI_pos0; bin_pos0+=varBIN_SIZE) {
			printf "%s\t%d\t%d\t%d\t%d\t%d\n", chr, bin_pos0, bin_pos0+varBIN_SIZE, tID, tIsLeft, tIsRight
			tID++
		}
	}
	BEGIN{ first_chr="NA"; first_pos0=0; first_pos1=1; tID=1; middle_pos1=1;}{
		second_chr=$1; second_pos0=$2; second_pos1=$3; 
		if (first_chr == second_chr) { # We are not generating bins before the first ORI in the chromosome and after the last ORI in the chromosome.
			middle_pos1 = (first_pos1 + second_pos0)/2
			printBinsRightReplicating(first_chr, first_pos1, middle_pos1); #tID++
			printBinsLeftReplicating(second_chr, second_pos0, middle_pos1); #tID++	
		}
		first_chr=second_chr; first_pos0=second_pos0; first_pos1=second_pos1;
	}' $INPUT_BED_ORI > $OUTPUT_BED_REGIONS
	printRegions $OUTPUT_BED_REGIONS
}

function mapOtherFeatures {	#$OUTPUT_BED_REGIONS, $ORIGINAL_TERRITORIES_BED --> several helpfiles and $OUTPUT_BED_ALLCOLUMNS
	INDEX_COLUMN=7
	echo "Running bedtools map -a $OUTPUT_BED_REGIONS -b $ORIGINAL_TERRITORIES_BED -c 7 -o min -null 0 | cut -f $INDEX_COLUMN > ${OUTPUT_BED_REGIONS}.tTxPlus..."
	bedtools map -a $OUTPUT_BED_REGIONS -b $ORIGINAL_TERRITORIES_BED -c 7 -o min -null 0 | cut -f $INDEX_COLUMN > ${OUTPUT_BED_REGIONS}.tTxPlus
	bedtools map -a $OUTPUT_BED_REGIONS -b $ORIGINAL_TERRITORIES_BED -c 8 -o min -null 0 | cut -f $INDEX_COLUMN > ${OUTPUT_BED_REGIONS}.tTxMinus
	bedtools map -a $OUTPUT_BED_REGIONS -b $ORIGINAL_TERRITORIES_BED -c 9 -o mean -null NaN | cut -f $INDEX_COLUMN > ${OUTPUT_BED_REGIONS}.tRT
	bedtools map -a $OUTPUT_BED_REGIONS -b $ORIGINAL_TERRITORIES_BED -c 10 -o mean -null NaN | cut -f $INDEX_COLUMN > ${OUTPUT_BED_REGIONS}.tExpr

	echo "Running paste $OUTPUT_BED_REGIONS ${OUTPUT_BED_REGIONS}.tTxPlus ${OUTPUT_BED_REGIONS}.tTxMinus ${OUTPUT_BED_REGIONS}.tRT ${OUTPUT_BED_REGIONS}.tExpr > $OUTPUT_BED_ALLCOLUMNS..."
	paste $OUTPUT_BED_REGIONS ${OUTPUT_BED_REGIONS}.tTxPlus ${OUTPUT_BED_REGIONS}.tTxMinus ${OUTPUT_BED_REGIONS}.tRT ${OUTPUT_BED_REGIONS}.tExpr > $OUTPUT_BED_ALLCOLUMNS
	sh printFile.sh $OUTPUT_BED_ALLCOLUMNS
}


RUN_PETRYK=false
RUN_BESNARD=false
RUN_RANDOM=true

if [ "$RUN_PETRYK" == "true" ]; then
	for PETRYK_SAMPLE in Hela_rep2; do # GM_rep1 GM_rep2 Hela_rep1 Hela_rep2; do
		DIR_PETRYK="replication/Petryk${PETRYK_SAMPLE}_territories";  [ -d $DIR_PETRYK ] || mkdir -p $DIR_PETRYK
		INPUT_BEDGRAPH_ORI="replication/Petryk/${PETRYK_SAMPLE}.bed" #GM_rep2.bed Hela_rep1.bed Hela_rep2.bed
		for TRACK_TYPE in tags_C tags_W RFD; do
			TRACK_NAME="(${TRACK_TYPE})"; selectTrackFromOKseq "${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}.${TRACK_TYPE}.bed"
		done
		BEDFILE_PASTED="${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}.C.W.RFD.bed"
		OUTPUT_BED_REGIONS="${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}_territories.firstColumns.AllRegions.bed"
		OUTPUT_BED_ALLCOLUMNS="${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}_territories.AllRegions.bed"
		paste "${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}.tags_C.bed" "${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}.tags_W.bed" "${DIR_PETRYK}/Petryk${PETRYK_SAMPLE}.RFD.bed" > $BEDFILE_PASTED
		generateRegionsFromOKseq $BEDFILE_PASTED
		mapOtherFeatures
	done
	# sort -k1,1 -k2,2n replication/Petryk/GM_rep1.bed > replication/Petryk/GM_rep1.sorted.bed
	# sh printFile.sh replication/Petryk/GM_rep1.sorted.bed
	# sort -k1,1 -k2,2n replication/Petryk/GM_rep2.bed > replication/Petryk/GM_rep2.sorted.bed
	# sh printFile.sh replication/Petryk/GM_rep2.sorted.bed

	# sort -k1,1 -k2,2n replication/Petryk/Hela_rep1.bed > replication/Petryk/Hela_rep1.sorted.bed
	# sh printFile.sh replication/Petryk/Hela_rep1.sorted.bed
fi

if [ "$RUN_BESNARD" == "true" ]; then
	BIN_SIZE=20000
	MAX_BINS=100
	BIN_SIZE_TEXT="20kbp"
	INPUT_BED_ORI="Besnard/output03mapped_Foulk_200_5_50/ORI_inMajority_Besnard_Foulk_200_5_50.bed"
	OUTPUT_BED_REGIONS="Besnard/output03mapped_Foulk_200_5_50/ORI_inMajority_Besnard_Foulk_200_5_50.territories.${BIN_SIZE_TEXT}.max${MAX_BINS}bins.6columns.bed"
	OUTPUT_BED_ALLCOLUMNS="Besnard/output03mapped_Foulk_200_5_50/ORI_inMajority_Besnard_Foulk_200_5_50.territories.${BIN_SIZE_TEXT}.max${MAX_BINS}bins.bed"
	generateRegionsAroundORI
	mapOtherFeatures
fi

if [ "$RUN_RANDOM" == "true" ]; then
	BIN_SIZE=20000
	MAX_BINS=100
	BIN_SIZE_TEXT="20kbp"
	BED_FROM_MATLAB="replication/RandomORI_territories/tableBedRandomORI.txt"
	INPUT_BED_ORI="replication/RandomORI_territories/tableBedRandomORI.sorted.bed"
	echo "RUNNING sort -k1,1 -k2,2n $BED_FROM_MATLAB > $INPUT_BED_ORI ..."
	sort -k1,1 -k2,2n $BED_FROM_MATLAB > $INPUT_BED_ORI
	sh printFile.sh $INPUT_BED_ORI
	OUTPUT_BED_REGIONS="replication/RandomORI_territories/RandomORI_territories.firstColumns.AllRegions.bed"
	OUTPUT_BED_ALLCOLUMNS="replication/RandomORI_territories/RandomORI_territories.AllRegions.bed"
	generateRegionsAroundORI
	mapOtherFeatures
	
	# [mtomkova@login2(cortex) mutationFrequency]$ wc -l replication/Haradhvala_territories/Haradhvala_territories.OutsideGenesAndBlacklisted.bed
	# 859457 replication/Haradhvala_territories/Haradhvala_territories.OutsideGenesAndBlacklisted.bed	
	# [mtomkova@login2(cortex) mutationFrequency]$ wc -l replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed
	# 154794 replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed	
	# [mtomkova@login2(cortex) mutationFrequency]$ awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed
	# 3095677412	
	# [mtomkova@login2(cortex) mutationFrequency]$ awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/Haradhvala_territories/Haradhvala_territories.OutsideAllGenesAndBlacklisted.bed
	# 797664574	
	# [mtomkova@login2(cortex) mutationFrequency]$ awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.AllRegions.bed
	# 699440000
	####
	# awk '{if (($5==1 || $6==1) && ($7==0 && $8==0)) {print}}' replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed | wc -l
	# 34981	
	# awk '{if (($5==1 || $6==1) && ($7==0 && $8==0)) {print}}' replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed | awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}'
	# 699507792
	###
	# cat replication/RandomORI_territories/RandomORI_territories.AllRegions.bed | wc -l
	# 94474
	# awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.AllRegions.bed
	# 1889480000
	# awk '{if (($5==1 || $6==1)) {print}}' replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed | wc -l
	# 55077
	# awk '{if (($5==1 || $6==1)) {print}}' replication/Haradhvala_territories/Haradhvala_territories.AllRegions.bed | awk 'BEGIN{s=0}{s+=($3-$2)}END{printf s}'
	# 1101427792
	###
	# cat replication/RandomORI_territories/RandomORI_territories.OutsideGenesAndBlacklisted.bed | wc -l
	# 529938
	# awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.OutsideGenesAndBlacklisted.bed
	# 621610930
	# awk '{if (($5==1 || $6==1)) {print}}' replication/Haradhvala_territories/Haradhvala_territories.OutsideGenesAndBlacklisted.bed | wc -l
	# 325222
	# awk '{if (($5==1 || $6==1)) {print}}' replication/Haradhvala_territories/Haradhvala_territories.OutsideGenesAndBlacklisted.bed | awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}'
	# 403395812	
	###
	# cat replication/RandomORI_territories/RandomORI_territories.AllRegions.bed | wc -l
	# 89454
	# awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.AllRegions.bed
	# 1789080000
	# cat replication/RandomORI_territories/RandomORI_territories.OutsideGenesAndBlacklisted.bed | wc -l
	# 502770
	# awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.OutsideGenesAndBlacklisted.bed
	# 589216937
	# cat replication/RandomORI_territories/RandomORI_territories.AllRegions.bed | wc -l
	# 94474
	# awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.AllRegions.bed
	# 1889480000
	# cat replication/RandomORI_territories/RandomORI_territories.OutsideGenesAndBlacklisted.bed | wc -l
	# 529938
	# awk 'BEGIN{s=0}{s+=$3-$2}END{printf s}' replication/RandomORI_territories/RandomORI_territories.OutsideGenesAndBlacklisted.bed
	# 621610930
fi

# INPUT_BED_ORI="ORI_test.bed"
# OUTPUT_BED_REGIONS="ORI_test.territories.${BIN_SIZE_TEXT}.max${MAX_BINS}bins.bed"

# Test 1:
# chr1	2100	2105
# chr1	5000	5001
# chr1	2000000	2001000
# chr2	2000000	2001000
# BIN_SIZE=1000
# MAX_BINS=1000

# Test 2:
# chr1	50000 50001
# chr1	250000 250001
# chr1	255000 255001
# BIN_SIZE=1000
# MAX_BINS=10

# Test 3:
# chr1	50 51
# chr1	250 251
# BIN_SIZE=1
# MAX_BINS=10

# Test 4:
# chr1	50 51
# chr1	250 251
# BIN_SIZE=10
# MAX_BINS=10



echo "Script run_snsGenerateTerritories.sh FINISHED."



# [mtomkova@login1(cortex) mutationFrequency]$ head replication/per_base_territories_20kb_line_numbers.bed.txt
# chr	  pos0	   pos1	  tID	  tIsLeft tIsRight tTxPlus tTxMinus	tRT			  tExpr
# chr1    0       20000   1       0       0       0       0       627.973951      369933.000000
# chr1    20000   40000   2       0       0       0       0       627.973951      369933.000000
# chr1    40000   60000   3       0       0       0       0       627.973951      369933.000000

########################## OLD ##########################
# echo "Running awk -v varBIN_SIZE=$BIN_SIZE -v varMAX_BINS=$MAX_BINS '{...}' $INPUT_BED_ORI > $OUTPUT_BED_REGIONS"
# awk -v varBIN_SIZE=$BIN_SIZE -v varMAX_BINS=$MAX_BINS '
# function myMin(value1, value2) { return(value1 < value2 ? value1 : value2) } # printf "myMin(%d, %d)=%d\n", value1, value2, value1 < value2 ? value1 : value2; 
# function myMax(value1, value2) { return(value1 > value2 ? value1 : value2) } 
# function printBinsRightReplicating(chr, ORI_pos1, middle_pos1_local) { 	# printf "printBinsRightReplicating(%s, %d, %d, %d)\n", chr, ORI_pos1, middle_pos1_local, tID
	# middle_pos1_local = myMin(middle_pos1_local, ORI_pos1+(varMAX_BINS*varBIN_SIZE));	tIsLeft=0; tIsRight=1; 	# printf "for (bin_pos0=%d; bin_pos0+%d<=%d; bin_pos0+=%d)\n", ORI_pos1, varBIN_SIZE, middle_pos1_local, varBIN_SIZE 
	# for (bin_pos0=ORI_pos1; bin_pos0+varBIN_SIZE<=middle_pos1_local; bin_pos0+=varBIN_SIZE) {
		# printf "%s\t%d\t%d\t%d\t%d\t%d\n", chr, bin_pos0, bin_pos0+varBIN_SIZE, tID, tIsLeft, tIsRight
		# tID++
	# }
# }
# function printBinsLeftReplicating(chr, ORI_pos0, middle_pos1_local) {
	# distanceFromORI = (ORI_pos0 - middle_pos1_local)
	# nBinsFromORI = (distanceFromORI - distanceFromORI%varBIN_SIZE)/varBIN_SIZE # floor(nBinsFromORI/varBIN_SIZE) 	# printf "distanceFromORI=%d, nBinsFromORI=%d\n", distanceFromORI, nBinsFromORI
	# nBinsFromORI = myMin(nBinsFromORI, varMAX_BINS)
	# middle_pos1_local = myMax(middle_pos1_local, ORI_pos0-varMAX_BINS*varBIN_SIZE);	tIsLeft=1; tIsRight=0; 	# printf "for (bin_pos0=%d-%d; bin_pos0+%d<=%d; bin_pos0+=%d)\n", ORI_pos0, nBinsFromORI*varBIN_SIZE, varBIN_SIZE, ORI_pos0, varBIN_SIZE 
	# for (bin_pos0=ORI_pos0-nBinsFromORI*varBIN_SIZE; bin_pos0+varBIN_SIZE<=ORI_pos0; bin_pos0+=varBIN_SIZE) {
		# printf "%s\t%d\t%d\t%d\t%d\t%d\n", chr, bin_pos0, bin_pos0+varBIN_SIZE, tID, tIsLeft, tIsRight
		# tID++
	# }
# }
# BEGIN{ first_chr="NA"; first_pos0=0; first_pos1=1; tID=1; middle_pos1=1;}{
	# second_chr=$1; second_pos0=$2; second_pos1=$3; 
	# if (first_chr != "NA") {
		# if (first_chr != second_chr) {
			# printBinsRightReplicating(first_chr, first_pos1, first_pos1+varMAX_BINS*varBIN_SIZE); #tID++
			# middle_pos1 = myMax(1, second_pos1 - 2*varMAX_BINS*varBIN_SIZE)
		# } else {
			# middle_pos1 = (first_pos1 + second_pos0)/2
			# printBinsRightReplicating(first_chr, first_pos1, middle_pos1); #tID++
		# }
	# }
	# printBinsLeftReplicating(second_chr, second_pos0, middle_pos1); #tID++	
	# first_chr=second_chr; first_pos0=second_pos0; first_pos1=second_pos1;
# }' $INPUT_BED_ORI > $OUTPUT_BED_REGIONS
