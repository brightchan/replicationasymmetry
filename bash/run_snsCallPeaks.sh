#!/bin/bash -l

#############################################
# SGE SUBMISSION SCRIPT run_snsCallPeaks.sh #
#############################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job?
#
#$ -N snsCallPeaks

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m eas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=10504:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=16G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows. 
#$ -pe smp 1

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
# module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch
# module load apps/macs/2.1.0/gcc-4.4.7+numpy-1.8.0


###############################
# APPLICATION LAUNCH COMMANDS
###############################

DIRECTORY=$1
SAMPLE_NAME=$2
PAIRED=$3
GENOME_REFERENCE_PATH=$4
DIRECTIONAL=$5
GENOME_SIZES_UCSC=$6
CONTROL_SAMPLE_MAPPED_FILE=$7
INPUT_BAMFILE=$8 # _mapped

if [ -z "$DIRECTORY" ] ||  [ -z "$SAMPLE_NAME" ] || [ -z "$PAIRED" ] || [ -z "$GENOME_REFERENCE_PATH" ] || [ -z "$DIRECTIONAL" ] || [ -z "$GENOME_SIZES_UCSC" ] || [ -z "$CONTROL_SAMPLE_INPUT_BAMFILE" ] || [ -z "$INPUT_BAMFILE" ]; then
    echo "Not enough parameters." 1>&2
    exit 1
fi

echo "Script run_snsCallPeaks.sh $DIRECTORY $SAMPLE_NAME $PAIRED $GENOME_REFERENCE_PATH $DIRECTIONAL $GENOME_SIZES_UCSC $CONTROL_SAMPLE_INPUT_BAMFILE $INPUT_BAMFILE ..."


SAMPLE_PATH=$DIRECTORY"/output02trimmed/"$SAMPLE_NAME
OUTPUT_DIRECTORY_LOGS=$DIRECTORY"/output00logs" # OUTPUT_DIRECTORY_PEAKS_ORIG=$DIRECTORY"/output03mapped" # INPUT_BAMFILE=$INPUT_BAMFILE #"${DIRECTORY}/output03mapped/${SAMPLE_NAME}_mapped.bam"
OUTPUT_DIRECTORY_PEAKS=$DIRECTORY"/output04peaks" #output03mapped_Foulk_200_5_50

FILE_BDG_TREAT="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_treat_pileup"
FILE_BDG_CONTROL="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_control_lambda"
FILE_BED_SUMMITS="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_summits"
FILE_SORTED_COORDINATE="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_mapped_sortedByCoordinate.bam"
FILE_DEPTH="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_mapped_sortedByCoordinate_depth.bam"
FILE_DEPTH_BED="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_mapped_sortedByCoordinate_depth.bed"
FILE_DEPTH_BW="${OUTPUT_DIRECTORY_MAPPED}/${SAMPLE_NAME}_mapped_sortedByCoordinate_depth.bw"

[ -d $OUTPUT_DIRECTORY_LOGS ] || mkdir $OUTPUT_DIRECTORY_LOGS
[ -d $OUTPUT_DIRECTORY_MAPPED ] || mkdir $OUTPUT_DIRECTORY_MAPPED

if [ "$DIRECTIONAL" == true ]; then
	PARAM_DIRECTIONAL=""
else
	PARAM_DIRECTIONAL="--non_directional"
fi

runPeakCalling=true
runBigWig=true
runSortByCoordinate=true
runCoverageBigWig=true
MEMORY_SIZE="-Xmx10G" # For smaller data


######### PEAK CALLING ##########
if [ "$runPeakCalling" == true ]; then
	if [ "$CONTROL_SAMPLE_MAPPED_FILE" == "NA" ]; then
		echo "Running: ===========macs2 callpeak -t ${MAPPED_FILE} --format=BAM --name=${SAMPLE_NAME} --outdir ${OUTPUT_DIRECTORY_MAPPED} --gsize=hs --bw=300 --qvalue=0.05 --mfold 10 30 -B ==========="
		macs2 callpeak -t ${MAPPED_FILE} --format=BAM --name=${SAMPLE_NAME} --outdir ${OUTPUT_DIRECTORY_MAPPED} --gsize=hs --bw=200 --qvalue=0.05 --mfold 5 50 -B
	else
		echo "Running: ===========macs2 callpeak -t ${MAPPED_FILE} -c ${CONTROL_SAMPLE_MAPPED_FILE} --format=BAM --name=${SAMPLE_NAME} --outdir ${OUTPUT_DIRECTORY_MAPPED} --gsize=hs --bw=300 --qvalue=0.05 --mfold 10 30 -B ==========="
		macs2 callpeak -t ${MAPPED_FILE} -c ${CONTROL_SAMPLE_MAPPED_FILE} --format=BAM --name=${SAMPLE_NAME} --outdir ${OUTPUT_DIRECTORY_MAPPED} --gsize=hs --bw=200 --qvalue=0.05 --mfold 5 50 -B
	fi
fi

######### BIG WIG ##########
if [ "$runBigWig" == true ]; then
	echo "Sorting ${FILE_BDG_TREAT}.bdg, ${FILE_BDG_CONTROL}.bdg, cutting ${FILE_BED_SUMMITS}.bdg and running ./bedGraphToBigWig on them..."
	sort -k1,1 -k2,2n "${FILE_BDG_TREAT}.bdg" > "${FILE_BDG_TREAT}.properBed"
	./bedGraphToBigWig "${FILE_BDG_TREAT}.properBed" $GENOME_SIZES_UCSC "${FILE_BDG_TREAT}.bw"

	sort -k1,1 -k2,2n "${FILE_BDG_CONTROL}.bdg" > "${FILE_BDG_CONTROL}.properBed"
	./bedGraphToBigWig "${FILE_BDG_CONTROL}.properBed" $GENOME_SIZES_UCSC "${FILE_BDG_CONTROL}.bw"

	cut -f 1,2,3,5 "${FILE_BED_SUMMITS}.bed" > "${FILE_BED_SUMMITS}.properBed"
	./bedGraphToBigWig "${FILE_BED_SUMMITS}.properBed" $GENOME_SIZES_UCSC "${FILE_BED_SUMMITS}.bw"
fi

###================ SORT BY COORDINATE ================###
if [ "$runSortByCoordinate" == true ]; then
	echo "Sorting java ${MEMORY_SIZE} -jar ~/bin/picard-tools/picard-tools-1.129/picard.jar SortSam INPUT=${MAPPED_FILE} OUTPUT=${FILE_SORTED_COORDINATE} SORT_ORDER=coordinate TMP_DIR=$OUTPUT_DIRECTORY_MAPPED"
	java $MEMORY_SIZE -jar ~/bin/picard-tools/picard-tools-1.129/picard.jar SortSam INPUT=${MAPPED_FILE} OUTPUT=$FILE_SORTED_COORDINATE SORT_ORDER=coordinate TMP_DIR=$OUTPUT_DIRECTORY_MAPPED
	
	echo "Running: ======== samtools depth $FILE_SORTED_COORDINATE > $FILE_DEPTH ========"
	samtools depth $FILE_SORTED_COORDINATE > $FILE_DEPTH
fi
	
if [ "$runCoverageBigWig" == true ]; then	
	echo "Running: ======== awk '{printf %s\t%d\t%d\t%d\n, $1, ($2-1), $2, $3, varSAMPLE}' $FILE_DEPTH >  $FILE_DEPTH_BED ========"
	awk '{printf "%s\t%d\t%d\t%d\n", $1, ($2-1), $2, $3, varSAMPLE}' $FILE_DEPTH | sort -k1,1 -k2,2n  >  $FILE_DEPTH_BED
	
	echo "Running: ======== ./bedGraphToBigWig $FILE_DEPTH_BED $GENOME_SIZES_UCSC $FILE_DEPTH_BW ========"
	./bedGraphToBigWig $FILE_DEPTH_BED $GENOME_SIZES_UCSC $FILE_DEPTH_BW
fi


echo "Script run_snsCallPeaks.sh FINISHED."
