#!/bin/bash -l

#######################################################
# SGE RUN ALL SUBMISSION SCRIPTs all_mfReplication.sh #
#######################################################
#$ -cwd
#$ -S /bin/bash
#$ -V
#$ -j y
#$ -M marketa.tomkova@ndm.ox.ac.uk
#$ -m as
#$ -l h_rt=48:00:00
#$ -l h_vmem=16G
#$ -pe smp 1

OUTPUT_FILE="jobs_all_mfReplication_"`date '+%Y_%m_%d-%H_%M'`".txt"

echo "Running sh_mfReplication.sh for the following files:" >> $OUTPUT_FILE

module load apps/bedtools/2.20.1
################################
TEXT_HARADHVALA="Haradhvala_territories"
TEXT_BESNARD="Besnard_territories"
TEXT_BESNARD1k="Besnard1k_territories"
TEXT_BESNARDB20k="BesnardB20k_territories"
TEXT_RandomORI="RandomORI_territories"

TEXT_OutsideAllGenesAndBlacklisted="OutsideAllGenesAndBlacklisted"
TEXT_OutsideGenesAndBlacklisted="OutsideGenesAndBlacklisted"
TEXT_OutsideBlacklisted="OutsideBlacklisted"
TEXT_OutsideGenes="OutsideGenes"
TEXT_AllRegions="AllRegions"

DIR_REPLICATION="replication"; 								[ -d $DIR_REPLICATION ] || mkdir -p $DIR_REPLICATION
DIR_MAPPABILITY="${DIR_REPLICATION}/Mappability"; 			[ -d $DIR_MAPPABILITY ] || mkdir -p $DIR_MAPPABILITY	
DIR_HARADHVALA="${DIR_REPLICATION}/${TEXT_HARADHVALA}"; 	[ -d $DIR_HARADHVALA ] || mkdir -p $DIR_HARADHVALA	
DIR_BESNARD="${DIR_REPLICATION}/${TEXT_BESNARD}"; 			[ -d $DIR_BESNARD ] || mkdir -p $DIR_BESNARD	
DIR_BESNARD1k="${DIR_REPLICATION}/${TEXT_BESNARD1k}"; 			[ -d $DIR_BESNARD1k ] || mkdir -p $DIR_BESNARD1k
DIR_BESNARDB20k="${DIR_REPLICATION}/${TEXT_BESNARDB20k}"; 			[ -d $DIR_BESNARDB20k ] || mkdir -p $DIR_BESNARDB20k	
DIR_RandomORI="${DIR_REPLICATION}/${TEXT_RandomORI}"; 			[ -d $DIR_RandomORI ] || mkdir -p $DIR_RandomORI

REMOVED_AGALM_REGIONS_BED="${DIR_MAPPABILITY}/allGenesAndLowMappabilityRegions.bed";
REMOVED_GALM_REGIONS_BED="${DIR_MAPPABILITY}/genesAndLowMappabilityRegions.bed";
REMOVED_BLACKLISTED_ONLY_BED="${DIR_MAPPABILITY}/blacklistedOnly.bed";		
REMOVED_GENES_ONLY_BED="${DIR_MAPPABILITY}/genesOnly.bed";	

################################ Needed input files: ################################
# Files in replication/Mappability/: 
	# mappability_100bp_windows.bed
	# mappabilityBelow0.99.merged.bed
	# Anshul_Hg19UltraHighSignalArtifactRegions.bed
	# Duke_Hg19SignalRepeatArtifactRegions.bed
	# wgEncodeHg19ConsensusSignalArtifactRegions.bed
# ../commonData/features/hg19/regions_GENEs.bed
# ../hg19/genomeSize_hg19.txt
# ../hg19/hg19_full.fa
# Files in ../commonData/features/hg19/mutations/mut_"*"_context96_genex_withGenes.onlyWGS.bedlike, where "*" is CANCER_TYPE
	# The format is tab-delimited: chromosome, pos0, pos1, sample name, from base, to base, sequence context, is CpGtoT mutations, strand, WGS/WES, gene name
	# E.g., chr1    565469  565470  DO50348 C       T       ACG     isCpGtoT        -       WGS     .
# Haradhvala: data/AsymTools/per_base_territories_20kb.txt
	# The format is tab-delimited: chromosome, start, end, is left, is right, is transcribed plus, is transcribed minus, replication timing, mean expression, A1, A2, A3, A4, A1_tx, A2_tx, A3_tx, A4_tx (from Haradhvala et al. 2016)
	# E.g., 1       1       20000   0       0       0       0       627.97395075929 369933  2065    3028    2895    2011    1170    1635    1723    1110
# Besnard: replication/Besnard_territories/Besnard_territories_line_numbers.bed (the same format as above, with line number column at the beginning)
# In each DIR_TERRITORIES_SOURCE/: (computed in matlab script script_replPrepareTerritories.m, see below)
	# table.Haradhvala_territories.mat
	# cellResult_background.Haradhvala_territories.OutsideGenes.mat
	# cellResult_background.Haradhvala_territories.OutsideBlacklisted.mat
	# cellResult_background.Haradhvala_territories.OutsideGenesAndBlacklisted.mat
	# cellResult_background.Haradhvala_territories.AllRegions.mat
#####################################################################################

function download_allGenes {
	echo "RUNNING download_allGenes ..."
	cd replication
	wget ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz
	gunzip gencode.v19.annotation.gtf.gz
	cd ../
	INPUT_FILE=replication/gencode.v19.annotation.gtf
	SORTED_FILE=replication/sortedGenes.hg19v19.txt
	SORTED_CODING_FILE=replication/sortedGenes.proteinCoding.hg19v19.txt
	SORTED_CODING_FILE_EXONS=replication/sortedExons.proteinCoding.hg19v19.txt
	echo "Creating a list of genes/exons/transcripts from $INPUT_FILE and saving them to $SORTED_FILE..."
	sh printFile.sh $INPUT_FILE
	grep -v "^#" $INPUT_FILE | gawk 'BEGIN{FS="\t"; OFS="\t"} {
		gene_id_result = gensub(/.*gene_id "([^\"]+)".*/, "\\1", "", $9)
		gene_type_result = gensub(/.*gene_type "([^\"]+)".*/, "\\1", "", $9)
		gene_name_result = gensub(/.*gene_name "([^\"]+)".*/, "\\1", "", $9)
		if ( match($9, ".*exon_number.*") ) {
			exon_number_result = gensub(/.*exon_number ([^ ]+);.*/, "\\1", "", $9)
		} else {
			exon_number_result = ""
		}
		print $1,$4,$5,gene_name_result,$3,$7,gene_id_result,gene_type_result,exon_number_result }' | sort -k1,1 -k2,2n -k3,3n > $SORTED_FILE
	
	echo "Format of the $SORTED_FILE is a tab-delimited with the following columns:"	
	echo "chromosome, start, end, gene_name, feature_type, strand, gene_id, gene_type, exon_number"
	sh printFile.sh $SORTED_FILE
	
	#### SOME ADDITIONAL INFO, NOT NEEDED IN THE REST OF THE CODE ####
	# echo "Selecting only protein coding genes from $SORTED_FILE and saving it to $SORTED_CODING_FILE..."
	# awk 'BEGIN{FS=" "; OFS="\t"}{
		# if ($5 == "gene" && $8 == "protein_coding") print $1,$2,$3,$4,$5,$6,$7,$8
	# }' $SORTED_FILE > $SORTED_CODING_FILE
	# sh printFile.sh $SORTED_CODING_FILE

	# echo "Selecting only protein coding exons from $SORTED_FILE and saving it to $SORTED_CODING_FILE_EXONS..."
	# awk 'BEGIN{FS=" "; OFS="\t"}{
		# if ($5 == "exon" && $8 == "protein_coding") print $1,$2,$3,$4,$5,$6,$7,$8,$9
	# }' $SORTED_FILE > $SORTED_CODING_FILE_EXONS
	# sh printFile.sh $SORTED_CODING_FILE_EXONS
	
	# echo "unique_gene_type_result (column 8)"
	# awk '{ARRAY_OCCURENCES[$8]++; ARRAY_TOTAL_COVERAGE[$8]+=($3-$2)}END{for (key in ARRAY_OCCURENCES) {printf "%s: %d (total coverage %d)\n", key, ARRAY_OCCURENCES[key], ARRAY_TOTAL_COVERAGE[key]}}' $SORTED_FILE > replication/sortedGenes.hg19v19.unique_gene_type_result.txt
	# cat replication/sortedGenes.hg19v19.unique_gene_type_result.txt
	
	# echo ""
	# echo "unique_gene_type_result2 (column 5)"
	# awk '{ARRAY_OCCURENCES[$5]++; ARRAY_TOTAL_COVERAGE[$5]+=($3-$2)}END{for (key in ARRAY_OCCURENCES) {printf "%s: %d (total coverage %d)\n", key, ARRAY_OCCURENCES[key], ARRAY_TOTAL_COVERAGE[key]}}' $SORTED_FILE > replication/sortedGenes.hg19v19.unique_gene_type_result2.txt
	# cat replication/sortedGenes.hg19v19.unique_gene_type_result2.txt
	
	# cut -f 1-3 $SORTED_FILE > $SORTED_FILE".bed"
	# bedtools merge -i $SORTED_FILE".bed" > replication/sortedGenes.hg19v19.merged.bed
	# sh printFile.sh replication/sortedGenes.hg19v19.merged.bed
	# awk 'BEGIN{sumCoverage=0}{sumCoverage+=$3-$2}END{printf "All genes coverage: %d\n", sumCoverage}' replication/sortedGenes.hg19v19.merged.bed
	
	# grep protein_coding $SORTED_FILE | cut -f 1-3 > $SORTED_FILE".proteinCoding.bed"
	# bedtools merge -i $SORTED_FILE".proteinCoding.bed" > replication/sortedGenes.hg19v19.merged.proteinCoding.bed
	# sh printFile.sh replication/sortedGenes.hg19v19.merged.proteinCoding.bed
	# awk 'BEGIN{sumCoverage=0}{sumCoverage+=$3-$2}END{printf "Protein coding coverage: %d\n", sumCoverage}' replication/sortedGenes.hg19v19.merged.proteinCoding.bed
	
	# cut -f 1-3 ../commonData/features/hg19/regions_GENEs.bed > tmp$$
	# bedtools merge -i tmp$$ > tmp$$.bed
	# sh printFile.sh tmp$$.bed
	# awk 'BEGIN{sumCoverage=0}{sumCoverage+=$3-$2}END{printf "Protein coding coverage: %d\n", sumCoverage}' tmp$$.bed
	
	# awk 'BEGIN{sumCoverage=0}{sumCoverage+=$3-$2}END{printf "Protein coding (not merged) coverage (all genes coverage): %d\n", sumCoverage}' ../commonData/features/hg19/regions_GENEs.bed
	
	# TMP_FILE=$SORTED_FILE$$
	# REMOVED_AGALM_REGIONS_BED="${DIR_MAPPABILITY}/allGenesAndLowMappabilityRegions.bed";
	# cut -f 1,2,3 replication/sortedGenes.hg19v19.merged.bed > $TMP_FILE
	# cut -f 1,2,3 ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed  >> $TMP_FILE
	# cut -f 1,2,3 ${DIR_MAPPABILITY}/Anshul_Hg19UltraHighSignalArtifactRegions.bed  >> $TMP_FILE
	# cut -f 1,2,3 ${DIR_MAPPABILITY}/Duke_Hg19SignalRepeatArtifactRegions.bed  >> $TMP_FILE
	# cut -f 1,2,3 ${DIR_MAPPABILITY}/wgEncodeHg19ConsensusSignalArtifactRegions.bed  >> $TMP_FILE
	
	# echo "Running sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted..." #>> $OUTPUT_FILE
	# sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted
	
	# echo "Running bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_AGALM_REGIONS_BED ..." #>> $OUTPUT_FILE
	# bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_AGALM_REGIONS_BED
	# sh printFile.sh $REMOVED_AGALM_REGIONS_BED
	####rm $TMP_FILE
	# awk 'BEGIN{sumCoverage=0}{sumCoverage+=$3-$2}END{printf "Total coverage (all genes and low mappability): %d\n", sumCoverage}' $REMOVED_AGALM_REGIONS_BED
	# awk 'BEGIN{sumCoverage=0}{sumCoverage+=$3-$2}END{printf "Total coverage (protein coding genes and low mappability): %d\n", sumCoverage}' $REMOVED_GALM_REGIONS_BED
}

function prepare_blacklist_1 {	### FIRST: prepare regions with mappability below 0.99 --> ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed
	echo "Running akw {'...'} ${DIR_MAPPABILITY}/mappability_100bp_windows.bed > ${DIR_MAPPABILITY}/mappabilityBelow0.99.bed to subtract 50bp and print only lines with value < 0.99." >> $OUTPUT_FILE
	awk '{ chr=$1; pos0=$2; pos1=$3; value=$4;
		if (value == "." || value < 0.99) { 
			pos0=(0 > pos0-50 ? 0 : pos0-50) # min(0, pos0-50)
			pos1=(0 > pos1-50 ? 0 : pos1-50) # min(0, pos1-50)
			printf "%s\t%d\t%d\t%f\n", chr, pos0, pos1, value
		}
	}' ${DIR_MAPPABILITY}/mappability_100bp_windows.bed > ${DIR_MAPPABILITY}/mappabilityBelow0.99.bed
	sh printFile.sh ${DIR_MAPPABILITY}/mappabilityBelow0.99.bed
	
	echo "Running bedtools merge -i ${DIR_MAPPABILITY}/mappabilityBelow0.99.bed > ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed ..." >> $OUTPUT_FILE
	bedtools merge -i ${DIR_MAPPABILITY}/mappabilityBelow0.99.bed > ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed
	sh printFile.sh ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed	
}

function prepare_blacklist_2 {	### SECOND: merge genes, mappabilityBelow0.99.bed, and other blacklisted regions --> $REMOVED_GALM_REGIONS_BED
	TMP_FILE=$REMOVED_GALM_REGIONS_BED$$
	cut -f 1,2,3 ../commonData/features/hg19/regions_GENEs.bed > $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/Anshul_Hg19UltraHighSignalArtifactRegions.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/Duke_Hg19SignalRepeatArtifactRegions.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/wgEncodeHg19ConsensusSignalArtifactRegions.bed  >> $TMP_FILE
	
	echo "Running sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted..." >> $OUTPUT_FILE
	sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted
	
	echo "Running bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_GALM_REGIONS_BED ..." >> $OUTPUT_FILE
	bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_GALM_REGIONS_BED
	sh printFile.sh $REMOVED_GALM_REGIONS_BED
	rm $TMP_FILE
}

function prepare_blacklist_3 {	### THIRD: merge genes, mappabilityBelow0.99.bed, and other blacklisted regions --> $REMOVED_BLACKLISTED_ONLY_BED
	TMP_FILE=$REMOVED_BLACKLISTED_ONLY_BED$$
	cut -f 1,2,3 ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/Anshul_Hg19UltraHighSignalArtifactRegions.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/Duke_Hg19SignalRepeatArtifactRegions.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/wgEncodeHg19ConsensusSignalArtifactRegions.bed  >> $TMP_FILE
	
	echo "Running sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted..." >> $OUTPUT_FILE
	sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted
	
	echo "Running bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_BLACKLISTED_ONLY_BED ..." >> $OUTPUT_FILE
	bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_BLACKLISTED_ONLY_BED
	sh printFile.sh $REMOVED_BLACKLISTED_ONLY_BED	 >> $OUTPUT_FILE
	rm $TMP_FILE
}

function prepare_blacklist_4 {	### FOURTH: prepare genes only --> $REMOVED_GENES_ONLY_BED
	TMP_FILE=$REMOVED_GENES_ONLY_BED$$
	cut -f 1,2,3 ../commonData/features/hg19/regions_GENEs.bed > $TMP_FILE
	
	echo "Running sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted..." >> $OUTPUT_FILE
	sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted
	
	echo "Running bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_GENES_ONLY_BED ..." >> $OUTPUT_FILE
	bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_GENES_ONLY_BED
	sh printFile.sh $REMOVED_GENES_ONLY_BED	 >> $OUTPUT_FILE
	rm $TMP_FILE	
}

function prepare_blacklist_5_allGenes_and_lowMap {	### SECOND: merge genes, mappabilityBelow0.99.bed, and other blacklisted regions --> $REMOVED_AGALM_REGIONS_BED
	TMP_FILE=$REMOVED_AGALM_REGIONS_BED$$
	cut -f 1,2,3 replication/sortedGenes.hg19v19.txt > $TMP_FILE # created in download_allGenes
	cut -f 1,2,3 ${DIR_MAPPABILITY}/mappabilityBelow0.99.merged.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/Anshul_Hg19UltraHighSignalArtifactRegions.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/Duke_Hg19SignalRepeatArtifactRegions.bed  >> $TMP_FILE
	cut -f 1,2,3 ${DIR_MAPPABILITY}/wgEncodeHg19ConsensusSignalArtifactRegions.bed  >> $TMP_FILE
	
	echo "Running sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted..." >> $OUTPUT_FILE
	sort -k1,1 -k2,2n $TMP_FILE > ${TMP_FILE}.sorted
	
	echo "Running bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_AGALM_REGIONS_BED ..." >> $OUTPUT_FILE
	bedtools merge -i ${TMP_FILE}.sorted > $REMOVED_AGALM_REGIONS_BED
	sh printFile.sh $REMOVED_AGALM_REGIONS_BED
	rm $TMP_FILE
}

function prepare_replTerritories_Haradhvala {	### Select relevant columns (--> $HARADHVALA_ASYMM_TERRITORIES) and add line numbers --> $HARADHVALA_ASYMM_TERRITORIES_LINENUMBERS	
	HARADHVALA_ASYMM_TERRITORIES="${DIR_HARADHVALA}/per_base_territories_20kb.${TEXT_AllRegions}.bed"; 		#per_base_territories_20kb	
	awk 'BEGIN{FS="\t"; OFS="\t"}{if ($1=="23") {$1="X"} if ($1=="24") {$1="Y"} if (NR>1) {$1="chr"$1; $2=$2-1;} print;}'  data/AsymTools/per_base_territories_20kb.txt | tail -n +2 | sort -k1,1 -k2,2n > $HARADHVALA_ASYMM_TERRITORIES
	sh printFile.sh $HARADHVALA_ASYMM_TERRITORIES
	# FILE_NAME data/AsymTools/per_base_territories_20kb.bed:
	# 154794 data/AsymTools/per_base_territories_20kb.bed
	##chr     st      en      is_left is_right        txplus  txminus rt      expr    A1      A2      A3      A4      A1_tx   A2_tx   A3_tx   A4_tx
	# chr1    0       20000   0       0       0       0       627.97395075929 369933  2065    3028    2895    2011    1170    1635    1723    1110
	# chr1    20000   40000   0       0       0       0       627.97395075929 369933  5224    4934    4952    4890    2787    2835    2678    2542
	# ...
	# chrY    59340000        59360000        0       0       0       0       NaN     NaN     0       0       0       0       0       0       0       0
	# chrY    59360000        59373566        0       0       0       0       NaN     NaN     0       0       0       0       0       0       0       0
	# ============
	
	### SECOND
	awk '{ chr=$1; pos0=$2; pos1=$3; is_left=$4; is_right=$5; txplus=$6; txminus=$7; rt=$8; expr=$9; 
		printf "%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t%f\n", chr, pos0, pos1, NR, is_left, is_right, txplus, txminus, rt, expr
	}' $HARADHVALA_ASYMM_TERRITORIES > $HARADHVALA_ASYMM_TERRITORIES_LINENUMBERS
	sh printFile.sh $HARADHVALA_ASYMM_TERRITORIES_LINENUMBERS
	# 154794 data/AsymTools/per_base_territories_20kb_line_numbers.bed
	# chr1    0       20000   1       0       0       0       0       627.973951      369933.000000
	# chr1    20000   40000   2       0       0       0       0       627.973951      369933.000000
	# ...
	# chrY    59340000        59360000        154793  0       0       0       0       0.000000        0.000000
	# chrY    59360000        59373566        154794  0       0       0       0       0.000000        0.000000
}

function prepare_replTerritories_subtractRemoved { ### Subtract $1 ($REMOVED_GALM_REGIONS_BED) from territories --> bedfile, defined by $2 ($TEXT_HARADHVALA) and $3 ($TEXT_OutsideGenesAndBlacklisted), uses ${DIR_REPLICATION}, ${TEXT_AllRegions}
	REMOVED_GALM_REGIONS_BED_LOCAL=$1	# e.g., $REMOVED_GALM_REGIONS_BED
	TEXT_TERRITORIES_SOURCE=$2 			# e.g., $TEXT_HARADHVALA
	TEXT_REMOVAL_TYPE=$3 				# e.g., $TEXT_OutsideGenesAndBlacklisted
	echo "Running prepare_replTerritories_subtractRemoved REMOVED_GALM_REGIONS_BED_LOCAL=${REMOVED_GALM_REGIONS_BED_LOCAL}, TEXT_TERRITORIES_SOURCE=${TEXT_TERRITORIES_SOURCE}, TEXT_REMOVAL_TYPE=${TEXT_REMOVAL_TYPE}..." >> $OUTPUT_FILE
	
	ASYMM_TERRITORIES_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/${TEXT_TERRITORIES_SOURCE}.${TEXT_AllRegions}.bed"	
	ASYMM_TERRITORIES_WITHOUT_REMOVED_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/${TEXT_TERRITORIES_SOURCE}.${TEXT_REMOVAL_TYPE}.bed" 
	echo "Running bedtools subtract -a $ASYMM_TERRITORIES_LOCAL -b $REMOVED_GALM_REGIONS_BED_LOCAL > $ASYMM_TERRITORIES_WITHOUT_REMOVED_LOCAL..." >> $OUTPUT_FILE
	bedtools subtract -a $ASYMM_TERRITORIES_LOCAL -b $REMOVED_GALM_REGIONS_BED_LOCAL  > $ASYMM_TERRITORIES_WITHOUT_REMOVED_LOCAL #| awk '{if ($3 > $2) print}'
	sh printFile.sh $ASYMM_TERRITORIES_WITHOUT_REMOVED_LOCAL	 >> $OUTPUT_FILE
}

function listAllCTs { ### Print all CTs with ...context96_genex_withGenes.onlyWGS.bedlike file --> $1
	LIST_CT_FILE=$1			# e.g., data/Mutations/list_all_cancerTypes.txt
	echo "Running listAllCTs LIST_CT_FILE=${LIST_CT_FILE}..." >> $OUTPUT_FILE
	
	echo -n "" > $LIST_CT_FILE
	for MUTATION_FILE_INPUT in "../commonData/features/hg19/mutations/mut_"*"_context96_genex_withGenes.onlyWGS.bedlike"; do 	# all cancer types
		CANCER_TYPE=${MUTATION_FILE_INPUT##*/mut_}  							# Only text after "/mut_"
		CANCER_TYPE=${CANCER_TYPE%_context96_genex_withGenes.onlyWGS.bedlike}  	# Removes "_context96_genex_withGenes.onlyWGS.bedlike" from the end
		echo "$CANCER_TYPE" >> $LIST_CT_FILE
	done
	sh printFile.sh $LIST_CT_FILE
}

function annotateMutationsWithTerritories { ### Intersect bedlikes of CTs in $1 (LIST_CT_FILE) with territories based on $2 ($TEXT_HARADHVALA), uses ${DIR_REPLICATION}, ${TEXT_AllRegions}
	LIST_CT_FILE=$1				# e.g., data/Mutations/list_ICGC_new_cancerTypes.txt
	TEXT_TERRITORIES_SOURCE=$2 	# e.g., $TEXT_HARADHVALA
	echo "Running annotateMutationsWithTerritories TEXT_TERRITORIES_SOURCE=${TEXT_TERRITORIES_SOURCE}, LIST_CT_FILE=${LIST_CT_FILE}..." >> $OUTPUT_FILE
	
	ASYMM_TERRITORIES_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/${TEXT_TERRITORIES_SOURCE}.${TEXT_AllRegions}.bed"	
	OUTPUT_DIR_ALLCONTEXTS_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/mut${TEXT_AllRegions}";		[ -d $OUTPUT_DIR_ALLCONTEXTS_LOCAL ] || mkdir -p $OUTPUT_DIR_ALLCONTEXTS_LOCAL

	while read CT; do
		MUTATION_FILE_INPUT="../commonData/features/hg19/mutations/mut_${CT}_context96_genex_withGenes.onlyWGS.bedlike"
		# MUTATION_FILE_OUTPUT=${MUTATION_FILE_INPUT##*/}  			# Only text after the last slash	
		MUTATION_FILE_OUTPUT="${OUTPUT_DIR_ALLCONTEXTS_LOCAL}/mut${TEXT_AllRegions}_${CT}_context96_genex_withGenes.onlyWGS.bedlike"
		
		echo "Running bedtools intersect -a $MUTATION_FILE_INPUT -b $ASYMM_TERRITORIES_LOCAL -wa -wb -sorted > $MUTATION_FILE_OUTPUT ..." >> $OUTPUT_FILE
		bedtools intersect -a $MUTATION_FILE_INPUT -b $ASYMM_TERRITORIES_LOCAL -wa -wb -sorted > $MUTATION_FILE_OUTPUT # INFO: removed -loj 
		sh printFile.sh $MUTATION_FILE_OUTPUT		>> $OUTPUT_FILE		
	done < "$LIST_CT_FILE"
}

function subtractMutationsInRemovedRegions {	### Subtract $1 ($REMOVED_GALM_REGIONS_BED) from mutations based on $2 ($TEXT_HARADHVALA) and $3 ($TEXT_OutsideGenesAndBlacklisted) and $4 (LIST_CT_FILE), uses ${DIR_REPLICATION}, ${TEXT_AllRegions}
	REMOVED_GALM_REGIONS_BED_LOCAL=$1	# e.g., $REMOVED_GALM_REGIONS_BED
	TEXT_TERRITORIES_SOURCE=$2 			# e.g., $TEXT_HARADHVALA
	TEXT_REMOVAL_TYPE=$3 				# e.g., $TEXT_OutsideGenesAndBlacklisted
	LIST_CT_FILE=$4						# e.g., data/Mutations/list_ICGC_new_cancerTypes.txt
	echo "Running subtractMutationsInRemovedRegions REMOVED_GALM_REGIONS_BED_LOCAL=${REMOVED_GALM_REGIONS_BED_LOCAL}, TEXT_TERRITORIES_SOURCE=${TEXT_TERRITORIES_SOURCE}, TEXT_REMOVAL_TYPE=${TEXT_REMOVAL_TYPE}, LIST_CT_FILE=${LIST_CT_FILE}..." >> $OUTPUT_FILE
	
	OUTPUT_DIR_ALLCONTEXTS_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/mut${TEXT_AllRegions}" 		# should already exist from annotateMutationsWithTerritories
	OUTPUT_DIRECTORY_CURRENT="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/mut${TEXT_REMOVAL_TYPE}";		[ -d $OUTPUT_DIRECTORY_CURRENT ] || mkdir -p $OUTPUT_DIRECTORY_CURRENT
	while read CT; do
		MUTATION_FILE_INPUT="${OUTPUT_DIR_ALLCONTEXTS_LOCAL}/mut${TEXT_AllRegions}_${CT}_context96_genex_withGenes.onlyWGS.bedlike"
		# MUTATION_FILE_OUTPUT=${MUTATION_FILE_INPUT##*/mut${TEXT_AllRegions}_}  			# Only text after the last slash	
		# MUTATION_FILE_OUTPUT="${OUTPUT_DIRECTORY_CURRENT}/mut${TEXT_REMOVAL_TYPE}_${MUTATION_FILE_OUTPUT}" 
		MUTATION_FILE_OUTPUT="${OUTPUT_DIRECTORY_CURRENT}/mut${TEXT_REMOVAL_TYPE}_${CT}_context96_genex_withGenes.onlyWGS.bedlike"
		if [ ! -f "${MUTATION_FILE_INPUT}" ] && [ -f "${MUTATION_FILE_INPUT}.txt" ]; then
			echo "RUNNING cp ${MUTATION_FILE_INPUT}.txt ${MUTATION_FILE_INPUT} ..."
			cp "${MUTATION_FILE_INPUT}.txt" "${MUTATION_FILE_INPUT}"
		fi
		
		echo "Running bedtools subtract -a $MUTATION_FILE_INPUT -b $REMOVED_GALM_REGIONS_BED_LOCAL > $MUTATION_FILE_OUTPUT ..." >> $OUTPUT_FILE
		bedtools subtract -a $MUTATION_FILE_INPUT -b $REMOVED_GALM_REGIONS_BED_LOCAL > $MUTATION_FILE_OUTPUT
		sh printFile.sh $MUTATION_FILE_OUTPUT	 >> $OUTPUT_FILE		
	done < "$LIST_CT_FILE"
}

function computeFrequenciesInTrinucleotides_1 {	### Runs run_mfSequenceInBins.sh to compute background frequencies in trinucleotides for $1 ($TEXT_HARADHVALA) and $2 ($TEXT_OutsideGenesAndBlacklisted)
	TEXT_TERRITORIES_SOURCE=$1 			# e.g., $TEXT_HARADHVALA
	TEXT_REMOVAL_TYPE=$2 				# e.g., $TEXT_OutsideGenesAndBlacklisted
	echo "Running computeFrequenciesInTrinucleotides_1 TEXT_TERRITORIES_SOURCE=${TEXT_TERRITORIES_SOURCE}, TEXT_REMOVAL_TYPE=${TEXT_REMOVAL_TYPE}..." >> $OUTPUT_FILE
	
	ASYMM_TERRITORIES_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/${TEXT_TERRITORIES_SOURCE}.${TEXT_REMOVAL_TYPE}.bed"		
	DIRECTORY_OUTPUTS="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/contextInTerritories_${TEXT_TERRITORIES_SOURCE}_${TEXT_REMOVAL_TYPE}/splitTerritories"; [ -d $DIRECTORY_OUTPUTS ] || mkdir -p $DIRECTORY_OUTPUTS
	CHROM_SIZE_FILE="../hg19/genomeSize_hg19.txt"
	GENOME_REFERENCE_PATH="../hg19/hg19_full.fa"
	SEQUENCE_INDEX_OF_MUTATED_BASE=2
	NBINS_ON_SIDES=0
	for STRAND in "+" "-"; do
		if [ "$STRAND" == "+" ]; then
			STRAND_TEXT="plus"
		elif [ "$STRAND" == "-" ]; then
			STRAND_TEXT="minus"
		fi	
		for BASE1 in A C G T; do
			for BASE2 in A C G T; do
				for BASE3 in A C G T; do
					SEQUENCE="${BASE1}${BASE2}${BASE3}"					
					OUTPUT_FILE_CSV="${DIRECTORY_OUTPUTS}/mappedFeatures_${TEXT_REMOVAL_TYPE}_${SEQUENCE}_${STRAND_TEXT}.csv" 
					echo "Running qsub run_mfSequenceInBins.sh $ASYMM_TERRITORIES_LOCAL $SEQUENCE $SEQUENCE_INDEX_OF_MUTATED_BASE $OUTPUT_FILE_CSV $GENOME_REFERENCE_PATH $NBINS_ON_SIDES $CHROM_SIZE_FILE $STRAND $STRAND_TEXT..." >> $OUTPUT_FILE
					qsub run_mfSequenceInBins.sh $ASYMM_TERRITORIES_LOCAL $SEQUENCE $SEQUENCE_INDEX_OF_MUTATED_BASE $OUTPUT_FILE_CSV $GENOME_REFERENCE_PATH $NBINS_ON_SIDES $CHROM_SIZE_FILE $STRAND $STRAND_TEXT >> $OUTPUT_FILE
				done
			done
		done
	done
}

function computeFrequenciesInTrinucleotides_2 {	### Combines results from run_mfSequenceInBins.sh into a file with one number per territory (for each trinucl and strand a separate file), using $1 ($TEXT_HARADHVALA) and $2 ($TEXT_OutsideGenesAndBlacklisted)
	TEXT_TERRITORIES_SOURCE=$1 			# e.g., $TEXT_HARADHVALA
	TEXT_REMOVAL_TYPE=$2 				# e.g., $TEXT_OutsideGenesAndBlacklisted
	echo "Running computeFrequenciesInTrinucleotides_2 TEXT_TERRITORIES_SOURCE=${TEXT_TERRITORIES_SOURCE}, TEXT_REMOVAL_TYPE=${TEXT_REMOVAL_TYPE}..." >> $OUTPUT_FILE
	
	ASYMM_TERRITORIES_AllRegions="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/${TEXT_TERRITORIES_SOURCE}.${TEXT_AllRegions}.bed"; N_TERRITORIES=`cat $ASYMM_TERRITORIES_AllRegions | wc -l` # 154794
	echo "N_TERRITORIES=${N_TERRITORIES} (for Haradhvala_territories should be 154794, for Besnard_territories should be 1360724)\n" >> $OUTPUT_FILE
	
	ASYMM_TERRITORIES_LOCAL="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/${TEXT_TERRITORIES_SOURCE}.${TEXT_REMOVAL_TYPE}.bed"		
	DIR_OUTPUT="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/contextInTerritories_${TEXT_TERRITORIES_SOURCE}_${TEXT_REMOVAL_TYPE}"; [ -d $DIR_OUTPUT ] || mkdir $DIR_OUTPUT
	DIR_INPUT="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/contextInTerritories_${TEXT_TERRITORIES_SOURCE}_${TEXT_REMOVAL_TYPE}/splitTerritories"
	for STRAND_TEXT in "plus" "minus"; do
		for BASE1 in A C G T; do
			for BASE2 in A C G T; do
				for BASE3 in A C G T; do
					SEQUENCE="${BASE1}${BASE2}${BASE3}"
					FILE_SEQUENCE_OCCURENCES="${DIR_INPUT}/mappedFeatures_${TEXT_REMOVAL_TYPE}_${SEQUENCE}_${STRAND_TEXT}.csv"
					FILE_OUTPUT_N_BASES_TOTAL="${DIR_OUTPUT}/mappedFeatures_${TEXT_REMOVAL_TYPE}_${SEQUENCE}_${STRAND_TEXT}.csv"					
					cut -f 4 $ASYMM_TERRITORIES_LOCAL | paste - $FILE_SEQUENCE_OCCURENCES | awk -v varNTerritories=$N_TERRITORIES 'BEGIN{
						for (iTerr=1; iTerr<=varNTerritories; iTerr++) {
							N_BASES_TOTAL[iTerr]=0
						}
					}{
						iTerr=$1; nBases=$2;
						N_BASES_TOTAL[iTerr]+=nBases
					}END{
						for (iTerr=1; iTerr<=varNTerritories; iTerr++) {
							printf "%d\n", N_BASES_TOTAL[iTerr]
						}
					}' > $FILE_OUTPUT_N_BASES_TOTAL
					sh printFile.sh $FILE_OUTPUT_N_BASES_TOTAL >> $OUTPUT_FILE
				done
			done
		done
	done
}

function call_run_mfReplicationMatlab { ### Runs run_mfReplicationMatlab.sh for all CTs in $3, based on $1 ($TEXT_HARADHVALA) and $2 ($TEXT_OutsideGenesAndBlacklisted) --> saves to "../replication/${4}/"	, uses ${DIR_REPLICATION}, ${TEXT_AllRegions} 
	TEXT_TERRITORIES_SOURCE=$1 			# e.g., $TEXT_HARADHVALA
	TEXT_REMOVAL_TYPE=$2 				# e.g., $TEXT_OutsideGenesAndBlacklisted
	LIST_CT_FILE=$3						# e.g., data/Mutations/list_ICGC_new_cancerTypes.txt
	DIR_SAVE_NAME=$4 					# e.g., replPrepareTables1119
	echo "Running call_run_mfReplicationMatlab TEXT_TERRITORIES_SOURCE=${TEXT_TERRITORIES_SOURCE}, TEXT_REMOVAL_TYPE=${TEXT_REMOVAL_TYPE}, LIST_CT_FILE=${LIST_CT_FILE}, DIR_SAVE_NAME=${DIR_SAVE_NAME}..." >> $OUTPUT_FILE
	
	MAT_TERRITORIES_NAME_WITHPATH="../${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/table.${TEXT_TERRITORIES_SOURCE}.mat"
	CELLRESULT_FILENAME="../replication/${TEXT_TERRITORIES_SOURCE}/cellResult_background.${TEXT_TERRITORIES_SOURCE}.${TEXT_REMOVAL_TYPE}.mat"
	CURRENT_DIRECTORY="${DIR_REPLICATION}/${TEXT_TERRITORIES_SOURCE}/mut${TEXT_REMOVAL_TYPE}"; 		# should already exist from subtractMutationsInRemovedRegions
	CURRENT_PREFIX="mut${TEXT_REMOVAL_TYPE}_"	
	DIR_SAVE="../replication/${DIR_SAVE_NAME}/"					#replPrepareTables0813b replPrepareTables0816 replPrepareTables1119
	
	if [ -z "$LIST_CT_FILE" ]; then
		echo "Running ls ${CURRENT_DIRECTORY}/${CURRENT_PREFIX}*_context96_genex_withGenes.onlyWGS.bedlike* ..." >> $OUTPUT_FILE
		ls "${CURRENT_DIRECTORY}/${CURRENT_PREFIX}"*"_context96_genex_withGenes.onlyWGS.bedlike"* | 
		grep -v "ovSNV" |
		sed "s#${CURRENT_DIRECTORY}/${CURRENT_PREFIX}##" | 
		sed 's#_context96_genex_withGenes.onlyWGS.bedlike.*##' | sort -u | while read CANCER_TYPE; do
			echo $CANCER_TYPE
			FILE_NAME="${CURRENT_DIRECTORY}/${CURRENT_PREFIX}${CANCER_TYPE}_context96_genex_withGenes.onlyWGS.bedlike"
			if [ -f "$FILE_NAME" ]; then
				echo "Renaming ${FILE_NAME} to ${FILE_NAME}.txt\n"
				mv "${FILE_NAME}" "${FILE_NAME}.txt"
			fi
			FILE_NAME="${FILE_NAME}.txt"
			if [ -s $FILE_NAME ]; then
				echo "Running qsub run_mfReplicationMatlab.sh $CANCER_TYPE ../$FILE_NAME $DIR_SAVE $MAT_TERRITORIES_NAME_WITHPATH $CELLRESULT_FILENAME" >> $OUTPUT_FILE
				qsub run_mfReplicationMatlab.sh $CANCER_TYPE "../${FILE_NAME}" $DIR_SAVE $MAT_TERRITORIES_NAME_WITHPATH $CELLRESULT_FILENAME >> $OUTPUT_FILE
			fi
		done
	else
		echo "while read CANCER_TYPE; do (...qsub run_mfReplicationMatlab.sh...) done < $LIST_CT_FILE" >> $OUTPUT_FILE
		while read CANCER_TYPE; do
			echo $CANCER_TYPE
			FILE_NAME="${CURRENT_DIRECTORY}/${CURRENT_PREFIX}${CANCER_TYPE}_context96_genex_withGenes.onlyWGS.bedlike"
			if [ -f "$FILE_NAME" ]; then
				echo "Renaming ${FILE_NAME} to ${FILE_NAME}.txt\n"
				mv "${FILE_NAME}" "${FILE_NAME}.txt"
			fi
			FILE_NAME="${FILE_NAME}.txt"
			if [ -s $FILE_NAME ]; then
				echo "Running qsub run_mfReplicationMatlab.sh $CANCER_TYPE ../$FILE_NAME $DIR_SAVE $MAT_TERRITORIES_NAME_WITHPATH $CELLRESULT_FILENAME" >> $OUTPUT_FILE
				qsub run_mfReplicationMatlab.sh $CANCER_TYPE "../${FILE_NAME}" $DIR_SAVE $MAT_TERRITORIES_NAME_WITHPATH $CELLRESULT_FILENAME >> $OUTPUT_FILE
			fi
		done < "$LIST_CT_FILE"
	fi
}


############################################################################################
##############      For each TYPE of ORI, the following steps were run       ###############
##############      Some matlab parts were run on a different machine,       ###############
##############      the relevant steps needed for copying files are          ###############
##############      noted below.                                             ###############
############################################################################################

#### CURRENT PARAMETERS ####--------------------------------------------------------------
CURRENT_LIST_CT_FILE="data/Mutations/list_smallMemory_cancerTypes.txt"  #list_largeMemory_cancerTypes list_smallMemory_cancerTypes list_allUsed_cancerTypes
# CURRENT_EXPNAME="Besnard_territories/replPrepareTables1129" 
# CURRENT_EXPNAME="Haradhvala_territories/replPrepareTables_${TEXT_OutsideAllGenesAndBlacklisted}" #replPrepareTables70116 replPrepareTables1125test2 replPrepareTables1125 replPrepareTables1125all  replPrepareTables70114
# CURRENT_EXPNAME="Besnard1k_territories/replPrepareTables1216" 
# CURRENT_EXPNAME="BesnardB20k_territories/replPrepareTables70101" #replPrepareTables70101
CURRENT_TEXT=$TEXT_RandomORI # #$TEXT_BESNARD TEXT_BESNARD1k TEXT_BESNARDB20k TEXT_HARADHVALA 

#### GENERAL PREPARATIONS ####--------------------------------------------------------------
# download_allGenes
# prepare_blacklist_5_allGenes_and_lowMap
# prepare_blacklist_1
# prepare_blacklist_2
# prepare_blacklist_3
# prepare_blacklist_4
# listAllCTs data/Mutations/list_all_cancerTypes.txt
# prepare_replTerritories_Haradhvala

#### TERRITORIES PREPARATIONS ####--------------------------------------------------------------
# prepare_replTerritories_subtractRemoved $REMOVED_AGALM_REGIONS_BED $CURRENT_TEXT $TEXT_OutsideAllGenesAndBlacklisted
# prepare_replTerritories_subtractRemoved $REMOVED_GALM_REGIONS_BED $CURRENT_TEXT $TEXT_OutsideGenesAndBlacklisted
# prepare_replTerritories_subtractRemoved $REMOVED_BLACKLISTED_ONLY_BED $CURRENT_TEXT $TEXT_OutsideBlacklisted
# prepare_replTerritories_subtractRemoved $REMOVED_GENES_ONLY_BED $CURRENT_TEXT $TEXT_OutsideGenes
### qsub ###
# computeFrequenciesInTrinucleotides_1 $CURRENT_TEXT $TEXT_OutsideAllGenesAndBlacklisted
# computeFrequenciesInTrinucleotides_1 $CURRENT_TEXT $TEXT_OutsideGenesAndBlacklisted
# computeFrequenciesInTrinucleotides_1 $CURRENT_TEXT $TEXT_OutsideBlacklisted
# computeFrequenciesInTrinucleotides_1 $CURRENT_TEXT $TEXT_OutsideGenes
# computeFrequenciesInTrinucleotides_1 $CURRENT_TEXT $TEXT_AllRegions
### ---- ###
# computeFrequenciesInTrinucleotides_2 $CURRENT_TEXT $TEXT_OutsideAllGenesAndBlacklisted
# computeFrequenciesInTrinucleotides_2 $CURRENT_TEXT $TEXT_OutsideGenesAndBlacklisted
# computeFrequenciesInTrinucleotides_2 $CURRENT_TEXT $TEXT_OutsideBlacklisted
# computeFrequenciesInTrinucleotides_2 $CURRENT_TEXT $TEXT_OutsideGenes
# computeFrequenciesInTrinucleotides_2 $CURRENT_TEXT $TEXT_AllRegions

#### MATLAB TABLES COMPUTATION ####--------------------------------------------------------------
## Copy directory like replication/Haradhvala_territories/contextInTerritories_Haradhvala_territories_AllRegions into the matlab directory data/Haradhvala_territories/
## Compute the territories table with script_replPrepareTerritories.m 
## Copy the computed files like save/table.Haradhvala_territories.mat and save/cellResult_background.Haradhvala_territories.OutsideGenesAndBlacklisted.mat into replication/Haradhvala_territories/

#### MUTATIONS COMPUTATION ####--------------------------------------------------------------
# annotateMutationsWithTerritories $CURRENT_LIST_CT_FILE $CURRENT_TEXT
# subtractMutationsInRemovedRegions $REMOVED_AGALM_REGIONS_BED $CURRENT_TEXT $TEXT_OutsideAllGenesAndBlacklisted $CURRENT_LIST_CT_FILE
# subtractMutationsInRemovedRegions $REMOVED_GALM_REGIONS_BED $CURRENT_TEXT $TEXT_OutsideGenesAndBlacklisted $CURRENT_LIST_CT_FILE
# subtractMutationsInRemovedRegions $REMOVED_BLACKLISTED_ONLY_BED $CURRENT_TEXT $TEXT_OutsideBlacklisted $CURRENT_LIST_CT_FILE
# subtractMutationsInRemovedRegions $REMOVED_GENES_ONLY_BED $CURRENT_TEXT $TEXT_OutsideGenes $CURRENT_LIST_CT_FILE
### qsub ###
# CURRENT_EXPNAME="${CURRENT_TEXT}/replPrepareTables_${TEXT_OutsideAllGenesAndBlacklisted}"; call_run_mfReplicationMatlab $CURRENT_TEXT $TEXT_OutsideAllGenesAndBlacklisted $CURRENT_LIST_CT_FILE $CURRENT_EXPNAME
CURRENT_EXPNAME="${CURRENT_TEXT}/replPrepareTables_${TEXT_OutsideGenesAndBlacklisted}"; call_run_mfReplicationMatlab $CURRENT_TEXT $TEXT_OutsideGenesAndBlacklisted $CURRENT_LIST_CT_FILE $CURRENT_EXPNAME
# CURRENT_EXPNAME="${CURRENT_TEXT}/replPrepareTables_${TEXT_OutsideBlacklisted}";  call_run_mfReplicationMatlab $CURRENT_TEXT $TEXT_OutsideBlacklisted $CURRENT_LIST_CT_FILE $CURRENT_EXPNAME
# CURRENT_EXPNAME="${CURRENT_TEXT}/replPrepareTables_${TEXT_OutsideGenes}";  call_run_mfReplicationMatlab $CURRENT_TEXT $TEXT_OutsideGenes $CURRENT_LIST_CT_FILE $CURRENT_EXPNAME
# CURRENT_EXPNAME="${CURRENT_TEXT}/replPrepareTables_${TEXT_AllRegions}";  call_run_mfReplicationMatlab $CURRENT_TEXT $TEXT_AllRegions $CURRENT_LIST_CT_FILE $CURRENT_EXPNAME
### ---- ###

echo "Script all_mfReplication.sh FINISHED." >> $OUTPUT_FILE
cat $OUTPUT_FILE



