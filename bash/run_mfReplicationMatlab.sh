#!/bin/bash -l

##########################################
# SGE SUBMISSION SCRIPT run_mfReplicationMatlab.sh #
##########################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job?
#
#$ -N mfReplicationMatlab

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m eas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=700:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes. --> 140G, 450G for the four biggest 200, normally 80
#$ -l h_vmem=80G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp 1

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
# module load apps/bismark/0.12.5/noarch 
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
# module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch
# module load apps/sra/2.3.5-2/gcc-4.4.7 
# module load apps/perl/5.18.0/gcc-4.4.7
module load apps/matlab/R2014a/bin 


# Test with: good, both versions for AML give the same result (fcn_replOneCT_step2matrix, fcn_replOneCT_step2). AML Matrix took 11.5min, whereas Vector took 45min. ICGC_ESAD_UK should take 6.16 min.
# qsub run_mfReplicationMatlab.sh AML ../replication/Besnard_territories/mutOutsideGenesAndBlacklisted/mutOutsideGenesAndBlacklisted_AML_context96_genex_withGenes.onlyWGS.bedlike.txt ../replication/replPrepareTables1129testMatrix/ ../replication/Besnard_territories/table.Besnard_territories.mat ../replication/Besnard_territories/cellResult_background.Besnard_territories.OutsideGenesAndBlacklisted.mat
# qsub run_mfReplicationMatlab.sh AML ../replication/Besnard_territories/mutOutsideGenesAndBlacklisted/mutOutsideGenesAndBlacklisted_AML_context96_genex_withGenes.onlyWGS.bedlike.txt ../replication/replPrepareTables1129testVector/ ../replication/Besnard_territories/table.Besnard_territories.mat ../replication/Besnard_territories/cellResult_background.Besnard_territories.OutsideGenesAndBlacklisted.mat
# qsub run_mfReplicationMatlab.sh ICGC_ESAD_UK ../replication/Besnard_territories/mutOutsideGenesAndBlacklisted/mutOutsideGenesAndBlacklisted_ICGC_ESAD_UK_context96_genex_withGenes.onlyWGS.bedlike.txt ../replication/replPrepareTables1129/ ../replication/Besnard_territories/table.Besnard_territories.mat ../replication/Besnard_territories/cellResult_background.Besnard_territories.OutsideGenesAndBlacklisted.mat
# qsub run_mfReplicationMatlab.sh ICGC_BRCA_EU ../replication/Besnard_territories/mutOutsideGenesAndBlacklisted/mutOutsideGenesAndBlacklisted_ICGC_BRCA_EU_context96_genex_withGenes.onlyWGS.bedlike.txt ../replication/replPrepareTables1129/ ../replication/Besnard_territories/table.Besnard_territories.mat ../replication/Besnard_territories/cellResult_background.Besnard_territories.OutsideGenesAndBlacklisted.mat

###############################
# APPLICATION LAUNCH COMMANDS
###############################

CANCER_TYPE=$1
FILE_NAME=$2
DIR_SAVE=$3 #DIR_SAVE="../replication/replPrepareTables_withoutBlacklist0814a/"	#replPrepareTables0813b
TERRITORIES_FILENAME=$4 	#"../replication/tableTerritories.mat"
CELLRESULT_FILENAME=$5 		#"../replication/cellResult_background.mat"


if [ -z "$CANCER_TYPE" ] || [ -z "$FILE_NAME" ] || [ -z "$DIR_SAVE" ] || [ -z "$TERRITORIES_FILENAME" ] || [ -z "$CELLRESULT_FILENAME" ]; then
    echo "Not enough parameters." 1>&2
    exit 1
fi

NOW="$(date +'%d/%m/%Y %H:%M:%S')"; SECONDS=0
echo "${NOW}| Script run_mfReplicationMatlab.sh $CANCER_TYPE $FILE_NAME $DIR_SAVE $TERRITORIES_FILENAME $CELLRESULT_FILENAME..."

cd matlabScripts

# FILE_NAME="'../../commonData/features/hg19/mutationsPerSampleWithReplicationAllContexts/replAnnotatedWithTerritories_mut_${CANCER_TYPE}_context96_genex_withGenes.onlyWGS.bedlike'"


echo "Running matlab -r fcn_replOneCT_step1('${CANCER_TYPE}', '${FILE_NAME}', '${TERRITORIES_FILENAME}', '${DIR_SAVE}'); quit" 
matlab -r "fcn_replOneCT_step1('${CANCER_TYPE}', '${FILE_NAME}', '${TERRITORIES_FILENAME}', '${DIR_SAVE}'); quit"

echo "Running matlab -r fcn_replOneCT_step2matrix('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit" 
matlab -r "fcn_replOneCT_step2matrix('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit"

# echo "Running matlab -r fcn_replOneCT_step2matrixIncludingTranscribed('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit" 
# matlab -r "fcn_replOneCT_step2matrixIncludingTranscribed('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit"

NOW="$(date +'%d/%m/%Y %H:%M:%S')"
DURATION=$SECONDS
echo "$(($DURATION / 60)) minutes and $(($DURATION % 60)) seconds elapsed."
echo "${NOW}| Script run_mfReplicationMatlab.sh FINISHED."
echo ""

######## OLD #############

# echo "Running matlab -r fcn_replOneCT_step3moreComplex('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit" 
# matlab -r "fcn_replOneCT_step3moreComplex('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit"

# echo "Running matlab -r fcn_replOneCT_step4NOS('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit" 
# matlab -r "fcn_replOneCT_step4NOS('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit"

# echo "Running matlab -r fcn_replOneCT_step2('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit" 
# matlab -r "fcn_replOneCT_step2('${CANCER_TYPE}', '${DIR_SAVE}', '${TERRITORIES_FILENAME}', '${CELLRESULT_FILENAME}'); quit"


