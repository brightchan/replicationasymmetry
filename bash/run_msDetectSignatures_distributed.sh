#!/bin/bash -l

################################################################
# SGE SUBMISSION SCRIPT run_msDetectSignatures_distributed.sh #
################################################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job?
#
#$ -N msDetectSignatures_distributed

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m as

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=1500:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes. 
#$ -l h_vmem=15G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows.
#$ -pe smp 1

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/imb
# endsnippet:script:environment_modules
# module load apps/bismark/0.12.5/noarch 
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
# module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch
# module load apps/sra/2.3.5-2/gcc-4.4.7 
# module load apps/perl/5.18.0/gcc-4.4.7
module load apps/matlab/R2014a/bin 
# module load apps/matlab/R2015b/bin



###############################
# APPLICATION LAUNCH COMMANDS
###############################


totalIterationsPerCore=$1
numberProcessesToExtract=$2
inputFileName=$3
nRuns=$4
runDataFilePrefix=$5
isStrandSpecific=$6
# outputFileName=$7
# totalCores=2

if [ -z "$totalIterationsPerCore" ] ||  [ -z "$numberProcessesToExtract" ] || [ -z "$inputFileName" ] || [ -z "$nRuns" ] || [ -z "$runDataFilePrefix" ] || [ -z "$isStrandSpecific" ]; then
    echo "Not enough parameters." 1>&2
    exit 1
fi

NOW="$(date +'%d/%m/%Y %H:%M:%S')"; SECONDS=0
echo "${NOW}| Script run_msDetectSignatures_distributed.sh $totalIterationsPerCore $numberProcessesToExtract $inputFileName $nRuns $runDataFilePrefix $isStrandSpecific $outputFileName..."

echo "Running matlab -r decipherMutationalProcesses_distributed($totalIterationsPerCore, $numberProcessesToExtract, $inputFileName, $nRuns, $runDataFilePrefix, $isStrandSpecific); quit"
matlab -r "decipherMutationalProcesses_distributed($totalIterationsPerCore, $numberProcessesToExtract, $inputFileName, $nRuns, $runDataFilePrefix, $isStrandSpecific); quit"




NOW="$(date +'%d/%m/%Y %H:%M:%S')"
DURATION=$SECONDS
echo "$(($DURATION / 60)) minutes and $(($DURATION % 60)) seconds elapsed."
echo "${NOW}| Script run_msDetectSignatures_distributed.sh FINISHED."
