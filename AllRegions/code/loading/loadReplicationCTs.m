function [structAllSamples, tableAllSamples, nTotalSamples, tableCancerTypes] = loadReplicationCTs(alwaysCompute, pathAndNameReplData, dataFields, dirDataCT)

pathAndName = [pathAndNameReplData, '_ReplicationCTs'];

%%
if (~alwaysCompute && exist([pathAndName, '.mat'],'file'))
    fprintf('Loading from %s...\n', pathAndName);
    load(pathAndName, 'structAllSamples', 'tableAllSamples', 'nTotalSamples', 'tableCancerTypes');
else
    %% List of cancer types and their filenames
    tableCancerTypes = readtable('data/cancerTypes.xlsx'); nCT = size(tableCancerTypes, 1); %cancerTypes = tableCancerTypes.CancerType;
    fprintf('Processing %d cancer types.\n', nCT);
    maxPattern = 96;
    %%
    nTotalSamples = 0;
    tableAllSamples = table();
    for feature = dataFields.features
        for simpleType = dataFields.simpleTypes
            structAllSamples.(simpleType{1}).(feature{1}) = sparse(NaN*ones(0, maxPattern));
        end
        for groupedType = dataFields.groupedTypes
            structAllSamples.(groupedType{1}).(feature{1}) = sparse(NaN*ones(0, 1));
        end
        for complexType = dataFields.complexTypes
            if (~isfield(structAllSamples, complexType{1}))
                structAllSamples.(complexType{1}) = cell(dataFields.nValues.(complexType{1}), 1);
            end
            for iValue = 1:dataFields.nValues.(complexType{1})
                structAllSamples.(complexType{1}){iValue}.(feature{1}) = sparse(NaN*ones(0, maxPattern));
            end
        end
    end
    for iCT = 1:nCT
        fprintf('Cancer type %s... ', tableCancerTypes.CancerType{iCT});
        cancerType = tableCancerTypes.CancerType{iCT};
        if (~strcmp(tableCancerTypes.SourceCancerType{iCT}, '-'))
            subselectCancerType(dirDataCT, tableCancerTypes.SourceCancerType{iCT}, cancerType, dataFields)
        end
        filename_structSamples = [dirDataCT, 'structSamples_', cancerType, '.mat'];
        if (~exist(filename_structSamples, 'file'))
            warning('WARNING: file %s does not exist.\n', filename_structSamples);
        else
            structSamples = [];     load(filename_structSamples, 'structSamples');
            tmp = sum(structSamples.LeadLagg.mutType1 + structSamples.LeadLagg.mutType2, 2);
            isMutated = (tmp > 0 & ~isnan(tmp));
            nSamples = sum(isMutated); fprintf('%d samples (from %d samples originally).\n', nSamples, size(structSamples.tableSamples, 1));
            nTotalSamples = nTotalSamples + nSamples;
            tableSamples = structSamples.tableSamples(isMutated,:);
            tableSamples.iCancerType = iCT*ones(nSamples, 1);
            tmpTissue = cell(nSamples, 1); tmpTissue(:) = {tableCancerTypes.Tissue{iCT}};
            tableSamples.Tissue = tmpTissue;
            tableAllSamples = [tableAllSamples; tableSamples];
            
            for feature = dataFields.features
                for simpleType = dataFields.simpleTypes
                    if (isfield(structSamples, simpleType))
                        structAllSamples.(simpleType{1}).(feature{1}) = [structAllSamples.(simpleType{1}).(feature{1}); ...
                            sparse(structSamples.(simpleType{1}).(feature{1})(isMutated,:))];
                    end
                end
                for groupedType = dataFields.groupedTypes
                    if (isfield(structSamples, groupedType))
                        structAllSamples.(groupedType{1}).(feature{1}) = [structAllSamples.(groupedType{1}).(feature{1}); ...
                            sparse(structSamples.(groupedType{1}).(feature{1})(isMutated,:))];
                    end
                end
            end
            for complexType = dataFields.complexTypes
                if (isfield(structSamples, complexType))
                    for iValue = 1:dataFields.nValues.(complexType{1})
                        for feature = fieldnames(structSamples.(complexType{1}){iValue})'
                            if (~isfield(structAllSamples.(complexType{1}){iValue}, feature{1}))
                                structAllSamples.(complexType{1}){iValue}.(feature{1}) = NaN*ones(0, maxPattern);
                            end
                            structAllSamples.(complexType{1}){iValue}.(feature{1}) = [structAllSamples.(complexType{1}){iValue}.(feature{1}); ...
                                sparse(structSamples.(complexType{1}){iValue}.(feature{1})(isMutated,:))];
                        end
                    end
                end
            end
        end
    end
    
    for simpleType = dataFields.simpleTypes
        structAllSamples.(simpleType{1}).asymmetryNegLogPValue = -log10(structAllSamples.(simpleType{1}).asymmetryPValue);
    end
    for groupedType = dataFields.groupedTypes
        structAllSamples.(groupedType{1}).asymmetryNegLogPValue = -log10(structAllSamples.(groupedType{1}).asymmetryPValue);
    end
    for complexType = dataFields.complexTypes
        for iValue = 1:dataFields.nValues.(complexType{1})
            structAllSamples.(complexType{1}){iValue}.asymmetryNegLogPValue = -log10(structAllSamples.(complexType{1}){iValue}.asymmetryPValue);
        end
    end
    fprintf('In total %d samples and %d cancer types... saving to %s...\n', nTotalSamples, nCT, pathAndName);
    % SAVE
    save(pathAndName, 'structAllSamples', 'tableAllSamples', 'nTotalSamples', 'tableCancerTypes', '-v7.3');
end