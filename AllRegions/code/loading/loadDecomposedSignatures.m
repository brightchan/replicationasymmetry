function decomposedSignatures = loadDecomposedSignatures(alwaysCompute, savePathBasic, tissueNames, evaluationOfDecomposedSignatures, territoriesSource)
% This script loads decomposed (extracted) mutational signatures from all cancer types, clusters them (together with the "official" COSMIC ones), 
% computes representants of the clusters --> final set of signatures, and annotates the final signatures with dominant direction (leading/lagging).
pathAndName = [savePathBasic, territoriesSource, '/decomposedSignatures_', territoriesSource, 'without_N1_N2'];

%%
if (~alwaysCompute && exist([pathAndName, '.mat'],'file'))
    load(pathAndName, 'decomposedSignatures');
else
    tic;
    plotFigures = false;
    nTissues = length(tissueNames);
    nRuns = evaluationOfDecomposedSignatures.nRuns;
    strandSpecTexts = evaluationOfDecomposedSignatures.strandSpecTexts;
    imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_decomposedSignatures_without_N1_N2/']; createDir(imagesPath);
    %%
    for iStrandSpec = 1:2
        strandSpecText = strandSpecTexts{iStrandSpec};
        totalNSigantures.(strandSpecText) = sum(evaluationOfDecomposedSignatures.bestNSignatures.(strandSpecText));
        matrixAllSignatures.(strandSpecText) = NaN*ones(evaluationOfDecomposedSignatures.nPatterns.(strandSpecText), totalNSigantures.(strandSpecText));
        exposuresAllSignatures.(strandSpecText) = cell(nTissues, 1);
        tissueName = cell(totalNSigantures.(strandSpecText), 1);
        iTissue = NaN*ones(totalNSigantures.(strandSpecText), 1);
        ignore = NaN*ones(totalNSigantures.(strandSpecText), 1);
        tableAllSignatures.(strandSpecText) = table(tissueName, iTissue, ignore);
        clear tissueName iTissue
        iSignature = 0;
        for iTissue = 1:nTissues
            tissueName = tissueNames{iTissue};
            nSignatures = evaluationOfDecomposedSignatures.bestNSignatures.(strandSpecText)(iTissue);
            fileNamePrefix = ['save/', territoriesSource, '/decomposedSignaturesSmaller/res_', tissueName, '_', num2str(nSignatures), 'k_', strandSpecText, '_', num2str(nRuns), 'runs.mat'];
            try
                fprintf('Loading %s...\n', fileNamePrefix);
                data = load(fileNamePrefix);
                localNSigantures = size(data.processes, 2);
                jSignature = iSignature + localNSigantures;
                exposuresAllSignatures.(strandSpecText){iTissue} = data.exposures;
                matrixAllSignatures.(strandSpecText)(:, (iSignature+1):jSignature) = data.processes.*repmat(evaluationOfDecomposedSignatures.normalisingVector.(strandSpecText), 1, nSignatures);
                tableAllSignatures.(strandSpecText).tissueName((iSignature+1):jSignature) = {tissueName};
                tableAllSignatures.(strandSpecText).iTissue((iSignature+1):jSignature) = iTissue;
                ignoreSignatures = false(nSignatures, 1);
                localMeanExposure = mean(sum(data.exposures)); %i.e., mean total exposure per sample (originally mean(exposures(:)))
                localISample = max(1, round(0.05*size(data.exposures, 2))); %95th percentile of samples
                for iLocalSignature = 1:nSignatures
                    localSortedExposures = sort(data.exposures(iLocalSignature,:), 'descend');
                    if (localSortedExposures(localISample)/localMeanExposure < 0.2)
                        ignoreSignatures(iLocalSignature) = true;
                    end
                end
                tableAllSignatures.(strandSpecText).ignore((iSignature+1):jSignature) = ignoreSignatures;
                subtypes.(strandSpecText) = data.input.subtypes;
                types.(strandSpecText) = data.input.types;
                iSignature = jSignature;
            catch
                fprintf('WARNING: problem with file %s.\n', fileNamePrefix);
            end
        end
        matrixAllSignatures.(strandSpecText) = matrixAllSignatures.(strandSpecText)./repmat(sum(matrixAllSignatures.(strandSpecText)), evaluationOfDecomposedSignatures.nPatterns.(strandSpecText), 1);
    end;
    %%
    
    for iStrandSpec = 1:2
        strandSpecText = strandSpecTexts{iStrandSpec};
        for iTissue = 1:nTissues
            tissueName = tissueNames{iTissue};
            nSignatures = evaluationOfDecomposedSignatures.bestNSignatures.(strandSpecText)(iTissue);
            isSignatureThisTissue = (tableAllSignatures.(strandSpecText).iTissue == iTissue);
            processes = matrixAllSignatures.(strandSpecText)(:, isSignatureThisTissue);
            exposures = exposuresAllSignatures.(strandSpecText){iTissue};
            localIgnore = tableAllSignatures.(strandSpecText).ignore(isSignatureThisTissue);
            Y = pdist(exposures');
            Z = linkage(Y);
            orderSamples = optimalleaforder(Z,Y);
            
            if (plotFigures)
                fig = createMaximisedFigure(3);  nR = nSignatures; nC = 1; colormap(colourLinspace([1,1,1], [.1,.2,.5], 50));
                axes('Position', [0.01, 0.1, 0.09, 0.83]); imagesc(exposures(:, orderSamples)); set(gca, 'XTick', [], 'YTick', []); title('Exposures'); xlabel('Samples'); %colorbar;
                plotTypes = false;
                localMeanExposure = mean(sum(exposures)); %i.e., mean total exposure per sample (originally mean(exposures(:)))
                localISample = max(1, round(0.05*size(exposures, 2)));
                for iSignature = 1:nSignatures
                    if (iSignature == nSignatures)
                        plotTypes = true;
                    end
                    subplot(nR,nC,iSignature);
                    plotSignatureDebugging(processes(:,iSignature), subtypes.(strandSpecText), types.(strandSpecText), plotTypes); ylabel(sprintf('signature %d', iSignature));
                    localSortedExposures = sort(exposures(iSignature,:), 'descend');
                    if (localIgnore(iSignature))
                        maxX = 96.5; maxY = max(get(gca, 'YLim')); %max(get(gca, 'XLim'))
                        plot([0.5,0.5; maxX,maxX], [maxY,0; 0,maxY], 'LineWidth', 2, 'Color', 0.5*[1,1,1]);
                    end
                    title(sprintf('%.0f, %.0f, %d, {\\color[rgb]{0 .5 .5}%.2f}', localSortedExposures(localISample), localMeanExposure, localISample, localSortedExposures(localISample)/localMeanExposure), 'HorizontalAlignment', 'right', 'Units', 'normalized', 'Position', [1 1])
                end
                suptitle(strrep(tissueName, '_', '\_'));
                mySaveAs(fig, imagesPath, ['signatures_', tissueName, '_', strandSpecText]);
            end
        end
    end
    %%
    % backgroundTrinuclFrequency = readtable('data/backgroundTrinuclFrequency_isLeftOrRightNotTx.txt'); %backgroundTrinuclFrequency
    for iStrandSpec = 1:2
        strandSpecText = strandSpecTexts{iStrandSpec};
        %allNormalisedSignatures.(strandSpecText) = NaN*allSignatures.(strandSpecText);
        allDenormalisedSignatures.(strandSpecText) = NaN*matrixAllSignatures.(strandSpecText);
        allSignatureNames.(strandSpecText) = cell(size(matrixAllSignatures.(strandSpecText), 2), 1);
        allSignatureIndexTissue.(strandSpecText) = NaN*ones(size(matrixAllSignatures.(strandSpecText), 2), 1);
        %allSignatureCTs.(strandSpecText) = cell(size(matrixAllSignatures.(strandSpecText), 2), 1);
        for iSignature = 1:size(matrixAllSignatures.(strandSpecText), 2)
            if (size(matrixAllSignatures.(strandSpecText), 1) == 96)
                %allNormalisedSignatures.(strandSpecText)(:, iSignature) = allSignatures.(strandSpecText)(:, iSignature)./backgroundTrinuclFrequency.leadingPlusLagging;
                allDenormalisedSignatures.(strandSpecText)(:, iSignature) = matrixAllSignatures.(strandSpecText)(:, iSignature);
            else
                %allNormalisedSignatures.(strandSpecText)(:, iSignature) = allSignatures.(strandSpecText)(:, iSignature)./[backgroundTrinuclFrequency.leading; backgroundTrinuclFrequency.lagging];
                allDenormalisedSignatures.(strandSpecText)(:, iSignature) = matrixAllSignatures.(strandSpecText)(:, iSignature);
            end
            %allNormalisedSignatures.(strandSpecText)(:, iSignature) = allNormalisedSignatures.(strandSpecText)(:, iSignature)/sum(allNormalisedSignatures.(strandSpecText)(:, iSignature));
            allDenormalisedSignatures.(strandSpecText)(:, iSignature) = allDenormalisedSignatures.(strandSpecText)(:, iSignature)/sum(allDenormalisedSignatures.(strandSpecText)(:, iSignature));
            %allSignatureNames.(strandSpecText){iSignature} = sprintf('s%d %s', iSignature, tableAllSignatures.(strandSpecText).tissueName{iSignature});
            allSignatureNames.(strandSpecText){iSignature} = tableAllSignatures.(strandSpecText).tissueName{iSignature};
            allSignatureIndexTissue.(strandSpecText)(iSignature) = tableAllSignatures.(strandSpecText).iTissue(iSignature);
        end
        allDenormalisedSignatures.(strandSpecText) = allDenormalisedSignatures.(strandSpecText)(:, ~tableAllSignatures.(strandSpecText).ignore);
        allSignatureNames.(strandSpecText) = allSignatureNames.(strandSpecText)(~tableAllSignatures.(strandSpecText).ignore);
        allSignatureIndexTissue.(strandSpecText) = allSignatureIndexTissue.(strandSpecText)(~tableAllSignatures.(strandSpecText).ignore);
        if (size(matrixAllSignatures.(strandSpecText), 1) == 96)
            %allNormalisedSignatures.(strandSpecText) = [allNormalisedSignatures.(strandSpecText), matrixSignaturesOfficialNormalised];
            allDenormalisedSignatures.(strandSpecText) = [allDenormalisedSignatures.(strandSpecText), evaluationOfDecomposedSignatures.matrixSignaturesOfficial];
        else
            %allNormalisedSignatures.(strandSpecText) = [allNormalisedSignatures.(strandSpecText), [matrixSignaturesOfficialNormalised; matrixSignaturesOfficialNormalised]];
            allDenormalisedSignatures.(strandSpecText) = [allDenormalisedSignatures.(strandSpecText), [evaluationOfDecomposedSignatures.matrixSignaturesOfficial; evaluationOfDecomposedSignatures.matrixSignaturesOfficial]/2];
        end
        allSignatureIsOfficial.(strandSpecText) = [false(length(allSignatureNames.(strandSpecText)), 1); true(length(evaluationOfDecomposedSignatures.signaturesOfficialNames), 1)];
        allSignatureIndexOfficial.(strandSpecText) = [NaN*ones(length(allSignatureNames.(strandSpecText)), 1); (1:length(evaluationOfDecomposedSignatures.signaturesOfficialNames))'];
        allSignatureNames.(strandSpecText) = [allSignatureNames.(strandSpecText); evaluationOfDecomposedSignatures.signaturesOfficialNames'];
        allSignatureIndexTissue.(strandSpecText) = [allSignatureIndexTissue.(strandSpecText); NaN*ones(length(evaluationOfDecomposedSignatures.signaturesOfficialNames), 1)];
        allSignatureCTs.(strandSpecText) = [tableAllSignatures.(strandSpecText).tissueName(~tableAllSignatures.(strandSpecText).ignore); evaluationOfDecomposedSignatures.signaturesOfficialNames']; %tmpOfficial = cell(nSignaturesOfficial, 1); tmpOfficial(:) = {'official'};
    end
    %%
    %     iStrandSpec = 1;
    %     strandSpecText = strandSpecTexts{iStrandSpec};
    %     distanceName = 'cosine';
    %     nTotalSignatures = 30;
    %     Y = pdist(matrixAllSignatures.(strandSpecText)', distanceName);
    %     Z = linkage(Y, 'complete');
    %     tmpN = 55; %decomposedSignatures.totalNSigantures.(strandSpecText)
    %     finalClusters = NaN*ones(totalNSigantures.(strandSpecText), tmpN);
    %     for nTotalSignatures = 1:tmpN
    %         finalClusters(:, nTotalSignatures) = cluster(Z,'maxclust', nTotalSignatures);
    %     end
    %     % eva = evalclusters(matrixAllSignatures.(strandSpecText)',finalClusters,'CalinskiHarabasz')
    %     % eva = evalclusters(matrixAllSignatures.(strandSpecText)',finalClusters,'DaviesBouldin')
    %     % eva = evalclusters(matrixAllSignatures.(strandSpecText)',finalClusters,'gap', 'Distance', distanceName)
    %     eva = evalclusters(matrixAllSignatures.(strandSpecText)',finalClusters,'silhouette', 'Distance', distanceName)
    %%
    for iStrandSpec = 2%1:2
        strandSpecText = strandSpecTexts{iStrandSpec};
        distanceName = 'cosine';
        nTotalSignatures = 27;
        Y = pdist(allDenormalisedSignatures.(strandSpecText)', distanceName);
        Z = linkage(Y, 'complete');
        finalClusters = cluster(Z,'maxclust', nTotalSignatures);
        cmapClusters = (1+hsv(nTotalSignatures))/2; %    colourClusters = cmapClusters(finalClusters, :);
        %
        if (plotFigures)
            fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',2*[0 0 40 20],'units','normalized','outerposition',[0 0 1 1]);
            subplot(2,1,1); [H,T,outperm] = dendrogram(Z, 0); set(gca, 'XTick', []); set(H, 'Color', [0,0,0]); %, 'Labels', []); % , 'ColorThreshold',  0.5*max(Z(:,3))
            xValues = 1:length(finalClusters); yValues1 = -0.04*ones(1, length(finalClusters)); yValues2 = -0.07*ones(1, length(finalClusters));
            for iCluster = 1:nTotalSignatures
                isSignatureThisCluster = (finalClusters(outperm) == iCluster);
                text(xValues(isSignatureThisCluster), yValues1(isSignatureThisCluster), cellstr(num2str(finalClusters(outperm(isSignatureThisCluster)), '%d')), 'HorizontalAlignment', 'center', 'FontWeight', 'bold', 'Background', cmapClusters(iCluster, :), 'Margin', 2);
                text(xValues(isSignatureThisCluster), yValues2(isSignatureThisCluster), allSignatureNames.(strandSpecText)(outperm(isSignatureThisCluster)), 'HorizontalAlignment', 'right', 'Rotation', 90, 'interpret', 'none', 'FontSize', 9, 'Background', cmapClusters(iCluster, :), 'Margin', 2);
            end
            subplot(2,1,2); imagesc(allDenormalisedSignatures.(strandSpecText)(:, outperm));
            suptitle(sprintf('Denormalised %s', strandSpecText));
            mySaveAs(fig, imagesPath, ['finalSignaturesMyClusteringTreeFull_complete_denormalised_', strandSpecText]);
        end
        % Split cluster 16 into S15,MSI_19 vs. S6,MSI_19 vs. S14,POLE_14
        iCluster = 16; isThisCluster = (finalClusters == iCluster); %         sum(isThisCluster)        allSignatureCTs.(strandSpecText)(finalClusters == iCluster)        sum(isThisCluster & ismember(allSignatureCTs.(strandSpecText), {'Signature14', 'POLE_14'}))
        nTotalSignatures = nTotalSignatures + 1; finalClusters(isThisCluster & ismember(allSignatureCTs.(strandSpecText), {'Signature14', 'POLE_14'})) = nTotalSignatures;
        nTotalSignatures = nTotalSignatures + 1; finalClusters(isThisCluster & ismember(allSignatureCTs.(strandSpecText), {'Signature6'})) = nTotalSignatures;
        finalClusters(find(isThisCluster & ismember(allSignatureCTs.(strandSpecText), {'MSI_19'}), 1, 'first')) = nTotalSignatures;
        % Split cluster 10 into S12,liver_303 vs. ovary_93,MSI_19
        iCluster = 10; isThisCluster = (finalClusters == iCluster); %         sum(isThisCluster)        allSignatureCTs.(strandSpecText)(finalClusters == iCluster)        sum(isThisCluster & ismember(allSignatureCTs.(strandSpecText), {'ovary_93', 'MSI_19'}))
        nTotalSignatures = nTotalSignatures + 1; finalClusters(isThisCluster & ismember(allSignatureCTs.(strandSpecText), {'ovary_93', 'MSI_19'})) = nTotalSignatures;
        iCluster = 6; isThisCluster = (finalClusters == iCluster); finalClusters(isThisCluster) = 5;
        iCluster = 9; isThisCluster = (finalClusters == iCluster); finalClusters(isThisCluster) = 15;
        %
        nOfficialSignatures = length(evaluationOfDecomposedSignatures.signaturesOfficialNames);
        finalCentroidsSignatures = NaN*ones(size(allDenormalisedSignatures.(strandSpecText), 1), nTotalSignatures);
        finalCentroidsIsHigherOnLeading = true(96, nTotalSignatures);
        officialSignatureIsHigherOnLeading = false(96, nOfficialSignatures);
        officialSignatureIsUsed = false(1, nTotalSignatures);
        officialSignatureClusterIndex = NaN*ones(1, nTotalSignatures);
        officialSignatureClustersWithTissue = false(nTotalSignatures, nTissues);
        addedSignatures = zeros(96, 0);
        addedSignaturesClusterIndex = zeros(96, 0);
        addedSignaturesIsHigherOnLeading = false(96, 0);
        addedSignaturesNames = cell(0, 0);
        addedSignaturesClustersWithTissue = false(0, nTissues);
        iAddedSignature = 0;
        for iCluster = 1:nTotalSignatures
            isThisCluster = (finalClusters == iCluster & ~allSignatureIsOfficial.(strandSpecText));
            if (iStrandSpec == 1 || sum(isThisCluster) == 0) % If there is any not official signature for the double signatures, we will use only those for the centroid.
                isThisCluster = (finalClusters == iCluster);
            else
                fprintf('Cluster %d contains %d non-official signatures.\n', iCluster, sum(isThisCluster));
            end
            currentCentroid = mean(allDenormalisedSignatures.(strandSpecText)(:, isThisCluster), 2);
            finalCentroidsSignatures(:, iCluster) = currentCentroid;
            if (iStrandSpec == 2)
                currentCentroidA = currentCentroid(1:end/2);
                currentCentroidB = currentCentroid(end/2+1:end);
                currentCentroid96 = (currentCentroidA + currentCentroidB)/2;
                currentCentroidDiff = (currentCentroidA - currentCentroidB);
                LD = allDenormalisedSignatures.(strandSpecText)(1:end/2, isThisCluster);
                LG = allDenormalisedSignatures.(strandSpecText)(end/2+1:end, isThisCluster);
                %                 figure; bar(mean(LD-LG, 2));
                %                 figure; bar(mean(LG./LD, 2));
                %                 figure; bar(mean(LG > LD, 2));
                if (mean(LD(:)>=LG(:)) >= 0.5) % (sum(LD(:)) > sum(LG(:))) % (mean(LD(:)>=LG(:)) >= 0.5) % majority is higher in leading mean(mean(LG > LD, 2) .* (currentCentroid96/sum(currentCentroid96)))
                    finalCentroidsIsHigherOnLeading(:, iCluster) = ~(mean(LG > LD, 2)>=2/3); % Leading is higher ~ lagging is not higher in 2/3 of cancer types
                else
                    finalCentroidsIsHigherOnLeading(:, iCluster) = (mean(LD > LG, 2)>=2/3); % Leading is higher in at least 2/3 of cancer types
                end
                % Originally 2 average over cancer types: finalCentroidsIsHigherOnLeading(:, iCluster) = round(mean(allDenormalisedSignatures.(strandSpecText)(1:end/2, isThisCluster) > allDenormalisedSignatures.(strandSpecText)(end/2+1:end, isThisCluster), 2));
                % Originally 1 which is higher in the mean signature: finalCentroidsIsHigherOnLeading(:, iCluster) = (currentCentroid(1:end/2) > currentCentroid(end/2+1:end));
                fig = createMaximisedFigure(1); nR = 4; nC = 1;
                myGeneralSubplot(nR,nC,1); plotSignatureDebugging(currentCentroid, subtypes.(strandSpecText), types.(strandSpecText), true); ylabel(sprintf('cluster %d', iCluster));
                yVal = 90*max(currentCentroid);
                for iBar = 1:16:96
                    currentCentroidBars = currentCentroid96(iBar+(0:15));
                    currentIsHigherOnLeading = finalCentroidsIsHigherOnLeading(iBar+(0:15), iCluster);
                    currentCentroidDiffBars = currentCentroidDiff(iBar+(0:15));
                    sumLead = 100*sum(currentCentroidBars(currentIsHigherOnLeading));
                    sumLagg = 100*sum(currentCentroidBars(~currentIsHigherOnLeading));
                    maxLead = 100*max(currentCentroidBars(currentIsHigherOnLeading));
                    maxLagg = 100*max(currentCentroidBars(~currentIsHigherOnLeading));
                    maxDiffLead = 100*max(currentCentroidDiffBars(currentIsHigherOnLeading));
                    maxDiffLagg = 100*max(currentCentroidDiffBars(~currentIsHigherOnLeading));
                    fprintf('Bar %d: sumLead=%.2f, sumLagg=%.2f, maxLead=%.2f, maxLagg = %.2f, maxDiffLead=%.2f, maxDiffLagg=%.2f\n', iBar, sumLead, sumLagg, maxLead, maxLagg, maxDiffLead, maxDiffLagg);
                    if ((~isempty(maxLagg) && maxLagg < 0.1) || sumLagg < sumLead/20 || (sumLagg < sumLead/2 && abs(maxDiffLagg) < 0.1))
                        finalCentroidsIsHigherOnLeading(iBar+(0:15), iCluster) = true;
                        text(iBar+7.5, yVal, {'lagg\_to\_lead', ' ', ' ', ' ', ' ', ' ', ' '}, 'HorizontalAlignment', 'center', 'FontWeight', 'bold');
                    elseif ((~isempty(maxLead) && maxLead < 0.1) || sumLead < sumLagg/20 || (sumLead < sumLagg/2 && abs(maxDiffLead) < 0.1))
                        finalCentroidsIsHigherOnLeading(iBar+(0:15), iCluster) = false;
                        text(iBar+7.5, yVal, {'lead\_to\_lagg', ' ', ' ', ' ', ' ', ' ', ' '}, 'HorizontalAlignment', 'center', 'FontWeight', 'bold');
                    end
                    text(iBar+7.5, yVal, {' ', sprintf('sumLead=%.3f', sumLead), sprintf('sumLagg=%.3f', sumLagg), sprintf('maxLead=%.3f', maxLead), sprintf('maxLagg=%.3f', maxLagg), ...
                        sprintf('maxDiffLead=%.3f', maxDiffLead), sprintf('maxDiffLagg=%.3f', maxDiffLagg)}, 'HorizontalAlignment', 'center');
                end
                currentCentroid96A = currentCentroid96; currentCentroid96A(~finalCentroidsIsHigherOnLeading(:, iCluster)) = 0;
                myGeneralSubplot(nR,nC,2); plotSignatureDebugging(currentCentroid96A, subtypes.(strandSpecText), types.(strandSpecText), true); ylabel('Leading part');
                currentCentroid96B = currentCentroid96; currentCentroid96B(finalCentroidsIsHigherOnLeading(:, iCluster)) = 0;
                myGeneralSubplot(nR,nC,3); plotSignatureDebugging(currentCentroid96B, subtypes.(strandSpecText), types.(strandSpecText), true); ylabel('Lagging part');
                %myGeneralSubplot(nR,nC,4); plotSignatureDebugging([sum(LD, 2); sum(LG, 2)], subtypes.(strandSpecText), types.(strandSpecText), true); ylabel('Distribution in CTs'); grid on;
                myGeneralSubplot(nR,nC,4); plotSignatureDebugging(mean(LG > LD, 2), subtypes.(strandSpecText), types.(strandSpecText), true); ylabel('CT: higher in LG (%)'); grid on;
                suptitle(sprintf('Cluster %d checking small mixtures', iCluster));
                if (plotFigures)
                    mySaveAs(fig, imagesPath, ['check_smallMixture_denormalised_', strandSpecText, '_', num2str(iCluster)]);
                end
                %
                if (plotFigures)
                    lstInputSignatures = find(finalClusters == iCluster)';
                    fig = createMaximisedFigure(2); nR = length(lstInputSignatures); nC = 1; jIS = 1;
                    for iIS = lstInputSignatures
                        myGeneralSubplot(nR,nC,jIS); plotSignatureDebugging(allDenormalisedSignatures.(strandSpecText)(:, iIS), subtypes.(strandSpecText), types.(strandSpecText), true);
                        if (allSignatureIsOfficial.(strandSpecText)(iIS))
                            ylabel(sprintf('{\\color[rgb]{0 .5 .5}%s}', strrep(allSignatureCTs.(strandSpecText){iIS}, '_', '\_')));
                        else
                            ylabel(sprintf('{\\color[rgb]{0 0 0}%s}', strrep(allSignatureCTs.(strandSpecText){iIS}, '_', '\_')));
                        end
                        jIS = jIS + 1;
                    end
                    suptitle(sprintf('Cluster %d', iCluster));
                    mySaveAs(fig, imagesPath, ['clusters_denormalised_', strandSpecText, '_', num2str(iCluster)]);
                end
                %
                isThisClusterOfficial = (finalClusters == iCluster & allSignatureIsOfficial.(strandSpecText));
                lstIndexTissues = allSignatureIndexTissue.(strandSpecText)(finalClusters == iCluster & ~allSignatureIsOfficial.(strandSpecText));
                if (sum(isThisClusterOfficial) > 0)
                    lstOfficialSignatures = allSignatureIndexOfficial.(strandSpecText)(finalClusters == iCluster & allSignatureIsOfficial.(strandSpecText));
                    officialSignatureIsHigherOnLeading(:, lstOfficialSignatures) = repmat(finalCentroidsIsHigherOnLeading(:, iCluster), 1, length(lstOfficialSignatures));
                    if (sum((finalClusters == iCluster & ~allSignatureIsOfficial.(strandSpecText))) > 0) % If there is also any non-official signature...
                        officialSignatureIsUsed(lstOfficialSignatures) = true;
                        officialSignatureClusterIndex(lstOfficialSignatures) = iCluster;
                        officialSignatureClustersWithTissue(lstOfficialSignatures, lstIndexTissues) = true;
                    end
                elseif (sum((finalClusters == iCluster & ~allSignatureIsOfficial.(strandSpecText))) > 0) % If there is also any non-official signature...
                    meanOfCurrentCentroid = (currentCentroid(1:end/2) + currentCentroid(end/2+1:end)/2);
                    meanOfCurrentCentroid = meanOfCurrentCentroid/sum(meanOfCurrentCentroid);
                    addedSignatures = [addedSignatures, meanOfCurrentCentroid];
                    addedSignaturesClusterIndex = [addedSignaturesClusterIndex, iCluster];
                    addedSignaturesIsHigherOnLeading = [addedSignaturesIsHigherOnLeading, finalCentroidsIsHigherOnLeading(:, iCluster)];
                    iAddedSignature = iAddedSignature+ 1;
                    addedSignaturesNames = [addedSignaturesNames, {sprintf('Signature N%d', iAddedSignature)}];
                    localOneLine = false(1, nTissues);
                    localOneLine(lstIndexTissues) = true;
                    addedSignaturesClustersWithTissue = [addedSignaturesClustersWithTissue; localOneLine];
                end
            end
        end
        if (plotFigures)
            %         figure; imagesc(finalCentroidsIsHigherOnLeading); colorbar;
            %
            iCluster = 0; nR = 5; nC = 1;
            for i = 1:ceil(nTotalSignatures/nR)
                fig = createMaximisedFigure(i); plotTypes = false;
                for j = 1:nR
                    if (iCluster < nTotalSignatures)
                        iCluster = iCluster + 1;
                        subplot(nR,nC,j); if (j == nR) plotTypes = true; end
                        plotSignatureDebugging(finalCentroidsSignatures(:,iCluster), subtypes.(strandSpecText), types.(strandSpecText), plotTypes); ylabel(sprintf('signature %d', iCluster));
                        tmpListSignatureNames = unique(allSignatureCTs.(strandSpecText)(finalClusters == iCluster & ~allSignatureIsOfficial.(strandSpecText)));
                        tmpListLength = length(tmpListSignatureNames);
                        tmpOfficialListSignatureNames = unique(allSignatureCTs.(strandSpecText)(finalClusters == iCluster & allSignatureIsOfficial.(strandSpecText)))';
                        if (sum(finalClusters == iCluster) < 10)
                            tmpTitle = {sprintf('%d original signatures', sum(finalClusters == iCluster)), sprintf('{\\color[rgb]{0 .5 .5}%s} %s', strjoin(tmpOfficialListSignatureNames), ...
                                strrep(strjoin(tmpListSignatureNames), '_', '\_'))};
                        else
                            tmpTitle = {sprintf('%d original signatures', sum(finalClusters == iCluster)), sprintf('{\\color[rgb]{0 .5 .5}%s} %s', strjoin(tmpOfficialListSignatureNames), ...
                                strrep(strjoin(tmpListSignatureNames(1:round(tmpListLength/2))), '_', '\_')), strrep(strjoin(tmpListSignatureNames(round(tmpListLength/2)+1:end)), '_', '\_')};
                        end
                        title(tmpTitle, 'VerticalAlignment', 'top', 'FontSize', 8); %'interpret', 'none',
                    end
                end
                suptitle(sprintf('Denormalised %d', i));
                mySaveAs(fig, imagesPath, ['finalSignaturesMyClusteringFull_complete_denormalised_', strandSpecText, '_', num2str(i)]);
            end
        end
        structClusters.(strandSpecText).finalCentroidsSignatures = finalCentroidsSignatures;
        structClusters.(strandSpecText).finalClusters = finalClusters;
        structClusters.(strandSpecText).finalCentroidsIsHigherOnLeading = finalCentroidsIsHigherOnLeading;
        structClusters.(strandSpecText).allDenormalisedSignatures = allDenormalisedSignatures.(strandSpecText);
        structClusters.(strandSpecText).allSignatureNames = allSignatureNames.(strandSpecText);
        structClusters.(strandSpecText).allSignatureIndexTissue = allSignatureIndexTissue.(strandSpecText);
        structClusters.(strandSpecText).allSignatureIsOfficial = allSignatureIsOfficial.(strandSpecText);
        structClusters.(strandSpecText).allSignatureIndexOfficial = allSignatureIndexOfficial.(strandSpecText);
        structClusters.(strandSpecText).allSignatureNames = allSignatureNames.(strandSpecText);
        structClusters.(strandSpecText).allSignatureIndexTissue = allSignatureIndexTissue.(strandSpecText);
        structClusters.(strandSpecText).allSignatureCTs = allSignatureCTs.(strandSpecText);
    end
    %     save('workspaces\workspace_loadDecomposedSignatures_2016_12_12.mat');
    %%
    clear finalSignatures
    finalSignatures.signatures = [evaluationOfDecomposedSignatures.matrixSignaturesOfficial(:, officialSignatureIsUsed), addedSignatures];
    finalSignatures.signaturesNames = [evaluationOfDecomposedSignatures.signaturesOfficialNames(officialSignatureIsUsed), addedSignaturesNames];
    finalSignatures.isHigherOnLeading = [officialSignatureIsHigherOnLeading(:, officialSignatureIsUsed), addedSignaturesIsHigherOnLeading];
    finalSignatures.clusterIndex = [officialSignatureClusterIndex(officialSignatureIsUsed), addedSignaturesClusterIndex];
    finalSignatures.nSignatures = length(finalSignatures.signaturesNames);
    finalSignatures.splitSignaturesA = finalSignatures.signatures;
    finalSignatures.splitSignaturesA(~finalSignatures.isHigherOnLeading) = 0;
    finalSignatures.splitSignaturesB = finalSignatures.signatures;
    finalSignatures.splitSignaturesB(finalSignatures.isHigherOnLeading) = 0;
    finalSignatures.splitSignaturesAOfficialNames = strcat(finalSignatures.signaturesNames, 'A');
    finalSignatures.splitSignaturesBOfficialNames = strcat(finalSignatures.signaturesNames, 'B');
    
    finalSignatures.isSignatureInCancerGroup = [officialSignatureClustersWithTissue(officialSignatureIsUsed, :); addedSignaturesClustersWithTissue];
    finalSignatures.tissueNames = tissueNames;
    %     figure; imagesc((finalSignatures.splitSignaturesA==0) + (finalSignatures.splitSignaturesB==0)); colorbar; % CHECK: Good, all one or two
    %     figure; imagesc((finalSignatures.splitSignaturesA + finalSignatures.splitSignaturesB)== finalSignatures.signatures); colorbar; % CHECK: Good, all one
    %     finalSignatures.splitSignatures = [finalSignatures.signatures, finalSignatures.signatures];
    %     finalSignatures.splitSignatures([~finalSignatures.isHigherOnLeading, false(96, finalSignatures.nSignatures)]) = 0;
    %     finalSignatures.splitSignatures([false(96, finalSignatures.nSignatures), finalSignatures.isHigherOnLeading]) = 0;
    %     finalSignatures.splitSignaturesOfficialNames = [strcat(finalSignatures.signaturesOfficialNames, 'A'), strcat(finalSignatures.signaturesOfficialNames, 'B')];
    %     CHECK: figure; imagesc((finalSignatures.splitSignatures(:,1:end/2)==0) + (finalSignatures.splitSignatures(:,1+end/2:end)==0)); colorbar; % Good, all zero or one
    %     CHECK: figure; imagesc((finalSignatures.splitSignatures(:,1:end/2) + finalSignatures.splitSignatures(:,1+end/2:end))== finalSignatures.signatures); colorbar; % Good, all one
    %%
    if (true || plotFigures)
        iFinalSignature = 0; nR = 5; nC = 1;
        for i = 1:ceil(finalSignatures.nSignatures/nR)
            fig = createMaximisedFigure(i); plotTypes = false;
            for j = 1:nR
                if (iFinalSignature < finalSignatures.nSignatures)
                    iFinalSignature = iFinalSignature + 1;
                    subplot(nR,nC,j); if (j == nR) plotTypes = true; end
                    plotSignatureDebugging([finalSignatures.splitSignaturesA(:,iFinalSignature); finalSignatures.splitSignaturesB(:,iFinalSignature)], subtypes.(strandSpecText), types.(strandSpecText), plotTypes);
                    title(sprintf('%s and %s', finalSignatures.splitSignaturesAOfficialNames{iFinalSignature}, finalSignatures.splitSignaturesBOfficialNames{iFinalSignature}), 'VerticalAlignment', 'top', 'FontSize', 8); %'interpret', 'none',
                end
            end
            suptitle(sprintf('Final denormalised %d', i));
            mySaveAs(fig, imagesPath, ['finalSignatures_denormalised_', strandSpecText, '_', num2str(i)]);
        end
    end
    %%
    decomposedSignatures.structClusters = structClusters;
    decomposedSignatures.matrixAllSignatures = matrixAllSignatures;
    decomposedSignatures.tableAllSignatures = tableAllSignatures;
    decomposedSignatures.totalNSigantures = totalNSigantures;
    decomposedSignatures.subtypes = subtypes;
    decomposedSignatures.types = types;
    decomposedSignatures.finalSignatures = finalSignatures;
    
    % SAVE
    save(pathAndName, 'decomposedSignatures');
    toc
end