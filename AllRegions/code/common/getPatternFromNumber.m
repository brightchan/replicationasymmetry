function [nuclLeft, nuclRight, nuclFrom, nuclTo] = getPatternFromNumber(number)

patternMiddleNames = {  'C>A', 'C>G', 'C>T', ...
                        'T>A', 'T>C', 'T>G'};
                    
patternContextNames = { 'AA', 'AC', 'AG', 'AT', ...
                        'CA', 'CC', 'CG', 'CT', ...
                        'GA', 'GC', 'GG', 'GT', ...
                        'TA', 'TC', 'TG', 'TT', };

if (number < 1 || number > 96)
    disp('wrong number');
end

pmn = floor((number-1)/16) + 1;
pcn = mod(number-1, 16) + 1;
patternContextName = patternContextNames{pcn};
patternMiddleName = patternMiddleNames{pmn};
nuclLeft = patternContextName(1);
nuclRight = patternContextName(2);
nuclFrom = patternMiddleName(1);
nuclTo = patternMiddleName(3);
