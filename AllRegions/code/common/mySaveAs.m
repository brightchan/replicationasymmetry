function mySaveAs(fig, myPath, pictureName, saveToEps)
% mySaveAs(fig, myPath, pictureName) saves the fig with pictureName into path as png

createDir(myPath);                                              % Checkes, that the directory exists.
saveas(fig, [myPath, pictureName], 'png');     % Saves the image.
% print(fig, [myPath, pictureName], '-dpng', '-r300');     % Saves the image.


% export_fig([myPath, pictureName], '-m2', '-nocrop')

if (nargin > 3 && saveToEps)
    print(fig, '-depsc2', '-painters', [myPath, pictureName,'.eps']) %THIS IS THE BEST!
end
% saveas(fig, [myPath, pictureName], 'pdf');     % Saves the image.

% plot2svg([myPath, pictureName,'.svg'], fig);   % svg

% fprintf('Now trying myaa...\n');
% myaa(8, 'standard', fig);
% print(fig, [myPath, pictureName], '-dpng', '-r300');     % Saves the image.

% fprintf('Now trying export_fig...\n');
% export_fig([myPath, pictureName], '-m2', '-nocrop')
% % export_fig [myPath, pictureName] -m3