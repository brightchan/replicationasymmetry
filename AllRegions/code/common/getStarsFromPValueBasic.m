function [nStars, starsText, pValue, pValueText] = getStarsFromPValueBasic(pValue)

pValue_oneStar = 0.05;
pValue_twoStars = 0.01;
pValue_threeStars = 0.001;

starsText = ''; nStars = 0;

if (pValue < pValue_oneStar)
    starsText = '*'; nStars = 1;
end
if (pValue < pValue_twoStars)
    starsText = '**'; nStars = 2;
end
if (pValue < pValue_threeStars)
    starsText = '***'; nStars = 3;
end


if (pValue < 0.0001)
    pValueText = sprintf('%.1e', pValue);
else
    pFormat = sprintf('%%.%df', nStars+2);
    pValueText = sprintf(pFormat, pValue);
end