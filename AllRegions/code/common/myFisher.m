function [pval, isLeftIeNegAssociation, pvalLeft, pvalRight] = myFisher(dataTable)


if (exist('fishertest', 'file')==2)
    [~, pval] = fishertest(dataTable);
    [~, pvalLeft] = fishertest(dataTable, 'tail', 'left');
    [~, pvalRight] = fishertest(dataTable, 'tail', 'right');
else
    a = dataTable(1,1);
    b = dataTable(2,1);
    c = dataTable(1,2);
    d = dataTable(2,2);
    pval = fexact(a, a+b+c+d, a+c, a+b, 'tail', 'b');
    pvalLeft = fexact(a, a+b+c+d, a+c, a+b, 'tail', 'l');
    pvalRight = fexact(a, a+b+c+d, a+c, a+b, 'tail', 'r');
end

if (pvalLeft < pvalRight)
    isLeftIeNegAssociation = true;
else
    isLeftIeNegAssociation = false;
end

%        R       NR
%  pos   a        c       K
%  neg   b        d       -
% total  N        -       M
%  p = fexact( a,M,K,N, options)