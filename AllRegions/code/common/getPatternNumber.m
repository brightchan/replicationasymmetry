function [patternNumber, nuclLeftNew, nuclRightNew] = getPatternNumber(nuclLeft, nuclRight, nuclFrom, nuclTo)
% getPatternNumber(nuclLeft, nuclRight, nuclFrom, nuclTo) returns number of the mutationalPattern

patternNumber = 0;
nuclLeftNew = nuclLeft;
nuclRightNew = nuclRight;

if (nuclLeft < 1 || nuclLeft > 4)
    fprintf('Problem: nuclLeft %s has number %d\n', int2nt(nuclLeft), nuclLeft);
    patternNumber = -1;
end
if (nuclRight < 1 || nuclRight > 4)
    fprintf('Problem: nuclLeft %s has number %d\n', int2nt(nuclRight), nuclRight);
    patternNumber = -1;
end
if (nuclFrom < 1 || nuclFrom > 4)
    fprintf('Problem: nuclLeft %s has number %d\n', int2nt(nuclFrom), nuclFrom);
    patternNumber = -1;
end
if (nuclTo < 1 || nuclTo > 4)
    fprintf('Problem: nuclLeft %s has number %d\n', int2nt(nuclTo), nuclTo);
    patternNumber = -1;
end

if (patternNumber >= 0)
    % patternMiddleNames = { 'C>A', 'C>G', 'C>T', ...
    %                         'T>A', 'T>C', 'T>G'};
    % patternContextNames = {'AA', 'AC', 'AG', 'AT', ...
    %                         'CA', 'CC', 'CG', 'CT', ...
    %                         'GA', 'GC', 'GG', 'GT', ...
    %                         'TA', 'TC', 'TG', 'TT', };
    
    % A C G T
    % 1 2 3 4
    nuclFromTransformation = [4 3 2 1];
    nuclFromTransform =      [1 0 1 0];
    
    
    % >A >C >G >T
    patternMiddle = [  4, 5, 6, 0;    % A>
        1, 0, 2, 3;    % C>
        1, 0, 2, 3;    % G>
        4, 5, 6, 0];   % T>
    
    % A    C   G   T
    patternContext = [  1,   2,  3,  4; % A
        5,   6,  7,  8; % C
        9,  10, 11, 12; % G
        13, 14, 15, 16];% T
    
    
    if (nuclFromTransform(nuclFrom))
        patternMiddleNumber = patternMiddle(nuclFromTransformation(nuclFrom), nuclFromTransformation(nuclTo));
        nuclLeftNew = nuclFromTransformation(nuclRight);        % ATTENTION! We are reading from the other strand, so we need complementary bases in the opposite order
        nuclRightNew = nuclFromTransformation(nuclLeft);        % ATTENTION! We are reading from the other strand, so we need complementary bases in the opposite order
    else
        patternMiddleNumber = patternMiddle(nuclFrom, nuclTo);        
        nuclLeftNew = nuclLeft;
        nuclRightNew = nuclRight;
    end
    patternContextNumber = patternContext(nuclLeftNew, nuclRightNew);
    patternNumber = (patternMiddleNumber-1)*16 + patternContextNumber;
end