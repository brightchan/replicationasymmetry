function currentExposures = runRegressionToExposures(matrixForRegression, mfVectorForRegression, nSignatures, scalingThresholdErrorIncrease, printInfo, iSample)

% scalingThresholdErrorIncrease: 1.005

[currentExposures,resnorm] = lsqnonneg(matrixForRegression, mfVectorForRegression); %,residual,exitflag,output,lambda
nIter = min(10, sum(currentExposures(1:nSignatures)+currentExposures(nSignatures+(1:nSignatures))>0));
if (nIter > 0)
    res = NaN*ones(nIter, 1);
    perIterCurrentExposures = NaN*ones(2*nSignatures, nIter);
    for iter = 1:nIter
        res(iter) = resnorm;
        expA = currentExposures(1:nSignatures);
        expB = currentExposures(nSignatures+(1:nSignatures));
        expMax = max([expA, expB], [], 2);
        if (max(expMax) > 0)
            minVal = min(expMax(expMax > 0));
            isAbove0 = (expA > minVal | expB > minVal);
            %                 sum(isAbove0)
            %                 sum(expA > 0 | expB > 0)
            isKeptCol = [isAbove0; isAbove0];
            currentExposures(:) = 0;
            [currentExposuresSmaller,resnorm] = lsqnonneg(matrixForRegression(:,isKeptCol), mfVectorForRegression); %,residual,exitflag,output,lambda
            currentExposures(isKeptCol) = currentExposuresSmaller;
        end
        perIterCurrentExposures(:, iter) = currentExposures;
    end
    %             fig = createMaximisedFigure(1); plot(res, 'o-');
    %             sum(res < res(1)*1.005)
    indexIter = find(res < res(1)*scalingThresholdErrorIncrease, 1, 'last');
    currentExposures = perIterCurrentExposures(:, indexIter);
    if (printInfo)
        fprintf('Sample %d: indexIter=%d, resnorm(1)=%.1e, resnorm(indexIter)=%.1e.\n', iSample, indexIter, res(1), res(indexIter));
    end
end