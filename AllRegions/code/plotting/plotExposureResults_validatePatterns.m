function plotExposureResults_validatePatterns(replData, territoriesSource)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
patternNames = replData.patternNames;
maxPattern = length(patternNames);
structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
simpleType = 'LeadLagg'; xVar = 'asymmetryDifference';
%%
isNotPOLE = ~strcmp(tableAllSamples.Tissue, 'POLE')';
matrixToPlot = full(structAllSamples.(simpleType).(xVar)(isNotPOLE,:)');
fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 60 30],'units','normalized','outerposition',[0 0 1 1]);
nR = 1; nC = 3; iS = 1; xS = .9; yS = 1;
for iPattern = [16,47,96]%[35:4:47] %[16,47,96]
    myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1;
    nhist(matrixToPlot(iPattern,:));
    p = signtest(matrixToPlot(iPattern,:));
    title({sprintf('%s: p %.1e', strrep(patternNames{iPattern}, 'to', '>'), p), sprintf('mean %.1e, median %.1e', nanmean(matrixToPlot(iPattern,:)), nanmedian(matrixToPlot(iPattern,:)))});
    xlabel('exposure on leading - lagging strand'); ylabel('number of samples');
end
% myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1;
% % tmpVector = mean(matrixToPlot([35:4:47],:));
% tmpVector = matrixToPlot([35:4:47],:); tmpVector = tmpVector(:);
% nhist(tmpVector);
% p = signrank(tmpVector);
% title({sprintf('CpG>T: p %.3f', p), sprintf('mean %.1e, median %.1e', nanmean(tmpVector), nanmedian(tmpVector))});
% xlabel('exposure on leading - lagging strand');

suptitle('Main components of signature 10 in POLE-WT cancers'); %{'Signature 10 main components', strrep(territoriesSource, '_', ' ')});
mySaveAs(fig, imagesPath, 'FigS12');
% mySaveAs(fig, imagesPath, ['signature10_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
% suptitle({'Signature 1 main components', strrep(titleTabuCTs, '_', ' ')});
% mySaveAs(fig, imagesPath, ['signature1_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
%%