function plotExposureResults_oriplotsTrinucleotides(replData, territoriesSource)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
patternNames = replData.patternNames;
maxPattern = length(patternNames);
structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
nTotalSamples = size(tableAllSamples, 1);
signatureExposuresSimple = replData.signatureExposuresSimple;
nValues = replData.dataFields.nValues;
signatureExposuresComplex = replData.signatureExposuresComplex;

colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.tissuesColourmap = hsv(nTissues);
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
signatureNames = replData.signatureNames;


% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);

% iTissue = 1;
[~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
% simpleType = 'LeadLagg';

% summedExposures = (signatureExposuresSimple.(simpleType).type1 + signatureExposuresSimple.(simpleType).type2);
%% ORI GRAPHS TRINUCLEOTIDES ___________________________________________________
fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',2*[0 0 30 20],'units','normalized','outerposition',[0 0 1 1]);
nR = 6; nC = 16; iS = 1; xS = .95; yS = .95;
for iPattern = 1:maxPattern
    myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1; hold on;
    plusStrandMean = NaN*ones(nValues.DistanceFromORI_LeadLagg, 1);
    plusStrandStd = NaN*ones(nValues.DistanceFromORI_LeadLagg, 1);
    minusStrandMean = NaN*ones(nValues.DistanceFromORI_LeadLagg, 1);
    minusStrandStd = NaN*ones(nValues.DistanceFromORI_LeadLagg, 1);
    for iValue = 1:nValues.DistanceFromORI_LeadLagg
        isCurrentOutlier = false(1,nTotalSamples);
        tmp = structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfPlus(listSamples,iPattern);
        tmp = sort(tmp);
        quartile95 = tmp(round(end*0.95));
        if (quartile95*2 < max(tmp))
            isCurrentOutlier = (structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfPlus(:,iPattern)>quartile95)';
        end
        tmp = structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfMinus(listSamples,iPattern);
        tmp = sort(tmp);
        quartile95 = tmp(round(end*0.95));
        if (quartile95*2 < max(tmp))
            isCurrentOutlier = isCurrentOutlier & (structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfMinus(:,iPattern)>quartile95)';
        end
        isCurrentOutlier = isCurrentOutlier(listSamples);
        plusStrandMean(iValue) = nanmean(structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfPlus(listSamples(~isCurrentOutlier),iPattern));
        plusStrandStd(iValue) = nanstd(structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfPlus(listSamples(~isCurrentOutlier),iPattern));
        minusStrandMean(iValue) = nanmean(structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfMinus(listSamples(~isCurrentOutlier),iPattern));
        minusStrandStd(iValue) = nanstd(structAllSamples.DistanceFromORI_LeadLagg{iValue}.mfMinus(listSamples(~isCurrentOutlier),iPattern));
    end
    plot(plusStrandMean, 'o', 'MarkerFaceColor', colours.plus, 'MarkerEdgeColor', 'none', 'MarkerSize', 4);
    plot(minusStrandMean, 'o', 'MarkerFaceColor', colours.minus, 'MarkerEdgeColor', 'none', 'MarkerSize', 4);
    xlim([0, nValues.DistanceFromORI_LeadLagg]); set(gca, 'XTick', [], 'YTick', []);
    try
        ylim([min([plusStrandMean; minusStrandMean]), max([plusStrandMean; minusStrandMean])]); yLimVal = get(gca, 'YLim'); xLimVal = get(gca, 'XLim');
    catch
    end
    if (mod(iPattern, nC) == 1)
    ylabel({'Mutation', 'frequency'}, 'FontSize', 12);
    end
    if (iPattern > maxPattern-nC)
        text(xLimVal(1), yLimVal(1), '-1Mbp', 'HorizontalAlignment', 'left', 'VerticalAlignment', 'top', 'FontSize', 8);
        text(mean(xLimVal), yLimVal(1), 'ORI', 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', 8);
        text(xLimVal(2), yLimVal(1), '+1Mbp', 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'FontSize', 8);
    end
    text(nValues.DistanceFromORI_LeadLagg/2, max(get(gca, 'YLim')), patternNames{iPattern}, 'HorizontalAlignment', 'center');
    drawnow;
end
suptitle(strrep(territoriesSource, '_', ' '));
mySaveAs(fig, imagesPath, ['aroundORI_', territoriesSource, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);

%%