function plotExposureResults_heatmapTrinucleotides(replData, territoriesSource)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
patternNames = replData.patternNames;
maxPattern = length(patternNames);
structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;

% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7; 
colours.tissuesColourmap = hsv(nTissues);

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);

% iTissue = 1;
[~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
simpleType = 'LeadLagg';
%% HEATMAPS TRINUCLEOTIDES
for iType = 1:4
    if (iType == 1)
        xVar = 'asymmetryDifference'; printVar = 'Leading - lagging mutation frequency';       normalise = true;
    elseif (iType == 2)
        xVar = 'asymmetryNegLogPValue'; printVar = 'p value of asymmetry';         normalise = false;
    elseif (iType == 3)
        xVar = 'mfType1'; printVar = 'leading';      normalise = false;
    elseif (iType == 4)
        xVar = 'mfType2'; printVar = 'lagging';      normalise = false;
    elseif (iType == 5)
        xVar = 'diffOverSum'; printVar = '(leading-lagging)/(leading+lagging)';      normalise = true;
    end
    fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 60 40],'units','normalized','outerposition',[0 0 1 1]);     axes('Position', [0.05, 0.05, 0.9, 0.83]);
    if (normalise)
        colormap([colourLinspace([0,0.5,1], 0.9*[1,1,1], 60); colourLinspace(0.9*[1,1,1], [1,0.1,0.2], 60)]);
        %             colormap([colourLinspace([0,0.5,1], [0,0.5,1], 60); [0,0,0]; colourLinspace([1,0.1,0.2], [1,0.1,0.2], 60)]);
        if (iType == 5)
            matrixToPlot = full(structAllSamples.(simpleType).(xVar)(listSamples,:)');
        else
            matrixToPlot = zscore(full(structAllSamples.(simpleType).(xVar)(listSamples,:)'));
        end
        %matrixToPlot = matrixToPlot.*repmat(tableGenomeTrinucleotides.genomeComputed, 1, size(matrixToPlot, 2));
        maxAbs = max(abs(matrixToPlot(:)));
    else
        colormap(colourLinspace(0.9*[1,1,1], [1,0.1,0.2], 120));
        matrixToPlot = structAllSamples.(simpleType).(xVar)(listSamples,:)'; matrixToPlot = matrixToPlot./repmat(sum(matrixToPlot), maxPattern, 1);
    end
    imagesc(matrixToPlot); colorbar;
    if (normalise)
        caxis([-maxAbs, maxAbs]);
    end
    set (gca, 'XTick', [], 'YTick', []); hold on;
    for iPattern = 1:maxPattern
        text(0.5, iPattern, patternNames{iPattern}, 'HorizontalAlignment', 'right', 'Color', colours.patternColormap(1+floor((iPattern-1)/16),:));
    end
    for iTissue = 1:nTissues
        listSamplesInCT = find(strcmp(tableAllSamples.Tissue(listSamples), tissueNames{iTissue}));
        if (~isempty(listSamplesInCT))
            plot([min(listSamplesInCT), max(listSamplesInCT)], 0.8*[1,1], 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'LineWidth', 2);
            plot(0.5+max(listSamplesInCT)*[1,1], 0.5+[0, size(matrixToPlot, 1)], 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'LineWidth', 0.5);
            text(mean(listSamplesInCT), 0, strrep(tissueNames{iTissue},'_',' '), 'Rotation', 45, 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'VerticalAlignment', 'bottom');
        end
    end
    suptitle(printVar);  %suptitle(sprintf('%s: %s', simpleType, printVar));
    mySaveAs(fig, imagesPath, [xVar, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%