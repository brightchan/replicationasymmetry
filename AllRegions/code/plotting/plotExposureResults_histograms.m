% function plotExposureResults_histograms(replData, territoriesSource, signatureExposuresSimple_LeadLagg_cell_Besnard1k)

%%
% Here we will plot signature plot, H ori plot, B ori plot, repl timing barplot, histogram, etc.
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', '');
% signatureNames = strrep(signatureNames, 'Signature', 'Signature ');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
signatureExposuresSimple_LeadLagg.Haradhvala = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix.Haradhvala = replData.signatureExposuresSimple.LeadLagg.type1Minustype2;
signatureExposuresSimple_LeadLagg.Besnard1k = signatureExposuresSimple_LeadLagg_cell_Besnard1k;
signatureExposuresSimple_matrix.Besnard1k = signatureExposuresSimple_LeadLagg_cell_Besnard1k.type1Minustype2;
%%
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_combined/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
minMeanExposure = 10;
font_size = 12;
font_name = 'Helvetica';% 'Lucida Sans'; % Console
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%% clear signatureResults
territoryNames = {'Haradhvala', 'Besnard1k'};
territoryNamesPrint = {'Haradhvala', 'Besnard'};
for iTerritoryName = 1:2
    territoryName = territoryNames{iTerritoryName};
    pValue = NaN*ones(nSignatures, 1);
    signatureResults = table(pValue);
    signatureResults.meanExposure = NaN*ones(nSignatures, 1);
    signatureResults.medianExposure = NaN*ones(nSignatures, 1);
    signatureResults.hasStrandAsymmetry = false(nSignatures, 1);
    signatureResults.colourTitle = zeros(nSignatures, 3);
    signatureResults.colourPositiveBars = zeros(nSignatures, 3);
    signatureResults.colourNegativeBars = zeros(nSignatures, 3);
    signatureResults.colourGCA = zeros(nSignatures, 3);
    signatureResults.titleText = cell(nSignatures, 3);
    for iSignature = 1:nSignatures
        isSampleUsed = signatureExposuresSimple_LeadLagg.(territoryName).meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        exposuresDifference = signatureExposuresSimple_matrix.(territoryName)(iSignature, isSampleUsed);
        signatureResults.pValue(iSignature) = signtest(exposuresDifference); %signrank
        signatureResults.meanExposure(iSignature) = mean(exposuresDifference);
        signatureResults.medianExposure(iSignature) = median(exposuresDifference);
    end
    [~, ~, ~, qValues]=fdr_bh(signatureResults.pValue);
    signatureResults.qValue_BenjaminiHochberg = qValues;
    signatureResults.qValue_Bonferroni = signatureResults.pValue*nSignatures;
    signatureResults.isSignificant = signatureResults.pValue < 0.05*nSignatures;
    signatureResults.isLeading = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure > 0); %signatureResults.meanExposure > 0 &
    signatureResults.isLagging = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure < 0); %signatureResults.meanExposure < 0 &
    signatureResults.hasStrandAsymmetry(signatureResults.isLeading | signatureResults.isLagging) = true;
    for iSignature = 1:nSignatures
        if (signatureResults.isLeading(iSignature))
            signatureResults.titleText{iSignature} = sprintf('%s: leading %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
            signatureResults.colourTitle(iSignature,:) = colours.leadingDark;
        elseif (signatureResults.isLagging(iSignature))
            signatureResults.titleText{iSignature} = sprintf('%s: lagging %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
            signatureResults.colourTitle(iSignature,:) = colours.laggingDark;
        else
            signatureResults.titleText{iSignature} = sprintf('%s n.s.', signatureNames{iSignature});
            signatureResults.colourTitle(iSignature,:) = colours.titleNotSignif;
        end
        if (signatureResults.hasStrandAsymmetry(iSignature))
            signatureResults.colourPositiveBars(iSignature,:) = colours.leading;
            signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
            signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
        else
            signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif;
            signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
            signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;%colours.titleNotSignif;
        end
    end
    signatureResults_cell.(territoryName) = signatureResults;
end
%%
nR = 1; nC = 2; iS = 1; xS = .85; yS = 1; xB = 0.08; yB = 0.1; xM = -0.02; yM = 0.05;
fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', [0 0 30 15],'units','normalized','outerposition',[0 0 1 1]);

posTitle.xShift = 0.05;
posTitle.yShift = 0.01;
posTitle.width = 0.02;
posTitle.height = 0.02;
posTitle.fontSize = 18;
posTitle.letters = {'a', 'b', 'c', 'd', 'e', 'f'};

for iS = 1:2
    if (iS == 1)
        varName = 'meanExposure'; titleText = 'mean asymmetry';
    else
        varName = 'medianExposure'; titleText = 'median asymmetry';
    end
    positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
    xValues = signatureResults_cell.Haradhvala.(varName);
    yValues = signatureResults_cell.Besnard1k.(varName);
    labels = signatureShortNames;
    fontSize = 14;
    ignoreDist = 0.05; maxForce = 0.01; shiftDist = 0.015; scaleBorderForce= 1;
    hOriginalDots = plot(xValues, yValues, 'o');
    [xValuesText, yValuesText] = labelRepelSimple(xValues, yValues, labels, fontSize-4, ignoreDist, maxForce, shiftDist, scaleBorderForce);
    delete(hOriginalDots);
    plot(xValues, yValues, 'o', 'MarkerFaceColor', [1,.5,0], 'MarkerEdgeColor', [.8,.3,.2]);
    text(xValuesText, yValuesText, labels, 'HorizontalAlignment', 'center', 'FontSize', fontSize-4, 'Color', 0.5*[1,1,1]);
    xlabel('method 1 (from replication timing)', 'FontSize', fontSize);
    ylabel('method 2 (from NS-seq)', 'FontSize', fontSize);
    title(titleText, 'FontSize', fontSize);
    xLimVal = get(gca, 'XLim'); yLimVal = get(gca, 'YLim');
    text(xLimVal(1), yLimVal(2), {' ', sprintf('  r = %.4f', corr(xValues, yValues))}, 'FontSize', fontSize, 'FontAngle', 'italic');
    axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
    text(.5,.5,posTitle.letters{iS},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center');
end
mySaveAs(fig, imagesPath, 'FigS20');
% plot(-log10(signatureResults_cell.Haradhvala.pValue), -log10(signatureResults_cell.Besnard1k.pValue), 'o');
%%
iFigS = 21;
nR = 5; nC = 4; iS = 1; xS = .8; yS = 0.8; xB = 0.05; yB = 0.1; xM = 0.02; yM = 0.02;
nSignaturesPerPlot = nR*(nC/2);
for iFirstSignature = 1:nSignaturesPerPlot:nSignatures
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', 1.3*[0 0 30 25],'units','normalized','outerposition',[0 0 1 1]);
    iS = 1;
    maxSignature = iFirstSignature+nSignaturesPerPlot-1;
    lstSignaturesSorted = iFirstSignature:maxSignature; lstSignatures = lstSignaturesSorted;
    lstSignatures(1:2:nSignaturesPerPlot) = lstSignaturesSorted(1:nSignaturesPerPlot/2);
    lstSignatures(1+1:2:nSignaturesPerPlot) = lstSignaturesSorted(1+nSignaturesPerPlot/2:end);
    iListSignature = 0;
    for iR = 1:nR
        for iC = 1:nC
            iTerritoryName = mod(iC+1, 2)+1;
            territoryName = territoryNames{iTerritoryName};
            territoryNamePrint = territoryNamesPrint{iTerritoryName};
            if (iTerritoryName == 1)
                iListSignature = iListSignature + 1;
            end
            iSignature = lstSignatures(iListSignature);
            if (iSignature <= nSignatures)
                if (iC <= 2)
                    xB = 0.05; xM = 0.05;
                else
                    xB = 0.1; xM = 0.00;
                end
                positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
                plot_xLabel = false;
                
                if (iR == nR)
                    plot_xLabel = true;
                end
                title_text = '';
                isSampleUsed = signatureExposuresSimple_LeadLagg.(territoryName).meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
                plot_yLabel = true;
                plot_quartile = true;
                plot_histogram(signatureExposuresSimple_matrix.(territoryName), iSignature, isSampleUsed, signatureResults_cell.(territoryName), plot_quartile, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name)
                if (iTerritoryName == 1)
                    ylabel(signatureNames{iSignature}, 'FontSize', font_size+2, 'FontWeight', 'bold');
                else
                    ylabel('n samples', 'FontSize', font_size-2);
                end
                if (iR == 1)
                    title(territoryNamePrint, 'FontSize', font_size+4, 'FontWeight', 'bold');
                end
                %         axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
                %         text(.5,.5,posTitle.letters{iS-1},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center');
            end
        end
    end
    mySaveAs(fig, imagesPath, sprintf('FigS%d', iFigS)); iFigS = iFigS + 1;
end
%%
% nR = 4; nC = 4; iS = 1; xS = 1; yS = 0.9; xB = 0.1; yB = 0.05; xM = 0.02; yM = 0.02;
% fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', [0 0 30 25],'units','normalized','outerposition',[0 0 1 1]);
% for iSignature = 1:nR*nC
%     positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
%     plot(signatureExposuresSimple_LeadLagg.Haradhvala.meanType1Type2(iSignature, :), signatureExposuresSimple_LeadLagg.Besnard1k.meanType1Type2(iSignature, :), '.');
% end
%%