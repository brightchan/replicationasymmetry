function plotExposureResults_oriplotsSignatures(replData, territoriesSource, binSize)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
% nTotalSamples = size(tableAllSamples, 1);
% signatureExposuresSimple = replData.signatureExposuresSimple;
% nValues = replData.dataFields.nValues;
% signatureExposuresComplex = replData.signatureExposuresComplex;

colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.tissuesColourmap = hsv(nTissues);
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
signatureNames = replData.signatureNames;

nORIValues = replData.dataFields.nValues.DistanceFromORI_LeadLagg;
% maxValueBP = binSize*nORIValues/2;
% maxValueBP_text = sprintf('%dMbp', round(maxValueBP/1e6));

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
% simpleType = 'LeadLagg';
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
% summedExposures = (signatureExposuresSimple.(simpleType).type1 + signatureExposuresSimple.(simpleType).type2);
font_size = 12; font_name = 'Lucida Console';
%% ORI GRAPHS SIGNATURE ___________________________________________________
for iTissue = 0%[-1,0,1,2]%:nTissues
    if (iTissue > 0)
        tissue = tissueNames{iTissue};
        isThisT = strcmp(tableAllSamples.Tissue, tissue)';
    elseif (iTissue < 0)
        tissue = 'notPOLE';
        isThisT = ~strcmp(tableAllSamples.Tissue, 'POLE')';
    else
        tissue = 'all tissues';
        isThisT = true(1, size(tableAllSamples, 1));
    end
    minMeanExposure = 10; % 1000
    for complexType = {'DistanceFromORI_LeadLagg'};%, 'DistanceFromORI_LeadLagg_Early', 'DistanceFromORI_LeadLagg_Late'} %DistanceFromORI_LeadLagg DistanceFromORI_LeadLagg_Early DistanceFromORI_LeadLagg_Late
        complexType = complexType{1};
        signatureExposuresComplex_cell = replData.signatureExposuresComplex.(complexType);
        fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',2*[0 0 32 20],'units','normalized','outerposition',[0 0 1 1]); %2*[0 0 32 20]
        nC = 5; nR = ceil(nSignatures/nC); iS = 1; xS = .85; yS = .7;
        for iSignature = 1:nSignatures
            myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1; hold on;
            %isExposed = summedExposures(iSignature,listSamples)>0; % Only samples which are exposed to this signature are taken into account.
            isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
            plot_xLabel = (iSignature > nSignatures-nC);
            plot_yLabel = (mod(iSignature, nC) == 1);
            title_text = signatureNames{iSignature};
            plot_oriplot(signatureExposuresComplex_cell, iSignature, isSampleUsed, binSize, nORIValues, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name);
        end
    end
    suptitle(sprintf('%s mean exposure >= %d', strrep([territoriesSource, ' ', tissue, ' ', complexType], '_', ' '), minMeanExposure));
    mySaveAs(fig, imagesPath, ['aroundORI_', territoriesSource, '_', tissue, '_', complexType, '_', num2str(minMeanExposure), '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%