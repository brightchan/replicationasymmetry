function plot_replbar(signatureExposuresComplex_cell, iSignature, isSampleUsed, signatureResults, typeName, nCurrentValues, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size, font_name, legShiftHeightFactor)

if (sum(isSampleUsed)>0)
colourTitle = 0.3*[1,1,1];
    if (strcmp(typeName, 'double_type1_type2'))
        matrixToPlot = zeros(nCurrentValues, 2);
        maxValues = zeros(nCurrentValues,2);
        ciValues{1} = NaN*ones(nCurrentValues, 2);
        ciValues{2} = NaN*ones(nCurrentValues, 2);
        ts = tinv([0.025  0.975],sum(isSampleUsed)-1);      % T-Score for 95% CI
        for iValue = 1:nCurrentValues
            values1 = signatureExposuresComplex_cell{iValue}.type1(iSignature, (isSampleUsed));
            values2 = signatureExposuresComplex_cell{iValue}.type2(iSignature, (isSampleUsed));
            matrixToPlot(iValue, 1) = mean(values1);
            matrixToPlot(iValue, 2) = mean(values2);% & isNotOutlier)));
            maxValues(iValue) = max(matrixToPlot(iValue, :));
            ciValues{1}(iValue,:) = mean(values1) + ts*std(values1)/sqrt(length(values1));                      % Confidence Intervals
            ciValues{2}(iValue,:) = mean(values2) + ts*std(values2)/sqrt(length(values2));                      % Confidence Intervals
        end
        myCurrentColours = [colours.leading; colours.lagging];
        myDarkerColours = [colours.leadingDark; colours.laggingDark];
        myLighterColours = [colours.leadingNotSignif; colours.laggingNotSignif]; 
        if (sum(~isnan(matrixToPlot(:)) & matrixToPlot(:)~=0)>2)
            xShift = [-.2, .2];
            for iValue = 1:nCurrentValues
                bar(iValue+xShift(1), matrixToPlot(iValue, 1), 0.3, 'FaceColor', myCurrentColours(1,:), 'EdgeColor', (3*myCurrentColours(1,:))/4);
                bar(iValue+xShift(2), matrixToPlot(iValue, 2), 0.3, 'FaceColor', myCurrentColours(2,:), 'EdgeColor', (3*myCurrentColours(2,:))/4);
                %                 plot(iValue+xShift(1)*[1,1], ciValues{1}(iValue,:), '-', 'Color', myDarkerColours(1,:));
                %                 plot(iValue+xShift(2)*[1,1], ciValues{2}(iValue,:), '-', 'Color', myDarkerColours(2,:));
                plot(iValue+xShift(1), matrixToPlot(iValue, 1), 'o', 'MarkerFaceColor', myLighterColours(1,:), 'MarkerEdgeColor', myDarkerColours(1,:));
                plot(iValue+xShift(2), matrixToPlot(iValue, 2), 'o', 'MarkerFaceColor', myLighterColours(2,:), 'MarkerEdgeColor', myDarkerColours(2,:));
            end
            %             xShiftStar = [-.2, .2];
            %             if (matrixToPlot(end, 1) <= matrixToPlot(end, 2))
            %                 xShiftStar(1) = .4;
            %             end
            for iType = 1:2
                linearModel = fitlm((1:nCurrentValues), matrixToPlot(:,iType)); 
                plot((1:nCurrentValues)+xShift(iType), linearModel.Fitted, 'k', 'LineWidth', 2, 'Color', myDarkerColours(iType,:));
                text(nCurrentValues+xShift(iType), 1.05*matrixToPlot(nCurrentValues, iType), [' ', repmat('*', 1, getStarsFromPValueBasic(coefTest(linearModel, [0,1])))], 'HorizontalAlignment', 'center', 'FontSize', 20, 'FontWeight', 'bold', 'Color', myDarkerColours(iType,:));%'VerticalAlignment', 'bottom', 
                %text(nCurrentValues+xShiftStar(iType), linearModel.feval(nCurrentValues), [' ', repmat('*', 1, getStarsFromPValueBasic(coefTest(linearModel, [0,1])))], 'FontSize', 20, 'FontWeight', 'bold', 'Color', myDarkerColours(iType,:));
            end
        end
        if (~isempty(xLabel_text))
            %             h = legend({'matching', 'inverse'}, 'Location', 'SouthWest', 'FontSize', font_size, 'FontName', font_name);
            %             legPos = get(h, 'Position'); legPos(2) = legPos(2) - 2*legPos(4); legPos(1) = legPos(1) - legPos(2); set(h, 'Position', legPos);
            h = legend({'matching', 'inverse'}, 'Location', 'South', 'FontSize', font_size, 'FontName', font_name); legPos = get(h, 'Position'); legPos(2) = legPos(2) - legShiftHeightFactor*legPos(4); set(h, 'Position', legPos);
        end
    else
        matrixToPlot = zeros(nCurrentValues, 1);
        maxValues = zeros(nCurrentValues, 1);
        for iValue = 1:nCurrentValues
            matrixToPlot(iValue) = mean(signatureExposuresComplex_cell{iValue}.(typeName)(iSignature, (isSampleUsed)));% & isNotOutlier)));
            maxValues(iValue) = max([mean(signatureExposuresComplex_cell{iValue}.type1(iSignature, (isSampleUsed))), ...% & isNotOutlier))), ...
                mean(signatureExposuresComplex_cell{iValue}.type2(iSignature, (isSampleUsed)))]);% & isNotOutlier)))]);
        end
        if (strcmp(typeName, 'type1Minustype2'))
            %         if (signatureResults.hasStrandAsymmetry(iSignature))
            myCurrentColours = [colours.lagging; colours.leading];
            %         else
            %             myCurrentColours = [colours.laggingNotSignif; colours.leadingNotSignif];
            %         end
            %             colourTitle = 0.3*[1,1,1];%signatureResults.titleColor(iSignature,:);
        else
            myCurrentColours = [0.8*[1,1,1]; 0.5*[1,1,1]];
            %             colourTitle = 0.3*[1,1,1];
        end
        if (sum(~isnan(matrixToPlot) & matrixToPlot~=0)>2)
            for iValue = 1:nCurrentValues
                bar(iValue, matrixToPlot(iValue), 'FaceColor', myCurrentColours((matrixToPlot(iValue)>0)+1,:), 'EdgeColor', 'none');
            end            
            linearModel = fitlm((1:nCurrentValues), matrixToPlot); plot((1:nCurrentValues), linearModel.Fitted, 'k', 'LineWidth', 3);
            text(nCurrentValues, linearModel.feval(nCurrentValues), repmat('*', 1, getStarsFromPValueBasic(coefTest(linearModel, [0,1]))), 'FontSize', 20, 'FontWeight', 'bold');
        end
        xlabel(xLabel_text, 'FontSize', font_size, 'FontName', font_name);
    end
    if (sum(~isnan(matrixToPlot(:)) & matrixToPlot(:)~=0)>2)
        ylabel(yLabel_text, 'FontSize', font_size, 'FontName', font_name);
        set(gca, 'XTick', [1, nCurrentValues], 'XTickLabel', labelsExtremes); set(gca, 'FontSize', font_size, 'FontName', font_name);
        if (ismember(typeName, {'type1', 'type2', 'double_type1_type2'})) %(iType > 2)
            ylim([0, max(maxValues(:)*1.05)]);
        end
        title(title_text, 'Color', colourTitle, 'FontSize', font_size, 'FontName', font_name); drawnow;
    end
    xlim([0, nCurrentValues+1]);
    set(gca, 'FontSize', font_size, 'FontName', font_name);
    drawnow;
end