function plot_volcano_main(tableSignatures, signatureShortNames, iType, xVar, xLabelText, yVar, significantVar, plottedLabelsVar, positiveColour, positiveText, negativeColour, negativeText, titleText)

%%
nColours = 100;
yLabelText = '-log_{10} p-value';
nSignatures = length(signatureShortNames);
cmap_general = ([colourLinspace(negativeColour, 0.9*[1,1,1], nColours); 0.5*[1,1,1]; colourLinspace(0.9*[1,1,1], positiveColour, nColours)]);
maxVal = (max(abs(tableSignatures.(xVar))));

tmpValues = round(nColours*tableSignatures.(xVar)/maxVal) + nColours+1;
tmpValues(~tableSignatures.(significantVar)) = nColours+1;
cmap = cmap_general(tmpValues, :);
font_size = 16;
colormap(gca, cmap_general);

cmap_edge = cmap/2;
%     if (iType == 2)
%         cmap_edge = repmat(colours.leadingDark, nColours, 1);
%     elseif (iType == 3)
%         cmap_edge = repmat(colours.laggingDark, nColours, 1);
%     end

[~, listSignatures] = sort(tableSignatures.(yVar));

if (iType == 1)
    markerSizeValue = 12;
else
    markerSizeValue = 10;
end

hOriginalDots = zeros(nSignatures, 1);
for iSignature = listSignatures'
    hOriginalDots(iSignature) = plot(tableSignatures.(xVar)(iSignature), tableSignatures.(yVar)(iSignature), 'o', 'MarkerFaceColor', cmap(iSignature, :), 'MarkerSize', markerSizeValue, 'MarkerEdgeColor', cmap_edge(iSignature, :)); %10
end

ignoreDist = 0.05; maxForce = 0.01; shiftDist = 0.015; scaleBorderForce= 1;
if (ismember(iType, [2,3]))
    xlim([-3, 3]); ylim([0, 4]); ignoreDist = 0.1; shiftDist = 0.02; 
end
if (ismember(iType, [5]))
    scaleBorderForce = 0;
end
yLimVal = get(gca, 'YLim'); xLimVal = get(gca, 'XLim'); xMax = max(abs(xLimVal)); %xMax = 3; %TODO
xLimVal = [-xMax, xMax]; xlim(xLimVal); % plot(get(gca, 'XLim'), -log10(0.05)*[1,1], '-r');
plot(0*[1,1], yLimVal, ':k');
text(xLimVal(1), 0, negativeText, 'Color', negativeColour, 'HorizontalAlignment', 'left', 'FontSize', font_size-1, 'VerticalAlignment', 'top');
text(xLimVal(2), 0, positiveText, 'Color', positiveColour, 'HorizontalAlignment', 'right', 'FontSize', font_size-1, 'VerticalAlignment', 'top');
colorbar('SouthOutside', 'Ticks', []); %[0,1], 'TickLabels', {'negativeText', 'positiveText'});

xlabel(xLabelText, 'FontSize', font_size); type = 'medianDifference';
ylabel(yLabelText, 'FontSize', font_size);
set(gca, 'FontSize', font_size); % title(sprintf('%s mean exposure >= %d', strrep(territoriesSource, '_', '\_'), minMeanExposure));

isPlotted = tableSignatures.(plottedLabelsVar);
for iSignature = listSignatures(~isPlotted(listSignatures))'
    plot(tableSignatures.(xVar)(iSignature), tableSignatures.(yVar)(iSignature), 'o', 'MarkerFaceColor', cmap(iSignature, :), 'MarkerSize', markerSizeValue, 'MarkerEdgeColor', cmap_edge(iSignature, :));
end
% labelRepel(tableSignatures.(xVar)(tableSignatures.(plottedLabelsVar)), tableSignatures.(yVar)(tableSignatures.(plottedLabelsVar)), 0, 0, signatureShortNames(tableSignatures.(plottedLabelsVar)), font_size-3);
xValues = tableSignatures.(xVar)(isPlotted);
yValues = tableSignatures.(yVar)(isPlotted);
labels = signatureShortNames(isPlotted);
fontSize = font_size - 3;
[xValuesText, yValuesText] = labelRepelSimple(xValues, yValues, labels, fontSize, ignoreDist, maxForce, shiftDist, scaleBorderForce);
% 
% labelRepel(tableSignatures.(xVar)(tableSignatures.(plottedLabelsVar)), tableSignatures.(yVar)(tableSignatures.(plottedLabelsVar)), signatureShortNames(tableSignatures.(plottedLabelsVar)), font_size-3);
delete(hOriginalDots);

for iSignature = listSignatures(isPlotted(listSignatures))'
    plot(tableSignatures.(xVar)(iSignature), tableSignatures.(yVar)(iSignature), 'o', 'MarkerFaceColor', cmap(iSignature, :), 'MarkerSize', markerSizeValue, 'MarkerEdgeColor', cmap_edge(iSignature, :));
end
isSignificant = tableSignatures.(significantVar)(isPlotted);
text(xValuesText(isSignificant), yValuesText(isSignificant), labels(isSignificant), 'HorizontalAlignment', 'center', 'FontSize', fontSize); 
text(xValuesText(~isSignificant), yValuesText(~isSignificant), labels(~isSignificant), 'HorizontalAlignment', 'center', 'FontSize', fontSize, 'Color', 0.5*[1,1,1]); 

if (~isempty(titleText))
    title(titleText);
end
