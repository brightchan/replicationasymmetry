function plot_histogram(signatureExposuresSimple_matrix, iSignature, isSampleUsed, signatureResults, plot_quartile, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name)
% isExposed = summedExposures(iSignature,listSamples)>0; % Only samples which are exposed to this signature are taken into account.
% isSampleUsed = signatureExposuresSimple.(simpleType).type1Minustype2(iSignature, listSamples(isExposed));
% signatureExposuresSimple_matrix = signatureExposuresSimple.(simpleType).type1Minustype2;
% plot_xLabel = (iSignature > nSignatures-nC)
% plot_yLabel = (mod(iSignature, nC) == 1)
% title_text = signatureNames{iSignature}
% font_size = 10;
%% HISTOGRAMS SIGNATURES ___________________________________________________
exposuresDifference = signatureExposuresSimple_matrix(iSignature, isSampleUsed);
if (sum(isSampleUsed)>0)
    bins.max = max(abs(exposuresDifference(:))); %4.2;%1e-4;
    sortedAbs = sort(abs(exposuresDifference(:)));
    bins.quartile95 = sortedAbs(round(end*0.95));
    bins.quartile97 = sortedAbs(round(end*0.97));
    bins.quartile99 = sortedAbs(round(end*0.99));
    bins.valueMax = max(exposuresDifference);
    bins.valueMin = min(exposuresDifference);
    if (plot_quartile && bins.quartile97*2 < bins.max)
        bins.max = bins.quartile97;
        meanExposure = mean(exposuresDifference(abs(exposuresDifference) <= bins.max));
    else
        meanExposure = mean(exposuresDifference);
        p = signtest(exposuresDifference);
        if (p < 0.05)
            signatureResults.colourPositiveBars(iSignature,:) = colours.leading;
            signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
            %signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
        else
            signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif;
            signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
            %signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;%colours.titleNotSignif;
        end
    end
    medianExposure = median(exposuresDifference);    
    %     myCurrentColours = [colours.leading; colours.lagging];
    myDarkerColours = [colours.laggingDark; colours.leadingDark];
    myLighterColours = [colours.laggingNotSignif; colours.leadingNotSignif];
    bins.step = bins.max/20;
    bins.range = -bins.max:bins.step:bins.max;
    bins.values = histc(exposuresDifference, bins.range);
    tmp = bins.values; tmp(bins.range>0) = 0; bar(bins.step/2+bins.range, tmp, 'FaceColor', signatureResults.colourNegativeBars(iSignature,:), 'EdgeColor', (3*signatureResults.colourNegativeBars(iSignature,:))/4, 'BarWidth', 1); hold on;
    tmp = bins.values; tmp(bins.range<0) = 0; bar(bins.step/2+bins.range, tmp, 'FaceColor', signatureResults.colourPositiveBars(iSignature,:), 'EdgeColor', (3*signatureResults.colourPositiveBars(iSignature,:))/4, 'BarWidth', 1);
    set(gca, 'XColor', signatureResults.colourGCA(iSignature,:), 'YColor', signatureResults.colourGCA(iSignature,:), 'Box', 'off');
    yLimVal = get(gca, 'YLim'); %xLimVal = get(gca, 'XLim');
    xlim([bins.range(1), bins.range(end)]);
    plot(medianExposure, 0.9*yLimVal(end), 's', 'MarkerFaceColor', myLighterColours(1+(medianExposure>0),:), 'MarkerEdgeColor', myDarkerColours(1+(medianExposure>0),:), 'MarkerSize', 8);
    plot(meanExposure, 0.9*yLimVal(end), 'o', 'MarkerFaceColor', myLighterColours(1+(meanExposure>0),:), 'MarkerEdgeColor', myDarkerColours(1+(meanExposure>0),:), 'MarkerSize', 5);
    plot(0*[1,1], yLimVal, ':k');
    if (bins.valueMax > bins.max)
        text(bins.range(end), (1*yLimVal(2)+5*yLimVal(1))/6, sprintf('%d ', sum(exposuresDifference>bins.max)), 'Color', signatureResults.colourPositiveBars(iSignature,:), 'HorizontalAlignment', 'right', 'FontSize', font_size-2, 'FontName', font_name, 'FontWeight', 'bold');
    end
    if (bins.valueMin < -bins.max)
        text(bins.range(1), (1*yLimVal(2)+5*yLimVal(1))/6, sprintf(' %d', sum(exposuresDifference<-bins.max)), 'Color', signatureResults.colourNegativeBars(iSignature,:), 'HorizontalAlignment', 'left', 'FontSize', font_size-2, 'FontName', font_name, 'FontWeight', 'bold');
    end
    if (plot_yLabel) %(mod(iSignature, nC) == 1)
        ylabel('n samples', 'FontSize', font_size, 'FontName', font_name); %number of samples
    end
    if (plot_xLabel) %(iSignature > nSignatures-nC)
        text(bins.range(1), 0, {' ', ' ', ' ', 'inverse'}, 'Color', colours.laggingDark, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle', 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold');
        text(bins.range(end), 0, {' ', ' ', ' ', 'matching'}, 'Color', colours.leadingDark, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'middle', 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold');
        yLimVal = get(gca, 'YLim'); 
        hDots(1) = plot(1, 3*yLimVal(end), 's', 'MarkerFaceColor', 0.8*[1,1,1], 'MarkerEdgeColor', 0.2*[1,1,1], 'MarkerSize', 8);
        hDots(2) = plot(1, 3*yLimVal(end), 'o', 'MarkerFaceColor', 0.8*[1,1,1], 'MarkerEdgeColor', 0.2*[1,1,1], 'MarkerSize', 5);
        ylim(yLimVal);
        %         h = legend(hDots, {'median', 'mean'}, 'Location', 'South', 'FontSize', font_size, 'FontName', font_name); legPos = get(h, 'Position'); legPos(2) = legPos(2) - 2*legPos(4); set(h, 'Position', legPos);
        legend(hDots, {'median', 'mean'}, 'Location', 'NorthWest', 'FontSize', font_size, 'FontName', font_name); %legPos = get(h, 'Position'); legPos(2) = legPos(2) - 2*legPos(4); set(h, 'Position', legPos);
        %         xlabel('{\color[rgb]{1 0 0}main} - {\color[rgb]{0 0 1}side} exposure', 'FontSize', font_size, 'FontName', font_name);
        xlabel({' ', 'matching - inverse exposure'}, 'FontSize', font_size, 'FontName', font_name);
    end
    if (~isempty(title_text))
        title(title_text, 'Color', signatureResults.colourTitle(iSignature,:), 'FontSize', font_size, 'FontName', font_name);
    end
    set(gca, 'FontSize', font_size, 'FontName', font_name);
    drawnow;
end
