function plot_volcano_insertTiming(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, mfTypeName, yLabelTextInset)
font_size = 12;
insetVar = ['RT_perSamples_percentageSignificantSlope_', mfTypeName];
insetVarPositive = ['RT_perSamples_percentagePositiveSlope_', mfTypeName];
insetVarNegative = ['RT_perSamples_percentageNegativeSlope_', mfTypeName];
tableSignatures.(insetVar) = tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName]) + tableSignatures.(['RT_perSamples_percentageNegativeSlope_', mfTypeName]);
[~, listSignatures] = sort(tableSignatures.(insetVar), 'descend');
nSignatures = length(signatureShortNames);

nColours = 100;
cmap_general = colourLinspace(0.9*[1,1,1], positiveColour, nColours);
maxVal = (max(abs(tableSignatures.(insetVarPositive))));
cmapPositive = cmap_general(round((nColours-1)*tableSignatures.(insetVarPositive)/maxVal)+1, :);
cmapPositive(~tableSignatures.(significantVar), 1) = 0.5;
cmapPositive(~tableSignatures.(significantVar), 2) = 0.5;
cmapPositive(~tableSignatures.(significantVar), 3) = 0.5;

cmap_general = colourLinspace(0.9*[1,1,1], negativeColour, nColours);
maxVal = (max(abs(tableSignatures.(insetVarNegative))));
cmapNegative = cmap_general(round((nColours-1)*tableSignatures.(insetVarNegative)/maxVal)+1, :);
cmapNegative(~tableSignatures.(significantVar), 1) = 0.5;
cmapNegative(~tableSignatures.(significantVar), 2) = 0.5;
cmapNegative(~tableSignatures.(significantVar), 3) = 0.5;

%fig = createMaximisedFigure(5);
% axes('Position', [0.13, 0.5, 0.36, 0.4]); set(gca, 'color', 'none'); hold on; % fig = createMaximisedFigure(3); hold on;         bar(1:nSignatures, stackedValues(listSignatures,:), 'stacked');
for iCol = 1:nSignatures
    bar(iCol, tableSignatures.(insetVarPositive)(listSignatures(iCol)), 'FaceColor',  cmapPositive(listSignatures(iCol), :), 'EdgeColor', cmapPositive(listSignatures(iCol), :)/2);
    bar(iCol, -tableSignatures.(insetVarNegative)(listSignatures(iCol)), 'FaceColor',  cmapNegative(listSignatures(iCol), :), 'EdgeColor', cmapNegative(listSignatures(iCol), :)/2);
end

xValues = 1:nSignatures;
yValues = tableSignatures.(insetVarPositive)(listSignatures)'; %yValuesOriginal = yValues;
% lstShifted = 4:2:nSignatures;
% yValues(lstShifted) = yValues(lstShifted-1)+2; 
% plot([xValues(lstShifted); xValues(lstShifted)], [yValuesOriginal(lstShifted)+0.05; yValues(lstShifted)], '-k');
text(xValues, yValues, signatureShortNames(listSignatures), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', font_size-2); %font_size-4
%text(xValues, -tableSignatures.(insetVarNegative)(listSignatures), signatureShortNames(listSignatures), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', font_size-3);
set(gca, 'XTick', [], 'XTickLabel', []); %ylim([-50, 50]); % ylim([-25, 40]);

if (ismember(mfTypeName, {'matching', 'inverse'}))
    yLimVal =[-5, 36]; ylim(yLimVal); 
end
yLimVal = get(gca, 'YLim'); %       %yLimVal = get(gca, 'YLim'); %yMax = max(abs(yLimVal)); yLimVal = [-yMax, yMax]; ylim(yLimVal);
yTickVal = get(gca, 'YTick');
yTickLabelVal = yTickVal; yTickLabelVal(yTickLabelVal<0) = -1*yTickLabelVal(yTickLabelVal<0);
set(gca, 'YTick', yTickVal, 'YTickLabel', yTickLabelVal, 'FontSize', font_size, 'XColor', 'none'); %1*[1,1,1]);
text(.5, yLimVal(2)+2, {'Samples with', 'positive slope (%)'}, 'FontSize', font_size, 'Color', positiveColour, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'bottom');
text(.5, yLimVal(1)+2, {'Samples with', 'negative slope (%)'}, 'FontSize', font_size, 'Color', negativeColour, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'top');
ylabel(yLabelTextInset, 'FontSize', font_size+1);
ylim(yLimVal);

% text(-nSignatures/12, yLimVal(2)/2, {'Samples with', 'positive slope (%)'}, 'FontSize', font_size, 'Rotation', 90, 'Color', positiveColour, 'HorizontalAlignment', 'center');
% text(-nSignatures/12, yLimVal(1)/2, {'Samples with', 'negative slope (%)'}, 'FontSize', font_size, 'Rotation', 90, 'Color', negativeColour, 'HorizontalAlignment', 'center');


%ylabel(yLabelTextInset, 'FontSize', font_size); % title(sprintf('%s mean exposure >= %d', strrep(territoriesSource, '_', '\_'), minMeanExposure));

%         stackedValues = [tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName]), tableSignatures.(['RT_perSamples_percentageNegativeSlope_', mfTypeName]), tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName])];
%         stackedValues(:,3) = 100-stackedValues(:,1)-stackedValues(:,2);

%         text(xValues(isPositive), tableSignatures.(insetVar)(listSignatures(isPositive)), signatureShortNames(listSignatures(isPositive)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', font_size-3);
%         text(xValues(~isPositive), tableSignatures.(insetVar)(listSignatures(~isPositive)), signatureShortNames(listSignatures(~isPositive)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', font_size-3);

% text(xValues(isPositive), tableSignatures.percentagePositiveSamples(listSignatures(isPositive))-50, signatureShortNames(listSignatures(isPositive)), 'Rotation', 45, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'bottom', 'FontSize', font_size-3);
% text(xValues(~isPositive), tableSignatures.percentagePositiveSamples(listSignatures(~isPositive))-50, signatureShortNames(listSignatures(~isPositive)), 'Rotation', 45, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'FontSize', font_size-3);
