% function plotExposureResults_heatmapSummary(replData, territoriesSource, decomposedSignatures)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
% signatureExposuresSimple = replData.signatureExposuresSimple;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', 'S');

colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);

% %%
% for field = {'total', 'type1', 'type2'}
%     field = field{1};
%     tmp1 = 100*signatureExposuresSimple.LeadLagg.(field)./repmat(sum(signatureExposuresSimple.LeadLagg.(field)), nSignatures, 1);
%     isBelow1 = tmp1 < 5; % All too small exposures will be set to 0.
%     signatureExposuresSimple.LeadLagg.(field)(isBelow1) = 0;
% end
% signatureExposuresSimple.LeadLagg.type1Minustype2 = (signatureExposuresSimple.LeadLagg.type1 - signatureExposuresSimple.LeadLagg.type2);

diffNormalised = signatureExposuresSimple_LeadLagg.type1Minustype2;
diffNormalised = diffNormalised./repmat(nanstd(diffNormalised), nSignatures, 1); % The mean stays the same
diffNormalised(isnan(diffNormalised)) = 0;
%%
xVal = signatureExposuresSimple_LeadLagg.type1Minustype2; scalingFactor = 1; yLab = {'{\bfExposure asymmetry}', '(main direction - side direction)'}; type = 'diff';
% xVal = log2(signatureExposuresSimple_LeadLagg.type1./signatureExposuresSimple_LeadLagg.type2); scalingFactor = 400; yLab = {'{\bfExposure asymmetry}', 'log2(main/side)'}; type = 'log';
minMeanExposure = 10;%1000;
maxYValue = 10000;



resultsPerCTPerSignature.percExposedPatients = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.meanRank = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.pValSignTest = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.quantile25 = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.quantile50 = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.quantile75 = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.percPositive = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.averageDiff = NaN*ones(nSignatures, nTissues);
resultsPerCTPerSignature.averageDiffNormalised = NaN*ones(nSignatures, nTissues);
nSamplesPerTissue = NaN*ones(nTissues, 1);
nTests = 0;
for iTissue = 1:nTissues
    isSampleThisTissue = ismember(tableAllSamples.Tissue, tissueNames{iTissue})';
    nSamplesPerTissue(iTissue) = sum(isSampleThisTissue);
    for iSignature = 1:nSignatures
        isOkSample2 = isSampleThisTissue & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
        exposures = signatureExposuresSimple_LeadLagg.total(iSignature, isSampleThisTissue);
        exposuresMean = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, isSampleThisTissue);
        exposuresDiff = signatureExposuresSimple_LeadLagg.type1Minustype2(iSignature, isOkSample2);
        [~, absoluteRanks] = sort(abs(exposuresDiff));
        nonAbsoluteRanks = absoluteRanks;
        nonAbsoluteRanks(exposuresDiff < 0) = -1 * nonAbsoluteRanks(exposuresDiff < 0);
        resultsPerCTPerSignature.meanRank(iSignature, iTissue) = mean(nonAbsoluteRanks);
        tmp = exposuresDiff;
        tmp(exposuresDiff > 0) = 1;
        tmp(exposuresDiff < 0) = -1;
        resultsPerCTPerSignature.meanRank(iSignature, iTissue) = mean(tmp);
        if (sum(~isnan(exposuresDiff))>0)
            resultsPerCTPerSignature.pValSignTest(iSignature, iTissue) = signtest(exposuresDiff); %signrank signtest
            nTests = nTests + 1;
        end
        resultsPerCTPerSignature.percExposedPatients(iSignature, iTissue) = mean(exposures > 0); %exposuresMean > 100);
        
        isOkSample = isSampleThisTissue & ~isnan(xVal(iSignature,:)) & ~isinf(xVal(iSignature,:)) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
        currentValues = xVal(iSignature, isOkSample);
        currentQtls = quantile(currentValues, [0.25,0.5,0.75]);
        resultsPerCTPerSignature.quantile25(iSignature, iTissue) = currentQtls(1);
        resultsPerCTPerSignature.quantile50(iSignature, iTissue) = currentQtls(2);
        resultsPerCTPerSignature.quantile75(iSignature, iTissue) = currentQtls(3);
        resultsPerCTPerSignature.percPositive(iSignature, iTissue) = mean(currentValues>0);
        resultsPerCTPerSignature.averageDiff(iSignature, iTissue) = mean(currentValues);
        resultsPerCTPerSignature.averageDiffNormalised(iSignature, iTissue) = mean(diffNormalised(iSignature, isOkSample2));
    end
end
isNoData = isnan(resultsPerCTPerSignature.pValSignTest);
resultsPerCTPerSignature.pValSignTest(isNoData) = 1;
[~, tmp] = mafdr(resultsPerCTPerSignature.pValSignTest(:));
resultsPerCTPerSignature.qValSignTest = reshape(tmp, nSignatures, nTissues);
% resultsPerCTPerSignature.meanRank(resultsPerCTPerSignature.pValSignTest > 0.05/length(resultsPerCTPerSignature.pValSignTest(:))) = 0;
resultsPerCTPerSignature.meanRank(resultsPerCTPerSignature.qValSignTest >= 0.05) = 0;
resultsPerCTPerSignature.percPositive(resultsPerCTPerSignature.qValSignTest >= 0.05) = 0.5;
resultsPerCTPerSignature.averageDiffNormalised(isNoData) = 0;
resultsPerCTPerSignature.averageDiffNormalised(resultsPerCTPerSignature.pValSignTest >= 0.05) = 0;
% resultsPerCTPerSignature.averageDiffNormalised(resultsPerCTPerSignature.qValSignTest >= 0.05) = 0;
% resultsPerCTPerSignature.averageDiffNormalised(resultsPerCTPerSignature.pValSignTest >= 0.05/(nSignatures*nTissues)) = 0;
resultsPerCTPerSignature.isStrictlySignificant_3star = (resultsPerCTPerSignature.pValSignTest < 0.001/(nSignatures*nTissues));
resultsPerCTPerSignature.isStrictlySignificant_2star = (resultsPerCTPerSignature.pValSignTest < 0.01/(nSignatures*nTissues));
resultsPerCTPerSignature.isStrictlySignificant_1star = (resultsPerCTPerSignature.pValSignTest < 0.05/(nSignatures*nTissues));
%%
nC = 400;
% cmp = colourLinspace([0,0,1], [1,0,0], 2*nC+1);
% leadColours = cmp(nC+1:end,:); %colourLinspace([1,1,1], [1,0,0], nC);
% interColours = cmp(nC,:); %colourLinspace([1,1,1], [1,0,0], nC);
% laggColours = flipud(cmp(1:nC,:)); %colourLinspace([1,1,1], [0,0,1], nC);
leadColours = colourLinspace([1,1,1], [1,0,0], nC);
interColours = [1,1,1];
laggColours = colourLinspace([1,1,1], [0,0,1], nC);
nLines = 100;
colours.lines = 0.8*[1,1,1];

fig = createMaximisedFigure(2); colormap([colourLinspace(colours.laggingDark, [1,1,1], 50); 0.9*[1,1,1]; colourLinspace([1,1,1], colours.leadingDark, 50)]); %lbmap(100, 'RedBlue')
myGeneralSubplot(1,1,1,1,1,0.1,0.05,0.05,0.23);%,xB,yB,xM,yM);
matrixToPlot = resultsPerCTPerSignature.averageDiffNormalised;
imagesc(matrixToPlot); 
% imagesc(resultsPerCTPerSignature.meanRank); hold on;
maxVal = max(abs(matrixToPlot(:)));
% maxVal = max(abs([min(matrixToPlot(:)), max(matrixToPlot(:))]));
caxis([-maxVal, maxVal]);
% caxis([0,1]);
h = colorbar; hold on;
ylabel(h, 'mean zscore difference of {\bfmatching} - {\bfinverse}', 'FontSize', 12);
for iRow = 1:nSignatures
    plot(0.5+[0,nTissues], (0.5+iRow)*[1,1], '-', 'Color', colours.lines);
end
for iCol = 1:nTissues
    plot((0.5+iCol)*[1,1], 0.5+[0,nSignatures], '-', 'Color', colours.lines);
    text(iCol, 0.5+nSignatures, sprintf('n=%d', sum(nSamplesPerTissue(iCol))), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', 9);
end
for iRow = 1:nSignatures
    for iCol = 1:nTissues
        starText = '';
        if (resultsPerCTPerSignature.isStrictlySignificant_3star(iRow, iCol))
            starText = '***';
        elseif (resultsPerCTPerSignature.isStrictlySignificant_2star(iRow, iCol))
            starText = '**';
        elseif (resultsPerCTPerSignature.isStrictlySignificant_1star(iRow, iCol))
            starText = '*';
        end
        text(0.5+iCol, -0.5+iRow, starText, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'Color', 0.6*[1,1,1]); %sprintf('%s %s', signatureShortNames{iRow}, tissueNames{iCol})); 
    end
end

% for iSignature = 1:nSignatures
%     for iTissue = 1:nTissues
%         if (resultsPerCTPerSignature.qValSignTest(iSignature, iTissue) < 0.05)
%             xMin = iTissue-0.5; xMax = xMin + 1; yMin = iSignature-0.5; yMax = yMin + 1; direction = 'x'; 
%             cMin = resultsPerCTPerSignature.quantile25(iSignature, iTissue); cMax = resultsPerCTPerSignature.quantile75(iSignature, iTissue);
%             %plot([xMin, xMax], [yMin, yMax], '-r');
%             plotColourGradientBox(xMin, xMax, yMin, yMax, cMin, cMax, direction, leadColours, interColours, laggColours, scalingFactor, nLines); drawnow;
%         end
%     end
% end
set(gca, 'YTick', 1:nSignatures, 'YTickLabel', signatureNames, 'XTick', []);
text(1:nTissues, 0*(1:nTissues), strrep(tissueNames, '_', ' '), 'Rotation', 55); %, 'interpret', 'none'
% suptitle(strrep(territoriesSource, '_', ' '));
mySaveAs(fig, imagesPath, 'FigS1');
% mySaveAs(fig, imagesPath, ['summary_', territoriesSource, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
%%