%% script_step3_plotExposureResults.m
clear; addpath(genpath('code/'));
savePathDiary = 'diaries/'; createDir(savePathDiary); scriptName = 'script_step1_prepareCancerTissues'; compName = 'HOME';
diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
%%
% prepareOfficialSignatures; % Prepares files with the signatures using a file downloaded from http://cancer.sanger.ac.uk/cancergenome/assets/signatures_probabilities.txt.
tissueNames = {'MSI_19', 'POLE_14', 'blood_lymphoid_274', 'blood_myeloid_56', 'bone_98', 'brain_237', 'breast_560', 'colorectum_35', 'gastric_90', 'kidney_clear_cell_95', 'liver_303', 'lung_adenocarcinoma_24', ...
    'lung_squamous_36', 'oesophagus_adenocarcinoma_219', 'oral_25', 'ovary_93', 'pancreas_406', 'prostate_294', 'skin_183'}; 
territoriesSource = 'Haradhvala_territories';
nMaxSignatures = 7; 
nRuns = 100;
minStability = 0.8;
alwaysComputeDecomposition = false;
alwaysComputeClustering = false;
savePathBasic = 'save/';
evaluationOfDecomposedSignatures = loadEvaluationOfDecomposedSignatures(alwaysComputeDecomposition, savePathBasic, tissueNames, nMaxSignatures, nRuns, minStability, territoriesSource);
decomposedSignatures = loadDecomposedSignatures(alwaysComputeClustering, savePathBasic, tissueNames, evaluationOfDecomposedSignatures, territoriesSource);
alwaysComputeReplication = false;
%%
% dirDataCT_Besnard1k = 'save/Besnard1k_territories/replPrepareTables1216/'; nameDataCT_Besnard1k = 'replPrepareTables1216'; territoriesSource_Besnard1k = 'Besnard1k_territories'; binSize_Besnard1k = 1000;
% tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource_Besnard1k, '.mat'];  nameTerritories = territoriesSource_Besnard1k;
% [signatureExposuresComplex_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_Besnard1k, tableAllSamples_Besnard1k] = loadSignatureExposuresComplex_cell(alwaysComputeReplication, savePathBasic, dirDataCT_Besnard1k, nameDataCT_Besnard1k, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
% signatureExposuresSimple_LeadLagg_cell_Besnard1k = loadSignatureExposuresSimple_LeadLagg_cell(alwaysComputeReplication, savePathBasic, dirDataCT_Besnard1k, nameDataCT_Besnard1k, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
%%
dirDataCT = 'save/Haradhvala_territories/replPrepareTables_AllRegions/'; nameDataCT = 'replPrepareTables_AllRegions'; territoriesSource = 'Haradhvala_territories'; binSize = 20000;
% dirDataCT = 'save/Haradhvala_territories/replPrepareTables_OutsideAllGenesAndBlacklisted/'; nameDataCT = 'replPrepareTables_OutsideAllGenesAndBlacklisted'; territoriesSource = 'Haradhvala_territories'; binSize = 20000;
tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource, '.mat'];  nameTerritories = territoriesSource; % created in script_replPrepareTerritories (originally script_60726_replPrepareTables)
replData = loadReplicationData(alwaysComputeReplication, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
plotFigures = true;
if (plotFigures)
    plotExposureResults_volcanoPlot(replData, territoriesSource, savePathBasic);
%     plotExposureResults_combined(replData, territoriesSource, binSize);
%     plotExposureResults_heatmapSummary(replData, territoriesSource, decomposedSignatures);
%     plotExposureResults_validatePatterns(replData, territoriesSource);
%     plotExposureResults_directionalSignatures(replData, territoriesSource, decomposedSignatures);
    %         plotExposureResults_scatterPlot(replData, territoriesSource);
    %         plotExposureResults_heatmapTrinucleotides(replData, territoriesSource);
    %         plotExposureResults_heatmapSignatures(replData, territoriesSource);
    %         [signatureResults] = plotExposureResults_histogramSignatures(replData, territoriesSource);
    %         plotExposureResults_barplotTiming(replData, territoriesSource, signatureResults);
    %         plotExposureResults_oriplotsTrinucleotides(replData, territoriesSource);
    %         plotExposureResults_oriplotsSignatures(replData, territoriesSource, binSize);
    %         plotExposureResults_clustering(replData, territoriesSource, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k);
end
%%
printNumbers = false;
if (printNumbers)
    cancerTypes = replData.tableCancerTypes.CancerType; nCT = length(cancerTypes);
    for iCT = 1:nCT
        fprintf('%s: %d.\n', cancerTypes{iCT}, sum(strcmp(replData.tableAllSamples.cancerType, cancerTypes{iCT})));
    end
end
%%
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
diary off;
if (sendingEmails)
    sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
end