%% script_step2_evaluateDecomposedSignatures.m
clear; addpath(genpath('code/'));
savePathDiary = 'diaries/'; createDir(savePathDiary); scriptName = 'script_step2_evaluateDecomposedSignatures'; compName = 'HOME';
diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
%% 
% prepareOfficialSignatures;
% tissueNames = {'POLEmut_14', 'blood_myeloid_56', 'blood_lymphoid_53', 'colon_14', 'breast_119', 'oesophagus_135', 'pancreas_421', 'kidney_95', 'skin_66', 'liver_88', 'lung_24', 'brain_MDB_100', 'brain_PA_101', 'gastric_100'}; %, 'colon_MSI_9'
% territoriesSource = 'OldHaradhvala_territories'; 
tissueNames = {'MSI_19', 'POLE_14', 'blood_myeloid_56', 'blood_lymphoid_274', 'bone_98', 'brain_237', 'breast_560', 'colorectum_35', 'gastric_90', 'kidney_clear_cell_95', 'liver_303', 'lung_adenocarcinoma_24', ...
    'lung_squamous_36', 'oesophagus_adenocarcinoma_219', 'oral_25', 'ovary_93', 'pancreas_406', 'prostate_294', 'skin_183'}; %'oesophagus_squamous_7', 'thyroid_26', 
territoriesSource = 'Haradhvala_territories'; 

nMaxSignatures = 7; %15
nRuns = 100;
minStability = 0.8;
alwaysCompute = true;
savePathBasic = 'save/';
evaluationOfDecomposedSignatures = loadEvaluationOfDecomposedSignatures(alwaysCompute, savePathBasic, tissueNames, nMaxSignatures, nRuns, minStability, territoriesSource);
decomposedSignatures = loadDecomposedSignatures(alwaysCompute, savePathBasic, tissueNames, evaluationOfDecomposedSignatures, territoriesSource);
%%
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
diary off;
if (sendingEmails)
    sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
end