%% script_replPrepareRandomTerritories.m
clear; addpath(genpath('code/'));
savePathDiary = 'diaries/'; createDir(savePathDiary); scriptName = 'script_replPrepareRandomTerritories'; compName = 'HOME';
diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
%% 
territoriesSource = 'Haradhvala_territories';  nBinsFromORI = 50;
% territoriesSource = 'Besnard_territories'; nBinsFromORI = 50;  %100
% territoriesSource = 'Besnard1k_territories'; nBinsFromORI = 1000;  %100
% territoriesSource = 'BesnardB20k_territories'; nBinsFromORI = 50;  %100
% territoriesSource = 'PetrykGM_rep1_territories';  nBinsFromORI = 1000;
% territoriesSource = 'PetrykGM_rep2_territories';  nBinsFromORI = 1000;
% removalType = 'OutsideAllGenesAndBlacklisted';  
removalType = 'OutsideGenesAndBlacklisted'; 
% removalType='OutsideBlacklisted';
% removalType='OutsideGenes';
% removalType='AllRegions'; 
alwaysComputeTerritories = false;
alwaysComputeBF = true;
%% DIRECTORIES AND OUTPUT MAT FILE NAMES
dirSave = 'save/'; createDir(dirSave);
currentDataDir = ['data/', territoriesSource, '/']; createDir(currentDataDir);
tableTerritoriesFileName = [dirSave, 'table.', territoriesSource, '.mat']; 
cellResult_background_fileName = [dirSave, 'cellResult_background.', territoriesSource, '.', removalType, '.mat'];
%% INPUT FILES:
inputTerritoriesFile = [currentDataDir, territoriesSource, '.AllRegions.bed']; % Input file with territories and line numbers (originally per_base_territories_20kb_line_numbers.bed)
% inputTerritoriesNOSFile = [currentDataDir, territoriesSource, '.NOS.csv']; % Input file with territories and line numbers (originally per_base_territories_20kb_line_numbers.bed)
dirBackground = [currentDataDir, 'contextInTerritories_', territoriesSource, '_', removalType]; % Directory with 2*4*4*4 = 128 files of trinucleotide frequencies on plus and minus strand in given territories.
prefixFiles = ['mappedFeatures_', removalType, '_']; % File names are: [prefixFiles, trinucleotide, '_', strandName, '.csv']
%% Load data
tableTerritories = fcn_prepareTerritories(tableTerritoriesFileName, alwaysComputeTerritories, inputTerritoriesFile, nBinsFromORI); %inputTerritoriesNOSFile
% cellResult = fcn_prepareBackgroundFrequencies(cellResult_background_fileName, alwaysComputeBF, dirBackground, tableTerritories, prefixFiles);
% writetable(tableTerritories, ['data/tableTerritories_', territoriesSource, '_', num2str(nBinsFromORI), '_bins.txt'], 'delimiter', '\t');
%%
tableChromSizes = readtable('data/hg19.chrom.sizes.txt', 'ReadVariableNames', false); tableChromSizes.Properties.VariableNames = {'chr', 'size'};
%%
isLeft = tableTerritories.tIsLeft;% & tableTerritories.isLeftOrRightNotTx;
isRight = tableTerritories.tIsRight;% & tableTerritories.isLeftOrRightNotTx;
difference = (isRight(2:end) + isLeft(1:end-1));
isLeftToRight = (isRight==1 & [0;isLeft(1:end-1)]==1);
tableTmp = tableTerritories(:,[1:6,11]);
tableTmp.isFirstRight = (isRight==1 & [0;isRight(1:end-1)]==0);
% sum(tableTmp.isFirstRight)
%%
lstIOri = find(tableTmp.isFirstRight); nRandomORI = length(lstIOri);
chr = cell(nRandomORI, 1);
tableBedRandomORI = table(chr);
tableBedRandomORI.pos1 = NaN*ones(nRandomORI, 1);
for iORI = 1:nRandomORI
    tableBedRandomORI.chr{iORI} = tableTmp.tChr{lstIOri(iORI)};
    tableBedRandomORI.pos1(iORI) = randi(tableChromSizes.size(strcmp(tableChromSizes.chr, tableTmp.tChr{lstIOri(iORI)}))); % random position in the chromosome.
end
tableBedRandomORI.pos0 = tableBedRandomORI.pos1 - 1;
writetable(tableBedRandomORI, 'data/RandomORI_territories/tableBedRandomORI.txt', 'delimiter', '\t', 'WriteVariableNames', false);
%%
totalCoveredPositions = sum(tableTmp.tPos1-tableTmp.tPos0) %sum(tableTmp.tPos1(tableTmp.isLeftOrRightNotTx)-tableTmp.tPos0(tableTmp.isLeftOrRightNotTx))
averageRadius = round(totalCoveredPositions/(nRandomORI*2))
averageRadius/20000
%%
% bedtools map -a replication/Haradhvala_territories/per_base_territories_20kb.bed -b ../commonData/features/hg19/positions_NUCLEOSOME_DYADs.bed -c 5 -o mean > per_base_territories_20kb.meanNOS.bed
% cut -f 18  per_base_territories_20kb.meanNOS.bed | awk '{if ($1 == ".") {$1 = "NaN"} print}' > replication/Haradhvala_territories/Haradhvala_territories.NOS.csv

% bedtools map -a replication/Besnard1k_territories/Besnard1k_territories.AllRegions.bed -b ../commonData/features/hg19/positions_NUCLEOSOME_DYADs.bed -c 5 -o mean -null NaN > replication/Besnard1k_territories/Besnard1k_territories.tmpForNOS.bed
% head replication/Besnard1k_territories/Besnard1k_territories.tmpForNOS.bed
% cut -f 11 replication/Besnard1k_territories/Besnard1k_territories.tmpForNOS.bed > replication/Besnard1k_territories/Besnard1k_territories.NOS.csv
%% Save list of territories
% writetable(tableTerritories(tableTerritories.isORI, 1:3), 'data/ORI_per_base_territories_20kb_line_numbers.bed.txt', 'delimiter', '\t', 'WriteVariableNames', false);
% writetable(tableTerritories(tableTerritories.tIsLeft, 1:3), 'data/LEFT_per_base_territories_20kb_line_numbers.bed.txt', 'delimiter', '\t', 'WriteVariableNames', false);
% writetable(tableTerritories(tableTerritories.tIsRight, 1:3), 'data/RIGHT_per_base_territories_20kb_line_numbers.bed.txt', 'delimiter', '\t', 'WriteVariableNames', false);
%%
% leading = NaN*ones(96, 1);
% lagging = NaN*ones(96, 1);
% backgroundTrinuclFrequency = table(leading, lagging);
% for iPattern = 1:length(cellResult.background.lagging) % 96
%     backgroundTrinuclFrequency.leading(iPattern) = sum(cellResult.background.leading{iPattern}(tableTerritories.isLeftOrRightNotTx));
%     backgroundTrinuclFrequency.lagging(iPattern) = sum(cellResult.background.lagging{iPattern}(tableTerritories.isLeftOrRightNotTx));
% end
% backgroundTrinuclFrequency.leadingPlusLagging = backgroundTrinuclFrequency.leading + backgroundTrinuclFrequency.lagging;
% fig = createMaximisedFigure(1);
% bar([backgroundTrinuclFrequency.leading, backgroundTrinuclFrequency.lagging]);
% writetable(backgroundTrinuclFrequency, 'data/backgroundTrinuclFrequency_isLeftOrRightNotTx.txt');
%%
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
diary off;
if (sendingEmails)
    sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
end
%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%% How data were moved from the old file and directory structure: %%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AT_rem_20kb --> OutsideGenesAndBlacklisted
% AT_remBlacklisted_20kb --> OutsideBlacklisted
% AT_remGenes_20kb --> OutsideGenes
% AT_20kb --> AllRegions
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%