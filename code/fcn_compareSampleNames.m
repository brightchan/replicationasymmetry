function result = fcn_compareSampleNames(clinicalSampleNames, sampleName)

if (strcmp(sampleName(1:4), 'TCGA') && length(sampleName) >= 12)
    sampleName = sampleName(1:12);
end

sampleName = strrep(sampleName, '-Tumor', '');

result = find(strcmp(clinicalSampleNames, sampleName));
