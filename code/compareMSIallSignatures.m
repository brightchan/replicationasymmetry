% function compareMSIallSignatures(replData)
fontName = 'Trebuchet MS';
set(0,'defaultAxesFontName',fontName);
set(0,'defaultTextFontName',fontName);
% imagesPath = ['images/',datestr(now, 'mmdd'),'_compareMSI/']; createDir(imagesPath);
imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
%%
colours.MSS = [0.1250    0.5781    0.5430];
colours.MSI = [1    0.6    0.2];
colours.bMMRD = [0.4940    0.1840    0.5560]; % light blue 0.3010    0.7450    0.9330
colours.POLE = [0.6350    0.0780    0.1840];

%
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresComplex_cell = replData.signatureExposuresComplex.ReplicationTiming_LeadLagg;
nCurrentValues_RT = replData.dataFields.nValues.ReplicationTiming_LeadLagg;

clear lstSamples.MSI
lstCTs.MSI = [31,35]; % 31={'TCGA_MSI_Strelka'}
lstCTs.bMMRD = 5; % 31={'TCGA_MSI_Strelka'}
lstCTs.POLE = 32:34; % 31={'TCGA_MSI_Strelka'}
lstCTs.MSS = [1:30,36];%[36,12];%,12];%,4,12]; % 10 (COCA-CA and pancrease are not clear enough)
lstSamples.MSI = {'DO219117'}; %{'DO219117', 'DO227620'}; bMMRd: , 'PD8604a', 'PD8604c2'
isSample.MSI = ismember(tableAllSamples.iCancerType, lstCTs.MSI)' | ismember(tableAllSamples.sampleName, lstSamples.MSI)'; sum(isSample.MSI)
isSample.bMMRD = ismember(tableAllSamples.iCancerType, lstCTs.bMMRD)' & ~ismember(tableAllSamples.sampleName, lstSamples.MSI)'; sum(isSample.bMMRD)
isSample.POLE = ismember(tableAllSamples.iCancerType, lstCTs.POLE)' & ~ismember(tableAllSamples.sampleName, lstSamples.MSI)'; sum(isSample.POLE)
isSample.MSS = ismember(tableAllSamples.iCancerType, lstCTs.MSS)' & ~ismember(tableAllSamples.sampleName, lstSamples.MSI)'; sum(isSample.MSS)

clear exposures;
typesMS = {'MSS', 'MSI', 'bMMRD', 'POLE'};
typesMSPrint = {{'MMR proficient', 'MSS'}, {'MMR deficient', 'MSI'}, {'MMR deficient', 'POLE-MUT bMMRD'}, {'MMR proficient', 'POLE-MUT'}};
% nSignatures = replData.nSignatures;
exposures = cell(replData.nSignatures,1);
minMeanExposure = 0;
for iSignature = 1:replData.nSignatures %17; %replData.signatureNames{iSignature}
    clear isSampleUsed;
    for iTypeMS = 1:length(typesMS)
        typeMS = typesMS{iTypeMS};
        exposures{iSignature}.together.(typeMS) = signatureExposuresSimple_LeadLagg.type1Minustype2(iSignature, isSample.(typeMS));
        %     exposures.together.(typeMS) = signatureExposuresSimple_LeadLagg.total(iSignature, isSample.(typeMS))./sum(signatureExposuresSimple_LeadLagg.total(:, isSample.(typeMS)));
        %     exposures.together.(typeMS) = (replData.structAllSamples.LeadLagg.mfTotal(isSample.(typeMS), 88)./sum(replData.structAllSamples.LeadLagg.mfTotal(isSample.(typeMS), 81:96), 2))';
        
        selectedTypes = {'all', 'above10'};
        for iSelectedType = 1:2
            selectedType = selectedTypes{iSelectedType};
            if (iSelectedType == 1)
                isSampleUsed.(selectedType).(typeMS) = isSample.(typeMS);
            else
                isSampleUsed.(selectedType).(typeMS) = isSample.(typeMS) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
            end
            exposures{iSignature}.(selectedType).nSamplesUsed.(typeMS) = sum(isSampleUsed.(selectedType).(typeMS));
            exposures{iSignature}.(selectedType).matching.(typeMS) = NaN*ones(sum(isSampleUsed.(selectedType).(typeMS)), nCurrentValues_RT);
            exposures{iSignature}.(selectedType).inverse.(typeMS) = NaN*ones(sum(isSampleUsed.(selectedType).(typeMS)), nCurrentValues_RT);
            exposures{iSignature}.(selectedType).total.(typeMS) = NaN*ones(sum(isSampleUsed.(selectedType).(typeMS)), nCurrentValues_RT);
            for iValue = 1:nCurrentValues_RT
                exposures{iSignature}.(selectedType).matching.(typeMS)(:,iValue) = signatureExposuresComplex_cell{iValue}.type1(iSignature, isSampleUsed.(selectedType).(typeMS))';
                exposures{iSignature}.(selectedType).inverse.(typeMS)(:,iValue) = signatureExposuresComplex_cell{iValue}.type2(iSignature, isSampleUsed.(selectedType).(typeMS))';
                exposures{iSignature}.(selectedType).total.(typeMS)(:,iValue) = signatureExposuresComplex_cell{iValue}.totalMF(iSignature, isSampleUsed.(selectedType).(typeMS))';
            end
        end
    end
end
%%
typesMSMajor = {'MSI', 'POLE'};
figNames = {'Fig4supplement11', 'Fig4supplement17'};
for iPlotType = 2%1:2
    fig = figure(iPlotType); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 40 25],'units','normalized','outerposition',[0 0 1 1]); fontSize = 12;
    iS=1; xS=.8; yS=.7; xB=.06; yB=.05; xM=.01; yM=.01;
    posTitle.xShift = 0.04;
    posTitle.yShift = 0.01;
    posTitle.width = 0.02;
    posTitle.height = 0.02;
    posTitle.fontSize = 18;
    posTitle.letters = {'A', 'B', 'C', 'D'}; jS = 1;
    iSelectedType = 2; selectedType = selectedTypes{iSelectedType}; signatureShortNames = strrep(strrep(replData.signatureNames, 'Signature ', ''), 'Signature', '');
    
    minSamples = 4;
    typeMSMajor = typesMSMajor{iPlotType};
    plotName = typeMSMajor;
    isUsedSignature = false(replData.nSignatures, 1);
    for iSignature = 1:replData.nSignatures
        if (exposures{iSignature}.(selectedType).nSamplesUsed.(typeMSMajor)>=minSamples)
            isUsedSignature(iSignature) = true;
        end
    end
    lstUsedSignatures = find(isUsedSignature)';
    if (iPlotType==1)
        lstMSTypes = 1:2;
        titleSignatureNames = replData.signatureNames; %signatureShortNames;
        listsSignatures = {lstUsedSignatures(1:floor(end/2)), lstUsedSignatures(floor(end/2)+1:end)};
        nR=length(lstMSTypes)*2; nC=floor(sum(isUsedSignature)/2); 
    else
        lstMSTypes = 1:4;
        titleSignatureNames = replData.signatureNames;
        listsSignatures = {lstUsedSignatures};
        nR=length(lstMSTypes); nC=sum(isUsedSignature); 
    end
    for iListSignatures = 1:length(listsSignatures)
        lstSignatures = listsSignatures{iListSignatures};
        for iTypeMS = lstMSTypes %1:length(typesMS)
            typeMS = typesMS{iTypeMS};
            for iSignature = lstSignatures %1:replData.nSignatures
                %             if (sum(exposures{iSignature}.(selectedType).nSamplesUsed.(typeMS)) < 2)
                %                 iS = iS + 1;
                %             else
                positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
                if (sum(exposures{iSignature}.(selectedType).nSamplesUsed.(typeMS)) < 2)
                    set(gca, 'XTick', [], 'YTick', [], 'XColor', 0.99*[1,1,1], 'YColor', 0.99*[1,1,1]);
                    titleThirdLine = ' ';
                else
                    titleThirdLine = '';
                    meanValues = nanmean(exposures{iSignature}.(selectedType).total.(typeMS));
                    bar(meanValues, 'FaceColor', colours.(typeMS), 'EdgeColor', 'none'); hold on; %hBar(1).FaceColor = colours.MSI;
                    width = 0.1;
                    for iValue = 1:nCurrentValues_RT
                        myStderrorBar(iValue, exposures{iSignature}.(selectedType).total.(typeMS)(:,iValue), width, colours.(typeMS)/2);
                    end
                    
                    linearModel = fitlm((1:nCurrentValues_RT), meanValues);
                    plot((1:nCurrentValues_RT), linearModel.Fitted, 'k', 'LineWidth', 2, 'Color', (0+colours.(typeMS))/2);
                    text(nCurrentValues_RT, 1.05*meanValues(end), [' ', repmat('*', 1, getStarsFromPValueBasic(coefTest(linearModel, [0,1])))], 'HorizontalAlignment', 'center', 'FontSize', 20, 'FontWeight', 'bold', 'Color', (0+colours.(typeMS))/2);
                    
                    yLimVal = get(gca, 'YLim'); yLimVal(2)=yLimVal(2)*1.05; yLimVal(1) = 0; ylim(yLimVal);
                    
                    text(0.8, 0.95*yLimVal(2), sprintf('log_2 FC = %.1f', log2(max([1,linearModel.Fitted(end)])/max([1,linearModel.Fitted(1)]))), 'VerticalAlignment', 'top', 'FontSize', fontSize-4);
                    %text(1, yLimVal(2), getPValueAsTextShort(signtest(exposures{iSignature}.(selectedType).total.(typeMS)(:,1), exposures{iSignature}.(selectedType).total.(typeMS)(:,end))));
                    xlabel(sprintf('n = %d', sum(exposures{iSignature}.(selectedType).nSamplesUsed.(typeMS))));
                    xlim([0.5, nCurrentValues_RT+0.5]);
                    set(gca, 'FontSize', fontSize-2, 'XTick', []); %[1,nCurrentValues_RT], 'XTickLabel', {'early', 'late'});
                end
                if (mod(iS, nC)==2) % (iSignature == lstSignatures(1))
                    ylabel([typesMSPrint{iTypeMS}, titleThirdLine], 'FontSize', fontSize, 'Color', colours.(typeMS));
                end
                title(titleSignatureNames{iSignature}); %{titleSignatureNames{iSignature}, ' '});
                drawnow;
                %             end
            end
        end
    end
    mySaveAs(fig, imagesPath, figNames{iPlotType}); %['MSI_comparison_all_', plotName]);
end
%%
% fig = createMaximisedFigure(1);
% subplot(1,2,1); plot(exposures.total.MSI');
% subplot(1,2,2); plot(exposures.total.MSS');
% subplot(1,2,1); bar(nanmean(exposures.total.MSI));
% subplot(1,2,2); bar(nanmean(exposures.total.MSS));
%%
% tmp = tableAllSamples(isSampleUsed.MSS,:);
% %% 10 COCA-CN, 22 PACA-AU (several could be, but are not), 23 PACA-CA (one could be, but is not), 24 PAEN-AU (one), 25 PAEN-IT (none)
% % isSampleUsed.ESAD = ismember(tableAllSamples.iCancerType, 12)' & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
% isSampleUsedTmp = ismember(tableAllSamples.iCancerType, 23)' & signatureExposuresSimple_LeadLagg.meanType1Type2(6, :)>1000;    %signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure &
% sum(isSampleUsedTmp)
% fig = createMaximisedFigure(1);
% imagesc(signatureExposuresSimple_LeadLagg.meanType1Type2(:,isSampleUsedTmp)); colorbar;
% set(gca, 'YTick', 1:replData.nSignatures, 'YTickLabel', replData.signatureNames, 'XTick', 1:sum(isSampleUsedTmp), 'XTickLabel', tableAllSamples.sampleName(isSampleUsedTmp));
% %%
% % ESAD-UK: {'DO219117'}; The other one is unclear: 'DO227620'
% % PACA-AU: {'DO33091'}
% isTmp = ismember(replData.tableAllSamples.sampleName, {'DO219117', 'DO227620'}); sum(isTmp) % , 'DO49079', 'DO49172', 'DO49195', 'DO33105', 'DO46688', 'DO49178' {'DO219117', 'DO227620', 'DO227511', 'DO227679'} %isSampleUsed.ESAD %, 'DO50450', 'DO219111' % , 'DO50326', 'DO50332', 'DO50362', 'DO50388', 'DO10840', 'DO227466', 'DO227523', 'DO227672', 'DO50309', 'DO50323', 'DO50374', 'DO50440', 'DO227814'
% tmpNames = replData.tableAllSamples(isTmp,:);
%
% fig = createMaximisedFigure(1);
% imagesc(signatureExposuresSimple_LeadLagg.meanType1Type2(:,isTmp)); colorbar;
% set(gca, 'YTick', 1:replData.nSignatures, 'YTickLabel', replData.signatureNames, 'XTick', 1:sum(isTmp), 'XTickLabel', tmpNames.sampleName);
%
% tmp = signatureExposuresSimple_LeadLagg.meanType1Type2([6,14,19,20,24],isTmp);
% tmp2 = max(tmp); %fig = createMaximisedFigure(2); histogram(tmp2);
% tmpNames.sampleName(tmp2>2500)
% tmp(:,tmp2>2500)
%%
% cd ../mutationFrequency/data/Mutations/ICGC
% grep deletion simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2 -k4,4 -u > deletions.simple_somatic_mutation.open.ESAD-UK.tsv
% cut -f 2,4 deletions.simple_somatic_mutation.open.ESAD-UK.tsv | sort | uniq -c > nDeletions.simple_somatic_mutation.open.ESAD-UK.tsv
% grep insertion simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2 -k4,4 -u > insertions.simple_somatic_mutation.open.ESAD-UK.tsv
% cut -f 2,4 insertions.simple_somatic_mutation.open.ESAD-UK.tsv | sort | uniq -c > nInsertions.simple_somatic_mutation.open.ESAD-UK.tsv
% tail -n +2 ncomms15180-s15.txt | cut -f 1-3 | sort -k1,1 -k2,2n > locations_MSI_WGS_CortesCiriano2017.bed
% tail -n +2 ncomms15180-s14.txt | cut -f 1-3 | sort -k1,1 -k2,2n > locations_MSI_WES_CortesCiriano2017.bed
% cat locations_MSI_WGS_CortesCiriano2017.bed locations_MSI_WES_CortesCiriano2017.bed | sort -k1,1 -k2,2n > locations_MSI_all_CortesCiriano2017.bed

% icgc_mutation_id
% icgc_donor_id
% project_code
% icgc_specimen_id
% icgc_sample_id
% matched_icgc_sample_id
% submitted_sample_id
% submitted_matched_sample_id
% chromosome
% chromosome_start
% chromosome_end
% chromosome_strand
% assembly_version	mutation_type	reference_genome_allele	mutated_from_allele	mutated_to_allele	quality_score	probability	total_read_count	mutant_allele_read_count	verification_status	verification_platform	biological_validation_status	biological_validation_platform	consequence_type	aa_mutation	cds_mutation	gene_affected	transcript_affected	gene_build_version	platform	experimental_protocol	sequencing_strategy	base_calling_algorithm	alignment_algorithm	variation_calling_algorithm	other_analysis_algorithm	seq_coverage	raw_data_repository	raw_data_accession	initial_data_release_date

% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' deletions.simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2n > deletions.ESAD-UK.bed
% bedtools intersect -a deletions.ESAD-UK.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > deletions.inLocationsMSI.ESAD-UK.bed
% cut -f 4,5 deletions.inLocationsMSI.ESAD-UK.bed | sort | uniq -c > nDeletions.inLocationsMSI.simple_somatic_mutation.open.ESAD-UK.tsv
%      15 DO219117	SP119779
%       6 DO227620	SP192313
%       1 DO227814	SP192578
%       1 DO50326	SP111001
%       1 DO50332	SP111006
%       1 DO50362	SP111101
%       1 DO50388	SP111038
%
% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' insertions.simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2n > insertions.ESAD-UK.bed
% bedtools intersect -a insertions.ESAD-UK.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > insertions.inLocationsMSI.ESAD-UK.bed
% cut -f 4,5 insertions.inLocationsMSI.ESAD-UK.bed | sort | uniq -c > nInsertions.inLocationsMSI.simple_somatic_mutation.open.ESAD-UK.tsv
%       1 DO10840	SP23498
%       3 DO219117	SP119779
%       1 DO227466	SP192162
%       1 DO227523	SP192604
%       1 DO227672	SP192260
%       1 DO227814	SP192578
%       1 DO50309	SP110846
%       1 DO50323	SP110865
%       1 DO50374	SP111031
%       1 DO50440	SP111017

% length(unique({'DO219117', 'DO227620', 'DO227814', 'DO50326', 'DO50332', 'DO50362', 'DO50388', 'DO10840', 'DO227466', 'DO227523', 'DO227672', 'DO50309', 'DO50323', 'DO50374', 'DO50440'}))
%%
% CT=PAEN-IT
% grep deletion ../other/simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2 -k4,4 -u > deletions.simple_somatic_mutation.open.${CT}.tsv
% cut -f 2,4 deletions.simple_somatic_mutation.open.${CT}.tsv | sort | uniq -c > nDeletions.simple_somatic_mutation.open.${CT}.tsv
% grep insertion ../other/simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2 -k4,4 -u > insertions.simple_somatic_mutation.open.${CT}.tsv
% cut -f 2,4 insertions.simple_somatic_mutation.open.${CT}.tsv | sort | uniq -c > nInsertions.simple_somatic_mutation.open.${CT}.tsv
%
% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' deletions.simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2n > deletions.${CT}.bed
% bedtools intersect -a deletions.${CT}.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > deletions.inLocationsMSI.${CT}.bed
% cut -f 4,5 deletions.inLocationsMSI.${CT}.bed | sort | uniq -c > nDeletions.inLocationsMSI.simple_somatic_mutation.open.${CT}.tsv
%
%
% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' insertions.simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2n > insertions.${CT}.bed
% bedtools intersect -a insertions.${CT}.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > insertions.inLocationsMSI.${CT}.bed
% cut -f 4,5 insertions.inLocationsMSI.${CT}.bed | sort | uniq -c > nInsertions.inLocationsMSI.simple_somatic_mutation.open.${CT}.tsv
