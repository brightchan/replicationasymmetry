function replData = loadReplicationData(alwaysCompute, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, territoriesSource, decomposedSignatures)
% This script computes exposures to signatures (in matching and inverse direction) for all samples in various types of regions: everything,
% replication timing quartiles, bins around ORI (or left/right transitions), etc.
% 
% Testing:
% clear;
% dirMutations = 'data/mutationsWGS';
% dirBackground = 'data/contextInTerritories';
% dirDataCT = 'save/replPrepareTables_withoutBlacklist0816/'; nameDataCT = 'withoutGenesAndBlacklisted';
% savePathBasic = 'save/'; createDir(savePathBasic);
% tableTerritoriesFileName = [savePathBasic, 'tableTerritories'];               tableTerritories = [];  load(tableTerritoriesFileName, 'tableTerritories'); nameTerritories = 'tableTerritoriesHaradhvala'; % created in script_60726_replPrepareTables
% nTerritories = size(tableTerritories, 1);
% maxPattern = 96;
% alwaysCompute = false;

pathAndNameReplData = [savePathBasic, territoriesSource, '/replData_', nameDataCT, '_', territoriesSource, 'without_N1_N2'];

%%
if (~alwaysCompute && exist([pathAndNameReplData, '.mat'],'file'))
    fprintf('Loading from %s...\n', pathAndNameReplData);
    load(pathAndNameReplData, 'replData');
else
    tic;
    scalingThresholdErrorIncrease = 1.005;
    maxPattern = 96;
    tableTerritories = [];  load(tableTerritoriesFileName, 'tableTerritories'); % created in script_60726_replPrepareTables
    tableGenomeTrinucleotides = readtable('data/signatures/hg19.chrom.sizes.backgroundTableTriNucl96.txt', 'delimiter', '\t', 'ReadVariableNames', true);
    %% List of cancer types and their filenames
    %     tableCancerTypes = readtable('data/cancerTypes.xlsx'); nCT = size(tableCancerTypes, 1); %cancerTypes = tableCancerTypes.CancerType;
    %     tableGenomeTrinucleotides = readtable('data/signatures/hg19.chrom.sizes.backgroundTableTriNucl96.txt', 'delimiter', '\t', 'ReadVariableNames', true);
    %     fprintf('Processing %d cancer types.\n', nCT);
    %%
    dataFields.features = {'mutType1', 'mutType2', 'mfType1', 'mfType2', 'asymmetryDifference', 'asymmetryPValue'};
    dataFields.featuresInComplex = {'mutType1', 'mutType2', 'mfType1', 'mfType2', 'asymmetryDifference', 'asymmetryPValue'};
    dataFields.simpleTypes = {'LeadLagg', 'TranscribedNontranscribed'};
    dataFields.groupedTypes = {'CpGtoT_LeadLagg'};%, 'CpHtoT_LeadLagg'};
    dataFields.complexTypes = {'ReplicationTiming_LeadLagg', 'Expression_LeadLagg', 'DistanceFromORI_LeadLagg'};%, 'DistanceFromORI_LeadLagg_Early', 'DistanceFromORI_LeadLagg_Late', 'NOS_LeadLagg'};
    dataFields.nValues.ReplicationTiming_LeadLagg = max(tableTerritories.bins_tRT);
    dataFields.nValues.Expression_LeadLagg = max(tableTerritories.bins_tExpr);
    dataFields.nValues.DistanceFromORI_LeadLagg = max(tableTerritories.indexBinFromORI);
    %dataFields.nValues.NOS_LeadLagg = max(tableTerritories.bins_tNOS);
    %dataFields.nValues.DistanceFromORI_LeadLagg_Early = max(tableTerritories.indexBinFromORI);
    %dataFields.nValues.DistanceFromORI_LeadLagg_Late = max(tableTerritories.indexBinFromORI);
    %%
    alwaysComputeReplData = alwaysCompute;
    [structAllSamples, tableAllSamples, nTotalSamples, tableCancerTypes] = loadReplicationCTs(alwaysComputeReplData, pathAndNameReplData, dataFields, dirDataCT);
    toc
    tic;
    %%
    matrixSignatures = decomposedSignatures.finalSignatures.signatures; % 96x21
    signatureNames = decomposedSignatures.finalSignatures.signaturesNames; % 1x21
    nSignatures = length(signatureNames);
    
    matrixForRegression = [decomposedSignatures.finalSignatures.splitSignaturesA, decomposedSignatures.finalSignatures.splitSignaturesB;
        decomposedSignatures.finalSignatures.splitSignaturesB, decomposedSignatures.finalSignatures.splitSignaturesA];
    %%
    fprintf('Computing signature exposures in %s...\n', strjoin(dataFields.simpleTypes));
    printInfo = true;
    for simpleType = dataFields.simpleTypes
        simpleType = simpleType{1}; 
        if (isfield(structAllSamples, simpleType) && ~isempty(structAllSamples.(simpleType)))
            signatureExposures.type1 = NaN*ones(nSignatures, nTotalSamples);
            signatureExposures.type2 = NaN*ones(nSignatures, nTotalSamples);
            signatureExposures.total = NaN*ones(nSignatures, nTotalSamples);
            background1 = structAllSamples.(simpleType).mutType1./structAllSamples.(simpleType).mfType1; background1(isnan(background1)) = 1;
            background2 = structAllSamples.(simpleType).mutType2./structAllSamples.(simpleType).mfType2; background2(isnan(background2)) = 1;
            structAllSamples.(simpleType).mfTotal = (structAllSamples.(simpleType).mutType1+structAllSamples.(simpleType).mutType2)./(background1+background2);
            for iSample = 1:nTotalSamples
                mfVectorForRegression = [...
                    structAllSamples.(simpleType).mfType1(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed; ...
                    structAllSamples.(simpleType).mfType2(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed; ...
                    ]; 
                currentExposures = runRegressionToExposures(matrixForRegression, mfVectorForRegression, nSignatures, scalingThresholdErrorIncrease, printInfo, iSample);
                
                signatureExposures.type1(:, iSample) = currentExposures(1:nSignatures);
                signatureExposures.type2(:, iSample) = currentExposures(nSignatures+(1:nSignatures));
                signatureExposures.original.type1(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(simpleType).mfType1(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);
                signatureExposures.original.type2(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(simpleType).mfType2(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);
                signatureExposures.total(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(simpleType).mfTotal(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);
                
            end
            summedExposures = (signatureExposures.type1 + signatureExposures.type2);
            normalisationMatrix = repmat(sum(summedExposures), nSignatures, 1);
            signatureExposures.type1Minustype2 = (signatureExposures.type1 - signatureExposures.type2);
            signatureExposures.meanType1Type2 = (signatureExposures.type1 + signatureExposures.type2);
            signatureExposuresSimple.(simpleType) = signatureExposures;
        end
    end
    whos
    %%
    fprintf('Computing signature exposures in %s...\n', strjoin(dataFields.complexTypes));
    printInfo = false;
    for iType = 1:2
        if (iType == 1)
            complexType = 'Expression_LeadLagg';     nCurrentValues = dataFields.nValues.Expression_LeadLagg;    labelsExtremes = {'low', 'high'};
        elseif (iType == 2)
            complexType = 'ReplicationTiming_LeadLagg';     nCurrentValues = dataFields.nValues.ReplicationTiming_LeadLagg;    labelsExtremes = {'early', 'late'};
        elseif (iType == 3)
            complexType = 'NOS_LeadLagg';     nCurrentValues = dataFields.nValues.NOS_LeadLagg;    labelsExtremes = {'low', 'high'};
        end
        if (isfield(structAllSamples, complexType) && ~isempty(structAllSamples.(complexType)))
            fprintf('Computing signature exposures in %s...\n', complexType);
            signatureExposuresComplex.(complexType) = cell(nCurrentValues, 1);
            for iValue = 1:nCurrentValues
                fprintf('iValue = %d...\n', iValue);
                signatureExposuresComplex.(complexType){iValue}.type1 = NaN*ones(nSignatures, nTotalSamples);
                signatureExposuresComplex.(complexType){iValue}.type2 = NaN*ones(nSignatures, nTotalSamples);
                signatureExposuresComplex.(complexType){iValue}.totalMF = NaN*ones(nSignatures, nTotalSamples);
                background1 = structAllSamples.(complexType){iValue}.mutType1./structAllSamples.(complexType){iValue}.mfType1; background1(isnan(background1)) = 1;
                background2 = structAllSamples.(complexType){iValue}.mutType2./structAllSamples.(complexType){iValue}.mfType2; background2(isnan(background2)) = 1;
                structAllSamples.(complexType){iValue}.totalMF = (structAllSamples.(complexType){iValue}.mutType1+structAllSamples.(complexType){iValue}.mutType2)./(background1+background2);
                for iSample = 1:nTotalSamples
                    mfVectorForRegression = [...
                        structAllSamples.(complexType){iValue}.mfType1(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed; ...
                        structAllSamples.(complexType){iValue}.mfType2(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed];
                    currentExposures = runRegressionToExposures(matrixForRegression, mfVectorForRegression, nSignatures, scalingThresholdErrorIncrease, printInfo, iSample);
                    
                    signatureExposuresComplex.(complexType){iValue}.type1(:, iSample) = currentExposures(1:nSignatures);
                    signatureExposuresComplex.(complexType){iValue}.type2(:, iSample) = currentExposures(nSignatures+(1:nSignatures));
                    signatureExposuresComplex.(complexType){iValue}.original.type1(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(complexType){iValue}.mfType1(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);%./normalisationMatrix(:,iSample);%/tableAllSamples.mfTotal(iSample));
                    signatureExposuresComplex.(complexType){iValue}.original.type2(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(complexType){iValue}.mfType2(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);%./normalisationMatrix(:,iSample);%/tableAllSamples.mfTotal(iSample));
                    signatureExposuresComplex.(complexType){iValue}.totalMF(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(complexType){iValue}.totalMF(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);%./normalisationMatrix(:,iSample);%/tableAllSamples.mfTotal(iSample));
                end
                signatureExposuresComplex.(complexType){iValue}.type1Minustype2 = signatureExposuresComplex.(complexType){iValue}.type1 - signatureExposuresComplex.(complexType){iValue}.type2;
            end
        end
    end
    for iType = 1%:3
        if (iType == 1)
            complexType = 'DistanceFromORI_LeadLagg';     nCurrentValues = dataFields.nValues.DistanceFromORI_LeadLagg;
        elseif (iType == 2)
            complexType = 'DistanceFromORI_LeadLagg_Early';     nCurrentValues = dataFields.nValues.DistanceFromORI_LeadLagg_Early;
        elseif (iType == 3)
            complexType = 'DistanceFromORI_LeadLagg_Late';     nCurrentValues = dataFields.nValues.DistanceFromORI_LeadLagg_Late;
        end
        if (isfield(structAllSamples, complexType) && ~isempty(structAllSamples.(complexType)))
            fprintf('Computing signature exposures in %s...\n', complexType);
            signatureExposuresComplex.(complexType) = cell(nCurrentValues, 1);
            signatureExposuresComplex.(complexType) = cell(nCurrentValues, 1);
            for iValue = 1:nCurrentValues
                fprintf('iValue = %d...\n', iValue);
                signatureExposuresComplex.(complexType){iValue}.mfPlus = NaN*ones(nSignatures, nTotalSamples);
                signatureExposuresComplex.(complexType){iValue}.mfMinus = NaN*ones(nSignatures, nTotalSamples);
                for iSample = 1:nTotalSamples
                    mfVectorForRegression = [...
                        structAllSamples.(complexType){iValue}.mfPlus(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed; ...
                        structAllSamples.(complexType){iValue}.mfMinus(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed];
                    currentExposures = runRegressionToExposures(matrixForRegression, mfVectorForRegression, nSignatures, scalingThresholdErrorIncrease, printInfo, iSample);
                    
                    signatureExposuresComplex.(complexType){iValue}.mfPlus(:, iSample) = currentExposures(1:nSignatures);
                    signatureExposuresComplex.(complexType){iValue}.mfMinus(:, iSample) = currentExposures(nSignatures+(1:nSignatures));
                    signatureExposuresComplex.(complexType){iValue}.original.mfPlus(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(complexType){iValue}.mfPlus(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);%./normalisationMatrix(:,iSample);%/tableAllSamples.mfTotal(iSample));
                    signatureExposuresComplex.(complexType){iValue}.original.mfMinus(:, iSample) = lsqnonneg(matrixSignatures, structAllSamples.(complexType){iValue}.mfMinus(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed);%./normalisationMatrix(:,iSample);%/tableAllSamples.mfTotal(iSample));
                end
            end
            fprintf('Sum of first value is: %f for plus, %f for minus.\n', sum(signatureExposuresComplex.(complexType){1}.mfPlus(:)), sum(signatureExposuresComplex.(complexType){1}.mfMinus(:)));
        end
    end
    %%
    patternNames = cell(maxPattern,1);
    for iPattern = 1:maxPattern
        patternNames{iPattern} = getPatternStringFromNumber(iPattern);
    end
    patterns.types96 = cell(maxPattern, 1);
    patterns.subtypes96 = cell(maxPattern, 1);
    patterns.types192 = cell(2*maxPattern, 1);
    patterns.subtypes192 = cell(2*maxPattern, 1);
    patterns.strandTypes192 = cell(2*maxPattern, 1);
    for iPattern = 1:maxPattern
        [nuclLeft, nuclRight, nuclFrom, nuclTo] = getPatternFromNumber(iPattern);
        patterns.types96{iPattern} = [nuclFrom, '>', nuclTo];
        patterns.subtypes96{iPattern} = [nuclLeft, nuclFrom, nuclRight];
    end
    patterns.types192 = [patterns.types96; patterns.types96];
    patterns.subtypes192 = [patterns.subtypes96; patterns.subtypes96];
    patterns.strandTypes192(1:end/2) = {'T'}; %{'leading'};
    patterns.strandTypes192(end/2+1:end) = {'U'}; %{'lagging'};
    %%
    fprintf('Saving everything...\n');
    replData.dataFields = dataFields;
    replData.matrixSignatures = matrixSignatures;
    replData.signatureNames = signatureNames;
    replData.nSignatures = nSignatures;
    replData.patterns = patterns;
    replData.patternNames = patternNames;
    replData.tableAllSamples = tableAllSamples;
    replData.structAllSamples = structAllSamples;
    replData.signatureExposuresSimple = signatureExposuresSimple;
    replData.signatureExposuresComplex = signatureExposuresComplex;
    replData.tableCancerTypes = tableCancerTypes;
    % SAVE
    save(pathAndNameReplData, 'replData', '-v7.3');
    toc
end