function saveReplicationTissues(replData, territoriesSource)

tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);

saveDir = ['save/', territoriesSource, '/dataForSignatureDecomposition/']; createDir(saveDir);

for iTissue = 1:nTissues
    tissueName = tissueNames{iTissue};
    isSampleThisTissue = strcmp(replData.tableAllSamples.Tissue, tissueName);
    if (sum(isSampleThisTissue) > 0)
        cancerType = tissueName;
        saveFile = sprintf('%s%s_%d_replication_asymmetry_genomes_LeadLagg', saveDir, tissueName, sum(isSampleThisTissue));
        tmpCancerTypes = unique(replData.tableAllSamples.cancerType(isSampleThisTissue));
        fprintf('Saving data for tissue %s_%d (%d samples, %d cancer types): %s\n', tissueName, sum(isSampleThisTissue), sum(isSampleThisTissue), length(tmpCancerTypes), strjoin(tmpCancerTypes'));
        sampleNames = strcat(replData.tableAllSamples.cancerType(isSampleThisTissue), '_', replData.tableAllSamples.sampleName(isSampleThisTissue));
        subtypes = replData.patterns.subtypes192;
        types = replData.patterns.types192;
        strandTypes = replData.patterns.strandTypes192;
        % originalGenomes = [replData.structAllSamples.LeadLagg.mfType1'; replData.structAllSamples.LeadLagg.mfType2'];
        originalGenomes = [replData.structAllSamples.LeadLagg.mutType1'; replData.structAllSamples.LeadLagg.mutType2'];
        originalGenomes = originalGenomes(:, isSampleThisTissue);
        save([saveFile, '_separately.mat'], 'cancerType', 'originalGenomes', 'subtypes', 'types', 'sampleNames', 'strandTypes');
        subtypes = replData.patterns.subtypes96;
        types = replData.patterns.types96;
        % originalGenomes = replData.structAllSamples.LeadLagg.mfType1' + replData.structAllSamples.LeadLagg.mfType2';
        originalGenomes = replData.structAllSamples.LeadLagg.mutType1' + replData.structAllSamples.LeadLagg.mutType2';
        originalGenomes = originalGenomes(:, isSampleThisTissue);
        save([saveFile, '_together.mat'], 'cancerType', 'originalGenomes', 'subtypes', 'types', 'sampleNames');
    end
end