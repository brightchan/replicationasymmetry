function [tableSamples, dataCT, matrixSignatures, signatureNames, patternNames] = fcn_loadCT_signatureExposures(cancerType)

% tableSignatures = readtable('data/signatures/signatures.txt', 'delimiter', '\t', 'ReadVariableNames', true);
% matrixSignatures = table2array(tableSignatures(:,4:(4+21))); signatureNames = tableSignatures.Properties.VariableNames(4:(4+21));



% tableGenomeTrinucleotides = readtable('data/signatures/hg19.chrom.sizes.backgroundTableTriNucl96.txt', 'delimiter', '\t', 'ReadVariableNames', true);
% backgroundTrinuclFrequency = readtable('data/signatures/backgroundTrinuclFrequency_isLeftOrRightNotTx.txt'); %backgroundTrinuclFrequency
tableSignatures = readtable('data/signatures/signaturesOfficial30.txt', 'delimiter', '\t', 'ReadVariableNames', true);
matrixSignatures = table2array(tableSignatures(:,4:(4+29))); signatureNames = tableSignatures.Properties.VariableNames(4:(4+29));
nSignatures = size(matrixSignatures, 2);
maxPattern = 96;
%%
dirMutationalSignatures = 'data/mutationalCatalogs/';
fprintf('Cancer type %s... ', cancerType);
filename_sign = [dirMutationalSignatures, 'mutationalCatalog_', cancerType, '.txt'];
if (~exist(filename_sign, 'file'))
    warning('WARNING: file %s does not exits.\n', filename_sign);
else
    tablePatternMatrix = readtable(filename_sign, 'delimiter', '\t', 'ReadVariableNames', false);
    columnNames = table2cell(tablePatternMatrix(1,:));
    sampleNames = columnNames(1:end-4)'; 
    tableSamples = table(sampleNames);
    tableSamples.isWGS = ismember(table2cell(tablePatternMatrix(2,1:end-4)), {'WGS'})';
    dataCT.catalogMutFreq = str2double(table2array(tablePatternMatrix(3:end, 1:end-4)))';    
    tablePatternMatrix = tablePatternMatrix(3:end,end-3:end);
    tablePatternMatrix.Properties.VariableNames = columnNames(end-3:end);
    exomesFrequencies = str2double(tablePatternMatrix.exomes);%, strcmp(columnNames, 'exomes'))));
    genomesFrequencies = str2double(tablePatternMatrix.genomes);%, strcmp(columnNames, 'genomes'))));
    
    nSamples = length(tableSamples.sampleNames);
    tableSamples.mfTotal = NaN*ones(nSamples, 1);
    tableSamples.meanPatternMF = NaN*ones(nSamples, 1);
    dataCT.catalogMutFreqNormalisedTrinucl = NaN*ones(nSamples, maxPattern);
    dataCT.matrixSignatureExposuresNormalisedTrinucl = NaN*ones(nSamples, nSignatures);
    dataCT.catalogMutFreqNormalisedTrinuclSample = NaN*ones(nSamples, maxPattern);
    dataCT.matrixSignatureExposuresNormalisedTrinuclSample = NaN*ones(nSamples, nSignatures);
        
    for iSample = 1:nSamples
        if (tableSamples.isWGS(iSample))
            currentFrequencies = genomesFrequencies;
        else
            currentFrequencies = exomesFrequencies;
        end
        tableSamples.mfTotal(iSample) = sum(dataCT.catalogMutFreq(iSample,:))/sum(currentFrequencies);
        dataCT.catalogMutFreqNormalisedTrinucl(iSample, :) = (dataCT.catalogMutFreq(iSample,:)./currentFrequencies');           tableSamples.meanPatternMF(iSample) = sum(dataCT.catalogMutFreqNormalisedTrinucl(iSample, :));
        dataCT.catalogMutations(iSample, :) = (dataCT.catalogMutFreq(iSample,:)./(currentFrequencies./genomesFrequencies)');           tableSamples.meanPatternMF(iSample) = sum(dataCT.catalogMutFreqNormalisedTrinucl(iSample, :));
        dataCT.catalogMutFreqNormalisedTrinuclSample(iSample, :) = dataCT.catalogMutFreqNormalisedTrinucl(iSample, :)/tableSamples.meanPatternMF(iSample);
        dataCT.matrixSignatureExposuresNormalisedTrinucl(iSample, :) = lsqnonneg(matrixSignatures, dataCT.catalogMutations(iSample, :)')';
        dataCT.matrixSignatureExposuresNormalisedTrinuclSample(iSample, :) = lsqnonneg(matrixSignatures, dataCT.catalogMutFreqNormalisedTrinuclSample(iSample, :)')';
    end
    fprintf('%s: %d genomes, %d exomes.\n', cancerType, sum(tableSamples.isWGS), sum(~tableSamples.isWGS));
end
%%
patternNames = cell(maxPattern,1);
for iPattern = 1:maxPattern
    patternNames{iPattern} = getPatternStringFromNumber(iPattern);
    %fprintf('%d: %s\n', iPattern, patternNames{iPattern});
end
