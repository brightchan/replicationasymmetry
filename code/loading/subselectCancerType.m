function subselectCancerType(dirDataCT, originalCancerType, cancerType, dataFields)

% DESCRIPTION:
% Loads original structSamples from [dirDataCT, 'structSamples_', cancerType, '.mat'], 
% selects samples which are in ['data/sampleLists/sampleList_', cancerType, '.txt'],
% and saves the new structSamples into [dirDataCT, 'structSamples_', cancerType, '.mat'].

% Testing:
% clear;
% dirDataCT = 'save/replPrepareTables_withoutBlacklist0816/';
% originalCancerType = 'AML';
% fileStructSamples = 'c:\Users\Marki\Documents\bsqc\replAsymmetry\save\replPrepareTables_withoutBlacklist0816\structSamples_AML.mat';
% cancerType = 'Alexandrov_Ding_AML';
% tableTerritories = [];  load('c:\Users\Marki\Documents\bsqc\replAsymmetry\save\tableTerritories.mat', 'tableTerritories'); % created in script_60726_replPrepareTables
% dataFields.features = {'mutType1', 'mutType2', 'mfType1', 'mfType2', 'asymmetryDifference', 'asymmetryPValue'};
% dataFields.featuresInComplex = {'mutType1', 'mutType2', 'mfType1', 'mfType2', 'asymmetryDifference', 'asymmetryPValue'};
% dataFields.simpleTypes = {'LeadLagg', 'TranscribedNontranscribed'};
% dataFields.groupedTypes = {'CpGtoT_LeadLagg', 'CpHtoT_LeadLagg'};
% dataFields.complexTypes = {'ReplicationTiming_LeadLagg', 'Expression_LeadLagg', 'DistanceFromORI_LeadLagg'};
% dataFields.nValues.ReplicationTiming_LeadLagg = max(tableTerritories.bins_tRT);
% dataFields.nValues.Expression_LeadLagg = max(tableTerritories.bins_tExpr);
% dataFields.nValues.DistanceFromORI_LeadLagg = max(tableTerritories.indexBinFromORI);
% fileSampleName = ['c:\Users\Marki\Documents\bsqc\replAsymmetry\data\sampleLists\sampleList_', cancerType, '.txt'];

fileSampleName = ['data/sampleLists/sampleList_', cancerType, '.txt'];
fileStructSamples = [dirDataCT, 'structSamples_', originalCancerType, '.mat'];
fileStructSamplesNew = [dirDataCT, 'structSamples_', cancerType, '.mat'];

if (~exist(fileStructSamples, 'file'))
    warning('WARNING: file %s does not exits.\n', fileStructSamples); return;
else
    structSamples = [];     load(fileStructSamples, 'structSamples');    structSamplesOriginal = structSamples;
end


if (~exist(fileSampleName, 'file'))
     warning('WARNING: file %s does not exits.\n', fileSampleName); return;
else
    listSamplesToKeep = textread(fileSampleName, '%s', 'delimiter', '\n');
end
%%
listOriginalSamples = structSamplesOriginal.tableSamples.sampleName;
isKeptSample = ismember(listOriginalSamples', listSamplesToKeep');

if (sum(isKeptSample) ~= length(listSamplesToKeep))
    warning('WARNING: listSamplesToKeep has %d samples, but only %d of them were also in the original structSamples.\n', length(listSamplesToKeep), sum(isKeptSample));
end

fprintf('subselectCancerType: %s structSamples has %d samples, listSamplesToKeep has %d samples, the overlap has %d samples.\n', cancerType, length(listOriginalSamples), length(listSamplesToKeep), sum(isKeptSample));

clear structSamplesNew
structSamplesNew.tableSamples = structSamplesOriginal.tableSamples(isKeptSample, :);
% structSamplesNew.tableSamples.originalCancerType = structSamplesNew.tableSamples.cancerType;
structSamplesNew.tableSamples.cancerType(:) = {cancerType};
% The structure of structSamples is:
% structSamples.tableSamples = table(nSamples, 2)
% structSamples.simpleTypes.features = matrix(nSamples, 96);
% structSamples.groupedTypes.features = matrix(nSamples, 1);
% structSamples.complexTypes{nValues.complexTypes}.features = matrix(nSamples, 96);

for simpleType = dataFields.simpleTypes
    if (isfield(structSamples, simpleType))
        for feature = fieldnames(structSamplesOriginal.(simpleType{1}))'
            structSamplesNew.(simpleType{1}).(feature{1}) = structSamplesOriginal.(simpleType{1}).(feature{1})(isKeptSample, :);
        end
    end
end
for groupedType = dataFields.groupedTypes
    if (isfield(structSamples, groupedType))
        for feature = fieldnames(structSamplesOriginal.(groupedType{1}))'
            structSamplesNew.(groupedType{1}).(feature{1}) = structSamplesOriginal.(groupedType{1}).(feature{1})(isKeptSample, :);
        end
    end
end
for complexType = dataFields.complexTypes
    if (isfield(structSamples, complexType))
        if (~isfield(structSamplesNew, complexType{1}))
            structSamplesNew.(complexType{1}) = cell(dataFields.nValues.(complexType{1}), 1);
        end
        for iValue = 1:dataFields.nValues.(complexType{1})
            for feature = fieldnames(structSamplesOriginal.(complexType{1}){iValue})'
                structSamplesNew.(complexType{1}){iValue}.(feature{1}) = structSamplesOriginal.(complexType{1}){iValue}.(feature{1})(isKeptSample, :);
            end
        end
    end
end
structSamples = structSamplesNew;
save(fileStructSamplesNew, 'structSamples');

