function resColourmap = colourLinspace(colour1, colour2, n)

resColourmap = [linspace(colour1(1), colour2(1), n)', linspace(colour1(2), colour2(2), n)', linspace(colour1(3), colour2(3), n)'];

