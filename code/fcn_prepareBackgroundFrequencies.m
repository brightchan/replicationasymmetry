function cellResult = fcn_prepareBackgroundFrequencies(cellResult_background_fileName, alwaysComputeBF, dirBackground, tableTerritories, prefixFiles)

% e.g., prefixFiles = 'mappedFeatures_AT_rem_20kb_';

%%
if (~alwaysComputeBF && exist(cellResult_background_fileName,'file'))
    fprintf('Loading cellResult from file %s...\n', cellResult_background_fileName);
    load(cellResult_background_fileName, 'cellResult'); 
else
    strands = {'plus', 'minus'}; 
    maxPattern = 96;
    bases = {'A', 'C', 'G', 'T'};
    nTerritories = size(tableTerritories, 1);
    %% Background frequency of trinucleotides in territories
    cellBackground.plus = cell(maxPattern, 1);
    cellBackground.minus = cell(maxPattern, 1);
    for iPattern = 1:maxPattern
        cellBackground.plus{iPattern} = zeros(nTerritories, 1);
        cellBackground.minus{iPattern} = zeros(nTerritories, 1);
    end
    nuclToTable = [4,1,4,1];
    for iStrand = 1:2
        strandName = strands{iStrand};
        for nuclLeft = 1:4
            for nuclFrom = 1:4
                for nuclRight = 1:4
                    patternNumber = getPatternNumber(nuclLeft, nuclRight, nuclFrom, nuclToTable(nuclFrom)); %mod(nuclFrom, 4)+1
                    listPatternNumbers = [patternNumber + (0:16:32)];
                    trinucleotide = [bases{nuclLeft}, bases{nuclFrom}, bases{nuclRight}];
                    fprintf('Reading trinucleotide %s %s: ', trinucleotide, strandName);
                    strPattern1 = getPatternStringFromNumber(listPatternNumbers(1));
                    strPattern2 = getPatternStringFromNumber(listPatternNumbers(2));
                    strPattern3 = getPatternStringFromNumber(listPatternNumbers(3));
                    fprintf('%s, %s, %s.\n', strPattern1, strPattern2, strPattern3);
                    if (~strcmp(strPattern1(1:3), strPattern2(1:3)) || ~strcmp(strPattern1(1:3), strPattern3(1:3)))
                        fprintf('ERROR: different patterns!!!!!!\n');
                    end
                    vectorPerTerritory = csvread([dirBackground, '/', prefixFiles, trinucleotide, '_', strandName, '.csv']);
                    cellBackground.(strandName){listPatternNumbers(1)} = cellBackground.(strandName){listPatternNumbers(1)} + vectorPerTerritory;
                    cellBackground.(strandName){listPatternNumbers(2)} = cellBackground.(strandName){listPatternNumbers(2)} + vectorPerTerritory;
                    cellBackground.(strandName){listPatternNumbers(3)} = cellBackground.(strandName){listPatternNumbers(3)} + vectorPerTerritory;
                end
            end
        end
    end
    %%
    cellResult = cellBackground;
    cellResult.background.leading = cell(maxPattern, 1);
    cellResult.background.lagging = cell(maxPattern, 1);
    cellResult.background.transcribed = cell(maxPattern, 1);
    cellResult.background.nontranscribed = cell(maxPattern, 1);
    for iPattern = 1:maxPattern
        cellResult.background.leading{iPattern} = zeros(nTerritories, 1); % left plus, right minus
        cellResult.background.lagging{iPattern} = zeros(nTerritories, 1); % right plus, left minus
        cellResult.background.leading{iPattern}(tableTerritories.tIsLeft) = cellBackground.plus{iPattern}(tableTerritories.tIsLeft);
        cellResult.background.leading{iPattern}(tableTerritories.tIsRight) = cellBackground.minus{iPattern}(tableTerritories.tIsRight);
        cellResult.background.lagging{iPattern}(tableTerritories.tIsRight) = cellBackground.plus{iPattern}(tableTerritories.tIsRight);
        cellResult.background.lagging{iPattern}(tableTerritories.tIsLeft) = cellBackground.minus{iPattern}(tableTerritories.tIsLeft);
        
        cellResult.background.transcribed{iPattern} = zeros(nTerritories, 1); % + plus, - minus
        cellResult.background.nontranscribed{iPattern} = zeros(nTerritories, 1); % - plus, + minus
        cellResult.background.transcribed{iPattern}(tableTerritories.tTxPlus) = cellBackground.plus{iPattern}(tableTerritories.tTxPlus);
        cellResult.background.transcribed{iPattern}(tableTerritories.tTxMinus) = cellBackground.minus{iPattern}(tableTerritories.tTxMinus);
        cellResult.background.nontranscribed{iPattern}(tableTerritories.tTxMinus) = cellBackground.plus{iPattern}(tableTerritories.tTxMinus);
        cellResult.background.nontranscribed{iPattern}(tableTerritories.tTxPlus) = cellBackground.minus{iPattern}(tableTerritories.tTxPlus);
    end
    fprintf('Saving to %s.\n', cellResult_background_fileName);
    save(cellResult_background_fileName, 'cellResult', '-v7.3');
end