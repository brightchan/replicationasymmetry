function tableTerritories = fcn_prepareTerritories(tableTerritoriesFileName, alwaysComputeTerritories, inputTerritoriesFile, nBinsFromORI, randomiseORI)%, inputTerritoriesNOSFile)

% General: prepares a table for each strand (leading/lagging), cancer type and pattern (96 possibilities) with a number of mutations per sample, territory. 
% And then number of pattern occurences per territory (for each strand and pattern).

%%
if (~alwaysComputeTerritories && exist(tableTerritoriesFileName,'file'))
    fprintf('Loading tableTerritories from file %s...\n', tableTerritoriesFileName);
    load(tableTerritoriesFileName, 'tableTerritories');
else
    copyfile(inputTerritoriesFile, [inputTerritoriesFile, '.txt']);
    inputTerritoriesFile = [inputTerritoriesFile, '.txt'];
    tableTerritories = readtable(inputTerritoriesFile, 'delimiter', '\t', 'ReadVariableNames', false);
    tableTerritories.Properties.VariableNames = {'tChr', 'tPos0', 'tPos1', 'tID', 'tIsLeft', 'tIsRight', 'tTxPlus', 'tTxMinus', 'tRT', 'tExpr'};
    if (randomiseORI)
        tableTerritories.tRT = tableTerritories.tRT(randperm(size(tableTerritories, 1)));
    end
    tableTerritories.tIsLeft = (tableTerritories.tIsLeft == 1);
    tableTerritories.tIsRight = (tableTerritories.tIsRight == 1);
    tableTerritories.tTxPlus = (tableTerritories.tTxPlus == 1);
    tableTerritories.tTxMinus = (tableTerritories.tTxMinus == 1);
    tableTerritories.isLeftOrRightNotTx = (tableTerritories.tIsLeft | tableTerritories.tIsRight) & (~tableTerritories.tTxPlus & ~tableTerritories.tTxMinus);
    tableTerritories.isTxNotLeftOrRight = (tableTerritories.tTxPlus | tableTerritories.tTxMinus) & (~tableTerritories.tIsLeft & ~tableTerritories.tIsRight);
    if (sum(tableTerritories.isTxNotLeftOrRight) == 0)
        tableTerritories.isTxNotLeftOrRight = (tableTerritories.tTxPlus | tableTerritories.tTxMinus);
    end        
    nTerritories = size(tableTerritories, 1);
    %tableTerritories.tNOS = dlmread(inputTerritoriesNOSFile, '\t');
    
    nValues = 4;
    for iType = 1:2
        if (iType == 1)
            typeName = 'tRT';
            isRelevantTerritory = tableTerritories.isLeftOrRightNotTx;
        elseif (iType == 2)
            typeName = 'tExpr';
            isRelevantTerritory = tableTerritories.isTxNotLeftOrRight;
        elseif (iType == 3)
            typeName = 'tNOS';
            isRelevantTerritory = tableTerritories.isLeftOrRightNotTx & ~isnan(tableTerritories.tNOS);
        end
        sortedValues = sort(tableTerritories.(typeName)(isRelevantTerritory));
        step = round(length(sortedValues)/nValues);
        cutoffs = sortedValues(step:step:(nValues-1)*step); cutoffs = [cutoffs; sortedValues(end) + 1];         % nhist(tableTerritories.(typeName)(isLeftOrRightNotTx)); hold on; plot(cutoffs, 500*[1,1,1,1], 'o');
        tmp = tableTerritories.(typeName); tmp(tmp > max(cutoffs)) = max(cutoffs);
        tableTerritories.(['bins_', typeName]) = NaN*ones(nTerritories, 1);
        tableTerritories.(['bins_', typeName])(~isnan(tableTerritories.(typeName))) = arrayfun(@(x) find(x <= cutoffs, 1, 'first'), tmp(~isnan(tableTerritories.(typeName))));
        fprintf('For type %s there are %d values with the following numbers of (relevant) territories\n', typeName, length(cutoffs));
        [cutoffs, histc(tableTerritories.(['bins_', typeName])(isRelevantTerritory), 1:max(tableTerritories.(['bins_', typeName])))]
    end
    %%
    tic;
    tmpChr = tableTerritories.tChr; tmpChr(strcmp(tmpChr, 'X')) = {'chr23'}; tmpChr(strcmp(tmpChr, 'Y')) = {'chr24'}; tmpNumChr = zeros(nTerritories, 1);
    for iTerritory = 1:nTerritories
        tmpNumChr(iTerritory) = str2double(tmpChr{iTerritory}(4:end));
    end
    toc
    disp('tmpNumChr finished.');
    %%
    tic;
    % distanceBinSize = 20000;
    isORI = false(nTerritories, 1);
    lastLeft = 0; lastTerritoryUsed = true;
    for iTerritory = 2:nTerritories
        if (tableTerritories.tIsLeft(iTerritory))
            lastLeft = iTerritory;
            lastTerritoryUsed = false;
        end
        if (~lastTerritoryUsed && tableTerritories.tIsRight(iTerritory) && ~tableTerritories.tIsRight(iTerritory-1))  % Transition to isRight
            isORI(round(mean([lastLeft, iTerritory]))) = true;
            lastTerritoryUsed = true;
        end
    end
    toc
    disp('First finished.');
    %
    tic;
    distanceFromLastORI = NaN*ones(nTerritories, 1);
    nBinsFromLastORI = NaN*ones(nTerritories, 1);
    lastORI = 0;
    for iTerritory = 2:nTerritories
        %         if (~strcmp(tableTerritories.tChr(iTerritory), tableTerritories.tChr(iTerritory-1)))
        if (tmpNumChr(iTerritory) ~= tmpNumChr(iTerritory-1))
            lastORI = 0;
        end
        if (isORI(iTerritory))
            lastORI = iTerritory;
        end
        if (lastORI > 0)
            distanceFromLastORI(iTerritory) = tableTerritories.tPos0(iTerritory) - tableTerritories.tPos0(lastORI);
            nBinsFromLastORI(iTerritory) = iTerritory - lastORI;
        end
        if (mod(iTerritory, floor(nTerritories/10)) == 0)
            fprintf('Second: iTerritory = %d (%.1f%%)\n', iTerritory, 100*(iTerritory/nTerritories));
        end
    end
    toc
    disp('Second finished.');
    tic;
    % OLD: tableTerritories.nBinsFromLastORI = floor(tableTerritories.distanceFromLastORI / distanceBinSize);
    distanceFromNextORI = NaN*ones(nTerritories, 1);
    nBinsFromNextORI = NaN*ones(nTerritories, 1);
    lastNextORI = 0;
    for iTerritory = nTerritories-1:-1:1
        %         if (~strcmp(tableTerritories.tChr(iTerritory), tableTerritories.tChr(iTerritory+1)))
        if (tmpNumChr(iTerritory) ~= tmpNumChr(iTerritory+1))
            lastNextORI = 0;
        end
        if (isORI(iTerritory))
            lastNextORI = iTerritory;
        end
        if (lastNextORI > 0)
            distanceFromNextORI(iTerritory) = tableTerritories.tPos0(lastNextORI) - tableTerritories.tPos0(iTerritory);
            nBinsFromNextORI(iTerritory) = lastNextORI - iTerritory;
        end
        if (mod(iTerritory, floor(nTerritories/10)) == 0)
            fprintf('Third: iTerritory = %d (%.1f%%)\n', iTerritory, 100*(iTerritory/nTerritories));
        end
    end
    % OLD: tableTerritories.nBinsFromNextORI = floor(tableTerritories.distanceFromNextORI / distanceBinSize);
    
    toc
    disp('Third finished.');
    tic;
    %%
    tableTerritories.nBinsFromORI = NaN*ones(nTerritories, 1);
    isLeftCloser = (nBinsFromLastORI < nBinsFromNextORI | (~isnan(nBinsFromLastORI) & isnan(nBinsFromNextORI)));
    tableTerritories.nBinsFromORI(isLeftCloser) = nBinsFromLastORI(isLeftCloser);
    tableTerritories.nBinsFromORI(~isLeftCloser) = -nBinsFromNextORI(~isLeftCloser);
    %%
    tableTerritories.indexBinFromORI = NaN*ones(nTerritories, 1);
    isRelevant = abs(tableTerritories.nBinsFromORI) <= nBinsFromORI; %50
    tableTerritories.indexBinFromORI(isRelevant) = tableTerritories.nBinsFromORI(isRelevant) + (nBinsFromORI+1);
    %%
    fprintf('Saving to %s.\n', tableTerritoriesFileName);
    save(tableTerritoriesFileName, 'tableTerritories'); %, '-v7.3'
end