function [currentExposuresPerSample, isKeptSignaturePerSample, resnormLastPerSample] = runPerturbatedRegression(matrixSignatures, mfTotal, tableGenomeTrinucleotides, decomposedSignatures, signatureNames, tableAllSamples, nPerturbations, thresholdMaxJitter, printInfo) %nSignatures, scalingThresholdErrorIncrease, 

% background1 = structAllSamples.(simpleType).mutType1./structAllSamples.(simpleType).mfType1; background1(isnan(background1)) = 1;
% background2 = structAllSamples.(simpleType).mutType2./structAllSamples.(simpleType).mfType2; background2(isnan(background2)) = 1;
% structAllSamples.(simpleType).mfTotal = (structAllSamples.(simpleType).mutType1+structAllSamples.(simpleType).mutType2)./(background1+background2);
% matrixSignatures = decomposedSignatures.finalSignatures.signatures; % 96x21
% mfTotal = structAllSamples.(simpleType).mfTotal;
matrixForRegression = matrixSignatures;
% iSample = 1753; %1753%1692%:nTotalSamples % 1680        1683        1692        1749        1753        1794        1798
% mfVectorForRegression = [...
%     full(structAllSamples.(simpleType).mfType1(iSample,:)').*tableGenomeTrinucleotides.genomeComputed; ...
%     full(structAllSamples.(simpleType).mfType2(iSample,:)').*tableGenomeTrinucleotides.genomeComputed];

% scalingThresholdErrorIncrease: 1.005
% nPerturbations = 100;
% thresholdMaxJitter = 0.1;
% printInfo = true;
imagesPath = ['images/',datestr(now, 'mmdd'),'_validationPerturbations/']; createDir(imagesPath);
signatureNamesTwice = [strrep(signatureNames, 'Signature', 'S'), strrep(signatureNames, 'Signature', 'S')]; 

nSignatures = size(matrixSignatures, 2);
nSamples = size(mfTotal, 1);
currentExposuresPerSample = NaN*ones(nSignatures, nSamples);
isKeptSignaturePerSample = NaN*ones(nSignatures, nSamples);
resnormLastPerSample = NaN*ones(nSamples, 1);
for iSample = 1:nSamples
    mfVectorForRegression = mfTotal(iSample,:)'.*tableGenomeTrinucleotides.genomeComputed;
    [currentExposures, resnormFirst, residualFirst] = lsqnonneg(matrixForRegression, mfVectorForRegression); %,residual,exitflag,output,lambda
    currentExposuresOriginal = currentExposures;
    nSignaturesAbove0 = sum(currentExposures>0);
    nIterations = min(1, sum(currentExposures>0));
    if (nIterations > 0)
        for iter = 1:nIterations
            if (max(currentExposures)>0)
                mfVectorForRegressionPerturbed = repmat(mfVectorForRegression, 1, nPerturbations);
                for iType = 1:length(mfVectorForRegression)
                    mfVectorForRegressionPerturbed(iType,:) = mfVectorForRegression(iType) + randn(1,nPerturbations)*mfVectorForRegression(iType)*0.05; % sd: 5% of the original value
                end
                currentExposuresPerturbed = repmat(NaN*currentExposures, 1, nPerturbations);
                for iPerturbation = 1:nPerturbations
                    currentExposuresPerturbed(:,iPerturbation) = lsqnonneg(matrixForRegression, mfVectorForRegressionPerturbed(:,iPerturbation));
                end
                
                normalisedExposuresPerturbed = currentExposuresPerturbed./repmat(currentExposures, 1, nPerturbations);
                quant25 = quantile(normalisedExposuresPerturbed', 0.25)';
                quant75 = quantile(normalisedExposuresPerturbed', 0.75)';
                
                largestChange1 = 1-quant25;
                largestChange2 = quant75-1;
                largestChange = largestChange1; largestChange(largestChange2>largestChange1) = largestChange2(largestChange2>largestChange1);
                largestChange(isinf(largestChange)) = 100;
                maxChange = max(largestChange(currentExposures>0));
                if (maxChange > thresholdMaxJitter)
                    isKeptCol = currentExposures>0 & largestChange < thresholdMaxJitter;
                    previousExposures = currentExposures;
                    currentExposures(:) = 0;
                    [currentExposuresSmaller, resnormLast, residualLast] = lsqnonneg(matrixForRegression(:,isKeptCol), mfVectorForRegression);
                    currentExposures(isKeptCol) = currentExposuresSmaller;
                else
                    break
                end
            end
        end
    end
    currentExposuresPerSample(:,iSample) = currentExposures;
    isKeptSignaturePerSample(:,iSample) = isKeptCol;
    resnormLastPerSample(iSample) = resnormLast;
    if (printInfo)
        fprintf('Sample %d: iterations=%d, first error=%.1e (%d signatures), last error=%.1e (%d signatures).\n', iSample, iter, resnormFirst, nSignaturesAbove0, resnormLast, sum(currentExposures>0));
        fig = createMaximisedFigure(4); nR = 2; nC = 2; iS = 1; maxMut = max([mfVectorForRegression; nanmean(mfVectorForRegressionPerturbed, 2)]);
        subplot(nR,nC,1); iS=iS+1; plot_signature([mfVectorForRegression/100; nanmean(mfVectorForRegressionPerturbed, 2)/100], decomposedSignatures.subtypes.strandSpec, decomposedSignatures.types.strandSpec, true, 'normal', ...
            '', 'genome-scaled # mutations', true, 14, 'Helvetica', maxMut, 1.2); ylim([0, maxMut*1.02]); grid on;
        
        subplot(nR,nC,iS); iS=iS+1; maxValue = nanmax(currentExposuresPerturbed,[],2); tmp = nanmean(currentExposuresPerturbed, 2)./previousExposures; tmp=tmp(maxValue>0); tmp(isinf(tmp)) = 1;
        boxplot(currentExposuresPerturbed(maxValue>0,:)'./previousExposures(maxValue>0)'); text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(maxValue>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');
        hold on; xLimVal=get(gca,'XLim'); plot(xLimVal, 0.9*[1,1], '-', xLimVal, 1.1*[1,1], '-'); ylim([0.5, 1.5]); grid on; ylabel('perturbed/unperturbed');
        
        subplot(nR,nC,iS); iS=iS+1; tmp = currentExposuresOriginal(currentExposuresOriginal>0); bar(tmp); ylabel('exposure');
        text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(currentExposuresOriginal>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center'); grid on;
        title(sprintf('Before: %.1e error, %.1e residual', resnormFirst, sum(abs(residualFirst))));
        
        subplot(nR,nC,iS); iS=iS+1; tmp = currentExposures(currentExposures>0); bar(tmp); ylabel('exposure');
        text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(currentExposures>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center'); grid on;
        title(sprintf('After: %.1e error, %.1e residual', resnormLast, sum(abs(residualLast))));
        
        suptitle(strrep(sprintf('%d: %s | %s', iSample, tableAllSamples.cancerType{iSample}, tableAllSamples.sampleName{iSample}), '_', ' '));
        mySaveAs(fig, imagesPath, ['validation_', num2str(iSample), '_', tableAllSamples.sampleName{iSample}]);
    end
end
% fig = createMaximisedFigure(1); nR = 2; nC = 2; iS = 1; maxMut = max([mfVectorForRegression; nanmean(mfVectorForRegressionPerturbed, 2)]);
% subplot(nR,nC,iS); iS=iS+1; plot_signature(mfVectorForRegression/100, decomposedSignatures.subtypes.strandUnspec, decomposedSignatures.types.strandUnspec, true, 'normal', ...
%     strrep(sprintf('%d: %s | %s', iSample, replData.tableAllSamples.cancerType{iSample}, replData.tableAllSamples.sampleName{iSample}), '_', ' '),...
%     'genome-scaled # mutations', true, 14, 'Helvetica', maxMut, 1.2); ylim([0, maxMut*1.02]); grid on;
% subplot(nR,nC,iS); iS=iS+1; tmp = currentExposuresOriginal(currentExposures>0); bar(tmp);
% text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(currentExposures>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center'); grid on;
% 
% subplot(nR,nC,iS); iS=iS+1; plot_signature(nanmean(mfVectorForRegressionPerturbed, 2)/100, decomposedSignatures.subtypes.strandSpec, decomposedSignatures.types.strandSpec, true, 'normal', ...
%     strrep(sprintf('%d: %s | %s', iSample, replData.tableAllSamples.cancerType{iSample}, replData.tableAllSamples.sampleName{iSample}), '_', ' '),...
%     'genome-scaled # mutations', true, 14, 'Helvetica', maxMut, 1.2); ylim([0, maxMut*1.02]); grid on;
% subplot(nR,nC,iS); iS=iS+1; maxValue = nanmax(currentExposuresPerturbed,[],2); tmp = nanmean(currentExposuresPerturbed, 2)./currentExposures; tmp=tmp(maxValue>0); tmp(isinf(tmp)) = 1;
% boxplot(currentExposuresPerturbed(maxValue>0,:)'./currentExposures(maxValue>0)'); text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(maxValue>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');
% hold on; xLimVal=get(gca,'XLim'); plot(xLimVal, 0.9*[1,1], '-', xLimVal, 1.1*[1,1], '-'); ylim([0.5, 1.5]); grid on;
%%
% nIterations = min(10, sum(currentExposures(1:nSignatures)+currentExposures(nSignatures+(1:nSignatures))>0));
% if (nIterations > 0)
%     res = NaN*ones(nIterations, 1);
%     perIterCurrentExposures = NaN*ones(2*nSignatures, nIterations);
%     for iter = 1:nIterations
%         res(iter) = resnorm;
%         expA = currentExposures(1:nSignatures);
%         expB = currentExposures(nSignatures+(1:nSignatures));
%         expMax = max([expA, expB], [], 2);
%         if (max(expMax) > 0)
%             minVal = min(expMax(expMax > 0));
%             isAbove0 = (expA > minVal | expB > minVal);
%             %                 sum(isAbove0)
%             %                 sum(expA > 0 | expB > 0)
%             isKeptCol = [isAbove0; isAbove0];
%             currentExposures(:) = 0;
%             [currentExposuresSmaller,resnorm] = lsqnonneg(matrixForRegression(:,isKeptCol), mfVectorForRegression); %,residual,exitflag,output,lambda
%             currentExposures(isKeptCol) = currentExposuresSmaller;
%         end
%         perIterCurrentExposures(:, iter) = currentExposures;
%     end
%     %             fig = createMaximisedFigure(1); plot(res, 'o-');
%     %             sum(res < res(1)*1.005)
%     indexIter = find(res < res(1)*scalingThresholdErrorIncrease, 1, 'last');
%     currentExposures = perIterCurrentExposures(:, indexIter);
%     if (printInfo)
%         fprintf('Sample %d: indexIter=%d, resnorm(1)=%.1e, resnorm(indexIter)=%.1e.\n', iSample, indexIter, res(1), res(indexIter));
%     end
% end