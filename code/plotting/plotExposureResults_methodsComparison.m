% function plotExposureResults_methodsComparison(replData, territoriesSource, signatureExposuresSimple_LeadLagg_cell_Besnard1k, replData_OAGAB, replData_AR);
% plotExposureResults_methodsComparison(replData, territoriesSource, signatureExposuresSimple_LeadLagg_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_RandomORI, replData_OAGAB, replData_AR, replData_RandomORI)
%%
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', '');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);

signatureExposuresSimple_LeadLagg.Haradhvala = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix.Haradhvala = replData.signatureExposuresSimple.LeadLagg.type1Minustype2;
signature_RT_log2FC_average.Haradhvala = tableSignatures.RT_log2FC_average;

signatureExposuresSimple_LeadLagg.Besnard1k = signatureExposuresSimple_LeadLagg_cell_Besnard1k;
signatureExposuresSimple_matrix.Besnard1k = signatureExposuresSimple_LeadLagg_cell_Besnard1k.type1Minustype2;
signature_RT_log2FC_average.Besnard1k = tableSignatures_Besnard1k.RT_log2FC_average;

signatureExposuresSimple_LeadLagg.Haradhvala_OAGAB = replData_OAGAB.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix.Haradhvala_OAGAB = replData_OAGAB.signatureExposuresSimple.LeadLagg.type1Minustype2;
signature_RT_log2FC_average.Haradhvala_OAGAB = tableSignatures_OAGAB.RT_log2FC_average;

signatureExposuresSimple_LeadLagg.Haradhvala_AR = replData_AR.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix.Haradhvala_AR = replData_AR.signatureExposuresSimple.LeadLagg.type1Minustype2;
signature_RT_log2FC_average.Haradhvala_AR = tableSignatures_AR.RT_log2FC_average;

% signatureExposuresSimple_LeadLagg.PetrykGM_rep1 = signatureExposuresSimple_LeadLagg_cell_PetrykGM_rep1;
% signatureExposuresSimple_matrix.PetrykGM_rep1 = signatureExposuresSimple_LeadLagg_cell_PetrykGM_rep1.type1Minustype2;
% signature_RT_log2FC_average.PetrykGM_rep1 = tableSignatures_PetrykGM_rep1.RT_log2FC_average;

signatureExposuresSimple_LeadLagg.RandomORI = signatureExposuresSimple_LeadLagg_cell_RandomORI;
signatureExposuresSimple_matrix.RandomORI = signatureExposuresSimple_LeadLagg_cell_RandomORI.type1Minustype2;
signature_RT_log2FC_average.RandomORI = tableSignatures_RandomORI.RT_log2FC_average;

%%
imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
minMeanExposure = 10;
font_size = 12;
fontName = 'Trebuchet MS'; %fontName = 'Helvetica'; % 'Lucida Sans'; % Console
set(0,'defaultAxesFontName',fontName);
set(0,'defaultTextFontName',fontName);
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%% clear signatureResults
territoryNames = {'Haradhvala', 'Besnard1k', 'Haradhvala_OAGAB', 'Haradhvala_AR', 'RandomORI'};
territoryNamesPrint = {'Haradhvala', 'Besnard', 'Haradhvala without all genes', 'Haradhvala all regions', 'RandomORI'};
territoryNamesLabel = {{'method 1', 'from replication timing', 'without protein-coding genes'}, ...
    {'method 2', 'from SNS-seq', 'without protein-coding genes'}, ...
    {'method 2', 'from replication timing', 'without any genes'}, ...
    {'method 2', 'from replication timing', 'without with all regions'}...
    {'method 2', 'from random ORI', 'without protein-coding genes'}}; 
for iTerritoryName = 1:5%[1,3,4]%length(territoryNames)
    territoryName = territoryNames{iTerritoryName};
    pValue = NaN*ones(nSignatures, 1);
    signatureResults = table(pValue);
    signatureResults.meanExposure = NaN*ones(nSignatures, 1);
    signatureResults.medianExposure = NaN*ones(nSignatures, 1);
    signatureResults.RT_log2FC_average = signature_RT_log2FC_average.(territoryName) ;
    signatureResults.hasStrandAsymmetry = false(nSignatures, 1);
    signatureResults.colourTitle = zeros(nSignatures, 3);
    signatureResults.colourPositiveBars = zeros(nSignatures, 3);
    signatureResults.colourNegativeBars = zeros(nSignatures, 3);
    signatureResults.colourGCA = zeros(nSignatures, 3);
    signatureResults.titleText = cell(nSignatures, 3);
    for iSignature = 1:nSignatures
        isSampleUsed = signatureExposuresSimple_LeadLagg.(territoryName).meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        exposuresDifference = signatureExposuresSimple_matrix.(territoryName)(iSignature, isSampleUsed);
        signatureResults.pValue(iSignature) = signtest(exposuresDifference); %signrank
        signatureResults.meanExposure(iSignature) = mean(exposuresDifference);
        signatureResults.medianExposure(iSignature) = median(exposuresDifference);
    end
    [~, ~, ~, qValues]=fdr_bh(signatureResults.pValue);
    signatureResults.qValue_BenjaminiHochberg = qValues;
    signatureResults.qValue_Bonferroni = signatureResults.pValue*nSignatures;
    signatureResults.isSignificant = signatureResults.pValue < 0.05*nSignatures;
    signatureResults.isLeading = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure > 0); %signatureResults.meanExposure > 0 &
    signatureResults.isLagging = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure < 0); %signatureResults.meanExposure < 0 &
    signatureResults.hasStrandAsymmetry(signatureResults.isLeading | signatureResults.isLagging) = true;
    for iSignature = 1:nSignatures
        if (signatureResults.isLeading(iSignature))
            signatureResults.titleText{iSignature} = sprintf('%s: leading %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
            signatureResults.colourTitle(iSignature,:) = colours.leadingDark;
        elseif (signatureResults.isLagging(iSignature))
            signatureResults.titleText{iSignature} = sprintf('%s: lagging %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
            signatureResults.colourTitle(iSignature,:) = colours.laggingDark;
        else
            signatureResults.titleText{iSignature} = sprintf('%s n.s.', signatureNames{iSignature});
            signatureResults.colourTitle(iSignature,:) = colours.titleNotSignif;
        end
        if (signatureResults.hasStrandAsymmetry(iSignature))
            signatureResults.colourPositiveBars(iSignature,:) = colours.leading;
            signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
            signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
        else
            signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif;
            signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
            signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;%colours.titleNotSignif;
        end
    end
    signatureResults_cell.(territoryName) = signatureResults;
end
%%
iTerritoryName = 1; lstJTerritories = [4,3,2];
fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', [0 0 30 30],'units','normalized','outerposition',[0 0 1 1]);
nR = length(lstJTerritories); nC = 3; iS = 1; xS = .8; yS = .8; xB = 0.1; yB = 0.1; xM = -0.02; yM = 0.02; fontSize = 14;

posTitle.xShift = 0.05;
posTitle.yShift = 0.01;
posTitle.width = 0.02;
posTitle.height = 0.02;
posTitle.fontSize = 18;
posTitle.letters = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'};
for jTerritoryName = lstJTerritories
    for iC = 1:nC
        positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS=iS+1; hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
        if (iC <= 3)
            if (iC == 1)
                varName = 'meanExposure'; titleText = 'mean asymmetry';
            elseif (iC == 2)
                varName = 'medianExposure'; titleText = 'median asymmetry';
            else
                varName = 'RT_log2FC_average'; titleText = {'mean replication timing','(log_2 fold-change)'};
            end
            xValues = signatureResults_cell.(territoryNames{iTerritoryName}).(varName);
            yValues = signatureResults_cell.(territoryNames{jTerritoryName}).(varName);
            ignoreDist = 0.05; maxForce = 0.01; shiftDist = 0.015; scaleBorderForce= 1;
            hOriginalDots = plot(xValues, yValues, 'o');
            [xValuesText, yValuesText] = labelRepelSimple(xValues, yValues, signatureShortNames, fontSize-4, ignoreDist, maxForce, shiftDist, scaleBorderForce);
            delete(hOriginalDots);
            plot(xValues, yValues, 'o', 'MarkerFaceColor', [1,.5,0], 'MarkerEdgeColor', [.8,.3,.2]);            
            text(xValuesText, yValuesText, signatureShortNames, 'HorizontalAlignment', 'center', 'FontSize', fontSize-4, 'Color', 0.5*[1,1,1]);            
            if (jTerritoryName == lstJTerritories(end))
                xlabel(territoryNamesLabel{iTerritoryName}); 
            end
            if (iC == 1)
                ylabel(territoryNamesLabel{jTerritoryName}); 
            end
            if (jTerritoryName == lstJTerritories(1))
                title(titleText, 'FontSize', fontSize);
            end
            xLimVal = get(gca, 'XLim'); yLimVal = get(gca, 'YLim');
            text(xLimVal(1), yLimVal(2), {' ', sprintf('  r = %.4f', corr(xValues, yValues))}, 'FontSize', fontSize, 'FontAngle', 'italic');
        end
        axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
        text(.5,.5,posTitle.letters{iS-1},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center');
    end
end
mySaveAs(fig, imagesPath, 'Fig2supplement2');
%%
if (true)
    iTerritoryName = 5;
    territoryName = territoryNames{iTerritoryName};
    territoryNamePrint = territoryNamesPrint{iTerritoryName};
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', 1.3*[0 0 30 30],'units','normalized','outerposition',[0 0 1 1]);
    nR = 6; nC = 5; iS = 1; xS = .8; yS = 0.73; xB = 0.05; yB = 0.08; xM = -0.02; yM = 0.01; iSignature = 0;
    for iR = 1:nR
        for iC = 1:nC
            iSignature = iSignature + 1;
            if (iSignature <= nSignatures)
                positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
                plot_xLabel = false;
                if (iSignature>nSignatures-nC)
                    plot_xLabel = true;
                end
                title_text = '';
                isSampleUsed = signatureExposuresSimple_LeadLagg.(territoryName).meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
                plot_yLabel = true;
                plot_quartile = true;
                plot_histogram(signatureExposuresSimple_matrix.(territoryName), iSignature, isSampleUsed, signatureResults_cell.(territoryName), plot_quartile, plot_xLabel, plot_yLabel, title_text, colours, font_size, fontName)
                p = nSignatures*signtest(signatureExposuresSimple_matrix.(territoryName)(iSignature,isSampleUsed));
                text(min(get(gca, 'XLim')), mean(get(gca, 'YLim')), ['   ', getPValueAsTextShort(p)], 'FontAngle', 'italic');
                title(signatureNames{iSignature}, 'FontSize', font_size+2, 'FontWeight', 'bold');
                ylabel('n samples', 'FontSize', font_size); drawnow;
            end
        end
    end
    mySaveAs(fig, imagesPath, 'Fig2supplement3'); 
end
%%
if (true)
    iTerritoryName = 5;
    territoryName = territoryNames{iTerritoryName};
    territoryNamePrint = territoryNamesPrint{iTerritoryName};
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', 1.3*[0 0 30 30],'units','normalized','outerposition',[0 0 1 1]);
    nR = 6; nC = 5; iS = 1; xS = .8; yS = 0.73; xB = 0.05; yB = 0.08; xM = -0.02; yM = 0.01; iSignature = 0;
    for iR = 1:nR
        for iC = 1:nC
            iSignature = iSignature + 1;
            if (iSignature <= nSignatures)
                positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
                xLabel_text = '';
                if (iSignature>nSignatures-nC)
                    xLabel_text = 'replication timing';
                end
                title_text = '';
                isSampleUsed = signatureExposuresSimple_LeadLagg.(territoryName).meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
                typeName_RT = 'double_type1_type2';
                yLabel_text = 'exposure';
                legShiftHeightFactor = 2; textScaleFactor = 1.5;
                nCurrentValues_RT = replData_RandomORI.dataFields.nValues.ReplicationTiming_LeadLagg;    labelsExtremes = {'early', 'late'};
                plot_replbar(replData_RandomORI.signatureExposuresComplex.ReplicationTiming_LeadLagg, iSignature, isSampleUsed, signatureResults_cell.(territoryName), ...
                    typeName_RT, nCurrentValues_RT, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size, fontName, legShiftHeightFactor, nSignatures)
                title(signatureNames{iSignature}, 'FontSize', font_size+2, 'FontWeight', 'bold');
            end
        end
    end
    mySaveAs(fig, imagesPath, 'Fig2supplement4'); 
end
