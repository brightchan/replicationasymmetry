function [h, x, f] = plotKMCurve(survivalTimes, censoredValues, colourValue, lineType, plotCI, inPercents, plotCensored)

% islogical(censoredValues)
coef = 1;
if (inPercents)
    coef = 100;
end

maxTime = max(survivalTimes);
[f, x, flow, fup] = ecdf(survivalTimes, 'censoring', censoredValues, 'function', 'survivor');
x = [0; x; maxTime]; f = [f(1); f; f(end)]; 
h = stairs(x, coef*f, lineType, 'Color', colourValue, 'LineWidth', 2); 
if (plotCI)
    flow = [flow(1); flow; flow(end)]; fup = [fup(1); fup; fup(end)];
    stairs(x, coef*flow, '-', 'Color', colourValue); 
    stairs(x, coef*fup, '-', 'Color', colourValue);
end

censoredPatients = survivalTimes(censoredValues); nCensored = length(censoredPatients);
censoredValues = zeros(nCensored, 1);
for iCensored = 1:nCensored
    currentTime = censoredPatients(iCensored);
    isThisInterval = [x;1] >= currentTime & [1;x] <= currentTime;
    index = find(isThisInterval, 1, 'first');
    censoredValues(iCensored) = f(index-1);
    %fprintf('currentTime %d: %f\n', currentTime, censoredValues(iCensored));
end
if (plotCensored)
    plot(censoredPatients, coef*censoredValues, '+', 'Color', colourValue, 'LineWidth', 1.5);
end