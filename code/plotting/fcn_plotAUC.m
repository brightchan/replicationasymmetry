function cutoffs = fcn_plotAUC(isDeceased, predictions, featureNameToPrint, expertCutoff, direction, plotGrayscale, FONT_SIZE, FONT_NAME)



if (plotGrayscale)
    lineColour = 0.3*[1,1,1];
else
    lineColour = 0.6*[.2,.2,1];  %[0,0,1];
end
    
%%
trueValue = true;
hold on; axis square;

[X,Y,T_basic,AUC,OPTROCPT_basic] = perfcurve(isDeceased, direction*predictions, trueValue);
% [~,~,T_basic,~,OPTROCPT_basic,~,~] = perfcurve(isPositive, predictions, true);


plot(100*X, 100*Y, 'LineWidth', 2.5, 'Color', lineColour);
xlabel('FPR (1- Specificity)', 'FontSize', FONT_SIZE, 'FontName', FONT_NAME);
ylabel('TPR (Sensitivity)', 'FontSize', FONT_SIZE, 'FontName', FONT_NAME);

% cutoff no. 4
valuePerICutoff = ((1-X) + Y)/2; [~, bestIndex] = max(valuePerICutoff);

% cutoff no. 2
[~,~,T_weighted,~,OPTROCPT_weighted,~,~] = perfcurve(isDeceased,  direction*predictions, trueValue, 'Cost', [0 0.7;0.3 0]);

% cutoff no. 3
if ( direction > 0)
    isPredictedTrue = predictions>expertCutoff; scaleLeg = 0.1;
else
    isPredictedTrue = predictions<expertCutoff; scaleLeg = 0.2;
end
cTable.TP = sum(isPredictedTrue & isDeceased);
cTable.FP = sum(isPredictedTrue & ~isDeceased);
cTable.FN = sum(~isPredictedTrue & isDeceased);
cTable.TN = sum(~isPredictedTrue & ~isDeceased);
cTable.TPR = cTable.TP / (cTable.TP + cTable.FN);   % Sensitivity
cTable.FPR = cTable.FP / (cTable.FP + cTable.TN);   % 1- Specificity

nCutoffs = 4;
xValue = zeros(nCutoffs, 1);
cutoffs = table(xValue);
cutoffs.yValue = zeros(nCutoffs, 1);
cutoffs.cutoffValue = zeros(nCutoffs, 1);
cutoffs.name = cell(nCutoffs, 1);
cutoffs.hLeg = zeros(nCutoffs, 1);
iC = 1; 
cutoffs.xValue(iC) = OPTROCPT_basic(1);     cutoffs.yValue(iC) = OPTROCPT_basic(2);     cutoffs.cutoffValue(iC) = direction*T_basic((X==OPTROCPT_basic(1))&(Y==OPTROCPT_basic(2)));             cutoffs.name{iC} = 'Slope based'; iC = iC + 1;
cutoffs.xValue(iC) = OPTROCPT_weighted(1);  cutoffs.yValue(iC) = OPTROCPT_weighted(2);  cutoffs.cutoffValue(iC) = direction*T_weighted((X==OPTROCPT_weighted(1))&(Y==OPTROCPT_weighted(2)));    cutoffs.name{iC} = 'Weighted optimal'; iC = iC + 1;
cutoffs.xValue(iC) = cTable.FPR;            cutoffs.yValue(iC) = cTable.TPR;            cutoffs.cutoffValue(iC) = expertCutoff;                                                                 cutoffs.name{iC} = 'Expert based'; iC = iC + 1;
cutoffs.xValue(iC) = X(bestIndex);          cutoffs.yValue(iC) = Y(bestIndex);          cutoffs.cutoffValue(iC) = direction*T_basic(bestIndex);                                                 cutoffs.name{iC} = 'Maximal spec.+sens.'; iC = iC + 1;

if (plotGrayscale)
    myColourmap = flipud(gray(nCutoffs+2)); myColourmap = myColourmap(2:end-1,:);
else
    myColourmap = flipud(autumn(nCutoffs+2)); myColourmap = myColourmap(2:end-1,:);
end
myMarks = {'s', 'o', 'h', 'd'};
myMarkSizes = [7, 7, 9, 7];
for iC = 1:nCutoffs
    cutoffs.hLeg(iC) = plot(100*cutoffs.xValue(iC), 100*cutoffs.yValue(iC), '.',...
    'Marker'          , myMarks{iC}         , ...
    'MarkerSize'      , myMarkSizes(iC)    , ... 
    'MarkerEdgeColor' , myColourmap(iC,:)/2 , ...
    'MarkerFaceColor' , myColourmap(iC,:));
    cutoffs.legend{iC} = sprintf('{\\bf %s} cut-off: %.4f\n\t spec. %d, sens. %d', cutoffs.name{iC}, (cutoffs.cutoffValue(iC)), round(100*(1-cutoffs.xValue(iC))), round(100*cutoffs.yValue(iC)));
end
set(gca, 'FontSize', FONT_SIZE, 'FontName', FONT_NAME);
epsilon = 0.01;
xlim(100*[-epsilon, 1]); ylim(100*[0, 1+epsilon]);

title(sprintf('%s AUC: %.2f', featureNameToPrint, AUC), 'FontSize', FONT_SIZE, 'FontName', FONT_NAME);
h = legend(cutoffs.hLeg, cutoffs.legend, 'Location', 'East', 'FontSize', FONT_SIZE-4, 'FontName', FONT_NAME);
legPos = get(h, 'Position'); legPos(1) = legPos(1) + legPos(3)*scaleLeg; legPos(2) = legPos(2) - legPos(4)/5; set(h, 'Position', legPos); %legPos(1) = legPos(1) - legPos(3)/10;
legend boxoff

cutoffs.colourmap = myColourmap;
cutoffs.marks = myMarks';
cutoffs.markSizes = myMarkSizes';