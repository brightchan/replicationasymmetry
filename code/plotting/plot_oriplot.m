function hPlot = plot_oriplot(signatureExposuresComplex_cell, iSignature, isSampleUsed, binSize, nORIValues, ORI_text, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name, legShiftHeightFactor)

maxValueBP = binSize*nORIValues/2;
if (maxValueBP>5e5)
    maxValueBP_text = sprintf('%dMbp', round(maxValueBP/1e6));
else
    maxValueBP_text = sprintf('%dkbp', round(maxValueBP/1e3));
end

if (~exist('legShiftHeightFactor', 'var'))
    legShiftHeightFactor = 2;
end

if (sum(isSampleUsed)>0)
    plusStrandMean = NaN*ones(nORIValues, 1);
    plusStrandStd = NaN*ones(nORIValues, 1);
    plusStrandSE = NaN*ones(nORIValues, 1);
    minusStrandMean = NaN*ones(nORIValues, 1);
    minusStrandStd = NaN*ones(nORIValues, 1);
    minusStrandSE = NaN*ones(nORIValues, 1);
    for iValue = 1:nORIValues
        plusStrandMean(iValue) = nanmean(signatureExposuresComplex_cell{iValue}.mfPlus(iSignature, (isSampleUsed)));
        plusStrandStd(iValue) = nanstd(signatureExposuresComplex_cell{iValue}.mfPlus(iSignature, (isSampleUsed)));
        plusStrandSE(iValue) = nanstd(signatureExposuresComplex_cell{iValue}.mfPlus(iSignature, (isSampleUsed)))/sqrt(sum(isSampleUsed));
        minusStrandMean(iValue) = nanmean(signatureExposuresComplex_cell{iValue}.mfMinus(iSignature, (isSampleUsed)));
        minusStrandStd(iValue) = nanstd(signatureExposuresComplex_cell{iValue}.mfMinus(iSignature, (isSampleUsed)));
        minusStrandSE(iValue) = nanstd(signatureExposuresComplex_cell{iValue}.mfMinus(iSignature, (isSampleUsed)))/sqrt(sum(isSampleUsed));
    end
    maxVal = max([plusStrandMean; minusStrandMean]);
    minVal = min([plusStrandMean; minusStrandMean]);
    
    
    exposuresMean = [plusStrandMean; minusStrandMean];
    quantile75mean = quantile(exposuresMean, .75);
    quantile25mean = quantile(exposuresMean, .25);
    k = 2;
    minValue = max([minVal, quantile25mean - k*(quantile75mean-quantile25mean)]);
    maxValue = min([maxVal, quantile75mean + k*(quantile75mean-quantile25mean)]);
    yLimVal = [minValue, maxValue];
    diffVal = maxValue - minValue;
    if (nORIValues > 200)
        gcaPos = get(gca, 'Position'); gcaHeight = 0.15; gcaWidth = 0.2;%gcaPos(4)/gcaPos(3); %gcaHeight = gcaPos(4); gcaWidth = gcaPos(4)/gcaPos(3); gcaHeight*gcaPos(3)/gcaPos(4); %gcaPos(3); gcaHeight = gcaPos(4);
        sizeX = round(nORIValues/(700*gcaWidth));
        sizeY = round(diffVal/(700*gcaHeight));
        opacity = 0.5;
        xVal = (1:nORIValues)';
        if (plotMedianFilter)
            filterSize = 240;
            medFiltPlusStrandMean = medfilt2(plusStrandMean, [filterSize 1], 'symmetric');
            medFiltMinusStrandMean = medfilt2(minusStrandMean, [filterSize 1], 'symmetric');
            
            %             filterSize = 501; sigmaValue = 10;
            %             medFiltPlusStrandMean = imgaussfilt(plusStrandMean, sigmaValue, 'FilterSize', filterSize);
            %             medFiltMinusStrandMean = imgaussfilt(minusStrandMean, sigmaValue, 'FilterSize', filterSize);
            hPlot(1) = plot(xVal, medFiltPlusStrandMean, '-', 'Color', colours.plus, 'LineWidth', 2);
            hPlot(2) = plot(xVal, medFiltMinusStrandMean, '-', 'Color', colours.minus, 'LineWidth', 2);
            yLimVal = [0, max([medFiltPlusStrandMean; medFiltMinusStrandMean])];
        else
            xValues = 1:nORIValues;
            plotRibbon = false;
            if (plotRibbon)
                h = patch([xValues, fliplr(xValues)], [smooth(plusStrandMean+plusStrandSE, 51)', fliplr(smooth(plusStrandMean-plusStrandSE, 51)')], colours.plus, 'EdgeColor', 'none');    set(h, 'FaceAlpha', 0.5);
                h = patch([xValues, fliplr(xValues)], [smooth(minusStrandMean+minusStrandSE, 51)', fliplr(smooth(minusStrandMean-minusStrandSE, 51)')], colours.minus, 'EdgeColor', 'none');    set(h, 'FaceAlpha', 0.5);
            end
            %             
            ylim(yLimVal); 
            hPlot(1) = plot(3*maxVal, 'o', 'MarkerFaceColor', colours.plus, 'MarkerEdgeColor', 'none', 'MarkerSize', 4);
            hPlot(2) = plot(3*maxVal, 'o', 'MarkerFaceColor', colours.minus, 'MarkerEdgeColor', 'none', 'MarkerSize', 4);
            transparentScatter(xVal,plusStrandMean,sizeX,sizeY,opacity,colours.plus);
            transparentScatter(xVal,minusStrandMean,sizeX,sizeY,opacity,colours.minus);
        end
    else
        xValues = 1:nORIValues;
        plotRibbon = false;
        if (plotRibbon)
            h = patch([xValues, fliplr(xValues)], [smooth(plusStrandMean+plusStrandSE)', fliplr(smooth(plusStrandMean-plusStrandSE)')], colours.plus, 'EdgeColor', 'none');    set(h, 'FaceAlpha', 0.5);
            h = patch([xValues, fliplr(xValues)], [smooth(minusStrandMean+minusStrandSE)', fliplr(smooth(minusStrandMean-minusStrandSE)')], colours.minus, 'EdgeColor', 'none');    set(h, 'FaceAlpha', 0.5);
        end
        hPlot(1) = plot(plusStrandMean, 'o', 'MarkerFaceColor', colours.plus, 'MarkerEdgeColor', 'none', 'MarkerSize', 3.5);
        hPlot(2) = plot(minusStrandMean, 'o', 'MarkerFaceColor', colours.minus, 'MarkerEdgeColor', 'none', 'MarkerSize', 3.5);
    end
    
    xlim([1, nORIValues]); set(gca, 'XTick', []); 
    ylim(yLimVal); yLimVal = get(gca, 'YLim'); xLimVal = get(gca, 'XLim');
    plot(mean(xLimVal)*[1,1], yLimVal, ':k');
    if (~isempty(title_text))
        title(title_text); %signatureNames{iSignature});
    end
    axis normal;
    if (plot_yLabel) %mod(iSignature, nC) == 1)
        ylabel('average exposure', 'FontSize', font_size-2, 'FontName', font_name);
    end
    if (plot_xLabel) %iSignature > nSignatures-nC)
        text(xLimVal(1), yLimVal(1), ['-', maxValueBP_text], 'HorizontalAlignment', 'left', 'VerticalAlignment', 'top', 'FontSize', font_size, 'FontName', font_name);
        text(mean(xLimVal), yLimVal(1), ORI_text, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', font_size, 'FontName', font_name);
        text(xLimVal(2), yLimVal(1), ['+', maxValueBP_text], 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'FontSize', font_size, 'FontName', font_name);
        %xlabel('leading - lagging');
        h = legend({'plus strand', 'minus strand'}, 'Location', 'South', 'FontSize', font_size, 'FontName', font_name); legPos = get(h, 'Position'); legPos(2) = legPos(2) - legShiftHeightFactor*legPos(4); set(h, 'Position', legPos);
    end
    set(gca, 'FontSize', font_size-2, 'FontName', font_name);
    drawnow;
end

%%
