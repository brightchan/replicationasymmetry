function [signatureResults] = plotExposureResults_histogramSignatures(replData, territoriesSource)

nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix = replData.signatureExposuresSimple.LeadLagg.type1Minustype2;

minMeanExposure = 10;
font_size = 10; font_name = 'Lucida Console';
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7; 
colours.tissuesColourmap = hsv(nTissues);

imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);
%%
pValue = NaN*ones(nSignatures, 1);
signatureResults = table(pValue);
signatureResults.meanExposure = NaN*ones(nSignatures, 1);
signatureResults.medianExposure = NaN*ones(nSignatures, 1);
signatureResults.hasStrandAsymmetry = false(nSignatures, 1);
signatureResults.colourTitle = zeros(nSignatures, 3);
signatureResults.colourPositiveBars = zeros(nSignatures, 3);
signatureResults.colourNegativeBars = zeros(nSignatures, 3);
signatureResults.colourGCA = zeros(nSignatures, 3);
for iSignature = 1:nSignatures
    isSampleUsed = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
    exposuresDifference = signatureExposuresSimple_matrix(iSignature, isSampleUsed);
    signatureResults.pValue(iSignature) = signrank(exposuresDifference);   
    signatureResults.meanExposure(iSignature) = mean(exposuresDifference);
    signatureResults.medianExposure(iSignature) = median(exposuresDifference);            
end
[~, ~, ~, qValues]=fdr_bh(signatureResults.pValue);
signatureResults.qValue_BenjaminiHochberg = qValues;
signatureResults.qValue_Bonferroni = signatureResults.pValue*nSignatures;
signatureResults.isLeading = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure > 0); %signatureResults.meanExposure > 0 & 
signatureResults.isLagging = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure < 0); %signatureResults.meanExposure < 0 & 
signatureResults.hasStrandAsymmetry(signatureResults.isLeading | signatureResults.isLagging) = true;
for iSignature = 1:nSignatures
    if (signatureResults.isLeading(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: leading %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.leadingDark;
    elseif (signatureResults.isLagging(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: lagging %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.laggingDark;
    else
        signatureResults.titleText{iSignature} = sprintf('%s n.s.', signatureNames{iSignature});
        signatureResults.colourTitle(iSignature,:) = colours.titleNotSignif;
    end
    if (signatureResults.hasStrandAsymmetry(iSignature))        
        signatureResults.colourPositiveBars(iSignature,:) = colours.leading; 
        signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
    else
        signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif; 
        signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
        signatureResults.colourGCA(iSignature,:) = colours.titleNotSignif;
    end
end
%% HISTOGRAMS SIGNATURES ___________________________________________________
fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',2*[0 0 30 20],'units','normalized','outerposition',[0 0 1 1]);
nC = 5; nR = ceil(nSignatures/nC); iS = 1; xS = .9; yS = .7;
for iSignature = 1:nSignatures
    myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1;
    isSampleUsed = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
    plot_xLabel = (iSignature > nSignatures-nC);
    plot_yLabel = (mod(iSignature, nC) == 1);
    title_text = signatureResults.titleText{iSignature};
    plot_histogram(signatureExposuresSimple_matrix, iSignature, isSampleUsed, signatureResults, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name)
end
suptitle(sprintf('%s: distribution of leading-lagging exposures to signatures in patients', strrep(territoriesSource, '_', ' ')));
mySaveAs(fig, imagesPath, ['signaturesBiasDistribution_', territoriesSource, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
%%