% function plotExposureResults_clustering(replData, territoriesSource, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k)

%%

%%
% Here we will plot signature plot, H ori plot, B ori plot, repl timing barplot, histogram, etc.
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', 'S');
signatureNames = strrep(signatureNames, 'Signature', 'Signature ');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
tableAllSamples = replData.tableAllSamples; nSamples = size(tableAllSamples, 1);
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix = signatureExposuresSimple_LeadLagg.type1Minustype2;
signatureExposuresComplex_cell_ORI_Haradhvala = replData.signatureExposuresComplex.DistanceFromORI_LeadLagg;
signatureExposuresComplex_RT = replData.signatureExposuresComplex.ReplicationTiming_LeadLagg;

nBins = length(signatureExposuresComplex_RT);


binSize_Haradhvala = 20000;
binSize_Besnard1k = 1000;
nORIValues_Haradhvala = length(signatureExposuresComplex_cell_ORI_Haradhvala); %replData.dataFields.nValues.DistanceFromORI_LeadLagg;
nORIValues_Besnard1k = length(signatureExposuresComplex_cell_Besnard1k);
ORI_text_Haradhvala = 'left/right transition';
ORI_text_Besnard = 'ORI';

finalSignatures = decomposedSignatures.finalSignatures;
finalSignatures.nSignatures = length(finalSignatures.signaturesNames);
types = decomposedSignatures.types;
subtypes = decomposedSignatures.subtypes;
strandSpecText = 'strandSpec';
%%
complexType = 'ReplicationTiming_LeadLagg';     nCurrentValues_RT = replData.dataFields.nValues.ReplicationTiming_LeadLagg;    labelsExtremes = {'early', 'late'};
% complexType = 'NOS_LeadLagg';     nCurrentValues = nValues.NOS_LeadLagg;    labelsExtremes = {'low', 'high'};
% typeNames = {'type1Minustype2', 'totalMF', 'type1', 'type2'};
% printNames = {'leading - lagging', 'total exposure', 'leading', 'lagging'};
signatureExposuresComplex_cell_RT_diff = replData.signatureExposuresComplex.(complexType);
%%
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_combined/']; createDir(imagesPath);
minMeanExposure = 10;
font_size = 10;
font_name = 'Helvetica';% 'Lucida Sans'; % Console
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%% clear signatureResults
pValue = NaN*ones(nSignatures, 1);
signatureResults = table(pValue);
signatureResults.meanExposure = NaN*ones(nSignatures, 1);
signatureResults.medianExposure = NaN*ones(nSignatures, 1);
signatureResults.hasStrandAsymmetry = false(nSignatures, 1);
signatureResults.colourTitle = zeros(nSignatures, 3);
signatureResults.colourPositiveBars = zeros(nSignatures, 3);
signatureResults.colourNegativeBars = zeros(nSignatures, 3);
signatureResults.colourGCA = zeros(nSignatures, 3);
signatureResults.titleText = cell(nSignatures, 3);
signatureResults.meanExposureFC = NaN*ones(nSignatures, 1);
signatureResults.percentagePositive = NaN*ones(nSignatures, 1);
% foldChange = log2(signatureExposuresSimple_LeadLagg.type1./signatureExposuresSimple_LeadLagg.type2);
for iSignature = 1:nSignatures
    isSampleUsed = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
    exposuresDifference = signatureExposuresSimple_matrix(iSignature, isSampleUsed);
    signatureResults.pValue(iSignature) = signtest(exposuresDifference); %signrank
    signatureResults.meanExposure(iSignature) = mean(exposuresDifference);
    signatureResults.medianExposure(iSignature) = median(exposuresDifference);
    %signatureResults.meanExposureFC(iSignature) = nanmean(foldChange(iSignature, isSampleUsed));
    signatureResults.meanExposureFC(iSignature) = log2(nanmean(signatureExposuresSimple_LeadLagg.type1(iSignature, isSampleUsed))/nanmean(signatureExposuresSimple_LeadLagg.type2(iSignature, isSampleUsed)));
    signatureResults.percentagePositive(iSignature) = 100*mean(exposuresDifference>0);
end
[~, ~, ~, qValues]=fdr_bh(signatureResults.pValue);
signatureResults.qValue_BenjaminiHochberg = qValues;
signatureResults.qValue_Bonferroni = signatureResults.pValue*nSignatures;
signatureResults.isLeading = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure > 0); %signatureResults.meanExposure > 0 &
signatureResults.isLagging = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure < 0); %signatureResults.meanExposure < 0 &
signatureResults.hasStrandAsymmetry(signatureResults.isLeading | signatureResults.isLagging) = true;
for iSignature = 1:nSignatures
    if (signatureResults.isLeading(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: leading %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.leadingDark;
    elseif (signatureResults.isLagging(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: lagging %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.laggingDark;
    else
        signatureResults.titleText{iSignature} = sprintf('%s n.s.', signatureNames{iSignature});
        signatureResults.colourTitle(iSignature,:) = colours.titleNotSignif;
    end
    if (signatureResults.hasStrandAsymmetry(iSignature))
        signatureResults.colourPositiveBars(iSignature,:) = colours.leading;
        signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
    else
        signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif;
        signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;%colours.titleNotSignif;
    end
end
%%
tissueGroupNames = {'POLE', 'MSI', 'lung', 'skin', 'gastro_oesophageal', 'liver', 'kidney', ''};
tissueGroupsSignatures = {[10, 13, 17, 25], [6, 14, 19, 20, 24, 29], [4], [7, 28], [16], [11, 15], [21], [1,2,3,4,5,7,8,9,11,12,15,16,18,21,22,23,26,27,28, 6, 14, 19, 20, 24, 29]}; %1:nSignatures
tissueGroups = {{'POLE'}, {'MSI'}, {'lung_adenocarcinoma','lung_squamous'}, {'skin'}, {'oesophagus_adenocarcinoma', 'gastric'}, {'liver'}, {'kidney_clear_cell'}, {''}};
lstTissueGroupIndex = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8];
lstIsInTissue = [true, false, true, false, true, false, true, false, true, false, true, false, true, false, false]; nTissueGroups = length(lstTissueGroupIndex);
mfTypes = {'totalMF', 'type1', 'type2'};
for iMFType = 1:length(mfTypes)
    mfType = mfTypes{iMFType};
    structSignatureResults.(mfType).RT_slope = NaN*ones(nSignatures, nTissueGroups);
    structSignatureResults.(mfType).RT_log2FC = NaN*ones(nSignatures, nTissueGroups);
    structSignatureResults.(mfType).RT_pVal = NaN*ones(nSignatures, nTissueGroups);
end
structSignatureResults.meanExposureDifference = NaN*ones(nSignatures, nTissueGroups);
structSignatureResults.medianExposureDifference = NaN*ones(nSignatures, nTissueGroups);
structSignatureResults.meanLog2FCDifference = NaN*ones(nSignatures, nTissueGroups);
structSignatureResults.percentagePositiveExposureDifferences = NaN*ones(nSignatures, nTissueGroups);
structSignatureResults.pValueExposureDifference = NaN*ones(nSignatures, nTissueGroups);
structSignatureResults.nSamples = NaN*ones(nSignatures, nTissueGroups);
structSignatureResults.isRelevantPair = false(nSignatures, nTissueGroups);
structSignatureResults.isAllSamples = false(nSignatures, nTissueGroups);
structSignatureResults.isSpecificTissue = false(nSignatures, nTissueGroups);
structSignatureResults.isNotSpecificTissue = false(nSignatures, nTissueGroups);
structSignatureResults.fullSignatureName = cell(nSignatures, nTissueGroups);
for iTissueGroup = 1:nTissueGroups
    structSignatureResults.isRelevantPair(tissueGroupsSignatures{lstTissueGroupIndex(iTissueGroup)}, iTissueGroup) = true;
    if (lstIsInTissue(iTissueGroup))
        isThisT = ismember(tableAllSamples.Tissue, tissueGroups{lstTissueGroupIndex(iTissueGroup)})';
        groupName = tissueGroupNames{lstTissueGroupIndex(iTissueGroup)};
        structSignatureResults.isAllSamples(:, iTissueGroup) = false;
        structSignatureResults.isSpecificTissue(:, iTissueGroup) = true;
        structSignatureResults.isNotSpecificTissue(:, iTissueGroup) = false;
    else
        isThisT = ~ismember(tableAllSamples.Tissue, tissueGroups{lstTissueGroupIndex(iTissueGroup)})';
        if (isempty(tissueGroupNames{lstTissueGroupIndex(iTissueGroup)}))
            structSignatureResults.isAllSamples(:, iTissueGroup) = true;
            structSignatureResults.isSpecificTissue(:, iTissueGroup) = false;
            structSignatureResults.isNotSpecificTissue(:, iTissueGroup) = false;
            groupName = 'all';
        else
            groupName = ['not ', tissueGroupNames{lstTissueGroupIndex(iTissueGroup)}];
            structSignatureResults.isAllSamples(:, iTissueGroup) = false;
            structSignatureResults.isSpecificTissue(:, iTissueGroup) = false;
            structSignatureResults.isNotSpecificTissue(:, iTissueGroup) = true;
        end
    end
    for iSignature = 1:nSignatures
        structSignatureResults.fullSignatureName{iSignature, iTissueGroup} = sprintf('%s %s', signatureShortNames{iSignature}, strrep(groupName, '_', ' '));
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        structSignatureResults.nSamples(iSignature, iTissueGroup) = sum(isSampleUsed);
        if (sum(isSampleUsed)>1)
            exposuresDifference = signatureExposuresSimple_matrix(iSignature, isSampleUsed);
            structSignatureResults.percentagePositiveExposureDifferences(iSignature, iTissueGroup) = 100*mean(exposuresDifference>0);
            structSignatureResults.pValueExposureDifference(iSignature, iTissueGroup) = signtest(exposuresDifference); %signrank
            structSignatureResults.meanExposureDifference(iSignature, iTissueGroup) = mean(exposuresDifference);
            structSignatureResults.medianExposureDifference(iSignature, iTissueGroup) = median(exposuresDifference);
            structSignatureResults.meanLog2FCDifference(iSignature, iTissueGroup) = log2(nanmean(signatureExposuresSimple_LeadLagg.type1(iSignature, isSampleUsed))/nanmean(signatureExposuresSimple_LeadLagg.type2(iSignature, isSampleUsed)));
            for iMFType = 1:length(mfTypes)
                mfType = mfTypes{iMFType};
                exposureInRTPerPatient.(mfType) = NaN*ones(nBins, nSamples);
                for iBin = 1:length(signatureExposuresComplex_RT)
                    exposureInRTPerPatient.(mfType)(iBin, :) = signatureExposuresComplex_RT{iBin}.(mfType)(iSignature, :);
                end
                replicationTimingMean = mean(exposureInRTPerPatient.(mfType)(:,isSampleUsed), 2);
                linearModel = fitlm((1:nBins), replicationTimingMean);
                structSignatureResults.(mfType).RT_pVal(iSignature, iTissueGroup) = coefTest(linearModel, [0,1]);
                if (structSignatureResults.(mfType).RT_pVal(iSignature, iTissueGroup) < 0.05)
                    structSignatureResults.(mfType).RT_slope(iSignature, iTissueGroup) = linearModel.Coefficients{2,1};
                    structSignatureResults.(mfType).RT_log2FC(iSignature, iTissueGroup) = log2(linearModel.Fitted(end)/linearModel.Fitted(1));
                end
            end
            for iORIType = 1:2
                if (iORIType == 1)
                    signatureExposuresComplex_cell = signatureExposuresComplex_cell_Besnard1k; oriType = 'Besnard1k';
                else
                    signatureExposuresComplex_cell = replData.signatureExposuresComplex.DistanceFromORI_LeadLagg; oriType = 'Haradhvala';
                end
                nORIValues = length(signatureExposuresComplex_cell); nOB = round(nORIValues/16); half = round(nORIValues/2);
                plusStrandMean = NaN*ones(nORIValues, 1);
                plusStrandStd = NaN*ones(nORIValues, 1);
                minusStrandMean = NaN*ones(nORIValues, 1);
                minusStrandStd = NaN*ones(nORIValues, 1);
                for iValue = 1:nORIValues
                    plusStrandMean(iValue) = nanmean(signatureExposuresComplex_cell{iValue}.mfPlus(iSignature, (isSampleUsed)));
                    plusStrandStd(iValue) = nanstd(signatureExposuresComplex_cell{iValue}.mfPlus(iSignature, (isSampleUsed)));
                    minusStrandMean(iValue) = nanmean(signatureExposuresComplex_cell{iValue}.mfMinus(iSignature, (isSampleUsed)));
                    minusStrandStd(iValue) = nanstd(signatureExposuresComplex_cell{iValue}.mfMinus(iSignature, (isSampleUsed)));
                end
                plusStrandMean_flipud = flipud(plusStrandMean);     plusStrandStd_flipud = flipud(plusStrandStd);
                minusStrandMean_flipud = flipud(minusStrandMean);   minusStrandStd_flipud = flipud(minusStrandStd);
                leading_distantFromORI = nanmean([plusStrandMean(1:nOB); minusStrandMean_flipud(1:nOB)]);
                leading_aroundORI = nanmean([plusStrandMean(half-nOB:half); minusStrandMean_flipud(half-nOB:half)]);
                lagging_distantFromORI = nanmean([minusStrandMean(1:nOB); plusStrandMean_flipud(1:nOB)]);
                lagging_aroundORI = nanmean([minusStrandMean(half-nOB:half); plusStrandMean_flipud(half-nOB:half)]);
                structSignatureResults.(oriType).ORI_log2FC_leading(iSignature, iTissueGroup) = log2(leading_distantFromORI/leading_aroundORI);
                structSignatureResults.(oriType).ORI_log2FC_lagging(iSignature, iTissueGroup) = log2(lagging_distantFromORI/lagging_aroundORI);
                structSignatureResults.(oriType).ORI_stdOverMean_leading(iSignature, iTissueGroup) = nanmean([plusStrandStd(1:half); minusStrandStd_flipud(1:half)] ./ [plusStrandMean(1:half); minusStrandMean_flipud(1:half)]);
                structSignatureResults.(oriType).ORI_stdOverMean_lagging(iSignature, iTissueGroup) = nanmean([minusStrandStd(1:half); plusStrandStd_flipud(1:half)] ./ [minusStrandMean(1:half); plusStrandMean_flipud(1:half)]);
                filterSize = round(nORIValues/8); %240;
                medFiltPlusStrandMean = medfilt2(plusStrandMean, [filterSize 1], 'symmetric');
                medFiltMinusStrandMean = medfilt2(minusStrandMean, [filterSize 1], 'symmetric');
                structSignatureResults.(oriType).ORI_log2FC_plus(iSignature, iTissueGroup) = log2(nanmean(medFiltPlusStrandMean(1:nOB))/nanmean(medFiltPlusStrandMean(3*nOB:4*nOB)));
                structSignatureResults.(oriType).ORI_log2FC_minus(iSignature, iTissueGroup) = log2(nanmean(medFiltMinusStrandMean(1:nOB))/nanmean(medFiltMinusStrandMean(3*nOB:4*nOB)));
                %
                if (structSignatureResults.isRelevantPair(iSignature, iTissueGroup))
                    fig = createMaximisedFigure(15); hold on;
                    for iOB = 1:nOB:nORIValues-nOB
                        bar(iOB, nanmean(plusStrandMean(iOB+(1:nOB))), nOB/2, 'FaceColor', colours.plus);
                        bar(iOB+nOB/2, nanmean(minusStrandMean(iOB+(1:nOB))), nOB/2, 'FaceColor', colours.minus);
                    end
                    xlim([-nOB/2, nORIValues-nOB]); ylabel('median exposure');
                    title({[structSignatureResults.fullSignatureName{iSignature, iTissueGroup}, ' ', oriType],...
                        sprintf('log2FC: %.1f, %.1f and std/mean: %.1f, %.1f', ...
                        structSignatureResults.(oriType).ORI_log2FC_leading(iSignature, iTissueGroup), structSignatureResults.(oriType).ORI_log2FC_lagging(iSignature, iTissueGroup), ...
                        structSignatureResults.(oriType).ORI_stdOverMean_leading(iSignature, iTissueGroup), structSignatureResults.(oriType).ORI_stdOverMean_lagging(iSignature, iTissueGroup))});
                    mySaveAs(fig, imagesPath, ['mean_', structSignatureResults.fullSignatureName{iSignature, iTissueGroup}, '_', oriType, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
                end
            end
        end
    end
end
%     structSignatureResults.RT_slope.(mfType)(structSignatureResults.RT_pVal.(mfType)>=0.05) = NaN;
%     structSignatureResults.RT_log2FC.(mfType)(structSignatureResults.RT_pVal.(mfType)>=0.05) = NaN;
% %%
% structSignatureResults.Besnard1k.ORI_log2FC_leading(structSignatureResults.Besnard1k.ORI_log2FC_leading < -3) = -3;
%%
structSignatureResults.isRelevantPair(:,4) = false; %remove not MSI
structSignatureResults.isRelevantPair(:,6) = false; %remove not lung
structSignatureResults.isRelevantPair(:,8) = false; %remove not skin
structSignatureResults.isRelevantPair(:,10) = false; %remove not gastro_oeso
structSignatureResults.isRelevantPair(:,12) = false; %remove not liver
structSignatureResults.isRelevantPair(:,14) = false; %remove not kidney
structSignatureResults.isRelevantPair([4,6,7,11,14,15,16,19,20,21,24,29],end) = false; %remove specific signatures from all tissues
exposureDifference_percentagePositive = 6*structSignatureResults.percentagePositiveExposureDifferences(:)/100-3;
% exposure_log2FC = structSignatureResults.meanLog2FCDifference(:);
clusteringData = table(exposureDifference_percentagePositive);
clusteringData.exposure_log2FC = structSignatureResults.meanLog2FCDifference(:);
% clusteringData.timing_slope = structSignatureResults.totalMF.RT_slope(:);
clusteringData.timing_log2FC = structSignatureResults.totalMF.RT_log2FC(:);
% clusteringData.timing_log10PValue = log10(structSignatureResults.totalMF.RT_pVal(:));
% clusteringData.exposureDifference_mean = structSignatureResults.meanExposureDifference(:);
% clusteringData.exposureDifference_log10PValue = log10(structSignatureResults.pValueExposureDifference(:));
% isSignificant = (structSignatureResults.pValueExposureDifference(:)<0.001);
% clusteringData.exposureDifference_significant = -log10(structSignatureResults.pValueExposureDifference(:))/20;% ones(size(isSignificant));
% clusteringData.exposureDifference_significant(exposureDifference_percentagePositive<0) = -1*clusteringData.exposureDifference_significant(exposureDifference_percentagePositive<0);
% clusteringData.exposureDifference_significant(~isSignificant & structSignatureResults.nSamples(:)>20) = 0;
% clusteringData.exposureDifference_median = structSignatureResults.medianExposureDifference(:);
clusteringData.exposure_log2FC = structSignatureResults.meanLog2FCDifference(:);
% clusteringData.ORI_Besnard1k_log2FC_plus = structSignatureResults.Besnard1k.ORI_log2FC_plus(:);
% clusteringData.ORI_Besnard1k_log2FC_minus = structSignatureResults.Besnard1k.ORI_log2FC_minus(:);
% clusteringData.ORI_Haradhvala_log2FC_plus = structSignatureResults.Haradhvala.ORI_log2FC_plus(:);
% clusteringData.ORI_Haradhvala_log2FC_minus = structSignatureResults.Haradhvala.ORI_log2FC_minus(:);
clusteringData.ORI_Besnard1k_log2FC_leading = structSignatureResults.Besnard1k.ORI_log2FC_leading(:);
clusteringData.ORI_Besnard1k_log2FC_lagging = structSignatureResults.Besnard1k.ORI_log2FC_lagging(:);
clusteringData.ORI_Haradhvala_log2FC_leading = structSignatureResults.Haradhvala.ORI_log2FC_leading(:);
clusteringData.ORI_Haradhvala_log2FC_lagging = structSignatureResults.Haradhvala.ORI_log2FC_lagging(:);
clusteringSignatureNames = structSignatureResults.fullSignatureName(:);
isRelevantRow = structSignatureResults.isRelevantPair(:) & structSignatureResults.nSamples(:)>0;
clusteringData = clusteringData(isRelevantRow,:);
clusteringSignatureNames = clusteringSignatureNames(isRelevantRow);
matrixClusteringData = table2array(clusteringData);
matrixClusteringData(matrixClusteringData < -3) = -3;
matrixClusteringData(matrixClusteringData > 3) = 3;
% matrixClusteringData(isinf(matrixClusteringData)) = 0;
matrixClusteringData(isnan(matrixClusteringData)) = 0;
matrixClusteringDataOriginal = matrixClusteringData; %% matrixClusteringDataOriginal(isnan(matrixClusteringDataOriginal)) = 0;
% matrixClusteringData = matrixClusteringDataOriginal;
meanPerCol = nanmean(matrixClusteringData, 1);
stdPerCol = nanstd(matrixClusteringData, 1);
nRows = size(matrixClusteringData, 1);
matrixClusteringData = (((matrixClusteringData-repmat(meanPerCol, nRows, 1)) ./ repmat(stdPerCol, nRows, 1))); %zscore(matrixClusteringData)';
% %% 2D
if (false)
    font_size = 14;
    fig = createMaximisedFigure(1);
    xType = 'exposure_log2FC'; xLabelText = 'Fold change in exposures: log2(matching/opposite)'; yType = 'timing_log2FC'; yLabelText = 'Fold change in replication timing: log2(late/early)';
    xVal = clusteringData.(xType); yVal = clusteringData.(yType);
    plot(xVal, yVal, 'o', 'MarkerFaceColor', [1,.5,0], 'MarkerEdgeColor', [1,.5,0]/2, 'MarkerSize', 10);
    text(xVal, yVal, strcat('...', strrep(clusteringSignatureNames, '_', ' ')), 'FontSize', font_size); set(gca, 'FontSize', font_size);
    xlabel(xLabelText, 'FontSize', font_size); ylabel(yLabelText, 'FontSize', font_size);
end
%% PCA
w = 1./var(matrixClusteringData); %, 'VariableWeights',w
[wcoeff,score,latent,tsquared,explained] = pca(matrixClusteringData); coefforth = inv(diag(std(matrixClusteringData)))*wcoeff;
fig = createMaximisedFigure(2);
xVal = score(:,1); xLabelText = 'PC1';  yVal = score(:,2); yLabelText = 'PC2';
plot(xVal, yVal, 'o', 'MarkerFaceColor', [1,.5,0], 'MarkerEdgeColor', [1,.5,0]/2, 'MarkerSize', 10);
text(xVal, yVal, strcat('...', strrep(clusteringSignatureNames, '_', ' ')), 'FontSize', font_size); set(gca, 'FontSize', font_size);
xlabel(xLabelText, 'FontSize', font_size); ylabel(yLabelText, 'FontSize', font_size);
mySaveAs(fig, imagesPath, ['signaturePCA_', '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
% hold on;
% figure; biplot(coefforth(:,1:2),'scores',score(:,1:2),'varlabels',clusteringData.Properties.VariableNames);
% plot(score(:,1),score(:,2),'+')
% xlabel('1st Principal Component');
% ylabel('2nd Principal Component');
% figure; pareto(explained)
% xlabel('Principal Component')
% ylabel('Variance Explained (%)')
% %%
% fig = createMaximisedFigure(4); imagesc(matrixClusteringData');
% set(gca, 'XTick', []);
% text(1:size(matrixClusteringData, 1), 0.5*ones(size(matrixClusteringData, 1),1), clusteringSignatureNames, 'Rotation', 45, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'bottom');
% set(gca, 'YTick', 1:size(matrixClusteringData, 2), 'YTickLabel', clusteringData.Properties.VariableNames);
%%
% strandSpecText = strandSpecTexts{iStrandSpec};
distanceName = 'euclidean'; %correlation
% nTotalSignatures = 27;
distances = pdist(matrixClusteringData);
tree = linkage(distances, 'complete'); %median
leafOrder = optimalleaforder(tree, distances);
% finalClusters = cluster(Z,'maxclust', nTotalSignatures);
% cmapClusters = (1+hsv(nTotalSignatures))/2; %    colourClusters = cmapClusters(finalClusters, :);
%
fig = figure(3); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',2*[0 0 40 20],'units','normalized','outerposition',[0 0 1 1]);
subplot(2,1,1); [H,T,outperm] = dendrogram(tree, 0, 'reorder', leafOrder); %set(gca, 'XTick', []); set(H, 'Color', [0,0,0]); %%, 'Labels', []); % , 'ColorThreshold',  0.5*max(Z(:,3))
xlim(0.5+[0, length(outperm)]); 
colorbar;
%     xValues = 1:length(finalClusters); yValues1 = -0.04*ones(1, length(finalClusters)); yValues2 = -0.07*ones(1, length(finalClusters));
%     for iCluster = 1:nTotalSignatures
%         isSignatureThisCluster = (finalClusters(outperm) == iCluster);
%         text(xValues(isSignatureThisCluster), yValues1(isSignatureThisCluster), cellstr(num2str(finalClusters(outperm(isSignatureThisCluster)), '%d')), 'HorizontalAlignment', 'center', 'FontWeight', 'bold', 'Background', cmapClusters(iCluster, :), 'Margin', 2);
%         text(xValues(isSignatureThisCluster), yValues2(isSignatureThisCluster), allSignatureNames.(strandSpecText)(outperm(isSignatureThisCluster)), 'HorizontalAlignment', 'right', 'Rotation', 90, 'interpret', 'none', 'FontSize', 9, 'Background', cmapClusters(iCluster, :), 'Margin', 2);
%     end
subplot(2,1,2); imagesc(matrixClusteringDataOriginal(outperm,:)');
set(gca, 'XTick', []);
text(1:size(matrixClusteringData, 1), 0.5*ones(size(matrixClusteringData, 1),1), clusteringSignatureNames(outperm), 'Rotation', 90, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'middle');
set(gca, 'YTick', 1:size(matrixClusteringData, 2), 'YTickLabel', strrep(clusteringData.Properties.VariableNames, '_', ' '));
%     suptitle(sprintf('Denormalised %s', strandSpecText));
%     mySaveAs(fig, imagesPath, ['finalSignaturesMyClusteringTreeFull_complete_denormalised_', strandSpecText]);
colorbar;
mySaveAs(fig, imagesPath, ['signatureClustering_', '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
% 
% save('workspaces\workspace_2017_01_29.mat', '-v7.3');
% save('workspaces\workspace_2017_01_29b.mat', '-v7.3');
%%
if (false)
    special_R23 = false;
    % listSignatures = [12, 10, 17, 16, 4, 6]; %[12, 2, 16, 4, 7, 10, 13, 17, 25, 19, 20, 6, 14, 24, 29];
    %     'Signature 1'     group
    %     'Signature 2'     group APOBEC
    %     'Signature 3'     group
    %     'Signature 4'     group mutagen
    %     'Signature 5'     group symmetric
    %     'Signature 6'     group MSI
    %     'Signature 7'     group mutagen
    %     'Signature 8'     group symmetric
    %     'Signature 9'     group
    %     'Signature 10'     group POLE
    %     'Signature 12'     group liver
    %     'Signature 13'     group APOBEC
    %     'Signature 14'     group POLE
    %     'Signature 15'     group MSI
    %     'Signature 16'     group liver
    %     'Signature 17'     group mutagen
    %     'Signature 18'     group POLE
    %     'Signature 19'     group symmetric
    %     'Signature 20'     group MSI
    %     'Signature 21'     group MSI
    %     'Signature 22'     group mutagen
    %     'Signature 23'     group symmetric
    %     'Signature 25'     group symmetric
    %     'Signature 26'     group MSI
    %     'Signature 28'     group POLE
    %     'Signature  N1'     group
    %     'Signature  N2'     group
    %     'Signature  N3'     group mutagen
    %     'Signature  N4'     group MSI
    % listSignatures = [12, 10, 10, 16, 4, 6];
    iL = 1; clear listSignatures % Good, 5+2+4+6+5+2+5=29
    listSignatures{iL} = [12, 10, 10, 16, 6, 5];    group_names{iL} = '';                   specials_R23{iL} = true;  specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [5, 8, 18, 22, 23];        group_names{iL} = 'no/little asymmetry';specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    specials_R23{iL} = false; specific_tissues{iL} = {'POLE'}; tissues_only{iL} = true; iL = iL + 1;
    listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    specials_R23{iL} = false; specific_tissues{iL} = {'POLE'}; tissues_only{iL} = false; iL = iL + 1;
    listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; iL = iL + 1;
    listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; iL = iL + 1;
    listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [11, 15];                  group_names{iL} = 'liver signatures';   specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [1, 3, 9, 26, 27];         group_names{iL} = 'other signatures';   specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [26, 27, 28, 29];          group_names{iL} = 'new signatures';     specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; iL = iL + 1;
    listSignatures{iL} = [7, 28];        group_names{iL} = 'mutagen signatures'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = true; iL = iL + 1;
    listSignatures{iL} = [7, 28];        group_names{iL} = 'mutagen signatures'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = false; iL = iL + 1;
    listSignatures{iL} = [4];        group_names{iL} = 'mutagen signatures'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_adenocarcinoma','lung_squamous'}; tissues_only{iL} = true; iL = iL + 1;
    listSignatures{iL} = [4];        group_names{iL} = 'mutagen signatures'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_adenocarcinoma','lung_squamous'}; tissues_only{iL} = false; iL = iL + 1;
    
    
    
    for iList = 15:16% 1:length(listSignatures) %[2,3,4,6,7,8]
        currentListSignatures = listSignatures{iList};
        group_name = group_names{iList};
        special_R23 = specials_R23{iList};
        specific_tissue = specific_tissues{iList};
        tissue_only = tissues_only{iList};
        % fig = createMaximisedFigure(1);
        nR_plus = 0;
        if (~isempty(group_name))
            nR_plus = 1;
        end
        %     nR = length(currentListSignatures); nC = 5; iS = 1; xS = 0.8; yS = 0.8; xB = 0.02; yB = 0.08*6/nR; xM = -0.03; yM = -0.01*6/nR;
        nR = length(currentListSignatures); nC = 5; iS = 1; xS = 0.8; yS = 0.8; xB = 0.02; yB = 0.08*log2(6)/max([1, log2(nR)]); xM = -0.03; yM = -0.01;
        fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 42 (nR+nR_plus)*4.4],'units','normalized','outerposition',[0 0 1 1]);
        
        plot_xLabel = false;
        for iR = 1:nR
            iSignature = currentListSignatures(iR);
            title_text = ''; %signatureNames{iSignature};
            if (iSignature == currentListSignatures(end))
                plot_xLabel = true;
            end
            if (special_R23 && iR == 2)
                plot_quartile = false;
                tissue = 'POLE^{mut}';
                isThisT = strcmp(tableAllSamples.Tissue, 'POLE')';
            elseif (special_R23 && iR == 3)
                plot_quartile = true;
                tissue = 'POLE^{WT}';
                isThisT = ~strcmp(tableAllSamples.Tissue, 'POLE')';
                %     elseif (iR == 5)
                %         plot_quartile = false;
                %         tissue = 'lung';
                %         isThisT = ismember(tableAllSamples.Tissue, {'lung_adenocarcinoma';'lung_squamous'})';
                %     elseif (iR == 6)
                %         plot_quartile = false;
                %         tissue = 'MSI';
                %         isThisT = strcmp(tableAllSamples.Tissue, 'MSI')';
            elseif (~isempty(specific_tissue))
                if (tissue_only)
                    plot_quartile = false;
                    tissue = strjoin(specific_tissue);
                    isThisT = ismember(tableAllSamples.Tissue, specific_tissue)';
                else
                    plot_quartile = true;
                    tissue = ['not ', strjoin(specific_tissue)];
                    isThisT = ~ismember(tableAllSamples.Tissue, specific_tissue)';
                end
            else
                plot_quartile = true;
                tissue = ''; %all samples
                isThisT = true(1, size(tableAllSamples, 1));
            end
            yLabel_text = signatureNames{iSignature}; %yLabel_text = [signatureShortNames{iSignature}, ' ', tissue];
            isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
            for iType = 1:nC
                if (iType == 1)
                    xS_minus = 0.01;
                else
                    xS_minus = 0;
                end
                yB_divide= 1;
                if (special_R23 && iR == 3 && iType == 1)
                    iS = iS + 1;
                    continue
                elseif (special_R23 && iR == 2 && iType == 1)
                    yB_divide = nR;
                end
                myGeneralSubplot(nR,nC,iS,xS-xS_minus,yS,xB,yB/yB_divide,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none');
                if (special_R23 && iR == 2 && iType == 1)
                    gcaPos = get(gca, 'Position'); gcaPos(2) = gcaPos(2) - gcaPos(4)/2; set(gca, 'Position', gcaPos);
                end
                
                if (iType == 1)
                    plot_signature([finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)], subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'flipped', title_text, yLabel_text, colours, font_size+4, font_name);
                    xVal = 100;
                    if (special_R23 && iR == 2)
                        text(xVal, 18, 'POLE^{mut}', 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold', 'HorizontalAlignment', 'left', 'Rotation', 90);
                        text(xVal, -20, '  POLE^{WT}', 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold', 'HorizontalAlignment', 'center', 'Rotation', 90);
                    else
                        text(xVal, 0, strrep(tissue, '_', ' '), 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold', 'HorizontalAlignment', 'center', 'Rotation', 90);
                    end
                elseif (iType == 2)
                    plot_yLabel = true; plotMedianFilter= false;
                    plot_oriplot(signatureExposuresComplex_cell_ORI_Haradhvala, iSignature, isSampleUsed, binSize_Haradhvala, nORIValues_Haradhvala, ORI_text_Haradhvala, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name);
                elseif (iType == 3)
                    plot_yLabel = true; plotMedianFilter= false;
                    plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name);
                elseif (iType == 4)
                    plot_yLabel = true;
                    plot_histogram(signatureExposuresSimple_matrix, iSignature, isSampleUsed, signatureResults, plot_quartile, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name)
                elseif (iType == 5)
                    typeName_RT = 'double_type1_type2'; %'totalMF';
                    yLabel_text = 'exposure';
                    if (iSignature == currentListSignatures(end))
                        xLabel_text = 'replication timing';
                    else
                        xLabel_text = '';
                    end
                    plot_replbar(signatureExposuresComplex_cell_RT_diff, iSignature, isSampleUsed, signatureResults, typeName_RT, nCurrentValues_RT, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size, font_name)
                    %         elseif (iType == 5)
                    %             typeName_RT = 'type1Minustype2';
                    %             yLabel_text = {'main - side', 'exposure'};
                    %             if (iSignature == listSignatures(end))
                    %                 xLabel_text = 'replication timing';
                    %             else
                    %                 xLabel_text = '';
                    %             end
                    %             plot_replbar(signatureExposuresComplex_cell_RT_diff, iSignature, isSampleUsed, signatureResults, typeName_RT, nCurrentValues_RT, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size)
                end
            end
        end
        % mySaveAs(fig, imagesPath, ['combined_', strandSpecText, '_', num2str(i)]);
        % suptitle(sprintf('%s: distribution of leading-lagging exposures to signatures in patients', strrep(territoriesSource, '_', ' ')));
        % mySaveAs(fig, imagesPath, ['combinedPlot_', territoriesSource, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
        % suptitle(sprintf('%s mean exposure >= %d', strrep([territoriesSource], '_', ' '), minMeanExposure));
        if (~isempty(group_name))
            suptitle(group_name);
        end
        mySaveAs(fig, imagesPath, ['combinedPlot_', strrep(group_name, '/', 'or'), '_', num2str(minMeanExposure), '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
        
    end
end
%%
if (false)
    currentListSignatures = [6, 14, 19, 20, 24, 29];
    nR = 1; nC = length(currentListSignatures); iS = 1; xS = 0.8; yS = 0.8; xB = 0.05; yB = 0.08; xM = 0.1; yM = -0.01;
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 nC*8 12],'units','normalized','outerposition',[0 0 1 1]);
    
    plot_yLabel = true; plotMedianFilter= true;
    for iSignature = currentListSignatures
        
        myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; %set(gca, 'Color', 'none');
        isThisT = ~strcmp(tableAllSamples.Tissue, 'MSI')';
        tmpColours = colours;
        tmpColours.plus = [1, 0.5, 0.1];
        tmpColours.minus = [0.1, 0.5, 0.75];
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        hPlot_MSS = plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, tmpColours, font_size+2, font_name);
        
        isThisT = strcmp(tableAllSamples.Tissue, 'MSI')';
        tmpColours = colours;
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        hPlot_MSI = plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, tmpColours, font_size+2, font_name);
        legend(gca, 'off'); %ylim([0, 1e4]);
        title(signatureNames{iSignature}, 'FontSize', font_size+4, 'FontName', font_name);
        plot_yLabel = false;
    end
    h = legend([hPlot_MSI, hPlot_MSS], {'MSS plus', 'MSS minus', 'MSI plus', 'MSI minus'}, 'Location', 'East', 'FontSize', font_size+4, 'FontName', font_name);
    legPos = get(h, 'Position'); legPos(1) = legPos(1) + 1.5*legPos(3); set(h, 'Position', legPos);
    mySaveAs(fig, imagesPath, ['combinedMSIPlot_', num2str(minMeanExposure), '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%
% iSignature = 0; nR = 5; nC = 3;
% for i = 1%:ceil(finalSignatures.nSignatures/nR)
%     fig = createMaximisedFigure(i);
%     for j = 1:nR*nC
%         if (iSignature < finalSignatures.nSignatures)
%             iSignature = iSignature + 1;
%             subplot(nR,nC,j); %if (j == nR) plotTypes = true; end
%             title_text = finalSignatures.signaturesNames{iSignature};
%             plot_signature([finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)], subtypes.(strandSpecText), types.(strandSpecText), plotTypes, 'flipped', title_text, font_size);
%         end
%     end
%     suptitle(sprintf('Final denormalised %d', i));
%     mySaveAs(fig, imagesPath, ['finalSignatures_denormalised_', strandSpecText, '_', num2str(i)]);
% end