function plotExposureResults_aroundORIplots(decomposedSignatures, territoriesSource, tableAllSamples_Besnard1k, signatureExposuresComplex_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_Besnard1k)


imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
nTotalSamples = size(tableAllSamples_Besnard1k, 1);
minMeanExposure = 10;
nSignatures = decomposedSignatures.finalSignatures.nSignatures;
nBins = length(signatureExposuresComplex_cell_Besnard1k);
lstDistant = [1:nBins/4, nBins-nBins/4:nBins];
lstClose = round([nBins/2-nBins/8+1:nBins/2+nBins/8-1]);%round([nBins/2-nBins/4+1:nBins/2+nBins/4-1]);%[nBins/2-1, nBins/2+1];
distantBins.plus = NaN*ones(nSignatures, nTotalSamples);
distantBins.minus = NaN*ones(nSignatures, nTotalSamples);
distantBins.together = NaN*ones(nSignatures, nTotalSamples);
closeBins.plus = NaN*ones(nSignatures, nTotalSamples);
closeBins.minus = NaN*ones(nSignatures, nTotalSamples);
closeBins.together = NaN*ones(nSignatures, nTotalSamples);
for iSignature = 1:nSignatures
    allBins.plus = zeros(nBins, nTotalSamples);
    allBins.minus = zeros(nBins, nTotalSamples);
    for iBin = 1:length(signatureExposuresComplex_cell_Besnard1k)
        allBins.plus(iBin,:) = signatureExposuresComplex_cell_Besnard1k{iBin}.mfPlus(iSignature, :);
        allBins.minus(iBin,:) = signatureExposuresComplex_cell_Besnard1k{iBin}.mfMinus(iSignature, :);
    end
    distantBins.plus(iSignature,:) = mean(allBins.plus(lstDistant,:));
    distantBins.minus(iSignature,:) = mean(allBins.minus(lstDistant,:));
    distantBins.together(iSignature,:) = mean([allBins.plus(lstDistant,:); allBins.minus(lstDistant,:)]);
    closeBins.plus(iSignature,:) = mean(allBins.plus(lstClose,:));
    closeBins.minus(iSignature,:) = mean(allBins.minus(lstClose,:));
    closeBins.together(iSignature,:) = mean([allBins.plus(lstClose,:); allBins.minus(lstClose,:)]);
end
%%
fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', [0 0 30 15],'units','normalized','outerposition',[0 0 1 1]);
nR = 2; nC = 6; iS = 1; xS = .8; yS = 0.8; xB = 0.08; yB = 0.05; xM = 0.02; yM = -0.04;
% posTitle.xShift = 0.05;
% posTitle.yShift = 0.01;
% posTitle.width = 0.02;
% posTitle.height = 0.02;
% posTitle.fontSize = 18;
% posTitle.letters = {'a', 'b', 'c', 'd', 'e', 'f'};
colours.distant = [0.0352    0.4805    0.5859];
colours.close = [0.5898    0.0156    0.0469];
cmapGroups = [colours.distant; colours.close];
for iType = 1:2
    if (iType == 1)
        isSampleUsed = strcmp(tableAllSamples_Besnard1k.Tissue, 'MSI')' & signatureExposuresSimple_LeadLagg_cell_Besnard1k.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        typeName = 'MSI';
    else
        isSampleUsed = ~strcmp(tableAllSamples_Besnard1k.Tissue, 'MSI')' & ~strcmp(tableAllSamples_Besnard1k.Tissue, 'POLE')' & signatureExposuresSimple_LeadLagg_cell_Besnard1k.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        %         isSampleUsed = find(isSampleUsed);
        %         rng(4);
        %         randomPermutation = randperm(length(isSampleUsed));
        %         isSampleUsed = isSampleUsed(randomPermutation(1:19));
        typeName = 'MSS';
    end
    lstSignatures = [6 14 19 20 24 29];
    for iSignature = lstSignatures
        positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); hold on; iS = iS + 1; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
        
        tmp = [distantBins.together(iSignature, isSampleUsed); closeBins.together(iSignature, isSampleUsed)]; 
        boxplot(tmp'); box off;
        hBox = findobj(gca,'Tag','Box'); set(hBox, 'Color', 'none');
        hMedian = findobj(gca,'Tag','Median');
        hMedianOuter = findobj(gca,'Tag','MedianOuter');
        hMedianInner = findobj(gca,'Tag','MedianInner');
        hOutliers = findobj(gca,'Tag','Outliers'); 
        for j=1:length(hBox)
            currentIndex = length(hBox)-j+1;
            currentColour = cmapGroups(currentIndex, :);
            hLeg(currentIndex) = patch(get(hBox(j),'XData'),get(hBox(j),'YData'), currentColour, 'EdgeColor', 'none');
            plot(get(hMedian(j),'XData'),get(hMedian(j),'YData'), '-', 'Color', currentColour/3, 'LineWidth', 1.5);
            set(hOutliers(j), 'MarkerEdgeColor', currentColour);
        end
        
        yLimVal = get(gca, 'YLim'); yStep = yLimVal(2)*0.05; yMax = yLimVal(2)+yStep; yLimVal(1) = 0; yLimVal(2) = yLimVal(2)+2.2*yStep; ylim(yLimVal); 
        plot([1,2], yMax*[1,1], '-k'); plot([1,2;1,2], [yMax,yMax;yMax-yStep/2,yMax-yStep/2], '-k');
        text(1.5, yMax, getPValueAsText(signtest(tmp(1,:), tmp(2,:)), true), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom');
        %med = round(median(tmp, 2)); 
        %text([1, 2], max(get(gca, 'YLim'))*[1,1], {num2str(med(1)), num2str(med(2))}, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top');
        set(gca, 'XTick', [1,2], 'XTickLabel', {'distant', 'close'}, 'FontSize', 10);
        if (iSignature == lstSignatures(1))
            %ylabel('Exposure', 'FontSize', 12);
            xValuesA = [-0.2, -0.6]; xValuesB = [-0.6, -1.0];
            text(xValuesA(iType), mean(yLimVal), 'Exposure', 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 12);
            text(xValuesB(iType), mean(yLimVal), typeName, 'HorizontalAlignment', 'center', 'Rotation', 90, 'FontSize', 14, 'FontWeight', 'bold');
        end
        if (iType == 1)
            title({decomposedSignatures.finalSignatures.signaturesNames{iSignature}, ' '}, 'VerticalALignment', 'bottom'); %sprintf('Together: p %s', getPValueAsText(signtest(tmp(1,:), tmp(2,:))))); 
        end
        %         axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
        %         text(.5,.5,posTitle.letters{iS},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center');
    end
end
mySaveAs(fig, imagesPath, ['debug_distant_vs_close_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
mySaveAs(fig, imagesPath, 'FigS13');
%%
printNumbers = false;
if (printNumbers)
    for iSignature = [6 14 19 20 24 29]
        for iType = 1:2
            if (iType == 1)
                isSampleUsed = strcmp(tableAllSamples_Besnard1k.Tissue, 'MSI')' & signatureExposuresSimple_LeadLagg_cell_Besnard1k.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
                typeName = 'MSI';
            else
                isSampleUsed = ~strcmp(tableAllSamples_Besnard1k.Tissue, 'MSI')' & ~strcmp(tableAllSamples_Besnard1k.Tissue, 'POLE')' & signatureExposuresSimple_LeadLagg_cell_Besnard1k.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
                isSampleUsed = find(isSampleUsed);
                rng(1);
                randomPermutation = randperm(length(isSampleUsed));
                isSampleUsed = isSampleUsed(randomPermutation(1:19));
                typeName = 'MSS';
            end
            fig = createMaximisedFigure(10); hold on;
            tmp = [distantBins.plus(iSignature, isSampleUsed); closeBins.plus(iSignature, isSampleUsed)]; subplot(1,3,1); boxplot(tmp'); title(sprintf('Plus: p %s', getPValueAsText(signtest(tmp(1,:), tmp(2,:))))); med = round(median(tmp, 2)); text([1, 2], max(get(gca, 'YLim'))*[1,1], {num2str(med(1)), num2str(med(2))}, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top');
            tmp = [distantBins.minus(iSignature, isSampleUsed); closeBins.minus(iSignature, isSampleUsed)]; subplot(1,3,2); boxplot(tmp'); title(sprintf('Minus: p %s', getPValueAsText(signtest(tmp(1,:), tmp(2,:))))); med = round(median(tmp, 2)); text([1, 2], max(get(gca, 'YLim'))*[1,1], {num2str(med(1)), num2str(med(2))}, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top');
            tmp = [distantBins.together(iSignature, isSampleUsed); closeBins.together(iSignature, isSampleUsed)]; subplot(1,3,3); boxplot(tmp'); title(sprintf('Together: p %s', getPValueAsText(signtest(tmp(1,:), tmp(2,:))))); med = round(median(tmp, 2)); text([1, 2], max(get(gca, 'YLim'))*[1,1], {num2str(med(1)), num2str(med(2))}, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top');
            suptitle(sprintf('%s %s', decomposedSignatures.finalSignatures.signaturesNames{iSignature}, typeName));
            imagesPath = ['images/debug/']; createDir(imagesPath);
            mySaveAs(fig, imagesPath, ['debug_distant_vs_close_', decomposedSignatures.finalSignatures.signaturesNames{iSignature}, '_', typeName, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
        end
    end
    
    cancerTypes = replData.tableCancerTypes.CancerType; nCT = length(cancerTypes);
    for iCT = 1:nCT
        fprintf('%s: %d.\n', cancerTypes{iCT}, sum(strcmp(replData.tableAllSamples.cancerType, cancerTypes{iCT})));
    end
end