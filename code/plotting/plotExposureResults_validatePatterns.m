% function plotExposureResults_validatePatterns(replData, territoriesSource)
fontName = 'Trebuchet MS';
set(0,'defaultAxesFontName',fontName);
set(0,'defaultTextFontName',fontName);
imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
%%
nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
patternNames = replData.patternNames;
maxPattern = length(patternNames);
structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
simpleType = 'LeadLagg'; xVar = 'asymmetryDifference'; minMeanExposure = 10;
%%
for iSigType = 1:4
    if (iSigType == 1)
        iSignature = 10; lstPatterns = [16,47,96]; figName = 'Fig4supplement13'; % 4:4:16
    elseif (iSigType == 2)
        iSignature = 13; lstPatterns = 8:4:16; figName = 'Fig4supplement14';
    elseif (iSigType == 3)
        iSignature = 17; lstPatterns = [5,9,12,16]; figName = 'Fig4supplement15';
    elseif (iSigType == 4)
        iSignature = 25; lstPatterns = [84,88,96]; figName = 'Fig4supplement16';
    end
    % isNotPOLE = ~strcmp(tableAllSamples.Tissue, 'POLE')'; minMeanExposure = 10;
    fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 40 30],'units','normalized','outerposition',[0 0 1 1]);
    nR = 2; nC = length(lstPatterns); iS = 1; xS = .9; yS = .6; xB = 0.08; yB = 0.1; xM = -0.01; yM = -0.01;
    for iSampleType = 1:2
        if (iSampleType == 1)
            isSampleUsed = ~strcmp(tableAllSamples.Tissue, 'POLE')' & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
            textSampleType = '{\bf POLE-WT}';
        else
            isSampleUsed = strcmp(tableAllSamples.Tissue, 'POLE')' & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
            textSampleType = '{\bf POLE-MUT}';
        end
        matrixToPlot = full(structAllSamples.(simpleType).(xVar)(isSampleUsed,:)');
        for iPattern = lstPatterns%[16,47,96]%[35:4:47] %[16,47,96]
            myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1;
            values = matrixToPlot(iPattern,:);
            %         nhist(values);
            max95 = quantile(abs(values), 0.95);
            %values = values(abs(values)<=max95);
            h = histogram(values, 50, 'BinLimits', [-max95,max95]);
            binEdges = h.BinEdges;
            %         maxEdge = max(abs(h.BinEdges));
            %         if (maxEdge > 0)
            %             binEdges = [-1*fliplr(h.BinEdges(h.BinEdges>0)), 0, h.BinEdges(h.BinEdges>0)];
            %         else
            %             binEdges = [h.BinEdges(h.BinEdges<0), 0, -1*fliplr(h.BinEdges(h.BinEdges<0))];
            %         end
            hold off;
            h = histogram(values(values<=0),binEdges); hold on;
            h = histogram(values(values>0),binEdges);
            yLimVal = get(gca, 'YLim');
            hMedian = plot(median(values), yLimVal(2), 's', 'MarkerFaceColor', 0.3*[1,1,1], 'MarkerEdgeColor', 'none');
            hMean = plot(mean(values), yLimVal(2), 'o', 'MarkerFaceColor', 0.6*[1,1,1], 'MarkerEdgeColor', 'none');
            legend([hMedian, hMean], {'median', 'mean'});
            xlim([-max95,max95]);
            %nhist(matrixToPlot(iPattern,:));
            p = signtest(matrixToPlot(iPattern,:));
            if (iPattern == lstPatterns(1))
                ylabel({textSampleType, 'number of samples'});
            else
                ylabel(' ');
            end
            title({sprintf('%s: p %.1e', strrep(patternNames{iPattern}, 'to', '>'), p), sprintf('mean %.1e, median %.1e', nanmean(matrixToPlot(iPattern,:)), nanmedian(matrixToPlot(iPattern,:)))});
            xlabel({'leading - lagging', 'mutation frequency'}); %ylabel('number of samples');
        end
    end
    % myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1;
    % % tmpVector = mean(matrixToPlot([35:4:47],:));
    % tmpVector = matrixToPlot([35:4:47],:); tmpVector = tmpVector(:);
    % nhist(tmpVector);
    % p = signrank(tmpVector);
    % title({sprintf('CpG>T: p %.3f', p), sprintf('mean %.1e, median %.1e', nanmean(tmpVector), nanmedian(tmpVector))});
    % xlabel('exposure on leading - lagging strand');
    
    suptitle({sprintf('Main components of %s', replData.signatureNames{iSignature}), ' '}); %{'Signature 10 main components', strrep(territoriesSource, '_', ' ')});
    mySaveAs(fig, imagesPath, [figName, '.png']);
    % mySaveAs(fig, imagesPath, ['signature10_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
    % suptitle({'Signature 1 main components', strrep(titleTabuCTs, '_', ' ')});
    % mySaveAs(fig, imagesPath, ['signature1_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%