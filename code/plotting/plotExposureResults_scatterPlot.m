% function plotExposureResults_scatterPlot(replData, territoriesSource)


nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;

% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];
% colours.patternColormap = jet(6)*0.7;
colours.tissuesColourmap = hsv(nTissues);
colours.tissuesMarkers = {'o', 's', 'p', 'd'}; nM = length(colours.tissuesMarkers);
signatureNames = replData.signatureNames;

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_scatterPlots/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
% simpleType = 'LeadLagg';
%%
xVal = signatureExposuresSimple_LeadLagg.type1Minustype2; xLab = 'Leading - Lagging';
% xVal = log2(signatureExposuresSimple_LeadLagg.type1) - log2(signatureExposuresSimple_LeadLagg.type2); xLab = 'log2(Leading) - log2(Lagging)';
% xVal = xVal ./(signatureExposuresSimple_LeadLagg.type1 + signatureExposuresSimple_LeadLagg.type2); xLab = '(Leading - Lagging)/(Leading + Lagging)';
% xVal = xVal./repmat(nanstd(xVal), nSignatures, 1); % The mean stays the same
% xVal(isnan(xVal)) = 0;
% xLab = 'zscore of (Leading - Lagging) over patients';


% xVal = signatureExposuresSimple_LeadLagg.type1Minustype2./signatureExposuresSimple_LeadLagg.total; xLab = '(Leading - Lagging)/total';
yVal = signatureExposuresSimple_LeadLagg.meanType1Type2; yLab = '(Leading + Lagging)/2';
% yVal = log2(signatureExposuresSimple_LeadLagg.meanType1Type2); yLab = 'log2((Leading + Lagging)/2)';
% yVal = signatureExposuresSimple_LeadLagg.total; yLab = 'Total';

% yVal = log2(signatureExposuresSimple_LeadLagg.type1 ./ signatureExposuresSimple_LeadLagg.type2); yLab = 'log2(Leading / Lagging)';

minExposureY = 1000;

for iSignature = 12%28%1:nSignatures
    fig = createMaximisedFigure(1); hold on;
    isThisTissueUsed = false(nTissues, 1);
    for iTissue = 1:nTissues
        tissue = tissueNames{iTissue};
        isThisT = strcmp(tableAllSamples.Tissue, tissue) & (yVal(iSignature,:) > minExposureY)';
        if (sum(isThisT)>0)
            isThisTissueUsed(iTissue) = true;
            plot(xVal(iSignature,isThisT), ...
                yVal(iSignature,isThisT), ...
                colours.tissuesMarkers{mod(iTissue-1,nM)+1}, 'MarkerFaceColor', colours.tissuesColourmap(iTissue,:), 'MarkerEdgeColor', 0.5*[1,1,1]);
        end
    end
        isThisS = (yVal(iSignature,:) > 7.5*minExposureY)'; %skin % strcmp(tableAllSamples.Tissue, 'breast') & 
        labelRepel(xVal(iSignature,isThisS), yVal(iSignature,isThisS), 0, 5, tableAllSamples.sampleName(isThisS), 10);
    %     tableTMP = table(sort(tableAllSamples.sampleName(isThisS)))
    %     writetable(tableTMP, 'skin_samples_N3.txt', 'WriteVariableNames', false)
    
    
    legend(strrep(tissueNames(isThisTissueUsed), '_', '\_'), 'Location', 'EastOutside');
    plot(get(gca, 'XLim'), minExposureY*[1,1], '-r');
    grid on;
    xlabel(xLab); ylabel(yLab); %ylabel('total'); %
    title(signatureNames{iSignature});
    mySaveAs(fig, imagesPath, ['scatter_', signatureNames{iSignature}]);%, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%
% isPOLE = strcmp(tableAllSamples.Tissue, 'POLE');
fig = createMaximisedFigure(1); hold on;
% boxplot(signatureExposuresSimple_LeadLagg.type1Minustype2(:,~isPOLE)', 'plotstyle', 'compact', 'labels', signatureNames, 'outliersize', 3);
% plot(quantile(signatureExposuresSimple_LeadLagg.type1Minustype2(:,~isPOLE)', 0.75), 'o');
xVal = signatureExposuresSimple_LeadLagg.type1Minustype2; scalingFactor = 5; yLab = {'{\bfExposure asymmetry}', '(main direction - side direction)'}; type = 'diff';
% xVal = log2(signatureExposuresSimple_LeadLagg.type1./signatureExposuresSimple_LeadLagg.type2); scalingFactor = 400; yLab = {'{\bfExposure asymmetry}', 'log2(main/side)'}; type = 'log';



nC = 400;
cmp = colourLinspace([0,0,1], [1,0,0], 2*nC+1);
leadColours = cmp(nC+1:end,:); %colourLinspace([1,1,1], [1,0,0], nC);
interColours = cmp(nC,:); %colourLinspace([1,1,1], [1,0,0], nC);
laggColours = flipud(cmp(1:nC,:)); %colourLinspace([1,1,1], [0,0,1], nC);
% nLines = 100;

nSamplesPerSignature = NaN*ones(nSignatures, 1);

minMeanExposure = 10;%1000;
maxYValue = 10000;

for iSignature = 1:nSignatures
    %isOkSample = xVal(iSignature,:)>100; %~isPOLE' &
    isOkSample = ~isnan(xVal(iSignature,:)) & ~isinf(xVal(iSignature,:)) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; %~isPOLE' &
    nSamplesPerSignature(iSignature) = sum(isOkSample);
    currentValues = xVal(iSignature, isOkSample);
    currentQtls = quantile(currentValues, [0.25,0.5,0.75]);
    if (max(currentQtls) > maxYValue)
        currentQtls(currentQtls > maxYValue) = maxYValue;
        plot(iSignature, 0.9*maxYValue, 'hk');
    end
    %     if (currentQtls(1) > 0)
    %         colour1 = leadColours(min([round(currentQtls(1)), nC]),:);
    %     else
    %         colour1 = laggColours(min([abs(round(currentQtls(1))), nC]),:);
    %     end
    %     if (currentQtls(3) > 0)
    %         colour2 = leadColours(min([round(currentQtls(3)), nC]),:);
    %     else
    %         colour2 = laggColours(min([abs(round(currentQtls(3))), nC]),:);
    %     end
    p = signtest(currentValues);
    if (p >= 0.001/nSignatures)
        pos = (1+leadColours)/2; zer = (1+interColours)/2; neg = (1+laggColours)/2;
    else
        pos = leadColours; zer = interColours; neg = laggColours;
        text(iSignature, currentQtls(1), {' ', sprintf('e%d', round(log10(p)))}, 'HorizontalAlignment', 'center', 'FontSize', 10);
    end
    
    plotColourGradientBox(iSignature-1/4, iSignature+1/4, currentQtls(1), currentQtls(3), currentQtls(1), currentQtls(3), 'y', pos, zer, neg, scalingFactor); drawnow;
    %     plot(iSignature, currentQtls(1), '+');
    plot(iSignature, currentQtls(2), '+k');
    %     plot(iSignature, currentQtls(3), '+');
    text(iSignature, currentQtls(3), {sprintf('%.d%%', round(100*mean(currentValues>0))), num2str(nSamplesPerSignature(iSignature)), ' ', ' '}, 'HorizontalAlignment', 'center');
    fprintf('%s: median %.2f, IQR %.2f-%.2f.\n', signatureNames{iSignature}, currentQtls(2), currentQtls(1), currentQtls(3));
end
ylabel(yLab);
% text(1:nSignatures, max(get(gca, 'YLim')) + 0*(1:nSignatures), num2str(nSamplesPerSignature), 'HorizontalAlignment', 'center');
% plotColourGradientBox(30-1/4, 30+1/4, -500, 500, colour1, colour2, 1000/2, 'y', leadColours, interColours, laggColours); drawnow; 

% violin(signatureExposuresSimple_LeadLagg.type1Minustype2(:,~isPOLE)'); 
set(gca, 'XTick', 1:nSignatures, 'XTickLabel', signatureNames);
try
    set(gca, 'XTickLabelRotation', 90);
end
grid on;
title(sprintf('%s mean exposure >= %d', strrep(territoriesSource, '_', '\_'), minMeanExposure));
mySaveAs(fig, imagesPath, ['asymmetryGradients_', type, '_', num2str(minMeanExposure), '_', territoriesSource]);%, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);

% for iSignature = 1:nSignature
%     signatureExposuresSimple_LeadLagg.type1Minustype2(iSignature)
% end