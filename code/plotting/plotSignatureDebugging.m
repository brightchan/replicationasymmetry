function plotSignature(signature, subtypes, types, plotTypes, strandSpecPlotType)

if (~exist('strandSpecPlotType', 'var'))
    strandSpecPlotType = 'normal';
end
hold on;
cmap = [.2 0.7 1; 0.3 0.3 0.3; 1 .1 .1; 0.5 0.5 0.5; .4 .9 .3; 1 0.5 0.8];
if (length(signature) == 96)
    iColour = 1;
    for iBar = 1:16:96
        bar(iBar+(0:15), 100*signature(iBar+(0:15)), 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
        for jBar = iBar+(0:15)
            text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', 'Lucida Console');
        end
        iColour = iColour + 1;
    end
elseif (length(signature) == 2*96)
    if strcmp(strandSpecPlotType, 'flipped')
        leadingPart = 100*signature(1:96);
        laggingPart = -100*signature(97:end);
        signatureValues = laggingPart;
        signatureValues(signatureValues == 0) = leadingPart(signatureValues == 0);
        colourPerBar = cmap(sort(repmat(1:6, 1, 16)),:);
        maxYLimVal = max([20; abs(signatureValues)+2]);
        for iBar = 1:96
            bar(iBar, signatureValues(iBar), 0.3, 'FaceColor', colourPerBar(iBar,:), 'EdgeColor', 'none');
        end
        %         iColour = 1;
        %         for iBar = 1:16:96
        %             bar(iBar+(0:15)-.2, signatureValues(iBar+(0:15)), 0.3, 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
        %             for jBar = iBar+(0:15)
        %                 %                 if (signature(96+jBar) == 0)
        %                 %                     text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', 'Lucida Console');
        %                 %                 else
        %                 %                     text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'left', 'FontName', 'Lucida Console');
        %                 %                 end
        %             end
        %             iColour = iColour + 1;
        %         end
        set(gca, 'XColor', 'none');
        yLimVal = [-maxYLimVal, maxYLimVal]; ylim(yLimVal);
        if (plotTypes)
            for iBar = 8:16:96 %listBars = 8:16:96;
                text(iBar, -maxYLimVal, types{iBar}, 'Color', colourPerBar(iBar,:), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontName', 'Lucida Console', 'FontSize', 12, 'FontWeight', 'bold');
            end
        end
    else
        iColour = 1;
        for iBar = 1:16:96
            bar(iBar+(0:15)-.2, 100*signature(iBar+(0:15)), 0.3, 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
            bar(iBar+(0:15)+.2, 100*signature(96+iBar+(0:15)), 0.3, 'FaceColor', (cmap(iColour,:))/2, 'EdgeColor', 'none');
            for jBar = iBar+(0:15)
                text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', 'Lucida Console');
            end
            iColour = iColour + 1;
        end
    end
end

set(gca, 'XTick', []);

if (~strcmp(strandSpecPlotType, 'flipped') && plotTypes)
    iType = 1;
    for iBar = 8:16:96
        text(iBar, 0, {' ', ' ', ' ', ' ', ' ', types{iBar}}, 'Color', cmap(iType,:), 'HorizontalAlignment', 'center', 'FontName', 'Lucida Console', 'FontSize', 12, 'FontWeight', 'bold');
        iType = iType + 1;
    end
end
set(gca, 'FontName', 'Lucida Console');

% yLimVal = get(gca, 'YLim'); 
% if (~exist('maxYLimVal', 'var'))
%     maxYLimVal = max([20, abs(yLimVal)]);
% end
% yLimVal = [-maxYLimVal, maxYLimVal]; ylim(yLimVal);
% % yLimVal(1) = min([yLimVal(1), -20]); yLimVal(2) = max([yLimVal(2), 20]); ylim(yLimVal);
drawnow;