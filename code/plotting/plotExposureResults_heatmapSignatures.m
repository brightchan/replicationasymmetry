function plotExposureResults_heatmapSignatures(replData, territoriesSource)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple = replData.signatureExposuresSimple;

% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7; 
colours.tissuesColourmap = hsv(nTissues);
signatureNames = replData.signatureNames;

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);

% iTissue = 1;
[~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
simpleType = 'LeadLagg';
%%
% for field = {'total', 'type1', 'type2'}
%     field = field{1};
%     tmp1 = 100*signatureExposuresSimple.LeadLagg.(field)./repmat(sum(signatureExposuresSimple.LeadLagg.(field)), nSignatures, 1);
%     isBelow1 = tmp1 < 1; % All too small exposures will be set to 0.
%     signatureExposuresSimple.LeadLagg.(field)(isBelow1) = 0;
% end
% signatureExposuresSimple.LeadLagg.type1Minustype2 = (signatureExposuresSimple.LeadLagg.type1 - signatureExposuresSimple.LeadLagg.type2);
%% HEATMAP SIGNATURES
for iType = 3%1:4
    if (iType == 1)
        xVar = 'type1'; printVar = 'leading';      normalise = false;
    elseif (iType == 2)
        xVar = 'type2'; printVar = 'lagging';        normalise = false;
    elseif (iType == 3)
        xVar = 'type1Minustype2'; printVar = 'Exposure in leading - lagging';         normalise = true;
    elseif (iType == 4)
        xVar = 'total'; printVar = 'Total exposure';         normalise = false;
    end
    fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 60 40],'units','normalized','outerposition',[0 0 1 1]);     axes('Position', [0.05, 0.05, 0.9, 0.83]);
    matrixToPlot = signatureExposuresSimple.(simpleType).(xVar)(:,listSamples);
    
    if (normalise)
        colormap([colourLinspace([0,0.5,1], 0.9*[1,1,1], 60); colourLinspace(0.9*[1,1,1], [1,0.1,0.2], 60)]);
        %matrixToPlot = zscore(matrixToPlot);
        matrixToPlot = matrixToPlot./repmat(nanstd(matrixToPlot), nSignatures, 1); % The mean stays the same
        matrixToPlot(isnan(matrixToPlot)) = 0;
    else
        colormap(colourLinspace(0.9*[1,1,1], [1,0.1,0.2], 120));
                matrixToPlot = matrixToPlot./repmat(sum(matrixToPlot), nSignatures, 1);
    end
    imagesc(matrixToPlot);
    if (normalise)
        caxis(max(abs(matrixToPlot(:)))*[-1,1]);
    end
    colorbar; set (gca, 'XTick', [], 'YTick', []); hold on;
    
    for iSignature = 1:nSignatures
        text(0.5, iSignature, signatureNames{iSignature}, 'HorizontalAlignment', 'right');
    end
    for iTissue = 1:nTissues
        listSamplesInCT = find(strcmp(tableAllSamples.Tissue(listSamples), tissueNames{iTissue}));
        if (~isempty(listSamplesInCT))
            plot([min(listSamplesInCT), max(listSamplesInCT)], 0.8*[1,1], 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'LineWidth', 2);
            plot(0.5+max(listSamplesInCT)*[1,1], 0.5+[0, size(matrixToPlot, 1)], 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'LineWidth', 0.5);
            text(mean(listSamplesInCT), 0, strrep(tissueNames{iTissue},'_',' '), 'Rotation', 45, 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'VerticalAlignment', 'bottom');
        end
    end
    suptitle({printVar, ' '});  %suptitle(sprintf('%s: %s', simpleType, printVar));
    mySaveAs(fig, imagesPath, [xVar, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%