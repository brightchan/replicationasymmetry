function maxValue = myStderrorBar(xValue, values, width, edgeColour)


% val25 = quantile(values, 0.25);
% val50 = quantile(values, 0.50);
% val75 = quantile(values, 0.75);
% rectangle('Position', [xValue-width/2 val25 width (val75-val25)], 'FaceColor', [faceColour, 0.5], 'EdgeColor', edgeColour)
% plot(xValue+width/2*[-1,1], val50*[1,1], '-', 'Color', edgeColour);

val25 = mean(values) - std(values)/sqrt(length(values));
val50 = mean(values);
val75 = mean(values) + std(values)/sqrt(length(values));

plot(xValue+width/2*[-1,1], val25*[1,1], '-', 'Color', edgeColour);
% plot(xValue+width/2*[-1,1], val50*[1,1], '-', 'Color', edgeColour);
plot(xValue+width/2*[-1,1], val75*[1,1], '-', 'Color', edgeColour);
plot(xValue*[1,1], [val25,val75], '-', 'Color', edgeColour);

maxValue = val75;