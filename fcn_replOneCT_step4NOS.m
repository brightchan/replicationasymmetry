%% fcn_replOneCT_step4NOS
function fcn_replOneCT_step4NOS(cancerType, dirSave, tableTerritoriesFileName, cellResult_background_fileName)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear;
% cancerType = 'TCGA_AML_Strelka';
% dirSave = 'save/';
% tableTerritoriesFileName = 'save/tableTerritories';
% cellResult_background_fileName = 'save/cellResult_background';
%%
addpath(genpath('code/'));
scriptName = 'fcn_replOneCT_step4NOS'; compName = 'CLUSTER';
% savePathDiary = 'diaries/'; createDir(savePathDiary);
% diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
% diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName); tic;
%% Info about territories
maxPattern = 96;
tableTerritories = [];  load(tableTerritoriesFileName, 'tableTerritories');     nTerritories = size(tableTerritories, 1); % created in script_60726_replPrepareTables
cellResult = [];        load(cellResult_background_fileName, 'cellResult');
cellResult_background_leading = cellResult.background.leading;
cellResult_background_lagging = cellResult.background.lagging;
cellResult_plus = cellResult.plus;
cellResult_minus = cellResult.minus;
clear cellResult;

bins_tRT = tableTerritories.bins_tRT;
bins_tExpr = tableTerritories.bins_tExpr;
bins_tNOS = tableTerritories.bins_tNOS;
indexBinFromORI = tableTerritories.indexBinFromORI;
isLeftOrRightNotTx = tableTerritories.isLeftOrRightNotTx;
% isTxNotLeftOrRight = tableTerritories.isTxNotLeftOrRight;
tTxPlus = tableTerritories.tTxPlus;
tTxMinus = tableTerritories.tTxMinus;
isTxNotLeftOrRight = tableTerritories.isTxNotLeftOrRight;
isNotNaNNOS = ~isnan(tableTerritories.tNOS);
clear tableTerritories;
dirSaveNew = [dirSave, '/NOS/']; createDir(dirSaveNew);
%%
% iCT = 0;
fprintf('Cancer type %s... ', cancerType);
filename_structSamples = [dirSave, 'structSamples_', cancerType, '.mat'];
filename_cellOneCT = [dirSave, 'replTables_cellOneCT_', cancerType, '.mat'];
filename_sampleName = [dirSave, 'replTables_sampleName_', cancerType, '.mat'];
filename_cellTx = [dirSave, 'replTables_cellTx_', cancerType, '.mat'];
if (exist(filename_cellOneCT, 'file') && exist(filename_sampleName, 'file') && exist(filename_cellTx, 'file') && exist(filename_structSamples, 'file'))
    structSamples = [];    load(filename_structSamples, 'structSamples');
    fprintf('Saving a copy of structSamples to %s.\n', [dirSaveNew, 'COPY_structSamples_', cancerType]);
    save([dirSaveNew, 'COPY_structSamples_', cancerType], 'structSamples', '-v7.3');
    
    sampleName = [];    load(filename_sampleName, 'sampleName');
    %cellTx = [];        load(filename_cellTx, 'cellTx');
    cellMutations = []; load([dirSave, 'replTables_cellMutations_', cancerType], 'cellMutations');
    cellMutations_plus = cellMutations.plus;
    cellMutations_minus = cellMutations.minus;
    clear cellMutations;
    cellOneCT = [];     load(filename_cellOneCT, 'cellOneCT');
    cellOneCT_leading = cellOneCT.leading;
    cellOneCT_lagging = cellOneCT.lagging;
    clear cellOneCT;
    nSamples = length(sampleName);
    fprintf('OK %d samples loaded.\n', nSamples);
    %     structSamples.tableSamples = table(sampleName);
    %     structSamples.tableSamples.cancerType = cell(nSamples, 1);
    %     structSamples.tableSamples.cancerType(:) = {cancerType};
    
    features = {'mutType1', 'mutType2', 'mfType1', 'mfType2', 'asymmetryDifference', 'asymmetryPValue'};
    %     simpleTypes = {'LeadLagg', 'TranscribedNontranscribed'}; %, 'LeadLagg_CFSs', 'LeadLagg_NFRs', 'LeadLagg_other'
    %     groupedTypes = {'CpGtoT_LeadLagg', 'CpHtoT_LeadLagg'};
    complexTypes = {'NOS_LeadLagg'}; %'ReplicationTiming_LeadLagg', 'Expression_LeadLagg', 'DistanceFromORI_LeadLagg', 'DistanceFromORI_LeadLagg_Early', 'DistanceFromORI_LeadLagg_Late'};
    nValues.ReplicationTiming_LeadLagg = max(bins_tRT);
    nValues.Expression_LeadLagg = max(bins_tExpr);
    nValues.DistanceFromORI_LeadLagg = max(indexBinFromORI);
    nValues.DistanceFromORI_LeadLagg_Early = max(indexBinFromORI);
    nValues.DistanceFromORI_LeadLagg_Late = max(indexBinFromORI);
    nValues.NOS_LeadLagg = max(bins_tNOS);
    for feature = features
        %         for simpleType = simpleTypes
        %             structSamples.(simpleType{1}).(feature{1}) = NaN*ones(nSamples, maxPattern);
        %         end
        %         for groupedType = groupedTypes
        %             structSamples.(groupedType{1}).(feature{1}) = NaN*ones(nSamples, 1);
        %         end
        for complexType = complexTypes
            if (~isfield(structSamples, complexType{1}))
                structSamples.(complexType{1}) = cell(nValues.(complexType{1}), 1);
            end
            for iValue = 1:nValues.(complexType{1})
                structSamples.(complexType{1}){iValue}.(feature{1}) = NaN*ones(nSamples, maxPattern);
            end
        end
    end
    whos
    fprintf('Initialisation complete.\n')
    %     for iSample = 1:nSamples
    %         fprintf('Sample %d (of %d)...\n', iSample, nSamples)
    for iPattern = 1:maxPattern
        fprintf('Pattern %d (of %d)...\n', iPattern, maxPattern)
        local_cellResult_background_leading_matrix = repmat(cellResult_background_leading{iPattern}, 1, nSamples);
        local_cellResult_background_lagging_matrix = repmat(cellResult_background_lagging{iPattern}, 1, nSamples);
%         fprintf('Simple types:\n'); tic;
%         % % % SIMPLE TYPES % % % %
%         for simpleType = simpleTypes
%             simpleType = simpleType{1};
%             if (strcmp(simpleType, 'LeadLagg'))
%                 isRelTerritory = isLeftOrRightNotTx;
%             elseif (strcmp(simpleType, 'TranscribedNontranscribed'))
%                 isRelTerritory = isTxNotLeftOrRight;
%                 %                 elseif (strcmp(simpleType, 'LeadLagg_CFSs'))
%                 %                     isRelTerritory = (tIsLeft | tIsRight) & tableTerritories.overlapWithCFS==1;
%                 %                 elseif (strcmp(simpleType, 'LeadLagg_NFRs'))
%                 %                     isRelTerritory = (tIsLeft | tIsRight) & tableTerritories.overlapWithNFR==1;
%                 %                 elseif (strcmp(simpleType, 'LeadLagg_other'))
%                 %                     isRelTerritory = (tIsLeft | tIsRight) & tableTerritories.overlapWithCFS<1 & tableTerritories.overlapWithNFR<1;
%             end
%             numeratorType1 = (cellOneCT_leading{iPattern}(isRelTerritory, :));
%             numeratorType2 = (cellOneCT_lagging{iPattern}(isRelTerritory, :));
%             denominatorType1 = local_cellResult_background_leading_matrix(isRelTerritory, :);
%             denominatorType2 = local_cellResult_background_lagging_matrix(isRelTerritory, :);
%             tmpResult = fcn_combineMatrixIntoStatistics(numeratorType1, numeratorType2, denominatorType1, denominatorType2);
%             for feature = features
%                 structSamples.(simpleType).(feature{1})(:, iPattern) = tmpResult.(feature{1});
%             end
%         end
%         toc
        fprintf('Complex types:\n'); tic;
        % % % % COMPLEX TYPES % % % %
        for complexType = complexTypes
            complexType = complexType{1};
            isRelevantRT = true(nTerritories, 1);
            if (strcmp(complexType, 'ReplicationTiming_LeadLagg'))
                currentBinValues = bins_tRT; %binName = 'bins_tRT';
            elseif (strcmp(complexType, 'Expression_LeadLagg'))
                currentBinValues = bins_tExpr; %binName = 'bins_tExpr';
            elseif (strcmp(complexType, 'DistanceFromORI_LeadLagg'))
                currentBinValues = indexBinFromORI; %binName = 'indexBinFromORI';
            elseif (strcmp(complexType, 'DistanceFromORI_LeadLagg_Early'))
                currentBinValues = indexBinFromORI;
                isRelevantRT = bins_tRT <= (nValues.ReplicationTiming_LeadLagg/2);
            elseif (strcmp(complexType, 'DistanceFromORI_LeadLagg_Late'))
                currentBinValues = indexBinFromORI;
                isRelevantRT = bins_tRT > (nValues.ReplicationTiming_LeadLagg/2);
            elseif (strcmp(complexType, 'NOS_LeadLagg'))
                currentBinValues = bins_tNOS; %binName = 'bins_tRT';
                isRelevantRT = isNotNaNNOS;
            end
            for iValue = 1:nValues.(complexType)
                relevantTerritories = isLeftOrRightNotTx & currentBinValues == iValue & isRelevantRT;
                numeratorType1 = (cellOneCT_leading{iPattern}(relevantTerritories, :));
                numeratorType2 = (cellOneCT_lagging{iPattern}(relevantTerritories, :));
                denominatorType1 = local_cellResult_background_leading_matrix(relevantTerritories, :);
                denominatorType2 = local_cellResult_background_lagging_matrix(relevantTerritories, :);
                tmpResult = fcn_combineMatrixIntoStatistics(numeratorType1, numeratorType2, denominatorType1, denominatorType2);
                for feature = features
                    structSamples.(complexType){iValue}.(feature{1})(:, iPattern) = tmpResult.(feature{1});
                end
                %                     if (strcmp(complexType, 'DistanceFromORI_LeadLagg'))
                relevantTerritories = (~tTxPlus & ~tTxMinus) & currentBinValues == iValue & isRelevantRT; % not transcribed, but all left/right/NaN replication
                structSamples.(complexType){iValue}.mfPlus(:, iPattern) = (sum((cellMutations_plus{iPattern}(relevantTerritories, :)), 1)./repmat(sum(cellResult_plus{iPattern}(relevantTerritories)), 1, nSamples))';
                structSamples.(complexType){iValue}.mfMinus(:, iPattern) = (sum((cellMutations_minus{iPattern}(relevantTerritories, :)), 1)./repmat(sum(cellResult_minus{iPattern}(relevantTerritories)), 1, nSamples))';
                %                     end
            end
        end
        toc
    end
    %         fprintf('Grouped types:\n'); tic;
    %         % % % % GROUPED TYPES % % % %
    %         for groupedType = groupedTypes
    %             groupedType = groupedType{1};
    %             if (strcmp(groupedType, 'CpGtoT_LeadLagg'))
    %                 patterns = 35:4:48;
    %             elseif (strcmp(groupedType, 'CpHtoT_LeadLagg'))
    %                 patterns = [33:34,36:38,40:42,44:46,48];
    %             end
    %             numeratorType1 = zeros(nTerritories, nSamples);
    %             numeratorType2 = zeros(nTerritories, nSamples);
    %             denominatorType1 = zeros(nTerritories, nSamples);
    %             denominatorType2 = zeros(nTerritories, nSamples);
    %             for iPattern = patterns
    %                 numeratorType1 = numeratorType1 + (cellOneCT_leading{iPattern}(:, :));
    %                 numeratorType2 = numeratorType2 + (cellOneCT_lagging{iPattern}(:, :));
    %                 denominatorType1 = denominatorType1 + repmat(cellResult_background_leading{iPattern}, 1, nSamples);
    %                 denominatorType2 = denominatorType2 + repmat(cellResult_background_lagging{iPattern}, 1, nSamples);
    %             end
    %             tmpResult = fcn_combineMatrixIntoStatistics(numeratorType1, numeratorType2, denominatorType1, denominatorType2);
    %             for feature = features
    %                 structSamples.(groupedType).(feature{1})(:) = tmpResult.(feature{1});
    %             end
    %         end
    toc
    %     end
    fprintf('Saving to %s.\n', [dirSaveNew, 'structSamples_', cancerType]);
    save([dirSaveNew, 'structSamples_', cancerType], 'structSamples', '-v7.3');
else
    error('ERROR: One of these files does not exist: %s or %s or %s.\n', filename_cellOneCT, filename_sampleName, filename_cellTx);
end
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName); toc
%%
% diary off;
% if (sendingEmails)
%     sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
% end