% function prepareOfficialSignatures
%% Prepareation of signatures:

tableSignaturesOfficialOld = readtable('data/signatures/signaturesOfficial.txt', 'delimiter', '\t', 'ReadVariableNames', true);
tableSignaturesOfficialNew = readtable('data/signatures/signatures_probabilities.txt', 'delimiter', '\t', 'ReadVariableNames', true);
permNewToOld = NaN*ones(96, 1);
for iPattern = 1:96
    permNewToOld(iPattern) = find(strcmp(tableSignaturesOfficialNew.SomaticMutationType, tableSignaturesOfficialOld.SomaticMutationType{iPattern}));
end
tableSignaturesOfficialNewCorrectOrder = tableSignaturesOfficialNew(permNewToOld, 1:33);
tableSignaturesOfficialNewCorrectOrderArchive = tableSignaturesOfficialNewCorrectOrder;
% tableSignaturesOfficialNewCorrectOrder = [tableSignaturesOfficialNewCorrectOrder(:,1:3), tableSignaturesOfficialOld(:,4:5), tableSignaturesOfficialNewCorrectOrder(:,5:end)];

writetable(tableSignaturesOfficialNewCorrectOrder, 'data/signatures/signaturesOfficial30.txt', 'delimiter', '\t', 'WriteVariableNames', true);
%%
figure; subplot(3,1,1); bar(tableSignaturesOfficialOld.Signature1A); subplot(3,1,2); bar(tableSignaturesOfficialOld.Signature1B); subplot(3,1,3); bar(tableSignaturesOfficialNewCorrectOrderArchive.Signature1); 
%% Prepareation of signatures:

% tableSignaturesOfficialOld = readtable('data/signatures/signaturesOfficial.txt', 'delimiter', '\t', 'ReadVariableNames', true);
% tableSignaturesOfficialNew = readtable('data/signatures/signatures_probabilities.txt', 'delimiter', '\t', 'ReadVariableNames', true);
% permNewToOld = NaN*ones(96, 1);
% for iPattern = 1:96
%     permNewToOld(iPattern) = find(strcmp(tableSignaturesOfficialNew.SomaticMutationType, tableSignaturesOfficialOld.SomaticMutationType{iPattern}));
% end
% tableSignaturesOfficialNewCorrectOrder = tableSignaturesOfficialNew(permNewToOld, 1:33);
% writetable(tableSignaturesOfficialNewCorrectOrder, 'data/signatures/signaturesOfficial30.txt', 'delimiter', '\t', 'WriteVariableNames', true);

% matrixSignaturesOfficialNormalised = matrixSignaturesOfficial;
% for iSignature = 1:nSignaturesOfficial
%     tmpSignatureNormalised = matrixSignaturesOfficial(:,iSignature);
%     tmpSignatureNormalised = tmpSignatureNormalised./tableGenomeTrinucleotides.genomeComputed;
%     matrixSignaturesOfficialNormalised(:,iSignature) = tmpSignatureNormalised/sum(tmpSignatureNormalised);
% end