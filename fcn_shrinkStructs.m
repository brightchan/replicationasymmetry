% function fcn_shrinkStructs(inputDir, outputDir)

%     inputDir = 'save/Haradhvala_territories/decomposedSignatures/';
%     outputDir = 'save/Haradhvala_territories/decomposedSignaturesSmaller/'; 
    inputDir = 'save/OldHaradhvala_territories/decomposedSignatures/';
    outputDir = 'save/OldHaradhvala_territories/decomposedSignaturesSmaller/'; 
lstFiles = dir([inputDir, '*.mat']);
createDir(outputDir);
for iFile = 1:length(lstFiles)
    fileNameInput = lstFiles(iFile).name;
    try
        fprintf('Loading %s...\n', fileNameInput);
        data = load([inputDir, fileNameInput]);
        processes = data.processes;
        exposures = data.exposures;
        input = data.input;
        fprintf('Saving to %s%s...\n', outputDir, fileNameInput);
        save([outputDir, fileNameInput], 'processes', 'exposures', 'input');
    catch
        fprintf('WARNING: problem with file %s.\n', fileNameInput);
    end
end