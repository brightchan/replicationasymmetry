%% fcn_replOneCT_step1
function fcn_replOneCT_step1(cancerType, inputFileName, tableTerritoriesFileName, dirSave)
% E.g., fcn_replOneCT_step1(ALL, ../../commonData/features/hg19/mutationsPerSampleWithReplicationAllContexts/replAnnotatedWithTerritories_mut_ALL_context96_genex_withGenes.onlyWGS.bedlike, ../replication/per_base_territories_20kb_line_numbers.bed.txt, ../replication/replPrepareTables/)
% E.g., fcn_replOneCT_step1('TCGAnormal_LUAD_ovSNV', 'data/mutationsWGS/replAnnotatedWithTerritories_mut_TCGAnormal_LUAD_ovSNV_context96_genex_withGenes.onlyWGS.bedlike', 'data/per_base_territories_20kb_line_numbers.bed.txt', 'data/contextInTerritories/')
% cancerType='TCGAnormal_LUAD_ovSNV';
% inputFileName='data/mutationsWGS/replAnnotatedWithTerritories_mut_TCGAnormal_LUAD_ovSNV_context96_genex_withGenes.onlyWGS.bedlike';
% tableTerritoriesFileName = [dirSave, 'tableTerritories'];
% territoriesFileName='data/per_base_territories_20kb_line_numbers.bed.txt';
% dirSave='data/contextInTerritories/';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear;
% cancerType = 'Liver';
% inputFileName = 'data/mutationsWGS/replAnnotatedWithTerritories_mut_Liver_context96_genex_withGenes.onlyWGS.bedlike.txt';
% dirSave = 'saveTmp/';
% tableTerritoriesFileName = 'save/tableTerritories'; % created in script_60726_replPrepareTables
%%
addpath(genpath('code/'));
scriptName = 'fcn_replPrepareOneCT'; compName = 'CLUSTER';
% savePathDiary = 'diaries/'; createDir(savePathDiary); 
% diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
% diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName); tic;
%% General: prepares a table for each strand (leading/lagging), cancer type and pattern (96 possibilities) with a number of mutations per sample, territory. And then number of pattern occurences per territory (for each strand and pattern).
tableTerritories = [];  load(tableTerritoriesFileName, 'tableTerritories');
nTerritories = size(tableTerritories, 1); 
strands = {'plus', 'minus'};
strandChars = {'+', '-'};
maxPattern = 96;
createDir(dirSave);
%%
fprintf('Cancer type %s...\n', cancerType);
if (exist(inputFileName, 'file') && strcmp(inputFileName(end-6:end), 'bedlike'))
    movefile(inputFileName, [inputFileName, '.txt'])
    fprintf('Moving file %s to %s.\n', inputFileName, [inputFileName, '.txt']);
end
if (strcmp(inputFileName(end-3:end), '.txt'))
    if (~exist(inputFileName, 'file') && exist(inputFileName(1:end-4), 'file'))
        movefile(inputFileName(1:end-4), inputFileName)
        fprintf('Moving file %s to %s.\n', inputFileName(1:end-4), inputFileName);
    end
else
    inputFileName = [inputFileName, '.txt'];
end
if (~exist(inputFileName, 'file'))
    error('ERROR: File %s does not exist.\n', inputFileName);
end
tableMutations = readtable([inputFileName], 'delimiter', '\t', 'ReadVariableNames', false);
tableMutations.Properties.VariableNames = {'chr', 'pos0', 'pos1', 'sample', 'fromBase', 'toBase', 'context', 'isCpGtoT', 'strand', 'isWGS', 'gene', 'tChr', 'tPos0', 'tPos1', 'tID', 'tIsLeft', 'tIsRight', 'tTxPlus', 'tTxMinus', 'tRT', 'tExpr'};

table_nMutations = size(tableMutations, 1);
table_context = tableMutations.context;
table_fromBase = tableMutations.fromBase; 
table_toBase = tableMutations.toBase;
table_strand = tableMutations.strand;
table_tID = tableMutations.tID;
table_sample = tableMutations.sample;
clear tableMutations;
tableTerritories_tIsLeft = tableTerritories.tIsLeft;
tableTerritories_tIsRight = tableTerritories.tIsRight;
tableTerritories_tTxPlus = tableTerritories.tTxPlus;
tableTerritories_tTxMinus = tableTerritories.tTxMinus;
clear tableTerritories;
whos
%%
patternNumber = zeros(table_nMutations, 1);
for iMutation = 1:table_nMutations
    patternNumber(iMutation) = getPatternNumberFromStrings(table_context{iMutation}, table_fromBase{iMutation}, table_toBase{iMutation});
%     if (tableMutations.patternNumber(iMutation) < 1)
%         fprintf('Wrong pattern: %d.\n', iMutation);
%     end
end
if (isnumeric(table_sample))
    table_sample = cellfun(@num2str, num2cell(table_sample), 'UniformOutput', false);
end
sampleName = unique(table_sample);
nSamples = length(sampleName);
fprintf('%d samples, %d territories, %d mutations.\n', nSamples, nTerritories, table_nMutations);
whos
%%
sampleNumber = zeros(table_nMutations, 1);
for iMutation = 1:table_nMutations
    sampleNumber(iMutation) = find(strcmp(sampleName, table_sample{iMutation}));
end
cellMutations_plus = cell(maxPattern, 1);
cellMutations_minus = cell(maxPattern, 1);
for iPattern = 1:maxPattern
    cellMutations_plus{iPattern} = sparse(zeros(nTerritories, nSamples));
    cellMutations_minus{iPattern} = sparse(zeros(nTerritories, nSamples));
end
for iMutation = 1:table_nMutations
    iPattern = patternNumber(iMutation);
    thisSampleNumber = sampleNumber(iMutation);
    territoryNumber = table_tID(iMutation);
    if (iPattern > 0 && iPattern <= maxPattern)
        if (strcmp(table_strand{iMutation}, '+'))
            cellMutations_plus{iPattern}(territoryNumber, thisSampleNumber) = cellMutations_plus{iPattern}(territoryNumber, thisSampleNumber) + 1;
        elseif (strcmp(table_strand{iMutation}, '-'))
            cellMutations_minus{iPattern}(territoryNumber, thisSampleNumber) = cellMutations_minus{iPattern}(territoryNumber, thisSampleNumber) + 1;
        else
            fprintf('WARNING: unknown strand %s.\n', table_strand{iMutation});
        end
        %strandName = strands{strcmp(strandChars, table_strand{iMutation})};
        %cellMutations.(strandName){iPattern}(territoryNumber, thisSampleNumber) = cellMutations.(strandName){iPattern}(territoryNumber, thisSampleNumber) + 1;
    end
end
%%
whos
fprintf('Computing leading and lagging...\n');
cellOneCT_leading = cell(maxPattern, 1);
cellOneCT_lagging = cell(maxPattern, 1);
for iPattern = 1:maxPattern
    cellOneCT_leading{iPattern} = sparse(zeros(nTerritories, nSamples));
    cellOneCT_lagging{iPattern} = sparse(zeros(nTerritories, nSamples));
    cellOneCT_leading{iPattern}(tableTerritories_tIsLeft, :) = cellMutations_plus{iPattern}(tableTerritories_tIsLeft, :);
    cellOneCT_leading{iPattern}(tableTerritories_tIsRight, :) = cellMutations_minus{iPattern}(tableTerritories_tIsRight, :);
    cellOneCT_lagging{iPattern}(tableTerritories_tIsRight, :) = cellMutations_plus{iPattern}(tableTerritories_tIsRight, :);
    cellOneCT_lagging{iPattern}(tableTerritories_tIsLeft, :) = cellMutations_minus{iPattern}(tableTerritories_tIsLeft, :);
end
fprintf('Saving to %s...\n', [dirSave, 'replTablesData_', cancerType]);
cellOneCT.leading = cellOneCT_leading;
cellOneCT.lagging = cellOneCT_lagging;
save([dirSave, 'replTables_cellOneCT_', cancerType], 'cellOneCT', '-v7.3'); clear cellOneCT_leading cellOneCT_lagging cellOneCT;
whos
%%
fprintf('Computing transcribed and nontranscribed...\n');
cellTx_transcribed = cell(maxPattern, 1);
cellTx_nontranscribed = cell(maxPattern, 1);
for iPattern = 1:maxPattern
    cellTx_transcribed{iPattern} = sparse(zeros(nTerritories, nSamples));
    cellTx_nontranscribed{iPattern} = sparse(zeros(nTerritories, nSamples));
    cellTx_transcribed{iPattern}(tableTerritories_tTxPlus, :) = cellMutations_plus{iPattern}(tableTerritories_tTxPlus, :);
    cellTx_transcribed{iPattern}(tableTerritories_tTxMinus, :) = cellMutations_minus{iPattern}(tableTerritories_tTxMinus, :);
    cellTx_nontranscribed{iPattern}(tableTerritories_tTxMinus, :) = cellMutations_plus{iPattern}(tableTerritories_tTxMinus, :);
    cellTx_nontranscribed{iPattern}(tableTerritories_tTxPlus, :) = cellMutations_minus{iPattern}(tableTerritories_tTxPlus, :);
end
fprintf('Saving to %s...\n', [dirSave, 'replTablesData_', cancerType]);
cellTx.transcribed = cellTx_transcribed;
cellTx.nontranscribed = cellTx_nontranscribed;
save([dirSave, 'replTables_cellTx_', cancerType], 'cellTx', '-v7.3'); clear cellTx_transcribed cellTx_nontranscribed cellTx;
cellMutations.plus = cellMutations_plus;
cellMutations.minus = cellMutations_minus;
save([dirSave, 'replTables_cellMutations_', cancerType], 'cellMutations', '-v7.3');
save([dirSave, 'replTables_sampleName_', cancerType], 'sampleName', '-v7.3');
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName); toc
%%
% diary off;
% if (sendingEmails)
%     sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
% end